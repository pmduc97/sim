﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SimStore_DucPham.Startup))]
namespace SimStore_DucPham
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
