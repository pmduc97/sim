﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SimStore_DucPham
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "TrangChu",
                url: "",
                defaults: new { controller = "Home", action = "Index" }
            );
            routes.MapRoute(
                name: "error404",
                url: "error404",
                defaults: new { controller = "Error", action = "Error404" }
            );
            routes.MapRoute(
                name: "ActiveAccount",
                url: "kich-hoat-tai-khoan",
                defaults: new { controller = "Account", action = "ActiveAccount" }
            );
            routes.MapRoute(
                name: "ActiveBill",
                url: "kich-hoat-don-hang",
                defaults: new { controller = "Home", action = "ActiveBill" }
            );
            routes.MapRoute(
                name: "CustomerManager",
                url: "customer-manager",
                defaults: new { controller = "Customer", action = "Index" }
            );
            routes.MapRoute(
                name: "CustomerAddress",
                url: "customer-address",
                defaults: new { controller = "Customer", action = "Address" }
            );
            routes.MapRoute(
                name: "CustomerAddressCreate",
                url: "customer-address/them",
                defaults: new { controller = "AddressOrders", action = "Create" }
            );
            routes.MapRoute(
                name: "NonActiveAccount",
                url: "huy-kich-hoat-tai-khoan",
                defaults: new { controller = "Account", action = "NonActiveAccount" }
            );
            routes.MapRoute(
                name: "phongthuyindex",
                url: "sim-phong-thuy",
                defaults: new { controller = "PhongThuy", action = "Index" }
            );
            routes.MapRoute(
                name: "traphongthy",
                url: "tra-phong-thuy",
                defaults: new { controller = "PhongThuy", action = "TraPhongThuy" }
            );
            routes.MapRoute(
                name: "error500",
                url: "error500",
                defaults: new { controller = "Error", action = "Error500" }
            );
            routes.MapRoute(
                name: "GioHang",
                url: "gio-hang",
                defaults: new { controller = "Cart", action = "Index" }
            );
            routes.MapRoute(
                name: "AdminLogin",
                url: "admin",
                defaults: new { controller = "Account", action = "AdminLogin" }
            );
            routes.MapRoute(
                name: "CustomerLogin",
                url: "dang-nhap",
                defaults: new { controller = "Account", action = "Login" }
            );
            routes.MapRoute(
                name: "CustomerRegister",
                url: "dang-ky",
                defaults: new { controller = "Account", action = "Register" }
            );
            routes.MapRoute(
                name: "TimSim",
                url: "tim-sim",
                defaults: new { controller = "Home", action = "TimSim" }
            );
            routes.MapRoute(
                name: "ProductByCarrier",
                url: "sim-theo-mang/{carrier}",
                defaults: new { controller = "Home", action = "ProductByCarrier", carrier = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "ProductByCategory",
                url: "sim-theo-loai/{category}",
                defaults: new { controller = "Home", action = "ProductByCategory", category = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "ProductByPrice",
                url: "sim-theo-gia/{price}",
                defaults: new { controller = "Home", action = "ProductByPrice", price = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "AllProduct",
                url: "tat-ca-sim",
                defaults: new { controller = "Home", action = "AllProduct" }
            );
            routes.MapRoute(
                name: "ChonSim",
                url: "chon-sim",
                defaults: new { controller = "Home", action = "ChonSim" }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}