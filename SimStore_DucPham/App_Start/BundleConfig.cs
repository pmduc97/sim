﻿using System.Web;
using System.Web.Optimization;

namespace SimStore_DucPham
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/AdminLTE/css").Include(
                "~/Plugins/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css",
                "~/Plugins/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css",
                "~/Plugins/AdminLTE/bower_components/Ionicons/css/ionicons.min.css",
                "~/Plugins/AdminLTE/dist/css/AdminLTE.min.css",
                "~/Plugins/AdminLTE/plugins/iCheck/square/blue.css",
                "~/Plugins/AdminLTE/dist/css/skins/_all-skins.min.css",
                "~/Content/Site.css",
                "~/Content/jquery-confirm.min.css"
                ));

            bundles.Add(new ScriptBundle("~/AdminLTE/js").Include(
                "~/Plugins/AdminLTE/bower_components/jquery/dist/jquery.min.js",
                "~/Plugins/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js",
                "~/Plugins/AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js",
                "~/Plugins/AdminLTE/bower_components/fastclick/lib/fastclick.js",
                "~/Plugins/AdminLTE/dist/js/adminlte.min.js",
                "~/Plugins/AdminLTE/dist/js/demo.js",
                "~/Scripts/shareJS.js"
                ));
        }
    }
}