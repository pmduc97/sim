﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SimStore_DucPham.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required(ErrorMessage = "Họ và tên không được rỗng")]
        [Display(Name = "Họ và tên")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Email không được rỗng")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Điện thoại không được rỗng")]
        [Display(Name = "Điện thoại")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Avatar không được rỗng")]
        [Display(Name = "Avatar")]
        public string UrlAvatar { get; set; }

        [Required(ErrorMessage = "Địa chỉ không được rỗng")]
        [Display(Name = "Địa chỉ")]
        public string Address { get; set; }

        //[Required(ErrorMessage = "Mật khẩu không được rỗng")]
        //[DataType(DataType.Password)]
        //[StringLength(100, ErrorMessage = "Độ dài mật khẩu không hợp lệ", MinimumLength = 6)]
        //[Display(Name = "Mật khẩu")]
        //public string Password { get; set; }

        //[DataType(DataType.Password)]
        //[Display(Name = "Xác nhận mật khẩu")]
        //[Compare("Password", ErrorMessage = "Xác nhận lại mật khẩu không khớp")]
        //public string ConfirmPassword { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = "Tài khoản không được rỗng")]
        [Display(Name = "Tài khoản")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Mật khẩu không được rỗng")]
        [DataType(DataType.Text)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [Display(Name = "Ghi nhớ?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Họ tên không được rỗng")]
        [StringLength(100, ErrorMessage = "Độ dài của tên không hợp lệ", MinimumLength = 1)]
        [Display(Name = "Họ và tên")]
        public string FullName { get; set; }

        [Display(Name = "Tài khoản")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Điện thoại không được rỗng")]
        [Display(Name = "Điện thoại")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Avatar")]
        public string UrlAvatar { get; set; }

        [Required(ErrorMessage = "Địa chỉ không được rỗng")]
        [Display(Name = "Địa chỉ")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Email không được rỗng")]
        [EmailAddress(ErrorMessage = "Email không đúng định dạng")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Mật khẩu không được rỗng")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "Độ dài mật khẩu không hợp lệ", MinimumLength = 6)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Xác nhận mật khẩu")]
        [Compare("Password", ErrorMessage = "Xác nhận lại mật khẩu không khớp")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Độ dài mật khẩu không hợp lệ", MinimumLength = 6)]
        [DataType(DataType.Text)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Xác nhận mật khẩu")]
        [Compare("Password", ErrorMessage = "Xác nhận lại mật khẩu không khớp")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}