﻿using System;

namespace SimStore_DucPham.Models
{
    public class BillModel
    {
        public string Id { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreateUserId { get; set; }
        public string Status { get; set; }
        public long SumMoney { get; set; }
        public DateTime UpdateTime { get; set; }
        public string UpdateUserId { get; set; }
        public bool IsPay { get; set; }
        public string Note { get; set; }
        public string ReasonCanceled { get; set; }
        public string AddressOrderId { get; set; }
        public bool IsOnline { get; set; }
        public string Code { get; set; }
    }

    public class BillCreateModel
    {
        public string Id { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreateUserId { get; set; }
        public string Status { get; set; }
        public long SumMoney { get; set; }
        public string Note { get; set; }
        public string AddressOrderId { get; set; }
        public bool IsOnline { get; set; }
        public string Code { get; set; }
    }
}