﻿using SimStore_DucPham.ViewModels;

namespace SimStore_DucPham.Models
{
    public class ActiveBillModel
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public BillViewModel Bill { get; set; }
    }
}