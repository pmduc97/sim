﻿using SimStore_DucPham.ViewModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SimStore_DucPham.Models
{
    public class SendMailModel
    {
        public ApplicationUser User { get; set; }
        public AddressOrderViewModel AddressOrder { get; set; }
        public BillModel Bill { get; set; }
        public string CustomerMail { get; set; }
        public string MailSubject { get; set; }
        public List<ProductViewModel> Products { get; set; }
    }

    public class SendMailActiveAccountModel
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        public string Email { get; set; }
    }
}