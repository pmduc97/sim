﻿using SimStore_DucPham.ViewModels;

namespace SimStore_DucPham.Models
{
    public class CartResultModel : ResultModel
    {
        public int Sum { get; set; }
        public ProductViewModel Product { get; set; }
    }
}