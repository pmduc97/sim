﻿namespace SimStore_DucPham.Models
{
    public class CarrierModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int CountProduct { get; set; }
    }
}