﻿using SimStore_DucPham.ViewModels;
using System.Collections.Generic;

namespace SimStore_DucPham.Models
{
    public class OrderAddressViewModel
    {
        public List<ProductViewModel> Products { get; set; }
        public AddressOrderViewModel Address { get; set; }
        public List<AddressOrderViewModel> Addresses { get; set; }
        public string Email { get; set; }
    }
}