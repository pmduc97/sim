﻿using SimStore_DucPham.ViewModels;
using System.Collections.Generic;

namespace SimStore_DucPham.Models
{
    public class OrderOkModel
    {
        public bool Status { get; set; }
        public string FullName { get; set; }
        public string BillId { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public bool IsOnline { get; set; }
        public string Email { get; set; }
        public List<ProductViewModel> Products { get; set; }
    }
}