﻿using System;

namespace SimStore_DucPham.Models
{
    public class HistoryBillModel
    {
        public string Id { get; set; }
        public string BillId { get; set; }
        public string Status { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreateUserId { get; set; }
        public bool IsRead { get; set; }
    }
}