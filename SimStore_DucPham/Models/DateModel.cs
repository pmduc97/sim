﻿namespace SimStore_DucPham.Models
{
    public class DateModel
    {
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public bool LunarLeap { get; set; }
        public string GioiTinh { get; set; }
    }

    public class DateViewModel
    {
        public string GioiTinh { get; set; }
        public DuongLichModel DuongLich { get; set; }
        public AmLichModel AmLich { get; set; }
    }

    public class DuongLichModel
    {
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string NgayThu { get; set; }
        public string CungHoangDao { get; set; }
    }

    public class AmLichModel
    {
        public CanChiModel DayCanChi { get; set; }
        public CanChiModel MonthCanChi { get; set; }
        public CanChiModel YearCanChi { get; set; }
        public bool LunarLeap { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public ConGiapModel ConGiap { get; set; }
        public CungSinhModel MenhCungSinh { get; set; }
        public CungPhiModel MenhCungPhi { get; set; }
        public string ImageConGiap { get; set; }
    }

    public class CungPhiModel
    {
        public string CungPhi { get; set; }
        public string Huong { get; set; }
        public string Hanh { get; set; }
        public string Phi { get; set; }
        public string Mau { get; set; }
    }

    public class CungSinhModel
    {
        public string Menh { get; set; }
        public string TrungHoa { get; set; }
        public string VietNam { get; set; }
    }

    public class CanChiModel
    {
        public string Can { get; set; }
        public string Chi { get; set; }
    }

    public class ConGiapModel
    {
        public string Ten { get; set; }
        public string TrungHoa { get; set; }
        public string VietNam { get; set; }
    }
}