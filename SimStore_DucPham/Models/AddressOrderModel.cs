﻿using System.ComponentModel.DataAnnotations;

namespace SimStore_DucPham.Models
{
    public class AddressOrderModel
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public bool IsOrder { get; set; }

        [Display(Name = "Mặc định")]
        public bool IsPay { get; set; }

        [Display(Name = "Tỉnh/Thành phố")]
        [Required(ErrorMessage = "Tỉnh/Thành phố không được rỗng")]
        public string City { get; set; }

        [Display(Name = "Quận/Huyện")]
        [Required(ErrorMessage = "Quận/Huyện không được rỗng")]
        public string District { get; set; }

        [Display(Name = "Phường/Xã")]
        [Required(ErrorMessage = "Phường/Xã không được rỗng")]
        public string Wards { get; set; }

        [Display(Name = "Số nhà/Tên đường")]
        [Required(ErrorMessage = "Số nhà/tên đường không được rỗng")]
        [StringLength(int.MaxValue, ErrorMessage = "Địa chỉ nhận không hợp lệ", MinimumLength = 5)]
        public string Address { get; set; }

        [Display(Name = "Người nhận")]
        [Required(ErrorMessage = "Người nhận không được rỗng")]
        [StringLength(100, ErrorMessage = "Tên người nhận không hợp lệ", MinimumLength = 6)]
        public string FullName { get; set; }

        [Display(Name = "Điện thoại")]
        [Required(ErrorMessage = "Điện thoại người nhận không được rỗng")]
        [StringLength(13, ErrorMessage = "Độ dài không hợp lệ", MinimumLength = 10)]
        public string PhoneNumber { get; set; }
    }

    public class AddressOrderDefaultModel
    {
        public string Id { get; set; }
        public string UserId { get; set; }

        [Display(Name = "Số nhà/Tên đường")]
        [Required(ErrorMessage = "Số nhà/tên đường không được rỗng")]
        [StringLength(int.MaxValue, ErrorMessage = "Địa chỉ nhận không hợp lệ", MinimumLength = 5)]
        public string Address { get; set; }

        [Display(Name = "Người nhận")]
        [Required(ErrorMessage = "Người nhận không được rỗng")]
        [StringLength(100, ErrorMessage = "Tên người nhận không hợp lệ", MinimumLength = 6)]
        public string FullName { get; set; }

        [Display(Name = "Điện thoại")]
        [Required(ErrorMessage = "Điện thoại người nhận không được rỗng")]
        [StringLength(13, ErrorMessage = "Độ dài không hợp lệ", MinimumLength = 10)]
        public string PhoneNumber { get; set; }
    }
}