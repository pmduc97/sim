﻿namespace SimStore_DucPham.Models
{
    public class Select2Model
    {
        public string id { get; set; }
        public string text { get; set; }
        public string IsActive { get; set; }
    }
}