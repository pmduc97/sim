﻿namespace SimStore_DucPham.Models
{
    public class RedirectModel
    {
        public string Url { get; set; }
        public string Title { get; set; }
    }
}