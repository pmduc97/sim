﻿using System.ComponentModel.DataAnnotations;

namespace SimStore_DucPham.Models
{
    public class CategoryModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Tên danh mục không được trống")]
        public string Name { get; set; }

        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int CountProduct { get; set; }
    }
}