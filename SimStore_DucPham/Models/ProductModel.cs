﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SimStore_DucPham.Models
{
    public class ProductModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Số SIM không được rỗng")]
        [Display(Name = "Số SIM")]
        [StringLength(15, ErrorMessage = "Độ dài số SIM không hợp lệ", MinimumLength = 10)]
        public string Number { get; set; }

        public string CarrierId { get; set; }

        [Required(ErrorMessage = "Giá SIM không được rỗng")]
        [Display(Name = "Giá")]
        [Range(0, long.MaxValue, ErrorMessage = "Giá bán không đúng định dạng")]
        public long Price { get; set; }

        public DateTime CreateTime { get; set; }
        public string CreateUserId { get; set; }
        public string Description { get; set; }
        public string CategoryId { get; set; }
        public long Sale { get; set; }
        public bool IsSell { get; set; }
        public bool IsActive { get; set; }
    }
}