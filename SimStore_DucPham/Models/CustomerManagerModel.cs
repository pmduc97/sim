﻿using SimStore_DucPham.ViewModels;
using System.Collections.Generic;

namespace SimStore_DucPham.Models
{
    public class CustomerManagerModel
    {
        public List<BillViewModel> Bills { get; set; }
        public List<AddressOrderViewModel> Address { get; set; }
    }
}