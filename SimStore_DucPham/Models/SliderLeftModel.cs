﻿using SimStore_DucPham.ViewModels;
using System.Collections.Generic;

namespace SimStore_DucPham.Models
{
    public class SliderLeftModel
    {
        public List<CarrierModel> Carriers { get; set; }
        public List<CategoryModel> Categories { get; set; }
        public List<PriceProductViewModel> PriceProducts { get; set; }
    }
}