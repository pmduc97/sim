﻿using SimStore_DucPham.ViewModels;
using System.Collections.Generic;

namespace SimStore_DucPham.Models
{
    public class HomeIndexModel
    {
        public List<ProductViewModel> ProductsNew { get; set; }
        public List<ProductViewModel> ProductsNormal { get; set; }
    }
}