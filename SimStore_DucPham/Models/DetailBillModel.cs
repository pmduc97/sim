﻿namespace SimStore_DucPham.Models
{
    public class DetailBillModel
    {
        public string Id { get; set; }
        public string BillId { get; set; }
        public string ProductId { get; set; }
        public long Price { get; set; }
    }
}