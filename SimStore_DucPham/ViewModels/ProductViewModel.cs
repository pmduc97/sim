﻿namespace SimStore_DucPham.ViewModels
{
    public class ProductViewModel
    {
        public string Id { get; set; }
        public string Number { get; set; }
        public string CarrierId { get; set; }
        public string CarrierName { get; set; }
        public string CarrierImage { get; set; }
        public long Price { get; set; }
        public string CreateTime { get; set; }
        public string CreateUserId { get; set; }
        public string CreateUserName { get; set; }
        public string Description { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public long Sale { get; set; }
        public bool IsSell { get; set; }
        public bool IsActive { get; set; }
    }
}