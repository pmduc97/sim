﻿using System.Collections.Generic;

namespace SimStore_DucPham.ViewModels
{
    public class OrderViewModel
    {
        public List<ProductViewModel> Products { get; set; }
        public List<AddressOrderViewModel> AddressOrders { get; set; }
    }
}