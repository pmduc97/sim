﻿namespace SimStore_DucPham.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public string CreateTime { get; set; }
        public bool IsActive { get; set; }
        public bool IsLock { get; set; }
        public string RoleId { get; set; }
    }
}