﻿namespace SimStore_DucPham.ViewModels
{
    public class PriceProductViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int CountProduct { get; set; }
    }
}