﻿using System.Collections.Generic;

namespace SimStore_DucPham.ViewModels
{
    public class HomeAdminModel
    {
        public BoxNumberModel BoxNumber { get; set; }
        public List<OrderNewModel> OrderNews { get; set; }
    }

    public class BoxNumberModel
    {
        public int OrderToday { get; set; }
        public int ProductSell { get; set; }
        public int BillNonPay { get; set; }
        public long SumPriceToday { get; set; }
    }

    public class OrderNewModel
    {
        public string Id { get; set; }
        public string Address { get; set; }
        public long SumPrice { get; set; }
        public string Status { get; set; }
        public string CreateTime { get; set; }
        public bool IsOnline { get; set; }
        public string FullName { get; set; }
    }
}