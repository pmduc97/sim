﻿using SimStore_DucPham.Models;
using System;
using System.Collections.Generic;

namespace SimStore_DucPham.ViewModels
{
    public class BillViewModel
    {
        public string Id { get; set; }
        public string ProductNumbers { get; set; }
        public string CreateTime { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string CreateUserId { get; set; }
        public string CreateUserName { get; set; }
        public string CreatePhoneNumber { get; set; }
        public string Status { get; set; }
        public long SumMoney { get; set; }
        public string UpdateUserId { get; set; }
        public string UpdateTime { get; set; }
        public bool IsPay { get; set; }
        public string Note { get; set; }
        public string ReasonCanceled { get; set; }
        public bool IsOnline { get; set; }
        public string AddressOrderId { get; set; }
        public string Code { get; set; }
        public AddressOrderViewModel AddressOrder { get; set; }
        public List<DetailBillViewModel> DetailBills { get; set; }
    }

    public class DetailBillViewModel
    {
        public string Id { get; set; }
        public string BillId { get; set; }
        public string ProductId { get; set; }
        public string ProductNumber { get; set; }
        public long ProductPrice { get; set; }
        public string ProductCarrierId { get; set; }
        public string ProductCarrierName { get; set; }
        public string ProductCarrierImage { get; set; }
        public string ProductCreateTime { get; set; }
        public string ProductDescription { get; set; }
        public string ProductCategoryId { get; set; }
        public string ProductCategoryName { get; set; }
    }

    public class AddressOrderViewModel : AddressOrderModel
    {
        public string CreateName { get; set; }
        public string AddressFull { get; set; }
    }
}