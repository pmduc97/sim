﻿namespace SimStore_DucPham.ViewModels
{
    public class SlideOrderViewModel
    {
        public string FullName { get; set; }
        public string Number { get; set; }
        public string CreateTime { get; set; }
    }
}