﻿using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SimStore_DucPham.Data
{
    public class RoleData
    {
        private string sqlQuery = string.Empty;

        /// <summary>
        /// Get tất cả các Role
        /// </summary>
        /// <returns></returns>
        public List<RoleModel> GetAllRole()
        {
            List<RoleModel> roles = new List<RoleModel>();

            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "SELECT * FROM AspNetRoles";
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                RoleModel role = new RoleModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Name = sqlDataReader["Name"].ToString()
                };
                roles.Add(role);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();

            return roles;
        }

        /// <summary>
        /// Get thông tin Role ứng với roleId truyền vào
        /// </summary>
        /// <param name="roleId">roleId cần tìm thông tin</param>
        /// <returns></returns>
        public RoleModel GetRoleById(string roleId)
        {
            RoleModel role = null;
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "SELECT * FROM AspNetRoles WHERE Id = @Id";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",roleId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                role = new RoleModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Name = sqlDataReader["Name"].ToString()
                };
            }

            sqlDataReader.Close();
            sqlConnectData.conn.Close();

            return role;
        }

        /// <summary>
        /// Thêm quyền mới với tên quyền truyền vào
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public ResultModel CreateRole(string roleName)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "INSERT INTO AspNetRoles(Id,Name) VALUES (@Id,@Name)";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",Guid.NewGuid().ToString()),
                new SqlParameter("Name",roleName)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Thêm quyền thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Chỉnh sửa tên của quyền có Id truyền vào
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public ResultModel UpdateRole(string roleId, string roleName)
        {
            RoleModel role = GetRoleById(roleId);
            if (role == null)
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Không tìm thấy quyền cần sửa đổi"
                };
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "UPDATE AspNetRoles SET Name = @Name WHERE Id = @Id";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",roleId),
                new SqlParameter("Name",roleName)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Sửa quyền thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Xóa quyền ứng với Id quyền truyền vào
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public ResultModel DeleteRole(string roleId)
        {
            RoleModel role = GetRoleById(roleId);
            if (role == null)
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Không tìm thấy quyền cần xóa"
                };
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "DELETE FROM AspNetRoles WHERE Id = @Id";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",roleId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Xóa quyền thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }
    }
}