﻿using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;
using SimStore_DucPham.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SimStore_DucPham.Data
{
    public class ProductData
    {
        private string sqlQuery = string.Empty;

        /// <summary>
        /// Get thông tin tất cả các SIM trong kho
        /// </summary>
        /// <returns></returns>
        public List<ProductViewModel> GetAllProduct()
        {
            List<ProductViewModel> products = new List<ProductViewModel>();
            sqlQuery = "SELECT * FROM View_GetAllProduct";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                ProductViewModel product = new ProductViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Number = sqlDataReader["Number"].ToString(),
                    CarrierId = sqlDataReader["CarrierId"].ToString(),
                    CarrierName = sqlDataReader["CarrierName"].ToString(),
                    Price = long.Parse(sqlDataReader["Price"].ToString()),
                    CreateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["CreateTime"].ToString()),
                    CreateUserId = sqlDataReader["CreateUserId"].ToString(),
                    CreateUserName = sqlDataReader["CreateUserName"].ToString(),
                    Description = sqlDataReader["Description"].ToString(),
                    CategoryId = sqlDataReader["CategoryId"].ToString(),
                    CategoryName = sqlDataReader["CategoryName"].ToString(),
                    Sale = long.Parse(sqlDataReader["Sale"].ToString()),
                    IsSell = bool.Parse(sqlDataReader["IsSell"].ToString()),
                    IsActive = bool.Parse(sqlDataReader["IsActive"].ToString()),
                    CarrierImage = sqlDataReader["Image"].ToString()
                };
                products.Add(product);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return products;
        }

        /// <summary>
        /// Get thông tin của một SIM trong kho
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public ProductViewModel GetInfoProduct(string productId)
        {
            ProductViewModel product = null;
            sqlQuery = "SELECT * FROM View_GetAllProduct WHERE Id = @ProductId";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("ProductId",productId)
            };
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                product = new ProductViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Number = sqlDataReader["Number"].ToString(),
                    CarrierId = sqlDataReader["CarrierId"].ToString(),
                    CarrierName = sqlDataReader["CarrierName"].ToString(),
                    Price = long.Parse(sqlDataReader["Price"].ToString()),
                    CreateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["CreateTime"].ToString()),
                    CreateUserId = sqlDataReader["CreateUserId"].ToString(),
                    CreateUserName = sqlDataReader["CreateUserName"].ToString(),
                    Description = sqlDataReader["Description"].ToString(),
                    CategoryId = sqlDataReader["CategoryId"].ToString(),
                    CategoryName = sqlDataReader["CategoryName"].ToString(),
                    Sale = long.Parse(sqlDataReader["Sale"].ToString()),
                    IsSell = bool.Parse(sqlDataReader["IsSell"].ToString()),
                    IsActive = bool.Parse(sqlDataReader["IsActive"].ToString()),
                    CarrierImage = sqlDataReader["Image"].ToString()
                };
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return product;
        }

        /// <summary>
        /// Get thông tin mặt hàng theo số sim
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public ProductViewModel GetInfoProductByNumber(string number)
        {
            ProductViewModel product = null;
            sqlQuery = "SELECT * FROM View_GetAllProduct WHERE Number = @Number";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Number",number)
            };
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                product = new ProductViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Number = sqlDataReader["Number"].ToString(),
                    CarrierId = sqlDataReader["CarrierId"].ToString(),
                    CarrierName = sqlDataReader["CarrierName"].ToString(),
                    Price = long.Parse(sqlDataReader["Price"].ToString()),
                    CreateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["CreateTime"].ToString()),
                    CreateUserId = sqlDataReader["CreateUserId"].ToString(),
                    CreateUserName = sqlDataReader["CreateUserName"].ToString(),
                    Description = sqlDataReader["Description"].ToString(),
                    CategoryId = sqlDataReader["CategoryId"].ToString(),
                    CategoryName = sqlDataReader["CategoryName"].ToString(),
                    Sale = long.Parse(sqlDataReader["Sale"].ToString()),
                    IsSell = bool.Parse(sqlDataReader["IsSell"].ToString()),
                    IsActive = bool.Parse(sqlDataReader["IsActive"].ToString()),
                    CarrierImage = sqlDataReader["Image"].ToString()
                };
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return product;
        }

        /// <summary>
        /// Thêm một mặt hàng SIM mới
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public ResultModel CreateProduct(ProductModel product)
        {
            if (CheckExistProductNumber(product.Number))
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Số SIM vừa nhập đã tồn tại trong kho"
                };
            }
            sqlQuery = "EXEC CreateProduct @Id,@Number,@CarrierId,@Price,@CreateTime,@CreateUserId,@Description,@CategoryId,@Sale,@IsSell,@IsActive";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",product.Id),
                new SqlParameter("Number",product.Number),
                new SqlParameter("CarrierId",product.CarrierId),
                new SqlParameter("Price",product.Price),
                new SqlParameter("CreateTime",product.CreateTime),
                new SqlParameter("CreateUserId",product.CreateUserId),
                new SqlParameter("Description",product.Description),
                new SqlParameter("CategoryId",product.CategoryId),
                new SqlParameter("Sale",product.Sale),
                new SqlParameter("IsSell",product.IsSell),
                new SqlParameter("IsActive",product.IsActive)
            };
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);
            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Thêm mặt hàng thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Xóa một mặt hàng theo Id
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public ResultModel DeleteProduct(string productId)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "DELETE FROM Product WHERE Id = @Id";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",productId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Xóa mặt hàng thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Cập nhật mặt hàng
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public ResultModel UpdateProduct(ProductModel product)
        {
            ProductViewModel productInfo = GetInfoProduct(product.Id);
            if (productInfo.IsSell)
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Không thể chỉnh sửa mặt hàng đã bán"
                };
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "EXEC UpdateProduct @Id,@CarrierId,@Price,@Description,@CategoryId";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",product.Id),
                new SqlParameter("CarrierId",product.CarrierId),
                new SqlParameter("Price",product.Price),
                new SqlParameter("Description",product.Description),
                new SqlParameter("CategoryId",string.IsNullOrEmpty(product.CategoryId)?KeyHelper.DefaultCategoryId:product.CategoryId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Sửa mặt hàng thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Kích hoạt, hủy kích hoạt mặt hàng
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public ResultModel IsActiveProduct(string productId, bool isActive)
        {
            string IsActive = "Hủy kích hoạt";
            if (isActive)
            {
                IsActive = "Kích hoạt";
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "UPDATE Product SET IsActive = @IsActive WHERE Id = @Id";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",productId),
                new SqlParameter("IsActive",isActive)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = IsActive + " mặt hàng thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Get 12 mặt hàng mới nhất hoạt động
        /// </summary>
        /// <returns></returns>
        public List<ProductViewModel> Get12ProductNewToHome()
        {
            List<ProductViewModel> products = new List<ProductViewModel>();
            sqlQuery = "EXEC Get12ProductNew";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                ProductViewModel product = new ProductViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Number = sqlDataReader["Number"].ToString(),
                    CarrierId = sqlDataReader["CarrierId"].ToString(),
                    CarrierName = sqlDataReader["CarrierName"].ToString(),
                    Price = long.Parse(sqlDataReader["Price"].ToString()),
                    Description = sqlDataReader["Description"].ToString()
                };
                products.Add(product);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return products;
        }

        /// <summary>
        /// Get 12 mặt hàng ngẫu nhiên hoạt động
        /// </summary>
        /// <returns></returns>
        public List<ProductViewModel> Get12ProductNormal()
        {
            List<ProductViewModel> products = new List<ProductViewModel>();
            sqlQuery = "EXEC Get12ProductNormal";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                ProductViewModel product = new ProductViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Number = sqlDataReader["Number"].ToString(),
                    CarrierId = sqlDataReader["CarrierId"].ToString(),
                    CarrierName = sqlDataReader["CarrierName"].ToString(),
                    Price = long.Parse(sqlDataReader["Price"].ToString()),
                    Description = sqlDataReader["Description"].ToString()
                };
                products.Add(product);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return products;
        }

        /// <summary>
        /// Get số lượng mặt hàng theo giá tiền
        /// </summary>
        /// <returns></returns>
        public List<PriceProductViewModel> GetPriceCountProduct()
        {
            List<PriceProductViewModel> priceProducts = new List<PriceProductViewModel>();
            sqlQuery = "SELECT * FROM dbo.GetPriceCountProduct() ORDER BY CountProduct DESC";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                PriceProductViewModel priceProduct = new PriceProductViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    CountProduct = int.Parse(sqlDataReader["CountProduct"].ToString())
                };
                priceProduct.Name = ConvertHelper.FormatNamePriceProduct(priceProduct.Id);
                priceProducts.Add(priceProduct);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return priceProducts;
        }

        /// <summary>
        /// Get tất cả các mặt hàng hoạt động
        /// </summary>
        /// <returns></returns>
        public List<ProductViewModel> GetAllProductActive()
        {
            List<ProductViewModel> products = new List<ProductViewModel>();
            sqlQuery = "EXEC GetAllProductActive";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                ProductViewModel product = new ProductViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Number = sqlDataReader["Number"].ToString(),
                    CarrierId = sqlDataReader["CarrierId"].ToString(),
                    CarrierName = sqlDataReader["CarrierName"].ToString(),
                    Price = long.Parse(sqlDataReader["Price"].ToString()),
                    CreateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["CreateTime"].ToString()),
                    CategoryName = sqlDataReader["CategoryName"].ToString(),
                    Sale = long.Parse(sqlDataReader["Sale"].ToString()),
                    CarrierImage = sqlDataReader["Image"].ToString()
                };
                products.Add(product);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return products;
        }

        /// <summary>
        /// Get tất cả mặt hàng hoạt động theo nhà mạng
        /// </summary>
        /// <param name="carrierId"></param>
        /// <returns></returns>
        public List<ProductViewModel> GetProductActiveByCarrier(string carrierId)
        {
            List<ProductViewModel> products = new List<ProductViewModel>();
            sqlQuery = "EXEC GetProductActiveByCarrier @CarrierId";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            sqlCommand.Parameters.Add(new SqlParameter("CarrierId", carrierId));
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                ProductViewModel product = new ProductViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Number = sqlDataReader["Number"].ToString(),
                    CarrierId = sqlDataReader["CarrierId"].ToString(),
                    CarrierName = sqlDataReader["CarrierName"].ToString(),
                    Price = long.Parse(sqlDataReader["Price"].ToString()),
                    CreateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["CreateTime"].ToString()),
                    CategoryName = sqlDataReader["CategoryName"].ToString(),
                    Sale = long.Parse(sqlDataReader["Sale"].ToString()),
                    CarrierImage = sqlDataReader["Image"].ToString()
                };
                products.Add(product);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return products;
        }

        /// <summary>
        /// Get tất cả mặt hàng hoạt động theo danh mục
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public List<ProductViewModel> GetProductActiveByCategory(string categoryId)
        {
            List<ProductViewModel> products = new List<ProductViewModel>();
            sqlQuery = "EXEC GetProductActiveByCategory @CategoryId";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            sqlCommand.Parameters.Add(new SqlParameter("CategoryId", categoryId));
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                ProductViewModel product = new ProductViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Number = sqlDataReader["Number"].ToString(),
                    CarrierId = sqlDataReader["CarrierId"].ToString(),
                    CarrierName = sqlDataReader["CarrierName"].ToString(),
                    Price = long.Parse(sqlDataReader["Price"].ToString()),
                    CreateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["CreateTime"].ToString()),
                    CategoryName = sqlDataReader["CategoryName"].ToString(),
                    Sale = long.Parse(sqlDataReader["Sale"].ToString()),
                    CarrierImage = sqlDataReader["Image"].ToString()
                };
                products.Add(product);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return products;
        }

        /// <summary>
        /// Get tất cả mặt hàng hoạt động theo giá
        /// </summary>
        /// <param name="priceId"></param>
        /// <returns></returns>
        public List<ProductViewModel> GetProductActiveByPrice(string priceId)
        {
            List<ProductViewModel> products = new List<ProductViewModel>();
            sqlQuery = "EXEC GetProductActiveByPrice @MinPrice,@MaxPrice";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            sqlCommand.Parameters.Add(new SqlParameter("MinPrice", long.Parse(priceId.Split('-')[0])));
            sqlCommand.Parameters.Add(new SqlParameter("MaxPrice", long.Parse(priceId.Split('-')[1])));
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                ProductViewModel product = new ProductViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Number = sqlDataReader["Number"].ToString(),
                    CarrierId = sqlDataReader["CarrierId"].ToString(),
                    CarrierName = sqlDataReader["CarrierName"].ToString(),
                    Price = long.Parse(sqlDataReader["Price"].ToString()),
                    CreateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["CreateTime"].ToString()),
                    CategoryName = sqlDataReader["CategoryName"].ToString(),
                    Sale = long.Parse(sqlDataReader["Sale"].ToString()),
                    CarrierImage = sqlDataReader["Image"].ToString()
                };
                products.Add(product);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return products;
        }

        /// <summary>
        /// Get tương đối mặt hàng hoạt động theo số
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public List<ProductViewModel> GetProductActiveByNumber(string number)
        {
            List<ProductViewModel> products = new List<ProductViewModel>();
            sqlQuery = "EXEC GetProductActiveByNumber @Number";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            sqlCommand.Parameters.Add(new SqlParameter("Number", number));
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                ProductViewModel product = new ProductViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Number = sqlDataReader["Number"].ToString(),
                    CarrierId = sqlDataReader["CarrierId"].ToString(),
                    CarrierName = sqlDataReader["CarrierName"].ToString(),
                    Price = long.Parse(sqlDataReader["Price"].ToString()),
                    CreateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["CreateTime"].ToString()),
                    CategoryName = sqlDataReader["CategoryName"].ToString(),
                    Sale = long.Parse(sqlDataReader["Sale"].ToString()),
                    CarrierImage = sqlDataReader["Image"].ToString()
                };
                products.Add(product);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return products;
        }

        /// <summary>
        /// Set giá trị bán hay chưa cho mặt hàng
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="isSell"></param>
        /// <returns></returns>
        public ResultModel SellProduct(string productId, bool isSell)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "UPDATE Product SET IsSell = @IsSell WHERE Id = @Id";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("IsSell",isSell),
                new SqlParameter("Id",productId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Cập nhật thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Kiểm tra tồn tại của số sim trong kho
        /// </summary>
        /// <param name="productNumber"></param>
        /// <returns></returns>
        private bool CheckExistProductNumber(string productNumber)
        {
            bool result = false;
            sqlQuery = "SELECT Id FROM Product WHERE Number = @ProductNumber";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("ProductNumber",productNumber)
            };
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                result = true;
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return result;
        }
    }
}