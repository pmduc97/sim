﻿using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SimStore_DucPham.Data
{
    public class CategoryData
    {
        private string sqlQuery = string.Empty;

        /// <summary>
        /// Get tất cả các danh mục
        /// </summary>
        /// <returns></returns>
        public List<CategoryModel> GetAllCategory()
        {
            List<CategoryModel> categories = new List<CategoryModel>();
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "EXEC GetFullCategory";
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                CategoryModel category = new CategoryModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Name = sqlDataReader["Name"].ToString(),
                    Description = sqlDataReader["Description"].ToString(),
                    IsActive = bool.Parse(sqlDataReader["IsActive"].ToString()),
                    CountProduct = int.Parse(sqlDataReader["ProductCount"].ToString())
                };
                categories.Add(category);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return categories;
        }

        /// <summary>
        /// Get tất cả danh mục hoạt động
        /// </summary>
        /// <returns></returns>
        public List<CategoryModel> GetAllCategoryActive()
        {
            List<CategoryModel> categories = new List<CategoryModel>();
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "EXEC GetFullCategoryActive";
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                CategoryModel category = new CategoryModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Name = sqlDataReader["Name"].ToString(),
                    CountProduct = int.Parse(sqlDataReader["ProductCount"].ToString())
                };
                categories.Add(category);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return categories;
        }

        /// <summary>
        /// Get thông tin danh mục theo Id
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public CategoryModel GetInfoCategory(string categoryId)
        {
            CategoryModel category = null;
            sqlQuery = "SELECT * FROM Category WHERE Id = @CategoryId";
            SqlConnectData sqlConnectData = new SqlConnectData();
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("CategoryId",categoryId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                category = new CategoryModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Name = sqlDataReader["Name"].ToString(),
                    Description = sqlDataReader["Description"].ToString(),
                    IsActive = bool.Parse(sqlDataReader["IsActive"].ToString())
                };
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return category;
        }

        /// <summary>
        /// Tạo mới một danh mục
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public ResultModel CreateCategory(CategoryModel category)
        {
            sqlQuery = "EXEC CreateCategory @Id,@Name,@Description,@IsActive";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",category.Id),
                new SqlParameter("Name",category.Name),
                new SqlParameter("Description",category.Description),
                new SqlParameter("IsActive",category.IsActive)
            };
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Thêm danh mục thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Chỉnh sửa một danh mục
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="categoryName"></param>
        /// <param name="categoryDescription"></param>
        /// <returns></returns>
        public ResultModel UpdateCategory(string categoryId, string categoryName, string categoryDescription)
        {
            if (!CheckExistCategory(categoryId))
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Không tìm thấy Danh mục cần sửa"
                };
            }
            if (string.IsNullOrEmpty(categoryName))
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Tên danh mục không được rỗng"
                };
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "EXEC UpdateCategory @Id,@Name,@Description";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",categoryId),
                new SqlParameter("Name",categoryName),
                new SqlParameter("Description",categoryDescription)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Sửa Danh mục thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Xóa một danh mục
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public ResultModel DeleteCategory(string categoryId)
        {
            if (!CheckExistCategory(categoryId))
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Không tìm thấy Danh mục cần xóa"
                };
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "DELETE FROM Category WHERE Id = @Id";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",categoryId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Xóa Danh mục thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Kích hoạt / tắt hoạt động một danh mục
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public ResultModel IsActiveCategory(string categoryId, bool isActive)
        {
            if (!CheckExistCategory(categoryId))
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Không tìm thấy Danh mục"
                };
            }
            string IsActive = "Hủy kích hoạt";
            if (isActive)
            {
                IsActive = "Kích hoạt";
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "UPDATE Category SET IsActive = @IsActive WHERE Id = @Id";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",categoryId),
                new SqlParameter("IsActive",isActive)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = IsActive + " Danh mục thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Kiểm tra sự tồn tại của danh mục
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        private bool CheckExistCategory(string categoryId)
        {
            bool result = false;
            sqlQuery = "SELECT Id FROM Category WHERE Id = @CategoryId";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("CategoryId",categoryId)
            };
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                result = true;
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return result;
        }
    }
}