﻿using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;
using SimStore_DucPham.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SimStore_DucPham.Data
{
    public class UserRoleData
    {
        private UserData userData = new UserData();
        private string sqlQuery = string.Empty;

        /// <summary>
        /// Kiểm tra người dùng có phải Quản trị không
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool UserIsAdmin(string userId)
        {
            bool result = false;
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "SELECT * FROM AspNetUserRoles WHERE UserId = @UserId AND RoleId = 'admin'";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("UserId",userId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                result = true;
            }

            sqlDataReader.Close();
            sqlConnectData.conn.Close();

            return result;
        }

        /// <summary>
        /// Kiểm tra người dùng có phải khách hàng không
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool UserIsCustomer(string userId)
        {
            bool result = false;
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "SELECT * FROM AspNetUserRoles WHERE UserId = @UserId AND RoleId = 'customer'";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("UserId",userId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                result = true;
            }

            sqlDataReader.Close();
            sqlConnectData.conn.Close();

            return result;
        }

        /// <summary>
        /// Thêm quyền khách hàng vào tài khoản
        /// </summary>
        /// <param name="userId"></param>
        public void InsertCustomerToUser(string userId)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "INSERT INTO AspNetUserRoles(UserId,RoleId) VALUES (@UserId,'customer')";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("UserId",userId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);
            sqlCommand.ExecuteNonQuery();
            sqlConnectData.conn.Close();
        }

        /// <summary>
        /// Đổi quyền tài khoản thành quản trị
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResultModel ChangeUserToAdmin(string userId)
        {
            UserViewModel user = userData.GetInfoUser(userId);
            if (user == null)
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Không tìm thấy tài khoản cần đổi quyền."
                };
            }
            if (user.RoleId.Equals("admin"))
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Tài khoản đang là quyền Quản trị rồi."
                };
            }

            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "UPDATE AspNetUserRoles SET RoleId = 'admin' WHERE UserId = @UserId";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("UserId",userId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Nâng quyền cho tài khoản thành Quản trị thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Đổi quyền tài khoản thành khách hàng
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResultModel ChangeUserToCustomer(string userId)
        {
            UserViewModel user = userData.GetInfoUser(userId);
            if (user == null)
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Không tìm thấy tài khoản cần đổi quyền."
                };
            }
            if (user.RoleId.Equals("customer"))
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Tài khoản đang là quyền Khách hàng rồi."
                };
            }

            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "UPDATE AspNetUserRoles SET RoleId = 'customer' WHERE UserId = @UserId";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("UserId",userId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Hạ quyền cho tài khoản thành Khách hàng thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }
    }
}