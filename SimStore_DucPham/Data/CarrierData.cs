﻿using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SimStore_DucPham.Data
{
    public class CarrierData
    {
        private string sqlQuery = string.Empty;

        /// <summary>
        /// Get tất cả các nhà mạng
        /// </summary>
        /// <returns></returns>
        public List<CarrierModel> GetAllCarrier()
        {
            List<CarrierModel> carriers = new List<CarrierModel>();
            sqlQuery = "EXEC GetFullCarrier";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                CarrierModel carrier = new CarrierModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Name = sqlDataReader["Name"].ToString(),
                    Image = sqlDataReader["Image"].ToString(),
                    Description = sqlDataReader["Description"].ToString(),
                    IsActive = bool.Parse(sqlDataReader["IsActive"].ToString()),
                    CountProduct = int.Parse(sqlDataReader["ProductCount"].ToString())
                };
                carriers.Add(carrier);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return carriers;
        }

        /// <summary>
        /// Get tất cả nhà mạng hoạt động
        /// </summary>
        /// <returns></returns>
        public List<CarrierModel> GetAllCarrierActive()
        {
            List<CarrierModel> carriers = new List<CarrierModel>();
            sqlQuery = "EXEC GetFullCarrierActive";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                CarrierModel carrier = new CarrierModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Name = sqlDataReader["Name"].ToString(),
                    CountProduct = int.Parse(sqlDataReader["ProductCount"].ToString())
                };
                carriers.Add(carrier);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return carriers;
        }

        /// <summary>
        /// Get thông tin một nhà mạng theo ID
        /// </summary>
        /// <param name="carrierId"></param>
        /// <returns></returns>
        public CarrierModel GetInfoCarrier(string carrierId)
        {
            CarrierModel carrier = null;
            sqlQuery = "SELECT * FROM Carrier WHERE Id = @Id";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            sqlCommand.Parameters.Add(new SqlParameter("Id", carrierId));
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                carrier = new CarrierModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Name = sqlDataReader["Name"].ToString(),
                    Image = sqlDataReader["Image"].ToString(),
                    Description = sqlDataReader["Description"].ToString(),
                    IsActive = bool.Parse(sqlDataReader["IsActive"].ToString())
                };
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return carrier;
        }

        /// <summary>
        /// Thêm mới một nhà mạng
        /// </summary>
        /// <param name="carrier"></param>
        /// <returns></returns>
        public ResultModel CreateCarrier(CarrierModel carrier)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "EXEC CreateCarrier @Id,@Name,@Image,@Description,@IsActive";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",carrier.Id),
                new SqlParameter("Name",carrier.Name),
                new SqlParameter("Image",carrier.Image),
                new SqlParameter("Description",carrier.Description),
                new SqlParameter("IsActive",carrier.IsActive)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Thêm nhà mạng thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Cập nhật nhà mạng
        /// </summary>
        /// <param name="carrierId"></param>
        /// <param name="carrierName"></param>
        /// <param name="carrierDescription"></param>
        /// <param name="carrierImage"></param>
        /// <returns></returns>
        public ResultModel UpdateCarrier(string carrierId, string carrierName, string carrierDescription, string carrierImage)
        {
            if (!CheckExistCarrier(carrierId))
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Không tìm thấy Nhà mạng cần sửa"
                };
            }
            if (string.IsNullOrEmpty(carrierName))
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Tên nhà mạng không được rỗng"
                };
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "EXEC UpdateCarrier @Id,@Name,@Description,@Image";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",carrierId),
                new SqlParameter("Name",carrierName),
                new SqlParameter("Description",carrierDescription),
                new SqlParameter("Image",carrierImage)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Sửa nhà mạng thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Xóa nhà mạng
        /// </summary>
        /// <param name="carrierId"></param>
        /// <returns></returns>
        public ResultModel DeleteCarrier(string carrierId)
        {
            if (!CheckExistCarrier(carrierId))
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Không tìm thấy nhà mạng cần xóa"
                };
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "DELETE FROM Carrier WHERE Id = @Id";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",carrierId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Xóa nhà mạng thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Kích hoạt/ Tắt hoạt động của nhà mạng
        /// </summary>
        /// <param name="carrierId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public ResultModel IsActiveCarrier(string carrierId, bool isActive)
        {
            if (!CheckExistCarrier(carrierId))
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Không tìm thấy nhà mạng"
                };
            }
            string IsActive = "Hủy kích hoạt";
            if (isActive)
            {
                IsActive = "Kích hoạt";
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "UPDATE Carrier SET IsActive = @IsActive WHERE Id = @Id";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",carrierId),
                new SqlParameter("IsActive",isActive)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = IsActive + " nhà mạng thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Chỉnh sửa nhà mạng
        /// </summary>
        /// <param name="carrierId"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        public ResultModel UpdateImage(string carrierId, string image)
        {
            if (!CheckExistCarrier(carrierId))
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Không tìm thấy nhà mạng"
                };
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "UPDATE Carrier SET Image = @Image WHERE Id = @Id";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",carrierId),
                new SqlParameter("Image",image)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Cập nhật ảnh nhà mạng thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Kiểm tra tồn tại nhà mạng
        /// </summary>
        /// <param name="carrierId"></param>
        /// <returns></returns>
        private bool CheckExistCarrier(string carrierId)
        {
            bool result = false;
            sqlQuery = "SELECT Id FROM Carrier WHERE Id = @CarrierId";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("CarrierId",carrierId)
            };
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                result = true;
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return result;
        }
    }
}