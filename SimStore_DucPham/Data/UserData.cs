﻿using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;
using SimStore_DucPham.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SimStore_DucPham.Data
{
    public class UserData
    {
        private string sqlQuery = string.Empty;

        /// <summary>
        /// Get tất cả các người dùng
        /// </summary>
        /// <returns></returns>
        public List<UserViewModel> GetAllUser()
        {
            List<UserViewModel> users = new List<UserViewModel>();
            sqlQuery = "SELECT * FROM View_GetAllUser";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                UserViewModel user = new UserViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    FullName = sqlDataReader["FullName"].ToString(),
                    Email = sqlDataReader["Email"].ToString(),
                    UserName = sqlDataReader["UserName"].ToString(),
                    CreateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["CreateTime"].ToString()),
                    IsActive = bool.Parse(sqlDataReader["IsActive"].ToString()),
                    RoleId = sqlDataReader["RoleId"].ToString(),
                    Avatar = sqlDataReader["Avatar"].ToString(),
                    Address = sqlDataReader["Address"].ToString(),
                    PhoneNumber = sqlDataReader["PhoneNumber"].ToString()
                };
                users.Add(user);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return users;
        }

        /// <summary>
        /// Get thông tin của 1 người dùng theo ID
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserViewModel GetInfoUser(string userId)
        {
            UserViewModel user = null;
            sqlQuery = "SELECT * FROM View_GetAllUser WHERE Id = @UserId";
            SqlConnectData sqlConnectData = new SqlConnectData();
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("UserId",userId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                user = new UserViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    FullName = sqlDataReader["FullName"].ToString(),
                    Email = sqlDataReader["Email"].ToString(),
                    UserName = sqlDataReader["UserName"].ToString(),
                    CreateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["CreateTime"].ToString()),
                    IsActive = bool.Parse(sqlDataReader["IsActive"].ToString()),
                    RoleId = sqlDataReader["RoleId"].ToString(),
                    Avatar = sqlDataReader["Avatar"].ToString(),
                    Address = sqlDataReader["Address"].ToString(),
                    PhoneNumber = sqlDataReader["PhoneNumber"].ToString()
                };
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return user;
        }

        /// <summary>
        /// Mở khóa cho người dùng
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResultModel UnLockUser(string userId)
        {
            UserViewModel user = GetInfoUser(userId);
            if (user == null)
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Không tìm thấy tài khoản cần mở khóa."
                };
            }
            if (user.IsActive)
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Tài khoản đang được mở khóa rồi."
                };
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "UPDATE AspNetUsers SET IsActive = 'False' WHERE Id = @UserId";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("UserId",userId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Mở khóa tài khoản thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Khóa người dùng
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResultModel LockUser(string userId)
        {
            UserViewModel user = GetInfoUser(userId);
            if (user == null)
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Không tìm thấy tài khoản cần khóa."
                };
            }
            if (!user.IsActive)
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Tài khoản đang khóa rồi."
                };
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "UPDATE AspNetUsers SET IsLock = 'True' WHERE Id = @UserId";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("UserId",userId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Khóa tài khoản thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Kích hoạt tài khoản
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResultModel ActiveUser(string userId)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "UPDATE AspNetUsers SET IsActive = 'True' WHERE Id = @UserId";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("UserId",userId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Kích hoạt tài khoản thành công. Đăng nhập để mua SIM ngay."
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Cập nhật thông tin người dùng
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="fullName"></param>
        /// <param name="email"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        public ResultModel UpdateProfile(string userId, string fullName, string email, string phoneNumber, string address)
        {
            UserViewModel user = GetInfoUser(userId);
            if (user == null)
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: Không tìm thấy tài khoản cần cập nhật."
                };
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "UPDATE AspNetUsers SET FullName = @FullName, Email = @Email, PhoneNumber = @PhoneNumber, Address = @Address WHERE Id = @UserId";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("FullName",fullName),
                new SqlParameter("Email",email),
                new SqlParameter("PhoneNumber",phoneNumber),
                new SqlParameter("Address",address),
                new SqlParameter("UserId",userId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Cập nhật dữ liệu tài khoản thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Xóa tài khoản
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResultModel DeleteUser(string userId)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "DELETE FROM AspNetUsers WHERE Id = @Id";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",userId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Xóa User thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }
    }
}