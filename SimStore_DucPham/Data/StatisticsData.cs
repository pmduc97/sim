﻿using SimStore_DucPham.Helper;
using SimStore_DucPham.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SimStore_DucPham.Data
{
    public class StatisticsData
    {
        private string sqlQuery = string.Empty;

        /// <summary>
        /// Get các thông tin cho trang thống kê mặt định Admin
        /// </summary>
        /// <returns>HomeAdminModel: BoxNumberModel,List<OrderNewModel></returns>
        public HomeAdminModel GetHomeAdmin()
        {
            BoxNumberModel boxNumber = new BoxNumberModel()
            {
                ProductSell = GetProductSell(),
                BillNonPay = GetBillNonPay(),
                SumPriceToday = GetSumMoneyToday(),
                OrderToday = GetSumOrderToday()
            };
            List<OrderNewModel> orderNews = GetTop10OrderNew();
            return new HomeAdminModel()
            {
                BoxNumber = boxNumber,
                OrderNews = orderNews
            };
        }

        /// <summary>
        /// Get 10 đơn hàng Online mới nhất
        /// </summary>
        /// <returns></returns>
        private List<OrderNewModel> GetTop10OrderNew()
        {
            List<OrderNewModel> orderNews = new List<OrderNewModel>();
            sqlQuery = "EXEC GetTop10OrderNew";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                OrderNewModel orderNew = new OrderNewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    IsOnline = bool.Parse(sqlDataReader["IsOnline"].ToString()),
                    CreateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["CreateTime"].ToString()),
                    Address = sqlDataReader["Address"].ToString(),
                    SumPrice = long.Parse(sqlDataReader["SumMoney"].ToString()),
                    FullName = sqlDataReader["FullName"].ToString(),
                    Status = ConvertHelper.ConvertStatus(sqlDataReader["Status"].ToString())
                };
                orderNews.Add(orderNew);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return orderNews;
        }

        /// <summary>
        /// Get tổng số mặt hàng đã bán
        /// </summary>
        /// <returns></returns>
        private int GetProductSell()
        {
            int result = 0;
            sqlQuery = "SELECT COUNT(Id) AS Tong FROM Product WHERE IsSell = 1";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                if (sqlDataReader["Tong"] != DBNull.Value)
                {
                    result = int.Parse(sqlDataReader["Tong"].ToString());
                }
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return result;
        }

        /// <summary>
        /// Get tổng số đơn hàng chưa thanh toán
        /// </summary>
        /// <returns></returns>
        private int GetBillNonPay()
        {
            int result = 0;
            sqlQuery = "SELECT COUNT(Id) AS Tong FROM Bill WHERE IsPay = 0 AND Status != 'HuyBo'";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                if (sqlDataReader["Tong"] != DBNull.Value)
                {
                    result = int.Parse(sqlDataReader["Tong"].ToString());
                }
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return result;
        }

        /// <summary>
        /// Get tổng tiền đã bán được hôm nay
        /// </summary>
        /// <returns></returns>
        private int GetSumMoneyToday()
        {
            int result = 0;
            sqlQuery = "EXEC GetSumPriceToday";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                if (sqlDataReader["Tong"] != DBNull.Value)
                {
                    result = int.Parse(sqlDataReader["Tong"].ToString());
                }
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return result;
        }

        /// <summary>
        /// Get tổng số đơn hàng hôm nay
        /// </summary>
        /// <returns></returns>
        private int GetSumOrderToday()
        {
            int result = 0;
            sqlQuery = "EXEC GetSumOrderToday";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                if (sqlDataReader["Tong"] != DBNull.Value)
                {
                    result = int.Parse(sqlDataReader["Tong"].ToString());
                }
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return result;
        }
    }
}