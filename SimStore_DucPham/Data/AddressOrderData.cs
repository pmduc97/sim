﻿using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;
using SimStore_DucPham.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SimStore_DucPham.Data
{
    public class AddressOrderData
    {
        private string sqlQuery = string.Empty;

        public ResultModel CreateAddressOrder(AddressOrderModel address)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "EXEC CreateAddressOrderOnlineDefault "
                        + "@Id,@UserId,@IsOrder,@IsPay,@City,@District,@Wards,@Address,@FullName,@PhoneNumber";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",address.Id),
                new SqlParameter("UserId",address.UserId),
                new SqlParameter("IsOrder",address.IsOrder),
                new SqlParameter("IsPay",address.IsPay),
                new SqlParameter("City",address.City),
                new SqlParameter("District",address.District),
                new SqlParameter("Wards",address.Wards),
                new SqlParameter("Address",address.Address),
                new SqlParameter("FullName",address.FullName),
                new SqlParameter("PhoneNumber",address.PhoneNumber)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Thêm thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Tạo mới địa chỉ giao,nhận hàng cho đơn Online mua nhanh
        /// </summary>
        /// <param name="addressOrder"></param>
        /// <returns></returns>
        public ResultModel CreateOrderOnlineDefault(AddressOrderDefaultModel addressOrder)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "EXEC CreateAddressOrderOnlineDefault @Id,@UserId,@FullName,@PhoneNumber,@Address";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",addressOrder.Id),
                new SqlParameter("UserId",addressOrder.UserId),
                new SqlParameter("FullName",addressOrder.FullName),
                new SqlParameter("PhoneNumber",addressOrder.PhoneNumber),
                new SqlParameter("Address",addressOrder.Address)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Thêm thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Get thông tin địa chỉ giao/nhận hàng
        /// </summary>
        /// <param name="addressOrderId"></param>
        /// <returns></returns>
        public AddressOrderViewModel GetDetailAddressOrder(string addressOrderId)
        {
            AddressOrderViewModel addressOrder = null;
            sqlQuery = "SELECT * FROM AddressOrder WHERE Id = @Id";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            sqlCommand.Parameters.Add(new SqlParameter("Id", addressOrderId));
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                addressOrder = new AddressOrderViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    UserId = sqlDataReader["UserId"].ToString(),
                    IsOrder = bool.Parse(sqlDataReader["IsOrder"].ToString()),
                    IsPay = bool.Parse(sqlDataReader["IsPay"].ToString()),
                    City = sqlDataReader["City"].ToString(),
                    District = sqlDataReader["District"].ToString(),
                    Wards = sqlDataReader["Wards"].ToString(),
                    Address = sqlDataReader["Address"].ToString(),
                    FullName = sqlDataReader["FullName"].ToString(),
                    PhoneNumber = sqlDataReader["PhoneNumber"].ToString()
                };
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return addressOrder;
        }

        public List<AddressOrderViewModel> GetAllAddress()
        {
            List<AddressOrderViewModel> addresses = new List<AddressOrderViewModel>();
            sqlQuery = "SELECT * FROM AddressOrder";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                var item = new AddressOrderViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    UserId = sqlDataReader["UserId"].ToString(),
                    IsOrder = bool.Parse(sqlDataReader["IsOrder"].ToString()),
                    IsPay = bool.Parse(sqlDataReader["IsPay"].ToString()),
                    City = sqlDataReader["City"].ToString(),
                    District = sqlDataReader["District"].ToString(),
                    Wards = sqlDataReader["Wards"].ToString(),
                    Address = sqlDataReader["Address"].ToString(),
                    FullName = sqlDataReader["FullName"].ToString(),
                    PhoneNumber = sqlDataReader["PhoneNumber"].ToString()
                };
                item.AddressFull = item.Wards + ", " + item.District + ", " + item.City;
                addresses.Add(item);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return addresses;
        }

        public ResultModel Delete(string addressId)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "DELETE FROM AddressOrder WHERE Id = @Id";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",addressId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Xóa thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        public ResultModel Update(AddressOrderModel address)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "EXEC UpdateAddressOrder @Id,@UserId,@IsOrder,@IsPay,@City,@District,@Wards,@Address,@FullName,@PhoneNumber";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",address.Id),
                new SqlParameter("UserId",address.UserId),
                new SqlParameter("IsOrder",address.IsOrder),
                new SqlParameter("IsPay",address.IsPay),
                new SqlParameter("City",address.City),
                new SqlParameter("District",address.District),
                new SqlParameter("Wards",address.Wards),
                new SqlParameter("Address",address.Address),
                new SqlParameter("FullName",address.FullName),
                new SqlParameter("PhoneNumber",address.PhoneNumber)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Sửa thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }
    }
}