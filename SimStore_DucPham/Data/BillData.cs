﻿using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;
using SimStore_DucPham.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SimStore_DucPham.Data
{
    public class BillData
    {
        #region Data

        private string sqlQuery = string.Empty;
        private ProductData productData = new ProductData();
        private AddressOrderData addressOrderData = new AddressOrderData();

        #endregion Data

        /// <summary>
        /// Tạo hóa đơn mới: tạo hóa đơn, history, chi tiết hóa đơn
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="products"></param>
        /// <returns></returns>
        public ResultModel CreateBill(BillCreateModel bill, List<ProductViewModel> products)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "EXEC CreateBill @Id,@CreateUserId,@CreateTime,@Status,@SumMoney,@Note,@AddressOrderId,@IsOnline,@Code";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",bill.Id),
                new SqlParameter("CreateUserId",bill.CreateUserId),
                new SqlParameter("CreateTime",bill.CreateTime),
                new SqlParameter("Status",bill.Status),
                new SqlParameter("SumMoney",bill.SumMoney),
                new SqlParameter("Note",bill.Note),
                new SqlParameter("AddressOrderId",bill.AddressOrderId),
                new SqlParameter("IsOnline",bill.IsOnline),
                new SqlParameter("Code",bill.Code)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                HistoryBillModel historyBill = new HistoryBillModel()
                {
                    Id = Guid.NewGuid().ToString(),
                    BillId = bill.Id,
                    Status = bill.Status,
                    CreateTime = bill.CreateTime,
                    CreateUserId = bill.CreateUserId,
                    IsRead = false
                };
                CreateHistoryBill(historyBill);
                foreach (ProductViewModel product in products)
                {
                    DetailBillModel detailBill = new DetailBillModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        BillId = bill.Id,
                        ProductId = product.Id,
                        Price = product.Price
                    };
                    CreateDetailBill(detailBill);
                }
                return new ResultModel()
                {
                    Status = true,
                    Message = "Thêm đơn hàng thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Tạo mới history bill
        /// </summary>
        /// <param name="historyBill"></param>
        /// <returns></returns>
        public ResultModel CreateHistoryBill(HistoryBillModel historyBill)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "EXEC CreateHistoryBill @Id,@BillId,@Status,@CreateTime,@CreateUserId,@IsRead";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",historyBill.Id),
                new SqlParameter("BillId",historyBill.BillId),
                new SqlParameter("Status",historyBill.Status),
                new SqlParameter("CreateTime",historyBill.CreateTime),
                new SqlParameter("CreateUserId",historyBill.CreateUserId),
                new SqlParameter("IsRead",historyBill.IsRead)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Thêm lịch sử đơn hàng thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Get 10 hóa đơn mới cho Slide phải
        /// </summary>
        /// <returns></returns>
        public List<SlideOrderViewModel> Get10OrderNew()
        {
            List<SlideOrderViewModel> slides = new List<SlideOrderViewModel>();
            sqlQuery = "EXEC Get10OrderNew";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                SlideOrderViewModel slide = new SlideOrderViewModel()
                {
                    FullName = sqlDataReader["FullName"].ToString(),
                    Number = sqlDataReader["Number"].ToString(),
                    CreateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["CreateTime"].ToString())
                };
                slides.Add(slide);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return slides;
        }

        /// <summary>
        /// Get tất cả các hóa đơn
        /// </summary>
        /// <returns></returns>
        public List<BillViewModel> GetAllBill()
        {
            List<BillViewModel> bills = new List<BillViewModel>();
            sqlQuery = "SELECT * FROM Bill ORDER BY CreateTime DESC";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                string updateTime = string.Empty;
                string updateUserId = string.Empty;
                string note = string.Empty;
                string reasonCanceled = string.Empty;
                if (sqlDataReader["UpdateUserId"] != DBNull.Value)
                {
                    updateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["UpdateTime"].ToString());
                    updateUserId = sqlDataReader["UpdateUserId"].ToString();
                }
                if (sqlDataReader["Note"] != DBNull.Value)
                {
                    note = sqlDataReader["Note"].ToString();
                }
                if (sqlDataReader["ReasonCanceled"] != DBNull.Value)
                {
                    reasonCanceled = sqlDataReader["ReasonCanceled"].ToString();
                }

                BillViewModel bill = new BillViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    CreateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["CreateTime"].ToString()),
                    CreateDateTime = DateTime.Parse(sqlDataReader["CreateTime"].ToString()),
                    CreateUserId = sqlDataReader["CreateUserId"].ToString(),
                    Status = ConvertHelper.ConvertStatus(sqlDataReader["Status"].ToString()),
                    SumMoney = long.Parse(sqlDataReader["SumMoney"].ToString()),
                    UpdateTime = updateTime,
                    UpdateUserId = updateUserId,
                    IsPay = bool.Parse(sqlDataReader["IsPay"].ToString()),
                    Note = note,
                    ReasonCanceled = reasonCanceled,
                    AddressOrderId = sqlDataReader["AddressOrderId"].ToString(),
                    IsOnline = bool.Parse(sqlDataReader["IsOnline"].ToString())
                };
                bill.AddressOrder = addressOrderData.GetDetailAddressOrder(bill.AddressOrderId);
                bill.DetailBills = GetDetailBillsByBillId(bill.Id);
                bill.ProductNumbers = string.Empty;
                int temp = 1, countDetail = bill.DetailBills.Count;
                foreach (var item in bill.DetailBills)
                {
                    if (temp < countDetail)
                    {
                        bill.ProductNumbers += ConvertHelper.FormatPhoneNumber(item.ProductNumber) + ", ";
                    }
                    else
                    {
                        bill.ProductNumbers += ConvertHelper.FormatPhoneNumber(item.ProductNumber);
                    }
                    temp++;
                }
                bills.Add(bill);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return bills;
        }

        public List<BillViewModel> GetBillByStatus(string status)
        {
            List<BillViewModel> bills = new List<BillViewModel>();
            sqlQuery = "SELECT * FROM Bill WHERE Status = @Status ORDER BY CreateTime DESC";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            sqlCommand.Parameters.Add(new SqlParameter("Status", status));
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                string updateTime = string.Empty;
                string updateUserId = string.Empty;
                string note = string.Empty;
                string reasonCanceled = string.Empty;
                if (sqlDataReader["UpdateUserId"] != DBNull.Value)
                {
                    updateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["UpdateTime"].ToString());
                    updateUserId = sqlDataReader["UpdateUserId"].ToString();
                }
                if (sqlDataReader["Note"] != DBNull.Value)
                {
                    note = sqlDataReader["Note"].ToString();
                }
                if (sqlDataReader["ReasonCanceled"] != DBNull.Value)
                {
                    reasonCanceled = sqlDataReader["ReasonCanceled"].ToString();
                }

                BillViewModel bill = new BillViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    CreateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["CreateTime"].ToString()),
                    CreateDateTime = DateTime.Parse(sqlDataReader["CreateTime"].ToString()),
                    CreateUserId = sqlDataReader["CreateUserId"].ToString(),
                    Status = ConvertHelper.ConvertStatus(sqlDataReader["Status"].ToString()),
                    SumMoney = long.Parse(sqlDataReader["SumMoney"].ToString()),
                    UpdateTime = updateTime,
                    UpdateUserId = updateUserId,
                    IsPay = bool.Parse(sqlDataReader["IsPay"].ToString()),
                    Note = note,
                    ReasonCanceled = reasonCanceled,
                    AddressOrderId = sqlDataReader["AddressOrderId"].ToString(),
                    IsOnline = bool.Parse(sqlDataReader["IsOnline"].ToString())
                };
                bill.AddressOrder = addressOrderData.GetDetailAddressOrder(bill.AddressOrderId);
                bill.DetailBills = GetDetailBillsByBillId(bill.Id);
                bill.ProductNumbers = string.Empty;
                int temp = 1, countDetail = bill.DetailBills.Count;
                foreach (var item in bill.DetailBills)
                {
                    if (temp < countDetail)
                    {
                        bill.ProductNumbers += ConvertHelper.FormatPhoneNumber(item.ProductNumber) + ", ";
                    }
                    else
                    {
                        bill.ProductNumbers += ConvertHelper.FormatPhoneNumber(item.ProductNumber);
                    }
                    temp++;
                }
                bills.Add(bill);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return bills;
        }

        /// <summary>
        /// Get thông tin của hóa đơn
        /// </summary>
        /// <param name="billId"></param>
        /// <returns></returns>
        public BillViewModel GetInfoBill(string billId)
        {
            BillViewModel bill = null;
            sqlQuery = "SELECT Bill.*,AspNetUsers.FullName AS CreateUserName,AspNetUsers.PhoneNumber AS CreatePhoneNumber FROM Bill INNER JOIN AspNetUsers ON Bill.CreateUserId = AspNetUsers.Id WHERE Bill.Id = @Id";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            sqlCommand.Parameters.Add(new SqlParameter("Id", billId));
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                string updateTime = string.Empty;
                string updateUserId = string.Empty;
                string note = string.Empty;
                string reasonCanceled = string.Empty;
                if (sqlDataReader["UpdateUserId"] != DBNull.Value)
                {
                    updateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["UpdateTime"].ToString());
                    updateUserId = sqlDataReader["UpdateUserId"].ToString();
                }
                if (sqlDataReader["Note"] != DBNull.Value)
                {
                    note = sqlDataReader["Note"].ToString();
                }
                if (sqlDataReader["ReasonCanceled"] != DBNull.Value)
                {
                    reasonCanceled = sqlDataReader["ReasonCanceled"].ToString();
                }
                string createPhoneNumber = string.Empty;
                if (sqlDataReader["CreatePhoneNumber"] != DBNull.Value)
                {
                    createPhoneNumber = sqlDataReader["CreatePhoneNumber"].ToString();
                }
                bill = new BillViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    Code = sqlDataReader["Code"].ToString(),
                    CreateTime = ConvertHelper.DateTimeTo24h(sqlDataReader["CreateTime"].ToString()),
                    CreateDateTime = DateTime.Parse(sqlDataReader["CreateTime"].ToString()),
                    CreateUserId = sqlDataReader["CreateUserId"].ToString(),
                    Status = ConvertHelper.ConvertStatus(sqlDataReader["Status"].ToString()),
                    SumMoney = long.Parse(sqlDataReader["SumMoney"].ToString()),
                    UpdateTime = updateTime,
                    UpdateUserId = updateUserId,
                    IsPay = bool.Parse(sqlDataReader["IsPay"].ToString()),
                    Note = note,
                    ReasonCanceled = reasonCanceled,
                    AddressOrderId = sqlDataReader["AddressOrderId"].ToString(),
                    IsOnline = bool.Parse(sqlDataReader["IsOnline"].ToString()),
                    CreatePhoneNumber = createPhoneNumber,
                    CreateUserName = sqlDataReader["CreateUserName"].ToString()
                };
                bill.AddressOrder = addressOrderData.GetDetailAddressOrder(bill.AddressOrderId);
                bill.DetailBills = GetDetailBillsByBillId(bill.Id);
                bill.ProductNumbers = string.Empty;
                int temp = 1, countDetail = bill.DetailBills.Count;
                foreach (var item in bill.DetailBills)
                {
                    if (temp < countDetail)
                    {
                        bill.ProductNumbers += ConvertHelper.FormatPhoneNumber(item.ProductNumber) + ", ";
                    }
                    else
                    {
                        bill.ProductNumbers += ConvertHelper.FormatPhoneNumber(item.ProductNumber);
                    }
                    temp++;
                }
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return bill;
        }

        /// <summary>
        /// Cập nhật trạng thái cho hóa đơn
        /// </summary>
        /// <param name="billId"></param>
        /// <param name="status"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResultModel UpdateStatusBill(string billId, string status, string userId)
        {
            bool isPay = false;
            if (status.Equals("DaThanhToan"))
            {
                isPay = true;
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "EXEC UpdateStatusBill @Id,@Status,@IsPay";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",billId),
                new SqlParameter("Status",status),
                new SqlParameter("IsPay",isPay)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                HistoryBillModel historyBill = new HistoryBillModel()
                {
                    Id = Guid.NewGuid().ToString(),
                    BillId = billId,
                    Status = status,
                    CreateTime = DateTime.Now,
                    CreateUserId = userId,
                    IsRead = false
                };
                CreateHistoryBill(historyBill);
                return new ResultModel()
                {
                    Status = true,
                    Message = "Đã đổi trạng thái sang: " + ConvertHelper.ConvertStatus(status)
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        public ResultModel ActiveBill(string code, BillViewModel bill)
        {
            if (!bill.Status.Equals("Đợi xác nhận mail"))
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Đơn hàng này đã xác nhận rồi hoặc đã bị hủy bỏ"
                };
            }
            bool check = CheckCodeBill(code, bill.Code);
            if (!check)
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Mã xác nhận ứng với đơn hàng không chính xác"
                };
            }
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "EXEC UpdateStatusBill @Id,@Status,@IsPay";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",bill.Id),
                new SqlParameter("Status","DoiXacNhan"),
                new SqlParameter("IsPay",false)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                HistoryBillModel historyBill = new HistoryBillModel()
                {
                    Id = Guid.NewGuid().ToString(),
                    BillId = bill.Id,
                    Status = "DoiXacNhan",
                    CreateTime = DateTime.Now,
                    CreateUserId = bill.CreateUserId,
                    IsRead = false
                };
                CreateHistoryBill(historyBill);
                return new ResultModel()
                {
                    Status = true,
                    Message = "Đã đổi trạng thái sang: " + ConvertHelper.ConvertStatus("DoiXacNhan")
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Hủy bỏ hóa đơn
        /// </summary>
        /// <param name="billId"></param>
        /// <param name="userId"></param>
        /// <param name="reasonCanceled"></param>
        /// <returns></returns>
        public ResultModel CanceledBill(string billId, string userId, string reasonCanceled)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "UPDATE Bill SET Status = 'HuyBo',ReasonCanceled = @ReasonCanceled WHERE Id = @Id";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",billId),
                new SqlParameter("ReasonCanceled",reasonCanceled)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                HistoryBillModel historyBill = new HistoryBillModel()
                {
                    Id = Guid.NewGuid().ToString(),
                    BillId = billId,
                    Status = "HuyBo",
                    CreateTime = DateTime.Now,
                    CreateUserId = userId,
                    IsRead = false
                };
                CreateHistoryBill(historyBill);
                List<KeyModel> keys = GetProductsByBillId(billId);
                foreach (var item in keys)
                {
                    UnSellProduct(item.Name);
                }
                return new ResultModel()
                {
                    Status = true,
                    Message = "Đã Hủy Bỏ đơn hàng này"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Get lý do hủy đơn
        /// </summary>
        /// <param name="billId"></param>
        /// <returns></returns>
        public ResultModel GetReasonCanceledOrder(string billId)
        {
            ResultModel result = null;
            sqlQuery = "SELECT ReasonCanceled FROM Bill WHERE Id = @Id";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            sqlCommand.Parameters.Add(new SqlParameter("Id", billId));
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                result = new ResultModel()
                {
                    Status = true,
                    Message = sqlDataReader["ReasonCanceled"].ToString()
                };
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return result;
        }

        /// <summary>
        /// Mở chuyển sang phẩn đã bán sang chưa bán
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        private ResultModel UnSellProduct(string productId)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "UPDATE Product SET IsSell = 0 WHERE Id = @Id";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",productId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                return new ResultModel()
                {
                    Status = true,
                    Message = "Đã mở bán cho sản phẩm"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Tạo mới chi tiết hóa đơn
        /// </summary>
        /// <param name="detailBill"></param>
        /// <returns></returns>
        private ResultModel CreateDetailBill(DetailBillModel detailBill)
        {
            SqlConnectData sqlConnectData = new SqlConnectData();
            sqlQuery = "EXEC CreateDetailBill @Id,@BillId,@ProductId,@Price";
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("Id",detailBill.Id),
                new SqlParameter("BillId",detailBill.BillId),
                new SqlParameter("ProductId",detailBill.ProductId),
                new SqlParameter("Price",detailBill.Price)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);

            int rowAffected;
            try
            {
                rowAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnectData.conn.Close();
                return new ResultModel()
                {
                    Status = false,
                    Message = "Lỗi: " + ex.Message
                };
            }
            sqlConnectData.conn.Close();
            if (rowAffected != 0)
            {
                productData.SellProduct(detailBill.ProductId, true);
                return new ResultModel()
                {
                    Status = true,
                    Message = "Thêm chi tiết đơn hàng thành công"
                };
            }
            else
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Có lỗi. Vui lòng thử lại"
                };
            }
        }

        /// <summary>
        /// Get list chi tiết hóa đơn với id hóa đơn nhập vào
        /// </summary>
        /// <param name="billId"></param>
        /// <returns></returns>
        private List<DetailBillViewModel> GetDetailBillsByBillId(string billId)
        {
            List<DetailBillViewModel> detailBills = new List<DetailBillViewModel>();
            sqlQuery = "SELECT * FROM View_DetailBill WHERE BillId = @BillId";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            sqlCommand.Parameters.Add(new SqlParameter("BillId", billId));
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                DetailBillViewModel detailBill = new DetailBillViewModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    BillId = sqlDataReader["BillId"].ToString(),
                    ProductCarrierId = sqlDataReader["ProductCarrierId"].ToString(),
                    ProductCarrierImage = sqlDataReader["ProductCarrierImage"].ToString(),
                    ProductCarrierName = sqlDataReader["ProductCarrierName"].ToString(),
                    ProductCategoryId = sqlDataReader["ProductCategoryId"].ToString(),
                    ProductCategoryName = sqlDataReader["ProductCategoryName"].ToString(),
                    ProductCreateTime = sqlDataReader["ProductCreateTime"].ToString(),
                    ProductDescription = sqlDataReader["ProductDescription"].ToString(),
                    ProductId = sqlDataReader["ProductId"].ToString(),
                    ProductNumber = sqlDataReader["ProductNumber"].ToString(),
                    ProductPrice = long.Parse(sqlDataReader["ProductPrice"].ToString())
                };
                detailBills.Add(detailBill);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return detailBills;
        }

        /// <summary>
        /// Get list productId của hóa đơn
        /// </summary>
        /// <param name="billId"></param>
        /// <returns></returns>
        private List<KeyModel> GetProductsByBillId(string billId)
        {
            List<KeyModel> keys = new List<KeyModel>();
            sqlQuery = "SELECT ProductId FROM DetailBill WHERE BillId = @BillId";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            sqlCommand.Parameters.Add(new SqlParameter("BillId", billId));
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                KeyModel key = new KeyModel()
                {
                    Name = sqlDataReader["ProductId"].ToString()
                };
                keys.Add(key);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return keys;
        }

        public void HuyDonHangKhongXacNhanMail()
        {
            List<BillModel> bills = GetDonKhongXacNhanMail();
            DateTime dateNow = DateTime.Now;
            foreach (BillModel bill in bills)
            {
                DateTime timeNew = bill.CreateTime.AddMinutes(double.Parse(System.Configuration.ConfigurationManager.AppSettings["TimeBillLimit"]));
                if (timeNew < dateNow)
                {
                    CanceledBill(bill.Id, bill.CreateUserId, "Không xác nhận mail xác thực đơn hàng");
                }
            }
        }

        private List<BillModel> GetDonKhongXacNhanMail()
        {
            List<BillModel> bills = new List<BillModel>();
            sqlQuery = "SELECT Id,CreateTime,CreateUserId FROM Bill WHERE Status = 'DoiXacNhanMail'";
            SqlConnectData sqlConnectData = new SqlConnectData();
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                BillModel bill = new BillModel()
                {
                    Id = sqlDataReader["Id"].ToString(),
                    CreateTime = DateTime.Parse(sqlDataReader["CreateTime"].ToString()),
                    CreateUserId = sqlDataReader["CreateUserId"].ToString()
                };
                bills.Add(bill);
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return bills;
        }

        private bool CheckCodeBill(string code, string billCode)
        {
            if (code.Equals(billCode))
            {
                return true;
            }
            return false;
        }
    }
}