﻿using System.Configuration;
using System.Data.SqlClient;

namespace SimStore_DucPham.Data
{
    public class SqlConnectData
    {
        /// <summary>
        /// Khai báo biến dạng SqlConnection
        /// </summary>
        public SqlConnection conn;

        /// <summary>
        /// Get đường kết nối cho ứng dụng từ webconfig
        /// </summary>
        private readonly string connectString = ConfigurationManager.ConnectionStrings["SimVIP"].ConnectionString;

        /// <summary>
        /// Hàm tạo khởi tạo đường kết nối tự động
        /// </summary>
        public SqlConnectData()
        {
            conn = new SqlConnection(connectString);
            conn.Open();
        }
    }
}