﻿using Newtonsoft.Json;
using SimStore_DucPham.Data;
using SimStore_DucPham.Filter;
using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;
using SimStore_DucPham.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimStore_DucPham.Controllers
{
    //[AuthFilterAdmin]
    //[SessionExpireFilterAdmin]
    //[Authorize(Roles = "Administrator")]
    public class AdminManagerController : Controller
    {
        #region Data

        private RoleData roleData = new RoleData();
        private UserData userData = new UserData();
        private UserRoleData userRoleData = new UserRoleData();
        private CategoryData categoryData = new CategoryData();
        private CarrierData carrierData = new CarrierData();
        private ProductData productData = new ProductData();
        private StatisticsData statisticsData = new StatisticsData();
        private BillData billData = new BillData();

        #endregion Data

        #region Statistics

        // GET: AdminManager
        public ActionResult Index()
        {
            return View();
        }

        #endregion Statistics

        #region Role Manager

        // GET: RoleManager
        public ActionResult Role()
        {
            return View();
        }

        public JsonResult GetAllRole()
        {
            List<RoleModel> roles = roleData.GetAllRole();
            return Json(new { data = roles }, JsonRequestBehavior.AllowGet);
        }

        #endregion Role Manager

        #region User Manager

        // GET: UserManager
        public ActionResult User()
        {
            return View();
        }

        public JsonResult GetAllUser()
        {
            List<UserViewModel> users = userData.GetAllUser();
            return Json(new { data = users }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInfoUser(string userId)
        {
            UserViewModel user = userData.GetInfoUser(userId ?? "");
            return Json(new { data = user }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ChangeUserToAdmin(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return Json(new { data = new ResultModel() { Status = false, Message = "UserId không được rỗng" } });
            }
            return Json(new { data = userRoleData.ChangeUserToAdmin(userId) });
        }

        [HttpPost]
        public JsonResult ChangeUserToCustomer(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return Json(new { data = new ResultModel() { Status = false, Message = "UserId không được rỗng" } });
            }
            ApplicationUser user = (ApplicationUser)Session[KeyHelper.Session_Login];
            if (user.Id.Equals(userId))
            {
                return Json(new { data = new ResultModel() { Status = false, Message = "Bạn đang dùng tài khoản này. Không thể thực hiện yêu cầu này." } });
            }
            return Json(new { data = userRoleData.ChangeUserToCustomer(userId) });
        }

        [HttpPost]
        public JsonResult LockUser(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return Json(new { data = new ResultModel() { Status = false, Message = "UserId không được rỗng" } });
            }
            ApplicationUser user = (ApplicationUser)Session[KeyHelper.Session_Login];
            if (user.Id.Equals(userId))
            {
                return Json(new { data = new ResultModel() { Status = false, Message = "Bạn đang dùng tài khoản này. Không thể thực hiện yêu cầu này." } });
            }
            return Json(new { data = userData.LockUser(userId) });
        }

        [HttpPost]
        public JsonResult UnLockUser(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return Json(new { data = new ResultModel() { Status = false, Message = "UserId không được rỗng" } });
            }
            return Json(new { data = userData.UnLockUser(userId) });
        }

        [HttpPost]
        public JsonResult UpdateProfile(string userId, string fullName, string email, string phoneNumber, string address)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return Json(new { data = new ResultModel() { Status = false, Message = "UserId không được rỗng" } });
            }
            ResultModel result = userData.UpdateProfile(userId, fullName ?? "", email ?? "", phoneNumber ?? "", address ?? "");
            return Json(new { data = result });
        }

        #endregion User Manager

        #region Category Manager

        public ActionResult Category()
        {
            return View();
        }

        public JsonResult GetAllCategory()
        {
            List<CategoryModel> categories = categoryData.GetAllCategory();
            return Json(new { data = categories }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInfoCategory(string categoryId)
        {
            CategoryModel category = categoryData.GetInfoCategory(categoryId ?? "");
            return Json(new { data = category }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsActiveCategory(string categoryId, bool isActive)
        {
            ResultModel result = categoryData.IsActiveCategory(categoryId, isActive);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult DeleteCategory(string categoryId)
        {
            ResultModel result = categoryData.DeleteCategory(categoryId);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult UpdateCategory(string categoryId, string categoryName, string categoryDescription)
        {
            ResultModel result = categoryData.UpdateCategory(categoryId, categoryName, categoryDescription);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult CreateCategory(string categoryStr)
        {
            CategoryModel category = JsonConvert.DeserializeObject<CategoryModel>(categoryStr);
            category.Id = category.Name.ToLower().Replace(' ', '-');
            category.Description = category.Description ?? "";
            ResultModel result = categoryData.CreateCategory(category);
            return Json(new { data = result });
        }

        #endregion Category Manager

        #region Carrier Manager

        public ActionResult Carrier()
        {
            return View();
        }

        public JsonResult GetAllCarrier()
        {
            List<CarrierModel> carriers = carrierData.GetAllCarrier();
            return Json(new { data = carriers }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInfoCarrier(string carrierId)
        {
            CarrierModel carrier = carrierData.GetInfoCarrier(carrierId);
            return Json(new { data = carrier }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsActiveCarrier(string carrierId, bool isActive)
        {
            ResultModel result = carrierData.IsActiveCarrier(carrierId, isActive);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult DeleteCarrier(string carrierId)
        {
            ResultModel result = carrierData.DeleteCarrier(carrierId);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult UpdateCarrier(string carrierId, string carrierName, string carrierDescription, string carrierImage)
        {
            ResultModel result = carrierData.UpdateCarrier(carrierId, carrierName, carrierDescription, carrierImage);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult CreateCarrier(string carrierStr)
        {
            CarrierModel carrier = JsonConvert.DeserializeObject<CarrierModel>(carrierStr);
            carrier.Id = carrier.Name.ToLower().Replace(' ', '-');
            carrier.Description = carrier.Description ?? "";
            ResultModel result = carrierData.CreateCarrier(carrier);
            return Json(new { data = result });
        }

        #endregion Carrier Manager

        #region Product Manager

        public ActionResult Product()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetAllProduct(string status, string carrier, string price, string sapxep)
        {
            List<ProductViewModel> products = productData.GetAllProduct().OrderByDescending(p => p.CreateTime).ToList();
            if (!string.IsNullOrEmpty(status))
            {
                if (!status.Equals("all"))
                {
                    if (status.Equals("sell"))
                    {
                        products = products.Where(p => p.IsSell).ToList();
                    }
                    else if (status.Equals("nosell"))
                    {
                        products = products.Where(p => !p.IsSell).ToList();
                    }
                    else if (status.Equals("active"))
                    {
                        products = products.Where(p => p.IsActive).ToList();
                    }
                    else
                    {
                        products = products.Where(p => !p.IsActive).ToList();
                    }
                }
            }
            if (!string.IsNullOrEmpty(carrier))
            {
                if (!carrier.Equals("all"))
                {
                    products = products.Where(p => p.CarrierId.Equals(carrier)).ToList();
                }
            }
            if (!string.IsNullOrEmpty(price))
            {
                if (!price.Equals("all"))
                {
                    products = products.Where(p => p.Price >= long.Parse(price.Split('-')[0]) * 1000 && p.Price <= long.Parse(price.Split('-')[1]) * 1000).ToList();
                }
                else
                {
                }
            }
            if (!string.IsNullOrEmpty(sapxep))
            {
                if (sapxep.Equals("price-az"))
                {
                    products = products.OrderBy(p => p.Price).ToList();
                }
                else if (sapxep.Equals("price-za"))
                {
                    products = products.OrderByDescending(p => p.Price).ToList();
                }
                else if (sapxep.Equals("random"))
                {
                    products = products.OrderBy(p => Guid.NewGuid()).ToList();
                }
                else
                {
                    products = products.OrderByDescending(p => p.CreateTime).ToList();
                }
            }

            //Server Slide Parameter
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]);
            //string searchValue = Request["search[value]"];
            int page = start / 10;

            //string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            //string sortDirection = Request["order[0][dir]"];

            int recordsTotal = products.Count();
            int recordsFiltered = products.Count();

            //products = products.OrderBy(sortColumnName + " " + sortDirection).ToList();

            //pagin
            products = products.Skip(start).Take(length).ToList();
            var json = Json(new { data = products, page, draw = Request["draw"], recordsTotal, recordsFiltered });
            json.MaxJsonLength = int.MaxValue;
            return json;
        }

        public JsonResult GetInfoProduct(string productId)
        {
            ProductViewModel product = productData.GetInfoProduct(productId);
            return Json(new { data = product }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CreateProduct(string jsonProduct)
        {
            ProductModel product = JsonConvert.DeserializeObject<ProductModel>(jsonProduct);
            ApplicationUser User = (ApplicationUser)Session[KeyHelper.Session_Login];

            product.Id = Guid.NewGuid().ToString();
            product.CreateTime = DateTime.Now;
            product.CreateUserId = User.Id;
            product.IsSell = false;
            product.Sale = 0;
            if (string.IsNullOrEmpty(product.CategoryId))
            {
                product.CategoryId = KeyHelper.DefaultCategoryId;
            }

            ResultModel result = productData.CreateProduct(product);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult UpdateProduct(string jsonProduct)
        {
            ProductModel product = JsonConvert.DeserializeObject<ProductModel>(jsonProduct);
            ResultModel result = productData.UpdateProduct(product);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult IsActiveProduct(string productId, bool isActive)
        {
            ResultModel result = productData.IsActiveProduct(productId, isActive);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult DeleteProduct(string productId)
        {
            ResultModel result = productData.DeleteProduct(productId);
            return Json(new { data = result });
        }

        public JsonResult GetAllCarrierSelect2()
        {
            List<Select2Model> select2s = ConvertHelper.CarrierToSelect2(carrierData.GetAllCarrier());
            return Json(new { items = select2s }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllCategorySelect2()
        {
            List<Select2Model> select2s = ConvertHelper.CategoryToSelect2(categoryData.GetAllCategory());
            return Json(new { items = select2s }, JsonRequestBehavior.AllowGet);
        }

        #endregion Product Manager

        #region Bill Manager

        public ActionResult Bill()
        {
            return View();
        }

        public JsonResult GetAllBill(string status, string isonline, string ispay, string date)
        {
            List<BillViewModel> bills = billData.GetAllBill();
            if (!string.IsNullOrEmpty(status))
            {
                if (!status.Equals("all"))
                {
                    bills = bills.Where(p => p.Status.Equals(status)).ToList();
                }
            }
            if (!string.IsNullOrEmpty(isonline))
            {
                if (isonline.Equals("online"))
                {
                    bills = bills.Where(p => p.IsOnline).ToList();
                }
                if (isonline.Equals("offline"))
                {
                    bills = bills.Where(p => !p.IsOnline).ToList();
                }
            }
            if (!string.IsNullOrEmpty(ispay))
            {
                if (ispay.Equals("dathanhtoan"))
                {
                    bills = bills.Where(p => p.IsPay).ToList();
                }
                if (ispay.Equals("chuathanhtoan"))
                {
                    bills = bills.Where(p => !p.IsPay).ToList();
                }
            }
            if (!string.IsNullOrEmpty(date))
            {
                DateTime startTime = DateTime.Parse(date.Split('_')[0]);
                DateTime endTime = DateTime.Parse(date.Split('_')[1]);
                if (startTime == endTime)
                {
                    bills = bills.Where(p => p.CreateDateTime.ToString("ddMMYYYY").Equals(startTime.ToString("ddMMYYYY"))).ToList();
                }
                else
                {
                    bills = bills.Where(p => p.CreateDateTime >= startTime && p.CreateDateTime < endTime.AddDays(1)).ToList();
                }
            }
            var json = Json(new { data = bills }, JsonRequestBehavior.AllowGet);
            json.MaxJsonLength = int.MaxValue;
            return json;
        }

        public JsonResult GetInfoBill(string billId)
        {
            BillViewModel bill = billData.GetInfoBill(billId);
            return Json(new { data = bill }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetReasonCanceledOrder(string billId)
        {
            ResultModel result = billData.GetReasonCanceledOrder(billId);
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateStatusBill(string billId, string status)
        {
            ApplicationUser User = (ApplicationUser)Session[KeyHelper.Session_Login];
            ResultModel result = billData.UpdateStatusBill(billId, status, User.Id);
            return Json(new { data = result });
        }

        [HttpPost]
        public JsonResult CanceledBill(string billId, string reasonCanceled)
        {
            ApplicationUser User = (ApplicationUser)Session[KeyHelper.Session_Login];
            ResultModel result = billData.CanceledBill(billId, User.Id, reasonCanceled);
            return Json(new { data = result });
        }

        #endregion Bill Manager

        #region ChildActionOnly

        [ChildActionOnly]
        public ActionResult DefaultStatistics()
        {
            return PartialView(statisticsData.GetHomeAdmin());
        }

        #endregion ChildActionOnly
    }
}