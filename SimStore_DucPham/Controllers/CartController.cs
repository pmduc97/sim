﻿using SimStore_DucPham.Data;
using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;
using SimStore_DucPham.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimStore_DucPham.Controllers
{
    public class CartController : Controller
    {
        private ProductData productData = new ProductData();
        private BillData billData = new BillData();
        private AddressOrderData addressOrderData = new AddressOrderData();

        // GET: Cart
        public ActionResult Index()
        {
            ViewBag.Title = "Tiến hành đặt hàng";

            if (Session[KeyHelper.Session_Cart] == null)
            {
                return View(new List<ProductViewModel>());
            }
            List<ProductViewModel> carts = (List<ProductViewModel>)Session[KeyHelper.Session_Cart];
            return View(carts);
        }

        public ActionResult Order()
        {
            if (Session[KeyHelper.Session_Cart] == null)
            {
                return RedirectToAction("Index");
            }
            ViewBag.Title = "Tiến hành đặt hàng";
            return View();
        }

        [HttpPost]
        public ActionResult FastOrder(string fullName, string phoneNumber, string address, string note)
        {
            if (string.IsNullOrEmpty(fullName) || string.IsNullOrEmpty(phoneNumber) || string.IsNullOrEmpty(address))
            {
                TempData["error"] = "Dữ liệu không đầy đủ";
                return RedirectToAction("Index");
            }
            if (Session[KeyHelper.Session_Cart] == null)
            {
                TempData["error"] = "Giỏ hàng bạn đang rỗng";
                return RedirectToAction("Index");
            }
            List<ProductViewModel> products = (List<ProductViewModel>)Session[KeyHelper.Session_Cart];
            if (products.Count == 0)
            {
                TempData["error"] = "Giỏ hàng bạn đang rỗng";
                return RedirectToAction("Index");
            }
            AddressOrderDefaultModel addressOrder = new AddressOrderDefaultModel()
            {
                Id = Guid.NewGuid().ToString(),
                UserId = KeyHelper.DefaultUserId,
                FullName = fullName,
                PhoneNumber = phoneNumber,
                Address = address
            };
            BillCreateModel billCreate = new BillCreateModel()
            {
                Id = Guid.NewGuid().ToString(),
                CreateTime = DateTime.Now,
                CreateUserId = KeyHelper.DefaultUserId,
                Status = "DoiXacNhan",
                SumMoney = products.Sum(p => p.Price),
                Note = note ?? "",
                AddressOrderId = addressOrder.Id,
                IsOnline = true
            };
            ResultModel result = addressOrderData.CreateOrderOnlineDefault(addressOrder);
            if (result.Status)
            {
                result = billData.CreateBill(billCreate, products);
                if (result.Status)
                {
                    OrderOkModel orderOk = new OrderOkModel()
                    {
                        Status = true,
                        FullName = fullName,
                        BillId = billCreate.Id,
                        Address = address,
                        PhoneNumber = phoneNumber,
                        IsOnline = true,
                        Products = products
                    };
                    Session.Remove(KeyHelper.Session_Cart);
                    TempData["OK"] = orderOk;
                    return RedirectToAction("OrderOK");
                }
                else
                {
                    OrderOkModel orderOk = new OrderOkModel()
                    {
                        Status = false
                    };
                    TempData["Fail"] = orderOk;
                    return RedirectToAction("OrderFail");
                }
            }
            else
            {
                OrderOkModel orderOk = new OrderOkModel()
                {
                    Status = false
                };
                TempData["Fail"] = orderOk;
                return RedirectToAction("OrderFail");
            }
        }

        public ActionResult OrderOK()
        {
            ViewBag.Title = "Đặt hàng thành công";
            OrderOkModel orderOk = (OrderOkModel)TempData["OK"];
            if (orderOk == null)
            {
                return RedirectToAction("Index");
            }
            return View(orderOk);
        }

        public ActionResult OrderFail()
        {
            OrderOkModel orderOk = (OrderOkModel)TempData["Fail"];
            if (orderOk == null)
            {
                return RedirectToAction("Index");
            }
            return View(orderOk);
        }

        [ChildActionOnly]
        public ActionResult SumProductCart()
        {
            int sum = 0;
            if (Session[KeyHelper.Session_Cart] != null)
            {
                List<ProductViewModel> carts = (List<ProductViewModel>)Session[KeyHelper.Session_Cart];
                sum = carts.Count;
            }
            return PartialView(sum);
        }

        [ChildActionOnly]
        public ActionResult GetCartToPartial()
        {
            List<ProductViewModel> carts = (List<ProductViewModel>)Session[KeyHelper.Session_Cart];
            return PartialView(carts);
        }

        public JsonResult CheckEmptyCart()
        {
            if (Session[KeyHelper.Session_Cart] == null)
            {
                return Json(new { data = false }, JsonRequestBehavior.AllowGet);
            }
            List<ProductViewModel> carts = (List<ProductViewModel>)Session[KeyHelper.Session_Cart];
            if (carts.Count == 0)
            {
                return Json(new { data = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { data = true }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDetailCart()
        {
            if (Session[KeyHelper.Session_Cart] == null)
            {
                return Json(new { data = new List<ProductViewModel>() }, JsonRequestBehavior.AllowGet);
            }
            List<ProductViewModel> carts = (List<ProductViewModel>)Session[KeyHelper.Session_Cart];
            return Json(new { data = carts }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult StartBuyProduct(string productId)
        {
            if (string.IsNullOrEmpty(productId))
            {
                return Json(new
                {
                    data = new ResultModel()
                    {
                        Status = false,
                        Message = "Sim ID không hợp lệ"
                    }
                });
            }
            ProductViewModel product = productData.GetInfoProduct(productId);
            if (product == null)
            {
                return Json(new
                {
                    data = new ResultModel()
                    {
                        Status = false,
                        Message = "Không tìm thấy Sim cần thêm"
                    }
                });
            }
            if (!product.IsActive)
            {
                return Json(new
                {
                    data = new ResultModel()
                    {
                        Status = false,
                        Message = "Sim này đã hết hoạt động mua bán"
                    }
                });
            }
            if (product.IsSell)
            {
                return Json(new
                {
                    data = new ResultModel()
                    {
                        Status = false,
                        Message = "Sim này đã được người khác đặt mua rồi"
                    }
                });
            }
            if (Session[KeyHelper.Session_Cart] == null)
            {
                List<ProductViewModel> tCarts = new List<ProductViewModel>
                {
                    product
                };
                Session[KeyHelper.Session_Cart] = tCarts;
                return Json(new
                {
                    data = new ResultModel()
                    {
                        Status = true,
                        Message = "Thêm Sim vào giỏ hàng thành công"
                    }
                });
            }
            List<ProductViewModel> carts = (List<ProductViewModel>)Session[KeyHelper.Session_Cart];
            if (carts.Count == 0)
            {
                carts.Add(product);
                Session[KeyHelper.Session_Cart] = carts;
                return Json(new
                {
                    data = new ResultModel()
                    {
                        Status = true,
                        Message = "Thêm Sim vào giỏ hàng thành công"
                    }
                });
            }
            List<ProductViewModel> pCarts = carts.Where(p => p.Id.Equals(productId)).ToList();
            if (pCarts.Count != 0)
            {
                return Json(new
                {
                    data = new ResultModel()
                    {
                        Status = true,
                        Message = "Sim này đang nằm trong giỏ hàng của bạn rồi"
                    }
                });
            }
            carts.Add(product);
            Session[KeyHelper.Session_Cart] = carts;
            return Json(new
            {
                data = new ResultModel()
                {
                    Status = true,
                    Message = "Thêm Sim vào giỏ hàng thành công"
                }
            });
        }

        [HttpPost]
        public JsonResult RemoveCartItem(string productId)
        {
            if (string.IsNullOrEmpty(productId))
            {
                return Json(new
                {
                    data = new ResultModel()
                    {
                        Status = false,
                        Message = "Sai dữ liệu"
                    }
                });
            }
            List<ProductViewModel> carts = (List<ProductViewModel>)Session[KeyHelper.Session_Cart];
            List<ProductViewModel> tCarts = carts.Where(p => p.Id.Equals(productId)).ToList();
            if (tCarts.Count == 0)
            {
                return Json(new
                {
                    data = new ResultModel()
                    {
                        Status = false,
                        Message = "Không có mặt hàng này trong giỏ"
                    }
                });
            }
            ProductViewModel product = tCarts.First();
            carts.Remove(product);
            Session[KeyHelper.Session_Cart] = carts;
            return Json(new
            {
                data = new CartResultModel()
                {
                    Status = true,
                    Message = "Xóa mặt hàng khỏi giỏ thành công",
                    Sum = carts.Count
                }
            });
        }

        [HttpPost]
        public JsonResult AddProductToCart(string productId)
        {
            if (string.IsNullOrEmpty(productId))
            {
                return Json(new
                {
                    data = new ResultModel()
                    {
                        Status = false,
                        Message = "Sim ID không hợp lệ"
                    }
                });
            }
            ProductViewModel product = productData.GetInfoProduct(productId);
            if (product == null)
            {
                return Json(new
                {
                    data = new ResultModel()
                    {
                        Status = false,
                        Message = "Không tìm thấy Sim cần thêm"
                    }
                });
            }
            if (!product.IsActive)
            {
                return Json(new
                {
                    data = new ResultModel()
                    {
                        Status = false,
                        Message = "Sim này đã hết hoạt động mua bán"
                    }
                });
            }
            if (product.IsSell)
            {
                return Json(new
                {
                    data = new ResultModel()
                    {
                        Status = false,
                        Message = "Sim này đã được người khác đặt mua rồi"
                    }
                });
            }
            if (Session[KeyHelper.Session_Cart] == null)
            {
                List<ProductViewModel> tCarts = new List<ProductViewModel>
                {
                    product
                };
                Session[KeyHelper.Session_Cart] = tCarts;
                return Json(new
                {
                    data = new CartResultModel()
                    {
                        Status = true,
                        Message = "Thêm Sim vào giỏ hàng thành công",
                        Sum = 1,
                        Product = product
                    }
                });
            }
            List<ProductViewModel> carts = (List<ProductViewModel>)Session[KeyHelper.Session_Cart];
            if (carts.Count == 0)
            {
                carts.Add(product);
                Session[KeyHelper.Session_Cart] = carts;
                return Json(new
                {
                    data = new CartResultModel()
                    {
                        Status = true,
                        Message = "Thêm Sim vào giỏ hàng thành công",
                        Sum = 1,
                        Product = product
                    }
                });
            }
            List<ProductViewModel> pCarts = carts.Where(p => p.Id.Equals(productId)).ToList();
            if (pCarts.Count != 0)
            {
                return Json(new
                {
                    data = new ResultModel()
                    {
                        Status = false,
                        Message = "Sim này đang nằm trong giỏ hàng của bạn rồi"
                    }
                });
            }
            carts.Add(product);
            Session[KeyHelper.Session_Cart] = carts;
            return Json(new
            {
                data = new CartResultModel()
                {
                    Status = true,
                    Message = "Thêm Sim vào giỏ hàng thành công",
                    Sum = carts.Count,
                    Product = product
                }
            });
        }
    }
}