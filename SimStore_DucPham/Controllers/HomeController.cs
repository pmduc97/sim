﻿using SimStore_DucPham.Data;
using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;
using SimStore_DucPham.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace SimStore_DucPham.Controllers
{
    public class HomeController : Controller
    {
        #region Data

        private ProductData productData = new ProductData();
        private CarrierData carrierData = new CarrierData();
        private CategoryData categoryData = new CategoryData();
        private BillData billData = new BillData();

        #endregion Data

        #region Data Home Index

        public ActionResult Index()
        {
            List<ProductViewModel> productsNew = productData.Get12ProductNewToHome();
            List<ProductViewModel> productsNormal = productData.Get12ProductNormal();
            HomeIndexModel homeIndex = new HomeIndexModel()
            {
                ProductsNew = productsNew,
                ProductsNormal = productsNormal
            };
            return View(homeIndex);
        }

        public JsonResult Get12ProductRandom()
        {
            List<ProductViewModel> products = productData.Get12ProductNormal();
            return Json(new { data = products }, JsonRequestBehavior.AllowGet);
        }

        #endregion Data Home Index

        #region ChildAction Layout

        [ChildActionOnly]
        public ActionResult _HomeSliderLeft()
        {
            List<CarrierModel> carriers = carrierData.GetAllCarrierActive();
            List<CategoryModel> categories = categoryData.GetAllCategoryActive();
            List<PriceProductViewModel> priceProducts = productData.GetPriceCountProduct();
            SliderLeftModel sliderLeft = new SliderLeftModel()
            {
                Carriers = carriers,
                Categories = categories,
                PriceProducts = priceProducts
            };
            return PartialView(sliderLeft);
        }

        [ChildActionOnly]
        public ActionResult _HomeSliderOrder()
        {
            List<SlideOrderViewModel> slides = billData.Get10OrderNew();
            return PartialView(slides);
        }

        #endregion ChildAction Layout

        #region PagedListTest

        //public ActionResult ProductByCarrier(string carrier, int? page)
        //{
        //    if (string.IsNullOrEmpty(carrier))
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    int pageSize = 2;
        //    int pageNumber = page ?? 1;
        //    List<ProductViewModel> products = productData.GetProductActiveByCarrier(carrier);
        //    ViewBag.Title = "Sim theo mạng " + carrierData.GetInfoCarrier(carrier).Name;
        //    ViewBag.Id = carrier;
        //    return View(products.ToPagedList(pageNumber, pageSize));
        //}

        //public PartialViewResult GetPageProductByCarrier(string carrier, int? page)
        //{
        //    int pageSize = 2;
        //    int pageNumber = page ?? 1;
        //    List<ProductViewModel> products = productData.GetProductActiveByCarrier(carrier);
        //    return PartialView("_PartialProductByCarrier", products.ToPagedList(pageNumber, pageSize));
        //}

        #endregion PagedListTest

        #region Data Product

        public ActionResult ProductByCarrier(string carrier)
        {
            if (string.IsNullOrEmpty(carrier))
            {
                return RedirectToAction("Index");
            }
            ViewBag.Title = "Sim theo mạng " + carrierData.GetInfoCarrier(carrier).Name;
            ViewBag.Id = "carrier_" + carrier;
            return View();
        }

        public ActionResult ProductByCategory(string category)
        {
            if (string.IsNullOrEmpty(category))
            {
                return RedirectToAction("Index");
            }
            ViewBag.Title = "Sim theo danh mục " + categoryData.GetInfoCategory(category).Name;
            ViewBag.Id = "category_" + category;
            return View();
        }

        public ActionResult ProductByPrice(string price)
        {
            if (string.IsNullOrEmpty(price))
            {
                return RedirectToAction("Index");
            }
            ViewBag.Title = ConvertHelper.FormatNamePriceProduct(price);
            ViewBag.Id = "price_" + price;
            return View();
        }

        public ActionResult AllProduct()
        {
            ViewBag.Title = "Tất cả sim của cửa hàng";
            ViewBag.Id = "all_all";
            return View();
        }

        public ActionResult TimSim(string number)
        {
            if (string.IsNullOrEmpty(number))
            {
                return RedirectToAction("Index");
            }
            KeyModel key = ConvertHelper.FormatNameNumberFind(number);
            ViewBag.Title = key.Name;
            string idNumber = "number_" + number.Replace('*', '%');
            if (key.Id == 1)
            {
                idNumber = "number_%" + number + "%";
            }
            ViewBag.Id = idNumber;
            return View();
        }

        public ActionResult ChonSim(string number)
        {
            if (string.IsNullOrEmpty(number))
            {
                return RedirectToAction("Index");
            }
            ProductViewModel product = productData.GetInfoProductByNumber(number);
            ViewBag.Title = "Sim " + ConvertHelper.FormatPhoneNumber(number);
            return View(product);
        }

        [HttpPost]
        public JsonResult GetAllProductActive(string carrier, string price, string sapxep)
        {
            List<ProductViewModel> products = productData.GetAllProductActive();
            if (!string.IsNullOrEmpty(carrier))
            {
                if (!carrier.Equals("all"))
                {
                    products = products.Where(p => p.CarrierId.Equals(carrier)).ToList();
                }
                else
                {
                }
            }
            if (!string.IsNullOrEmpty(price))
            {
                if (!price.Equals("all"))
                {
                    products = products.Where(p => p.Price >= long.Parse(price.Split('-')[0]) * 1000 && p.Price <= long.Parse(price.Split('-')[1]) * 1000).ToList();
                }
                else
                {
                }
            }
            if (!string.IsNullOrEmpty(sapxep))
            {
                if (sapxep.Equals("price-az"))
                {
                    products = products.OrderBy(p => p.Price).ToList();
                }
                else if (sapxep.Equals("price-za"))
                {
                    products = products.OrderByDescending(p => p.Price).ToList();
                }
                else if (sapxep.Equals("random"))
                {
                    products = products.OrderBy(p => Guid.NewGuid()).ToList();
                }
                else
                {
                }
            }

            //Server Slide Parameter
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]);
            //string searchValue = Request["search[value]"];

            //string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            //string sortDirection = Request["order[0][dir]"];

            int recordsTotal = products.Count();
            int recordsFiltered = products.Count();

            //products = products.OrderBy(sortColumnName + " " + sortDirection).ToList();

            //pagin
            products = products.Skip(start).Take(length).ToList();
            var json = Json(new { data = products, draw = Request["draw"], recordsTotal, recordsFiltered });
            json.MaxJsonLength = int.MaxValue;
            return json;
        }

        [HttpPost]
        public JsonResult GetProductActiveByCarrier(string carrierId, string price, string sapxep)
        {
            List<ProductViewModel> products = productData.GetProductActiveByCarrier(carrierId);
            if (!string.IsNullOrEmpty(price))
            {
                if (!price.Equals("all"))
                {
                    products = products.Where(p => p.Price >= long.Parse(price.Split('-')[0]) * 1000 && p.Price <= long.Parse(price.Split('-')[1]) * 1000).ToList();
                }
                else
                {
                }
            }
            if (!string.IsNullOrEmpty(sapxep))
            {
                if (sapxep.Equals("price-az"))
                {
                    products = products.OrderBy(p => p.Price).ToList();
                }
                else if (sapxep.Equals("price-za"))
                {
                    products = products.OrderByDescending(p => p.Price).ToList();
                }
                else if (sapxep.Equals("random"))
                {
                    products = products.OrderBy(p => Guid.NewGuid()).ToList();
                }
                else
                {
                }
            }
            //Server Slide Parameter
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]);
            //string searchValue = Request["search[value]"];

            //string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            //string sortDirection = Request["order[0][dir]"];

            int recordsTotal = products.Count();
            int recordsFiltered = products.Count();

            //products = products.OrderBy(sortColumnName + " " + sortDirection).ToList();

            //pagin
            products = products.Skip(start).Take(length).ToList();
            var json = Json(new { data = products, draw = Request["draw"], recordsTotal, recordsFiltered });
            json.MaxJsonLength = int.MaxValue;
            return json;
        }

        [HttpPost]
        public JsonResult GetProductActiveByCategory(string categoryId, string carrier, string price, string sapxep)
        {
            List<ProductViewModel> products = productData.GetProductActiveByCategory(categoryId);
            if (!string.IsNullOrEmpty(carrier))
            {
                if (!carrier.Equals("all"))
                {
                    products = products.Where(p => p.CarrierId.Equals(carrier)).ToList();
                }
                else
                {
                }
            }
            if (!string.IsNullOrEmpty(price))
            {
                if (!price.Equals("all"))
                {
                    products = products.Where(p => p.Price >= long.Parse(price.Split('-')[0]) * 1000 && p.Price <= long.Parse(price.Split('-')[1]) * 1000).ToList();
                }
                else
                {
                }
            }
            if (!string.IsNullOrEmpty(sapxep))
            {
                if (sapxep.Equals("price-az"))
                {
                    products = products.OrderBy(p => p.Price).ToList();
                }
                else if (sapxep.Equals("price-za"))
                {
                    products = products.OrderByDescending(p => p.Price).ToList();
                }
                else if (sapxep.Equals("random"))
                {
                    products = products.OrderBy(p => Guid.NewGuid()).ToList();
                }
                else
                {
                }
            }
            //Server Slide Parameter
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]);
            //string searchValue = Request["search[value]"];

            //string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            //string sortDirection = Request["order[0][dir]"];

            int recordsTotal = products.Count();
            int recordsFiltered = products.Count();

            //products = products.OrderBy(sortColumnName + " " + sortDirection).ToList();

            //pagin
            products = products.Skip(start).Take(length).ToList();
            var json = Json(new { data = products, draw = Request["draw"], recordsTotal, recordsFiltered });
            json.MaxJsonLength = int.MaxValue;
            return json;
        }

        [HttpPost]
        public JsonResult GetProductActiveByPrice(string carrier, string price, string sapxep)
        {
            List<ProductViewModel> products = productData.GetProductActiveByPrice(price);
            if (!string.IsNullOrEmpty(carrier))
            {
                if (!carrier.Equals("all"))
                {
                    products = products.Where(p => p.CarrierId.Equals(carrier)).ToList();
                }
                else
                {
                }
            }
            if (!string.IsNullOrEmpty(sapxep))
            {
                if (sapxep.Equals("price-az"))
                {
                    products = products.OrderBy(p => p.Price).ToList();
                }
                else if (sapxep.Equals("price-za"))
                {
                    products = products.OrderByDescending(p => p.Price).ToList();
                }
                else if (sapxep.Equals("random"))
                {
                    products = products.OrderBy(p => Guid.NewGuid()).ToList();
                }
                else
                {
                }
            }
            //Server Slide Parameter
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]);
            //string searchValue = Request["search[value]"];

            //string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            //string sortDirection = Request["order[0][dir]"];

            int recordsTotal = products.Count();
            int recordsFiltered = products.Count();

            //products = products.OrderBy(sortColumnName + " " + sortDirection).ToList();

            //pagin
            products = products.Skip(start).Take(length).ToList();
            var json = Json(new { data = products, draw = Request["draw"], recordsTotal, recordsFiltered });
            json.MaxJsonLength = int.MaxValue;
            return json;
        }

        [HttpPost]
        public JsonResult GetProductActiveByNumber(string number, string carrier, string price, string sapxep)
        {
            List<ProductViewModel> products = productData.GetProductActiveByNumber(number);
            if (!string.IsNullOrEmpty(carrier))
            {
                if (!carrier.Equals("all"))
                {
                    products = products.Where(p => p.CarrierId.Equals(carrier)).ToList();
                }
                else
                {
                }
            }
            if (!string.IsNullOrEmpty(price))
            {
                if (!price.Equals("all"))
                {
                    products = products.Where(p => p.Price >= long.Parse(price.Split('-')[0]) * 1000 && p.Price <= long.Parse(price.Split('-')[1]) * 1000).ToList();
                }
                else
                {
                }
            }
            if (!string.IsNullOrEmpty(sapxep))
            {
                if (sapxep.Equals("price-az"))
                {
                    products = products.OrderBy(p => p.Price).ToList();
                }
                else if (sapxep.Equals("price-za"))
                {
                    products = products.OrderByDescending(p => p.Price).ToList();
                }
                else if (sapxep.Equals("random"))
                {
                    products = products.OrderBy(p => Guid.NewGuid()).ToList();
                }
                else
                {
                }
            }
            //Server Slide Parameter
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]);
            //string searchValue = Request["search[value]"];

            //string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            //string sortDirection = Request["order[0][dir]"];

            int recordsTotal = products.Count();
            int recordsFiltered = products.Count();

            //products = products.OrderBy(sortColumnName + " " + sortDirection).ToList();

            //pagin
            products = products.Skip(start).Take(length).ToList();
            var json = Json(new { data = products, draw = Request["draw"], recordsTotal, recordsFiltered });
            json.MaxJsonLength = int.MaxValue;
            return json;
        }

        #endregion Data Product

        public ActionResult ActiveBill(string code, string billId)
        {
            ActiveBillModel model = new ActiveBillModel();

            var bill = billData.GetInfoBill(billId);
            if (bill == null)
            {
                model.Status = false;
                model.Message = "Không tìm thấy đơn hàng cần xác nhận";
                return View(model);
            }
            var result = billData.ActiveBill(code, bill);
            model.Status = result.Status;
            model.Message = result.Message;
            model.Bill = bill;
            return View(model);
        }
    }
}