﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;

namespace SimStore_DucPham.Controllers
{
    public class PhongThuyController : Controller
    {
        // GET: PhongThuy
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TraPhongThuy(string ngaysinh, string thangsinh, string namsinh, string gioitinh)
        {
            if (string.IsNullOrEmpty(ngaysinh) || string.IsNullOrEmpty(thangsinh) || string.IsNullOrEmpty(namsinh) || string.IsNullOrEmpty(gioitinh))
            {
                return Redirect("/sim-phong-thuy");
            }
            DateModel date = new DateModel()
            {
                Day = int.Parse(ngaysinh),
                Month = int.Parse(thangsinh),
                Year = int.Parse(namsinh),
                GioiTinh = gioitinh
            };
            string _gioiTinh = gioitinh.Equals("nam") ? "Nam" : "Nữ";
            DateViewModel dateView = CalendarHelper.GetInfoDateByDuongLich(date, 7);
            ViewBag.Title = "Sim phong thuỷ " + _gioiTinh + " " + ngaysinh + "/" + thangsinh + "/" + namsinh;
            return View(dateView);
        }
    }
}