﻿using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;
using System.Web.Mvc;

namespace SimStore_DucPham.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Error404()
        {
            var statusCode = (int)System.Net.HttpStatusCode.NotFound;
            Response.StatusCode = statusCode;
            Response.TrySkipIisCustomErrors = true;
            HttpContext.Response.StatusCode = statusCode;
            HttpContext.Response.TrySkipIisCustomErrors = true;
            return View();
        }

        public ActionResult Error500()
        {
            Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
            Response.TrySkipIisCustomErrors = true;
            return View();
        }

        public JsonResult GetDateInfo(int dd, int mm, int yy, string gioitinh)
        {
            DateModel dateDuong = new DateModel()
            {
                Day = dd,
                Month = mm,
                Year = yy,
                GioiTinh = gioitinh
            };
            DateViewModel dateView = CalendarHelper.GetInfoDateByDuongLich(dateDuong, 7);
            return Json(new { data = dateView }, JsonRequestBehavior.AllowGet);
        }
    }
}