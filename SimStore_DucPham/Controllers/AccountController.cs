﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SimStore_DucPham.Data;
using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;

namespace SimStore_DucPham.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private UserRoleData userRoleData = new UserRoleData();
        private UserData userData = new UserData();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: /Account/AdminLogin
        [AllowAnonymous]
        public ActionResult AdminLogin()
        {
            string msg = (string)TempData["msg"];
            if (msg != null)
            {
                ModelState.AddModelError("", msg);
                return View();
            }
            return View();
        }

        //POST: /Account/AdminLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AdminLogin(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            ApplicationUser user = await UserManager.FindByNameAsync(model.UserName);
            if (user == null)
            {
                ModelState.AddModelError("", "Không tồn tài tài khoản này");
                return View();
            }
            if (!user.IsActive)
            {
                ModelState.AddModelError("", "Tài khoản bị khóa");
                return View();
            }
            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    {
                        if (userRoleData.UserIsAdmin(user.Id))
                        {
                            Session[KeyHelper.Session_Login] = user;
                            return RedirectToAction("Index", "AdminManager");
                        }
                        else
                        {
                            TempData["msg"] = "Thông tin đăng nhập không đúng";
                            return RedirectToAction("AdminLogin");
                        }
                    }

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = "", model.RememberMe });

                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Thông tin đăng nhập không đúng");
                    return View();
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            string url = returnUrl ?? "/Home/";
            ViewBag.ReturnUrl = url;
            string msg = (string)TempData["msg"];
            if (msg != null)
            {
                ModelState.AddModelError("", msg);
                return View();
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult RegisterOK()
        {
            string msg = (string)TempData["msg"];
            if (msg != null)
            {
                ViewBag.Message = msg;
                return View();
            }
            return RedirectToAction("Login");
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            ApplicationUser user = await UserManager.FindByNameAsync(model.UserName);
            if (user == null)
            {
                ModelState.AddModelError("", "Không tồn tài tài khoản này");
                return View();
            }
            if (!user.IsActive)
            {
                ModelState.AddModelError("", "Tài khoản chưa được kích hoạt");
                return View();
            }
            if (user.IsLock)
            {
                ModelState.AddModelError("", "Tài khoản bị khóa");
                return View();
            }
            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, false, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    {
                        if (userRoleData.UserIsCustomer(user.Id))
                        {
                            Session[KeyHelper.Session_Login] = user;
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            TempData["msg"] = "Thông tin đăng nhập không đúng";
                            return RedirectToAction("Login");
                        }
                    }
                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, model.RememberMe });

                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Thông tin đăng nhập không đúng");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes.
            // If a user enters incorrect codes for a specified amount of time then the user account
            // will be locked out for a specified amount of time.
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                //TempData["msg"] = "Chức năng đăng ký mới đang tạm dừng để phát triển. Vui lòng đăng nhập bằng Facebook của bạn";
                //return RedirectToAction("Login");
                var user = new ApplicationUser { FullName = model.FullName, UserName = model.Email, Email = model.Email, CreateTime = DateTime.Now, IsActive = false, Avatar = KeyHelper.DefaultAvatar, Address = model.Address, PhoneNumber = model.PhoneNumber, IsLock = false };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var userInfo = UserManager.FindByName(model.Email);
                    SendMailActiveAccountModel sendMail = new SendMailActiveAccountModel()
                    {
                        UserId = userInfo.Id,
                        Email = userInfo.Email
                    };
                    ResultModel resultModel = SendMailHelper.SendMailActiveAccount(sendMail);
                    if (resultModel.Status)
                    {
                        TempData["msg"] = "Đăng ký tài khoản ' " + model.Email + " ' thành công. Chúng tôi đã gửi một Email đến " + model.Email + " để tiến hành kích hoạt tài khoản của bạn. Vui lòng truy cập Email và làm theo hướng dẫn để kích hoạt tài khoản. Xin cảm ơn !";
                        return RedirectToAction("RegisterOK");
                    }
                    else
                    {
                        userData.DeleteUser(userInfo.Id);
                        ModelState.AddModelError("", resultModel.Message);
                        return View(model);
                    }
                    //userRoleData.InsertCustomerToUser(userInfo.Id);
                    //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ActiveAccount(SendMailActiveAccountModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Message = "Thông tin kích hoạt tài khoản không đầy đủ";
                return View();
            }
            var userInfo = UserManager.FindByName(model.Email);
            if (userInfo == null)
            {
                ViewBag.Message = "Không tìm thấy tài khoản cần kích hoạt";
                return View();
            }
            if (!(userInfo.Id.Equals(model.UserId) && userInfo.Email.Equals(model.Email)))
            {
                ViewBag.Message = "Thông tin kích hoạt tài khoản không khớp";
                return View();
            }
            if (userInfo.IsActive)
            {
                ViewBag.Message = "Tài khoản này đã được kích hoạt. Đăng nhập mua SIM ngay";
                return View();
            }
            userRoleData.InsertCustomerToUser(userInfo.Id);
            ResultModel result = userData.ActiveUser(userInfo.Id);
            ViewBag.Message = result.Message;
            return View();
        }

        [AllowAnonymous]
        public ActionResult NonActiveAccount(SendMailActiveAccountModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Message = "Thông tin hủy kích hoạt tài khoản không đầy đủ";
                return View();
            }
            var userInfo = UserManager.FindByName(model.Email);
            if (userInfo == null)
            {
                ViewBag.Message = "Không tìm thấy tài khoản cần hủy kích hoạt";
                return View();
            }
            if (!(userInfo.Id.Equals(model.UserId) && userInfo.Email.Equals(model.Email)))
            {
                ViewBag.Message = "Thông tin hủy kích hoạt tài khoản không khớp";
                return View();
            }
            if (userInfo.IsActive)
            {
                ViewBag.Message = "Tài khoản này đã được kích hoạt. Không thể hủy kích hoạt tại đây.";
                return View();
            }
            userData.DeleteUser(userInfo.Id);
            ViewBag.Message = "Hủy đăng ký tài khoản thành công. Xin lỗi vì sự phiền toái này đến bạn";
            return View();
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, model.ReturnUrl, model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl, string error)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null || !string.IsNullOrEmpty(error))
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    var userInfo = UserManager.FindByName(loginInfo.Email);
                    Session[KeyHelper.Session_Login] = userInfo;
                    return RedirectToLocal(returnUrl);

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });

                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    string avatar = string.Format("https://graph.facebook.com/{0}/picture", loginInfo.Login.ProviderKey);
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email, FullName = loginInfo.ExternalIdentity.Name, UrlAvatar = avatar });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            //if (User.Identity.IsAuthenticated)
            //{
            //    return RedirectToAction("Index", "Manage");
            //}

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, CreateTime = DateTime.Now, FullName = model.FullName, IsActive = true, Avatar = model.UrlAvatar, PhoneNumber = model.PhoneNumber, Address = model.Address, IsLock = false };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    var userInfo = UserManager.FindByName(model.Email);
                    userRoleData.InsertCustomerToUser(userInfo.Id);
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        Session[KeyHelper.Session_Login] = userInfo;
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOffAdmin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOffAdmin()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Clear();
            return RedirectToAction("AdminLogin");
        }

        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Clear();
            return RedirectToLocal("/");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                if (error.EndsWith("is already taken."))
                {
                    ModelState.AddModelError("", error.Substring(0, (error.Length - "is already taken.".Length)) + "đã tồn tại");
                }
                else
                {
                    ModelState.AddModelError("", error);
                }
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        #endregion Helpers
    }
}