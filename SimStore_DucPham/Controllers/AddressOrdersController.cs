﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SimStore_DucPham.Data.Entities;
using SimStore_DucPham.Filter;
using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;

namespace SimStore_DucPham.Controllers
{
    [AuthFilter]
    [SessionExpireFilter]
    [Authorize(Roles = "Customer")]
    public class AddressOrdersController : Controller
    {
        private SimVIPEntities db = new SimVIPEntities();

        // GET: AddressOrders
        public ActionResult Index()
        {
            return View(db.AddressOrders.ToList());
        }

        // GET: AddressOrders/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AddressOrder addressOrder = db.AddressOrders.Find(id);
            if (addressOrder == null)
            {
                return HttpNotFound();
            }
            return View(addressOrder);
        }

        public ActionResult SetDefault(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return Redirect("/customer-address");
            }
            ApplicationUser user = (ApplicationUser)Session[KeyHelper.Session_Login];
            var addressLast = db.AddressOrders.Where(p => p.IsPay && p.UserId.Equals(user.Id)).FirstOrDefault();
            if (addressLast == null)
            {
                return Redirect("/customer-address");
            }
            addressLast.IsPay = false;

            var addressSet = db.AddressOrders.Where(p => p.Id.Equals(id) && p.UserId.Equals(user.Id)).FirstOrDefault();
            if (addressSet == null)
            {
                return Redirect("/customer-address");
            }
            addressSet.IsPay = true;
            db.SaveChanges();
            return Redirect("/customer-address");
        }

        [HttpPost]
        public JsonResult SelectAddressJson(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return Json(new { data = new ResultModel() { Status = false, Message = "Id không được rỗng" } });
            }
            Session[KeyHelper.Session_Address] = id;
            return Json(new { data = new ResultModel() { Status = true, Message = "Ok" } });
        }

        // GET: AddressOrders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AddressOrders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,IsOrder,IsPay,City,District,Wards,Address,FullName,PhoneNumber")] AddressOrder addressOrder)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = (ApplicationUser)Session[KeyHelper.Session_Login];
                addressOrder.Id = Guid.NewGuid().ToString();
                addressOrder.UserId = user.Id;
                addressOrder.IsOrder = true;
                if (db.AddressOrders.Where(p => p.UserId.Equals(user.Id)).Count() == 0)
                {
                    addressOrder.IsPay = true;
                }
                else
                {
                    if (addressOrder.IsPay)
                    {
                        var addr = db.AddressOrders.Where(p => p.IsPay && p.UserId.Equals(user.Id)).First();
                        addr.IsPay = false;
                    }
                }
                db.AddressOrders.Add(addressOrder);
                db.SaveChanges();
                return Redirect("/customer-address");
            }

            return View(addressOrder);
        }

        // GET: AddressOrders/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AddressOrder addressOrder = db.AddressOrders.Find(id);
            if (addressOrder == null)
            {
                return HttpNotFound();
            }
            return View(addressOrder);
        }

        // POST: AddressOrders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserId,IsOrder,IsPay,City,District,Wards,Address,FullName,PhoneNumber")] AddressOrder addressOrder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(addressOrder).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(addressOrder);
        }

        // GET: AddressOrders/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AddressOrder addressOrder = db.AddressOrders.Find(id);
            if (addressOrder == null)
            {
                return HttpNotFound();
            }
            return View(addressOrder);
        }

        // POST: AddressOrders/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            AddressOrder addressOrder = db.AddressOrders.Find(id);
            db.AddressOrders.Remove(addressOrder);
            db.SaveChanges();
            return Json(new
            {
                data = new ResultModel()
                {
                    Status = true,
                    Message = "Xóa địa chỉ giao thành công"
                }
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}