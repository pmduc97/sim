﻿using SimStore_DucPham.Data;
using SimStore_DucPham.Filter;
using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;
using SimStore_DucPham.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimStore_DucPham.Controllers
{
    [AuthFilter]
    [SessionExpireFilter]
    [Authorize(Roles = "Customer")]
    public class CustomerController : Controller
    {
        private BillData billData = new BillData();
        private AddressOrderData addressOrder = new AddressOrderData();

        // GET: Customer
        public ActionResult Index()
        {
            ApplicationUser user = (ApplicationUser)Session[KeyHelper.Session_Login];
            var customerManager = new CustomerManagerModel()
            {
                Address = addressOrder.GetAllAddress().Where(p => p.UserId.Equals(user.Id)).ToList(),
                Bills = billData.GetAllBill().Where(p => p.CreateUserId.Equals(user.Id)).ToList()
            };
            return View(customerManager);
        }

        public ActionResult Address()
        {
            ApplicationUser user = (ApplicationUser)Session[KeyHelper.Session_Login];
            var customerManager = new CustomerManagerModel()
            {
                Address = addressOrder.GetAllAddress().Where(p => p.UserId.Equals(user.Id)).ToList(),
                Bills = billData.GetAllBill().Where(p => p.CreateUserId.Equals(user.Id)).ToList()
            };
            return View(customerManager);
        }

        [HttpPost]
        public JsonResult CanceledBill(string billId, string reasonCanceled)
        {
            ApplicationUser User = (ApplicationUser)Session[KeyHelper.Session_Login];
            ResultModel result = billData.CanceledBill(billId, User.Id, reasonCanceled);
            return Json(new { data = result });
        }

        public JsonResult GetInfoBill(string billId)
        {
            BillViewModel bill = billData.GetInfoBill(billId);
            return Json(new { data = bill }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetReasonCanceledOrder(string billId)
        {
            ResultModel result = billData.GetReasonCanceledOrder(billId);
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActiveBill(string code, string billId)
        {
            ActiveBillModel model = new ActiveBillModel();

            var bill = billData.GetInfoBill(billId);
            if (bill == null)
            {
                model.Status = false;
                model.Message = "Không tìm thấy đơn hàng cần xác nhận";
                return View(model);
            }
            var result = billData.ActiveBill(code, bill);
            model.Status = result.Status;
            model.Message = result.Message;
            model.Bill = bill;
            return View(model);
        }
    }
}