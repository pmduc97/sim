﻿using SimStore_DucPham.Data;
using SimStore_DucPham.Filter;
using SimStore_DucPham.Models;
using SimStore_DucPham.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimStore_DucPham.Controllers
{
    [AuthFilter]
    [SessionExpireFilter]
    [Authorize(Roles = "Customer")]
    public class APICustomerController : Controller
    {
        private UserData userData = new UserData();

        public JsonResult GetInfoUser(string userId)
        {
            UserViewModel user = userData.GetInfoUser(userId ?? "");
            return Json(new { data = user }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdateProfile(string userId, string fullName, string email, string phoneNumber, string address)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return Json(new { data = new ResultModel() { Status = false, Message = "UserId không được rỗng" } });
            }
            ResultModel result = userData.UpdateProfile(userId, fullName ?? "", email ?? "", phoneNumber ?? "", address ?? "");
            return Json(new { data = result });
        }
    }
}