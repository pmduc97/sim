﻿using SimStore_DucPham.Data;
using SimStore_DucPham.Filter;
using SimStore_DucPham.Helper;
using SimStore_DucPham.Models;
using SimStore_DucPham.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimStore_DucPham.Controllers
{
    [AuthFilter]
    [SessionExpireFilter]
    [Authorize(Roles = "Customer")]
    public class OrderController : Controller
    {
        private AddressOrderData adressOrderData = new AddressOrderData();
        private BillData billData = new BillData();

        // GET: Order
        public ActionResult Index()
        {
            ViewBag.Title = "Đặt hàng";

            ApplicationUser user = (ApplicationUser)Session[KeyHelper.Session_Login];
            var products = new List<ProductViewModel>();
            var address = adressOrderData.GetAllAddress().Where(p => p.UserId.Equals(user.Id) && p.IsPay).FirstOrDefault();

            if (Session[KeyHelper.Session_Cart] != null)
            {
                products = (List<ProductViewModel>)Session[KeyHelper.Session_Cart];
            }
            if (Session[KeyHelper.Session_Address] != null)
            {
                string addressId = (string)Session[KeyHelper.Session_Address];
                var addressCheck = adressOrderData.GetAllAddress().Where(p => p.Id.Equals(addressId)).FirstOrDefault();
                if (addressCheck != null)
                {
                    address = addressCheck;
                }
            }
            var model = new OrderAddressViewModel()
            {
                Address = address,
                Products = products,
                Email = user.Email,
                Addresses = adressOrderData.GetAllAddress().Where(p => p.UserId.Equals(user.Id)).ToList()
            };
            return View(model);
        }

        public ActionResult DatHang()
        {
            ApplicationUser user = (ApplicationUser)Session[KeyHelper.Session_Login];
            List<ProductViewModel> products = (List<ProductViewModel>)Session[KeyHelper.Session_Cart];
            var address = adressOrderData.GetAllAddress().Where(p => p.UserId.Equals(user.Id) && p.IsPay).FirstOrDefault();

            if (Session[KeyHelper.Session_Address] != null)
            {
                string addressId = (string)Session[KeyHelper.Session_Address];
                var addressCheck = adressOrderData.GetAllAddress().Where(p => p.Id.Equals(addressId)).FirstOrDefault();
                if (addressCheck != null)
                {
                    address = addressCheck;
                }
            }
            BillCreateModel billCreate = new BillCreateModel()
            {
                Id = Guid.NewGuid().ToString(),
                CreateTime = DateTime.Now,
                CreateUserId = user.Id,
                Status = "DoiXacNhanMail",
                SumMoney = products.Sum(p => p.Price),
                Note = "",
                AddressOrderId = address.Id,
                IsOnline = true,
                Code = SendMailHelper.RandomString(8, false)
            };

            ResultModel result = billData.CreateBill(billCreate, products);
            if (result.Status)
            {
                BillModel billModel = new BillModel()
                {
                    Id = billCreate.Id,
                    CreateTime = billCreate.CreateTime,
                    CreateUserId = billCreate.CreateUserId,
                    IsOnline = true,
                    IsPay = false,
                    Note = billCreate.Note,
                    Code = billCreate.Code
                };
                SendMailModel sendMail = new SendMailModel()
                {
                    User = user,
                    Bill = billModel,
                    CustomerMail = user.Email,
                    AddressOrder = address,
                    MailSubject = ConfigurationManager.AppSettings["SiteName"] + ": Email xác nhận đơn hàng " + billModel.Id + " | " + billCreate.CreateTime.ToString("dd/MM/yyyy HH:mm:ss"),
                    Products = products
                };
                SendMailHelper.SendMailActiveBill(sendMail);
                OrderOkModel orderOk = new OrderOkModel()
                {
                    Status = true,
                    FullName = address.FullName,
                    BillId = billCreate.Id,
                    Address = address.Address,
                    PhoneNumber = address.PhoneNumber,
                    IsOnline = true,
                    Email = user.Email,
                    Products = products
                };
                Session.Remove(KeyHelper.Session_Cart);
                TempData["OK"] = orderOk;
                return RedirectToAction("OrderOK", "Cart");
            }
            else
            {
                OrderOkModel orderOk = new OrderOkModel()
                {
                    Status = false
                };
                TempData["Fail"] = orderOk;
                return RedirectToAction("OrderFail", "Cart");
            }
        }
    }
}