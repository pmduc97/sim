﻿using SimStore_DucPham.Helper;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace SimStore_DucPham.Filter
{
    public class AuthFilter : ActionFilterAttribute, IAuthorizationFilter
    {
        /// <summary>
        /// Kiểm tra sự tồn tại của Session đăng nhập
        /// </summary>
        /// <param name="filterContext"></param>
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            object session = HttpContext.Current.Session[KeyHelper.Session_Login];
            if (session == null)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        /// <summary>
        /// Copy trên mạng. Hiểu là để lọc session nếu không có thì redirect về link bên trong
        /// </summary>
        /// <param name="filterContext"></param>
        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            if (filterContext.Result == null || filterContext.Result is HttpUnauthorizedResult)
            {
                filterContext.Result = new RedirectToRouteResult("Default", new System.Web.Routing.RouteValueDictionary
                {
                    {"controller","Account" },
                    {"action","Login" },
                    {"returnUrl",filterContext.HttpContext.Request.RawUrl }
                });
            }
        }
    }
}