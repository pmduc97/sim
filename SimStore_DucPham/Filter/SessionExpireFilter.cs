﻿using SimStore_DucPham.Helper;
using System.Web;
using System.Web.Mvc;

namespace SimStore_DucPham.Filter
{
    public class SessionExpireFilter : ActionFilterAttribute
    {
        /// <summary>
        /// Kiểm tra hết hạn Session chưa
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var session = HttpContext.Current.Session[KeyHelper.Session_Login];

            // check if session is supported
            if (session == null)
            {
                // check if a new session id was generated
                filterContext.Result = new RedirectResult("~/Account/Login");
                return;
            }

            base.OnActionExecuting(filterContext);
        }
    }
}