namespace SimStore_DucPham.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class capnhat : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IsLock", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsLock");
        }
    }
}
