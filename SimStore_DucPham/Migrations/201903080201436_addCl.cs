namespace SimStore_DucPham.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "CreateTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsActive");
            DropColumn("dbo.AspNetUsers", "CreateTime");
        }
    }
}
