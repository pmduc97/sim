﻿var countProduct = 0;
$(function () {
    var key = $('#id-get').val().split('_');
    GetProductByPrice(key[1]);
    $('#select-carrier').on('change', function () {
        var carrier = this.value;
        var sapxep = $('#select-sapxep').val();
        GetProductByPrice(key[1], carrier, sapxep);
    });
    $('#btn-xem-tat-ca').off('click').on('click', function () {
        window.location.href = "/tat-ca-sim";
    });
    $('#select-sapxep').on('change', function () {
        var carrier = $('#select-carrier').val();
        var sapxep = this.value;
        GetProductByPrice(key[1], carrier, sapxep);
    });
});
function GetProductByPrice(price, carrier, sapxep) {
    var table = $('#table-product').DataTable({
        "pageLength": 10,
        "lengthChange": false,
        "searching": false,
        "processing": true,
        "serverSide": true,
        //"lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
        destroy: true,
        "ajax": {
            "url": "/Home/GetProductActiveByPrice",
            "type": "post",
            "data": { price: price, carrier: carrier, sapxep: sapxep },
            "dataType": "json"
        },
        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Xem _MENU_ mục",
            "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
            "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Đầu",
                "sPrevious": "Trước",
                "sNext": "Tiếp",
                "sLast": "Cuối"
            }
        },
        "columns": [
            {
                "data": "Number", "width": "120px", "orderable": false, "className": "text-center", "render": function (id, type, row) {
                    return "<a href='/chon-sim?number=" + id + "'><div class='product-view'>" + formatPhone(id) + "</div></a>";
                }
            },
            {
                "data": "CarrierName", "width": "100px", "orderable": false, "className": "text-center", "render": function (id) {
                    return EditCarrierImage(id);
                }
            },
            {
                "data": "Price", "width": "130px", "orderable": false, "className": "text-center", "render": function (id) {
                    return "<div class='price-view'>" + formatPrice(id) + "</div>";
                }
            },
            {
                "data": "Id", "width": "200px", "className": "text-center", "render": function (id, type, row) {
                    return "<button onclick=AddProductToCart('" + id + "','" + formatPhone(row.Number) + "') style='margin-right: 5px;' class='btn btn-xs btn-warning'><span class='glyphicon glyphicon-shopping-cart'></span> Thêm vào giỏ</button>" +
                        "<button onclick=BuyProduct('" + id + "','" + row.Number + "') class='btn btn-xs btn-danger'>Đặt mua</button>";
                },
                "orderable": false
            }

        ],
        "order": [],
        "initComplete": function (settings, json) {
            $('#num-product').html('Tìm thấy ' + json.recordsTotal + ' Sim');
        }
    });
}