﻿$(function () {
    $('#random-btn').off('click').on('click', function () {
        RandomProduct();
    });
});
function RandomProduct() {
    $.ajax({
        url: '/Home/Get12ProductRandom',
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            var data = res.data;
            if (data.length === 0) {
                $('#pSimRandom').html(DefaultEmptyProductLi);
            }
            else {
                var html = '';
                $.each(data, function (key, item) {
                    html += EditProductLi(item);
                });
                $('#pSimRandom').html(html);
            }
        },
        error: function (e) {
            $('#pSimRandom').html(DefaultEmptyProductLi);
        }
    });
}