﻿var countProduct = 0;
$(function () {
    var key = $('#id-get').val().split('_');
    GetProductByNumber(key[1]);
    $('#btn-xem-tat-ca').off('click').on('click', function () {
        window.location.href = "/tat-ca-sim";
    });
    $('#select-carrier').on('change', function () {
        var carrier = this.value;
        var price = $('#select-price').val();
        var sapxep = $('#select-sapxep').val();
        GetProductByNumber(key[1], carrier, price, sapxep);
    });
    $('#select-price').on('change', function () {
        var carrier = $('#select-carrier').val();
        var price = this.value;
        var sapxep = $('#select-sapxep').val();
        GetProductByNumber(key[1], carrier, price, sapxep);
    });
    $('#select-sapxep').on('change', function () {
        var carrier = $('#select-carrier').val();
        var price = $('#select-price').val();
        var sapxep = this.value;
        GetProductByNumber(key[1], carrier, price, sapxep);
    });
});
function EditFindNumber(key, number) {
    var arrKey = key.split('_');
    var result = '';
    if (arrKey[0] == 'cochua') {
        result = number.replace(arrKey[1], "<span style='color:red;'>" + arrKey[1] + "</span>");
    }
    else if (arrKey[0] == 'phanduoi') {
        var vitri = number.lastIndexOf(arrKey[1]);
        var strFirst = number.substring(0, vitri);
        var strLast = "<span style='color:red;'>" + number.substring(vitri, number.length) + "</span>";
        result = strFirst + strLast;
    }
    else if (arrKey[0] == 'phandau') {
        var vitri = arrKey[1].length
        var strFirst = "<span style='color:red;'>" + number.substring(0, vitri) + "</span>";
        var strLast = number.substring(vitri, number.length);
        result = strFirst + strLast;
    }
    else {
        var first = "<span style='color:red;'>" + number.substring(0, arrKey[1].length) + "</span>";
        var mid = number.substring(arrKey[1].length, number.length - arrKey[2].length);
        var last = "<span style='color:red;'>" + number.substring(number.length - arrKey[2].length, number.length) + "</span>";
        result = first + mid + last;
    }
    return result;
}
function GetProductByNumber(number, carrier, price, sapxep) {
    var keyNumberGet = '';
    if (number.charAt(0) == '%' && number.charAt(number.length - 1) == '%') {
        keyNumberGet = "cochua_" + number.substring(1, number.length - 1);
    }
    else if (number.charAt(0) == '%' && number.charAt(number.length - 1) != '%') {
        keyNumberGet = "phanduoi_" + number.substring(1, number.length);
    }
    else if (number.charAt(0) != '%' && number.charAt(number.length - 1) == '%') {
        keyNumberGet = "phandau_" + number.substring(0, number.length - 1);
    }
    else {
        keyNumberGet = "bentrong_" + number.substring(0, number.indexOf("%")) + "_" + number.substring(number.indexOf("%") + 1, number.length);
    }
    var table = $('#table-product').DataTable({
        "pageLength": 10,
        "lengthChange": false,
        "searching": false,
        "processing": true,
        "serverSide": true,
        //"lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
        destroy: true,
        "ajax": {
            "url": "/Home/GetProductActiveByNumber",
            "type": "post",
            "data": { number: number, price: price, carrier: carrier, sapxep: sapxep },
            "dataType": "json"
        },
        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Xem _MENU_ mục",
            "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
            "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Đầu",
                "sPrevious": "Trước",
                "sNext": "Tiếp",
                "sLast": "Cuối"
            }
        },
        "columns": [
            {
                "data": "Number", "width": "120px", "orderable": false, "className": "text-center", "render": function (id, type, row) {
                    return "<a href='/chon-sim?number=" + id + "'><div class='product-view'>" + EditFindNumber(keyNumberGet, id) + "</div></a>";
                }
            },
            {
                "data": "CarrierName", "width": "100px", "orderable": false, "className": "text-center", "render": function (id) {
                    return EditCarrierImage(id);
                }
            },
            {
                "data": "Price", "width": "130px", "orderable": false, "className": "text-center", "render": function (id) {
                    return "<div class='price-view'>" + formatPrice(id) + "</div>";
                }
            },
            {
                "data": "Id", "width": "200px", "className": "text-center", "render": function (id, type, row) {
                    return "<button onclick=AddProductToCart('" + id + "','" + formatPhone(row.Number) + "') style='margin-right: 5px;' class='btn btn-xs btn-warning'><span class='glyphicon glyphicon-shopping-cart'></span> Thêm vào giỏ</button>" +
                        "<button onclick=BuyProduct('" + id + "','" + row.Number + "') class='btn btn-xs btn-danger'>Đặt mua</button>";
                },
                "orderable": false
            }

        ],
        "order": [],
        "initComplete": function (settings, json) {
            $('#num-product').html('Tìm thấy ' + json.recordsTotal + ' Sim');
        }
    });
}