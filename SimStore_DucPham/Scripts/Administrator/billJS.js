﻿$(function () {
    GetAllBill();
    $('#reservation').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Xem toàn thời gian',
            applyLabel: 'Đồng ý'
        }
    });

    $('#reservation').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        var timecheck = picker.startDate.format('YYYY-MM-DD') + "_" + picker.endDate.format('YYYY-MM-DD');
        $('#timeCheck').val(timecheck);
        var status = $('#select-status').val();
        var isonline = $('#select-isonline').val();
        var ispay = $('#select-ispay').val();
        GetAllBill(status, isonline, ispay, timecheck);
    });

    $('#reservation').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('Tất cả thời gian');
        $('#timeCheck').val('');
        var status = $('#select-status').val();
        var isonline = $('#select-isonline').val();
        var ispay = $('#select-ispay').val();
        GetAllBill(status, isonline, ispay, '');
    });
    //$('#reservation').daterangepicker({
    //    opens: 'left'
    //}, function (start, end, label) {
    //    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    //    var timecheck = start.format('YYYY-MM-DD') + "_" + end.format('YYYY-MM-DD');
    //    $('#timeCheck').val(timecheck);
    //    var status = $('#select-status').val();
    //    var isonline = $('#select-isonline').val();
    //    var ispay = $('#select-ispay').val();
    //    GetAllBill(status, isonline, ispay, timecheck);
    //    });
    $('#select-status').on('change', function () {
        var status = this.value;
        var isonline = $('#select-isonline').val();
        var ispay = $('#select-ispay').val();
        var timecheck = $('#timeCheck').val();
        GetAllBill(status, isonline, ispay, timecheck);
    });
    $('#select-isonline').on('change', function () {
        var isonline = this.value;
        var status = $('#select-status').val();
        var ispay = $('#select-ispay').val();
        var timecheck = $('#timeCheck').val();
        GetAllBill(status, isonline, ispay, timecheck);
    });
    $('#select-ispay').on('change', function () {
        var ispay = this.value;
        var status = $('#select-status').val();
        var isonline = $('#select-isonline').val();
        var timecheck = $('#timeCheck').val();
        GetAllBill(status, isonline, ispay, timecheck);
    });
});
function GetDetail(billId) {
    $('#shDienThoai').show();
    var htmlBtn = "<button type='button' class='btn btn-default btn-sm pull-left' data-dismiss='modal'>Đóng</button>";
    $.ajax({
        url: '/AdminManager/GetInfoBill',
        data: { billId: billId },
        type: 'get',
        dataType: 'json',
        success: function (res) {
            var data = res.data;
            $('#iBillId').val(data.Id);
            $('#iLoaiDonHang').html(data.IsOnline ? "Đơn hàng Online" : "Mua hàng tại quầy");
            $('#iTrangThai').html(data.Status);
            $('#iNguoiDat').html(data.CreateUserName);
            $('#iThoiGianDat').html(data.CreateTime);
            $('#iDienThoai').html(data.CreatePhoneNumber);
            $('#iThanhToan').html(data.IsPay ? "Đã thanh toán" : "Chưa thanh toán");
            $('#iNguoiNhan').html(data.AddressOrder.FullName);
            $('#iDienThoaiNhan').html(data.AddressOrder.PhoneNumber);
            $('#iDiaChiNhan').html(data.AddressOrder.Address);
            $('#iGhiChu').html(data.Note);
            $('#iTongTien').html(formatPrice(data.SumMoney));
            var html = '';
            $.each(data.DetailBills, function (index, item) {
                html += "<tr><td>" + item.ProductNumber + "</td><td>" + item.ProductCarrierName + "</td><td>" + formatPrice(item.ProductPrice) + "</td><td>" + item.ProductCategoryName + "</td></tr>";
            });
            $('#iDonHang').html(html);
            if (data.CreateUserName == 'DefaultCustomer' && data.IsOnline) {
                $('#iNguoiDat').html('Mua nhanh không đăng nhập');
                $('#shDienThoai').hide();
            }
            if (data.Status == 'Đợi xác nhận') {
                htmlBtn += "<button type='button' class='btn btn-danger btn-sm pull-left' onclick=HuyDon('" + data.Id + "')>Hủy Đơn Hàng</button>" +
                    "<button type='button' class='btn btn-info btn-sm' onclick=DaXacNhan('" + data.Id + "')>Đã xác nhận đơn hàng</button>";
            }
            else if (data.Status == 'Đã xác nhận') {
                htmlBtn += "<button type='button' class='btn btn-danger btn-sm pull-left' onclick=HuyDon('" + data.Id + "')>Hủy Đơn Hàng</button>" +
                    "<button type='button' class='btn btn-info btn-sm' onclick=DangGiaoHang('" + data.Id + "')>Đang giao đơn hàng này</button>";
            }
            else if (data.Status == 'Đang giao hàng') {
                htmlBtn += "<button type='button' class='btn btn-danger btn-sm pull-left' onclick=HuyDon('" + data.Id + "')>Hủy Đơn Hàng</button>" +
                    "<button type='button' class='btn btn-info btn-sm' onclick=DaGiaoHang('" + data.Id + "')>Đã giao thành công</button>";
            }
            else if (data.Status == 'Hủy bỏ') {
                htmlBtn += "<button type='button' class='btn btn-danger btn-sm' onclick=XemLyDo('" + data.Id + "')>Xem lý do</button>";
            }
            $('#listBtn').html(htmlBtn);
            $('#info-bill').modal('show');
        },
        error: function () {
            $('#errorLoadModal').modal('show');
        }
    });
}
function XemLyDo(billId) {
    $.ajax({
        url: '/AdminManager/GetReasonCanceledOrder',
        data: { billId: billId },
        type: 'get',
        dataType: 'json',
        success: function (res) {
            var data = res.data;
            if (data.Status) {
                $.alert({
                    title: 'Lý do hủy đơn:',
                    content: data.Message
                });
            }
            else {
                $.notify(data.Message, "error");
            }
        },
        error: function () {
            $.notify("Lỗi không xác định", "error");
        }
    });
}
function DaGiaoHang(billId) {
    $.confirm({
        title: "Thông báo",
        content: "Xác nhận Đã giao đơn hàng này?",
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                $.ajax({
                    url: '/AdminManager/UpdateStatusBill',
                    data: { billId: billId, status: "DaGiaoHang" },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            $('#info-bill').modal('hide');
                            $.notify(data.Message, "success");
                            GetAllBill();
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}
function DangGiaoHang(billId) {
    $.confirm({
        title: "Thông báo",
        content: "Xác nhận Đang giao đơn hàng này?",
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                $.ajax({
                    url: '/AdminManager/UpdateStatusBill',
                    data: { billId: billId, status: "DangGiaoHang" },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            $('#info-bill').modal('hide');
                            $.notify(data.Message, "success");
                            GetAllBill();
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}
function GetAllBill(status, isonline, ispay, datecheck) {
    var table = $('#bill-table').DataTable({
        "pageLength": 10,
        //"lengthChange": false,
        //"searching": false,
        "processing": true,
        destroy: true,
        "ajax": {
            "url": "/AdminManager/GetAllBill",
            "type": "get",
            "data": { status: status, isonline: isonline, ispay: ispay, date: datecheck },
            "dataType": "json"
        },
        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Xem _MENU_ mục",
            "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
            "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Đầu",
                "sPrevious": "Trước",
                "sNext": "Tiếp",
                "sLast": "Cuối"
            }
        },
        "columns": [
            { "data": null, "width": "40px", "orderable": false, "className": "text-center" },
            {
                "data": "ProductNumbers", "width": "100px", "orderable": false, "className": "details-control", "render": function (id) {
                    return "<span style='color: #0098db;font-weight: bold;'>" + id + "</span>";
                }
            },
            {
                "data": "SumMoney", "width": "60px", "orderable": false, "render": function (id) {
                    return "<span style='color: #892020;font-weight: bold;margin-right: 30%;float:right;'>" + formatPrice(id) + "</span>";
                }
            },
            {
                "data": "CreateTime", "width": "90px", "className": "text-center", "orderable": false,
            },
            {
                "data": "IsOnline", "width": "90px", "className": "text-center", "orderable": false, "render": function (id) {
                    if (id) {
                        return "Đặt hàng Online";
                    }
                    return "Mua trực tiếp";
                }
            },
            {
                "data": "Status", "width": "65px", "orderable": false, "className": "text-center",
                "render": function (id) {
                    return "<span class='label label-" + GetClassByStatus(id) + "'>" + id + "</span>";
                }
            },
            {
                "data": "Id", "width": "70px", "className": "text-center", "render": function (id, type, row) {
                    var getdetail = "<a class='btn btn-default btn-xs' onclick=GetDetail('" + id + "') style='margin-right:3px;'><i class='fa fa-eye'></i> Chi tiết</a>";
                    return getdetail;
                },
                "orderable": false
            }

        ]
        //,
        //"order": [[3, 'desc']]
    });
    table.on('order.dt search.dt', function () {
        table.column(0, {}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
    $('#bill-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
            //tr.fadeOut(500);
        }
        else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
            //tr.fadeIn(500);
        }
    });
}
function HuyDon(billId) {
    $.confirm({
        title: "Xác nhận hủy bỏ đơn hàng này?",
        content: "<div class='form-group'><label>Lý do hủy đơn</label>" +
            "<textarea id='pLyDoHuyDon' rows='3' class='form-control'></textarea></div>",
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                var lyDoHuyDon = $('#pLyDoHuyDon').val();
                if (lyDoHuyDon == '' || !lyDoHuyDon) {
                    $.notify("Lý do không được rỗng", "error");
                    return;
                }

                $.ajax({
                    url: '/AdminManager/CanceledBill',
                    data: { billId: billId, reasonCanceled: lyDoHuyDon },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            $('#info-bill').modal('hide');
                            $.notify(data.Message, "success");
                            GetAllBill();
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}
function DaXacNhan(billId) {
    $.confirm({
        title: "Thông báo",
        content: "Bạn chắc chắn đã xác nhận đơn hàng này?",
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                $.ajax({
                    url: '/AdminManager/UpdateStatusBill',
                    data: { billId: billId, status: "DaXacNhan" },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            $('#info-bill').modal('hide');
                            $.notify(data.Message, "success");
                            GetAllBill();
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}
/* Formatting function for row details - modify as you need */
function format(d) {
    var marginpx = $('#bill-table > thead > tr > th:nth-child(1)').width() + 10;
    // `d` is the original data object for the row
    var html = '<table class="table table-responsive table-bordered" style="width: 50%;margin-left: ' + marginpx + 'px;background-color: #f5f5f5;">' +
        '<thead><tr><th>Số sim</th><th class="text-center">Mạng</th><th>Giá</th><th>Danh mục</th></tr></thead><tbody>';
    var data = d.DetailBills;
    $.each(data, function (index, item) {
        html += '<tr>' +
            '<td>' + formatPhone(item.ProductNumber) + '</td>' +
            '<td class="text-center">' + EditCarrierImage(item.ProductCarrierName) + '</td>' +
            '<td>' + formatPrice(item.ProductPrice) + '</td>' +
            '<td>' + item.ProductCategoryName + '</td>' +
            '</tr>';
    });
    html += '</tbody></table>';
    return html;
}