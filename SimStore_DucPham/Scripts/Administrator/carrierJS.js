﻿$(function () {
    GetAll();
    $('#image-base').val(DefaultImageCarrier);
    $("#imageUpload").change(function () {
        readURL(this);
    });
    $("#imageUpload2").change(function () {
        readURL2(this);
    });
    $('#add-carrier').off('click').on('click', function () {
        CreateCarrier();
    });
    $('#update-carrier').off('click').on('click', function () {
        UpdateCarrier();
    });
});
function ResetImageDafault() {
    $('#image-base').val(DefaultImageCarrier);
    $('#imagePreview').css('background-image', 'url(' + DefaultImageCarrier + ')');
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(250);
            $('#image-base').val(e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imagePreview2').css('background-image', 'url(' + e.target.result + ')');
            $('#imagePreview2').hide();
            $('#imagePreview2').fadeIn(250);
            $('#image-base-2').val(e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function GetAll() {
    var table = $('#carrier-table').DataTable({
        destroy: true,
        "ajax": {
            "url": "/AdminManager/GetAllCarrier",
            "type": "get",
            "dataType": "json"
        },
        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Xem _MENU_ mục",
            "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
            "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Đầu",
                "sPrevious": "Trước",
                "sNext": "Tiếp",
                "sLast": "Cuối"
            }
        },
        "columns": [
            { "data": null, "width": "30px", "orderable": false, "className": "text-center" },

            {
                "data": "Name", "width": "220px", "className": "text-center", "render": function (id, type, row) {
                    return id + "<span class='label label-info f-right'>" + row.CountProduct + " mặt hàng" + "</span>";
                }
            },
            {
                "data": "Image", "width": "120px", "orderable": false, "className": "text-center", "render": function (id) {
                    return "<img src='" + id + "' style='width: 50px;height: 25px;' />";
                }
            },
            { "data": "Description", "autoWidth": true, "orderable": false, "className": "text-center" },
            {
                "data": "IsActive", "width": "100px", "className": "text-center",
                "render": function (id) {
                    if (id === true) {
                        return "<span class='label label-success'>Hoạt động</span>";
                    }
                    else {
                        return "<span class='label label-danger'>Tắt</span>";
                    }
                }
            },
            {
                "data": "Id", "width": "200px", "className": "text-center", "render": function (id, type, row) {
                    var getdetail = "<a class='btn btn-default btn-xs' onclick=GetDetail('" + id + "') style='margin-right:3px;'><i class='fa fa-eye'></i> Chi tiết</a>";
                    var lockString = '';
                    if (row.IsActive) {
                        lockString = "<a class='btn btn-danger btn-xs' onclick=LockCarrier('" + id + "') style='margin-right:3px;'><i class='fa fa-lock'></i> Tắt&nbsp;</a>";
                    }
                    else {
                        lockString = "<a class='btn btn-success btn-xs' onclick=UnLockCarrier('" + id + "') style='margin-right:3px;'><i class='fa fa-unlock'></i> Mở</a>";
                    }
                    return getdetail + lockString;
                },
                "orderable": false
            }

        ],
        "order": [[1, 'asc']]
    });
    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}
function CreateCarrier() {
    var categoryName = $('#iName').val();
    if (categoryName == null || categoryName == '') {
        $.notify("Tên nhà mạng không được rỗng", "error");
        return;
    }
    var categoryDes = $('#iDescription').val();
    var isActive = $('#iIsActive').is(":checked");
    var carrierImage = $('#image-base').val();
    var categoryModel = {
        Name: categoryName,
        Description: categoryDes,
        IsActive: isActive,
        Image: carrierImage
    };
    $.ajax({
        url: '/AdminManager/CreateCarrier',
        data: { carrierStr: JSON.stringify(categoryModel) },
        type: 'post',
        dataType: 'json',
        success: function (res) {
            var data = res.data;
            if (data.Status) {
                $('#modal-carrier').modal('hide');
                $('#iName').val('');
                $('#iDescription').val('');
                ResetImageDafault();
                GetAll();
                $.notify(data.Message, "success");
            }
            else {
                $.notify(data.Message, "error");
            }
        },
        error: function () {
            $.notify("Lỗi không xác định", "error");
        }
    });
}
function GetDetail(carrierId) {
    $.ajax({
        url: '/AdminManager/GetInfoCarrier',
        data: { carrierId: carrierId },
        type: 'get',
        dataType: 'json',
        success: function (res) {
            var data = res.data;
            $('#pId').val(data.Id);
            $('#pName').val(data.Name);
            $('#pDescription').val(data.Description);
            $('#image-base-2').val(data.Image);
            $('#imagePreview2').css('background-image', 'url(' + data.Image + ')');
            $('#carrier-info').modal('show');
        },
        error: function () {
            $('#errorLoadModal').modal('show');
        }
    });
}
function UpdateCarrier() {
    var carrierName = $('#pName').val();
    if (carrierName == null || carrierName == '') {
        $.notify("Tên nhà mạng không được rỗng", "error");
        return;
    }
    var carrierDes = $('#pDescription').val();
    var carrierId = $('#pId').val();
    var carrierImage = $('#image-base-2').val();
    $.ajax({
        url: '/AdminManager/UpdateCarrier',
        data: { carrierId: carrierId, carrierName: carrierName, carrierDescription: carrierDes, carrierImage: carrierImage },
        type: 'post',
        dataType: 'json',
        success: function (res) {
            var data = res.data;
            if (data.Status) {
                GetAll();
                $('#carrier-info').modal('hide');
                $.notify(data.Message, "success");
            }
            else {
                $.notify(data.Message, "error");
            }
        },
        error: function () {
            $('#errorLoadModal').modal('show');
        }
    });
}
function LockCarrier(carrierId) {
    $.confirm({
        title: "Thông báo",
        content: "Tắt hoạt động nhà mạng này?",
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                $.ajax({
                    url: '/AdminManager/IsActiveCarrier',
                    data: { carrierId: carrierId, isActive: false },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            GetAll();
                            $.notify(data.Message, "success");
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}
function UnLockCarrier(carrierId) {
    $.confirm({
        title: "Thông báo",
        content: "Mở hoạt động nhà mạng này?",
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                $.ajax({
                    url: '/AdminManager/IsActiveCarrier',
                    data: { carrierId: carrierId, isActive: true },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            GetAll();
                            $.notify(data.Message, "success");
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}