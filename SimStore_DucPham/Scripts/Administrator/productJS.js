﻿$(function () {
    GetAllProduct();
    SettingSelect2();
    $('#add-product').off('click').on('click', function () {
        CreateProduct();
    });
    $('#update-product').off('click').on('click', function () {
        UpdateProduct();
    });
    $('#select-status').on('change', function () {
        var status = this.value;
        var carrier = $('#select-carrier').val();
        var price = $('#select-price').val();
        var sapxep = $('#select-sapxep').val();
        GetAllProduct(status, carrier, price, sapxep);
    });
    $('#select-carrier').on('change', function () {
        var carrier = this.value;
        var status = $('#select-status').val();
        var price = $('#select-price').val();
        var sapxep = $('#select-sapxep').val();
        GetAllProduct(status, carrier, price, sapxep);
    });
    $('#select-price').on('change', function () {
        var status = $('#select-status').val();
        var carrier = $('#select-carrier').val();
        var price = this.value;
        var sapxep = $('#select-sapxep').val();
        GetAllProduct(status, carrier, price, sapxep);
    });
    $('#select-sapxep').on('change', function () {
        var status = $('#select-status').val();
        var carrier = $('#select-carrier').val();
        var price = $('#select-price').val();
        var sapxep = this.value;
        GetAllProduct(status, carrier, price, sapxep);
    });
});
function GetAllProduct(status, carrier, price, sapxep) {
    var table = $('#product-table').DataTable({
        "pageLength": 10,
        //"lengthChange": false,
        //"searching": false,
        "processing": true,
        "serverSide": true,
        destroy: true,
        "ajax": {
            "url": "/AdminManager/GetAllProduct",
            "type": "post",
            "data": { status: status, carrier: carrier, price: price, sapxep: sapxep },
            "dataType": "json"
        },
        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Xem _MENU_ mục",
            "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
            "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Đầu",
                "sPrevious": "Trước",
                "sNext": "Tiếp",
                "sLast": "Cuối"
            }
        },
        "columns": [
            {
                "data": null, "width": "30px", "orderable": false, "className": "text-center"
            },
            {
                "data": "Number", "width": "100px", "className": "text-center", "orderable": false,
                "render": function (id, type, row) {
                    var sellString = '';
                    if (row.IsSell) {
                        sellString = "<span style='margin-left:5px;' class='label label-warning'>Đã bán&nbsp;</span>";
                    }
                    else {
                        sellString = "<span style='margin-left:5px;' class='label label-default'>Còn hàng</span>";
                    }
                    return formatPhone(id) + sellString;
                }
            },
            {
                "data": "CarrierName", "width": "100px", "orderable": false, "className": "text-center", "render": function (id) {
                    return EditCarrierImage(id);
                }
            },
            {
                "data": "Price", "width": "60px", "orderable": false, "className": "text-center", "render": function (id) {
                    return formatPrice(id);
                }
            },
            { "data": "CreateTime", "width": "115px", "className": "text-center", "orderable": false },
            {
                "data": "IsActive", "width": "85px", "className": "text-center", "orderable": false,
                "render": function (id) {
                    if (id === true) {
                        return "<span class='label label-success'>Hoạt động</span>";
                    }
                    else {
                        return "<span class='label label-danger'>Tắt</span>";
                    }
                }
            },
            {
                "data": "Id", "width": "150px", "className": "text-center", "render": function (id, type, row) {
                    var getdetail = "<a class='btn btn-default btn-xs' onclick=GetDetail('" + id + "') style='margin-right:3px;'><i class='fa fa-eye'></i> Chi tiết</a>";
                    var lockString = '';
                    if (row.IsActive) {
                        lockString = "<a class='btn btn-danger btn-xs' onclick=LockProduct('" + id + "') style='margin-right:3px;'><i class='fa fa-lock'></i> Tắt&nbsp;</a>";
                    }
                    else {
                        lockString = "<a class='btn btn-success btn-xs' onclick=UnLockProduct('" + id + "') style='margin-right:3px;'><i class='fa fa-unlock'></i> Mở</a>";
                    }
                    var deleteString = '';
                    if (row.IsSell) {
                        lockString = "<a class='btn btn-danger btn-xs disabled' style='margin-right:3px;'><i class='fa fa-lock'></i> Tắt&nbsp;</a>";
                        deleteString = "<a class='btn bg-purple btn-xs disabled' style='margin-right:3px;'><i class='fa fa-trash'></i> Xóa</a>";
                    }
                    else {
                        deleteString = "<a class='btn bg-purple btn-xs' onclick=DeleteProduct('" + id + "') style='margin-right:3px;'><i class='fa fa-trash'></i> Xóa</a>";
                    }
                    return getdetail + lockString + deleteString;
                },
                "orderable": false
            }

        ],
        "order": [],
        "drawCallback": function (settings) {
            var api = this.api();
            table.column(0, {}).nodes().each(function (cell, i) {
                cell.innerHTML = (i + 1) + (10 * (api.context[0].json.page));
            });
            // Output the data for the visible rows to the browser's console
        }
        //,
        //"initComplete": function (settings, json) {
        //    table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
        //        cell.innerHTML = i + 1;
        //    }).draw();
        //    //$('#num-product').html('Tìm thấy ' + json.recordsTotal + ' Sim');
        //}
    });
    //table.on('order.dt search.dt', function () {
    //    table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
    //        cell.innerHTML = i + 1;
    //    });
    //}).draw();
}
function SettingSelect2() {
    $('#iCarrier').select2({
        ajax: {
            url: '/AdminManager/GetAllCarrierSelect2',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: data.items
                };
            },
            delay: 100,
            cache: true
        }
        ,
        escapeMarkup: function (markup) { return markup; },
        templateResult: formatResult,
        templateSelection: formatSelection,
        placeholder: "Chọn nhà mạng...",
        minimumInputLength: 0,
        maximumSelectionLength: 1,
        multiple: true
    });
    $('#iCategory').select2({
        ajax: {
            url: '/AdminManager/GetAllCategorySelect2',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: data.items
                };
            },
            delay: 100,
            cache: true
        }
        ,
        escapeMarkup: function (markup) { return markup; },
        templateResult: formatResult,
        templateSelection: formatSelection,
        placeholder: "Chọn danh mục...",
        minimumInputLength: 0,
        maximumSelectionLength: 1,
        multiple: true
    });
    $('#pCarrier').select2({
        ajax: {
            url: '/AdminManager/GetAllCarrierSelect2',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: data.items
                };
            },
            delay: 100,
            cache: true
        }
        ,
        escapeMarkup: function (markup) { return markup; },
        templateResult: formatResult,
        templateSelection: formatSelection,
        placeholder: "Chọn nhà mạng...",
        minimumInputLength: 0,
        maximumSelectionLength: 1,
        multiple: true
    });
    $('#pCategory').select2({
        ajax: {
            url: '/AdminManager/GetAllCategorySelect2',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: data.items
                };
            },
            delay: 100,
            cache: true
        }
        ,
        escapeMarkup: function (markup) { return markup; },
        templateResult: formatResult,
        templateSelection: formatSelection,
        placeholder: "Chọn danh mục...",
        minimumInputLength: 0,
        maximumSelectionLength: 1,
        multiple: true
    });
}
function CreateProduct() {
    var regexNumber = /((09|03|02|01|04|06|07|08|05)+([0-9]{8})\b)/g;
    var regexPrice = new RegExp('^[0-9]+$');
    var productNumber = $('#iNumber').val();
    if (!productNumber.match(regexNumber)) {
        $.notify("Số SIM không hợp lệ", "error");
        return;
    }
    var productPrice = $('#iPrice').val();
    if (!productPrice.match(regexPrice)) {
        $.notify("Giá bán không hợp lệ", "error");
        return;
    }
    if (productPrice <= 0) {
        $.notify("Giá bán phải lớn hơn 0", "error");
        return;
    }
    var productCarrierCheck = $('#iCarrier').val();
    if (productCarrierCheck.length === 0) {
        $.notify("Chọn một nhà mạng cho SIM này", "error");
        return;
    }
    var productCarrier = productCarrierCheck[0];
    var productCategoryCheck = $('#iCategory').val();
    var productCategory = '';
    if (productCategoryCheck.length === 1) {
        productCategory = productCategoryCheck[0];
    }
    var productDes = $('#iDescription').val();
    var isActive = $('#iIsActive').is(":checked");

    var productModel = {
        Number: productNumber,
        CarrierId: productCarrier,
        Price: productPrice,
        CategoryId: productCategory,
        Description: productDes,
        IsActive: isActive
    };
    $.ajax({
        url: '/AdminManager/CreateProduct',
        data: { jsonProduct: JSON.stringify(productModel) },
        type: 'post',
        dataType: 'json',
        success: function (res) {
            var data = res.data;
            if (data.Status) {
                $('#modal-create-product').modal('hide');
                $('#iNumber').val('');
                $('#iDescription').val('');
                $('#iPrice').val('');
                $('#iCarrier').val(null).trigger('change');
                $('#iCategory').val(null).trigger('change');
                GetAllProduct();
                $.notify(data.Message, "success");
            }
            else {
                $.notify(data.Message, "error");
            }
        },
        error: function () {
            $.notify("Lỗi không xác định", "error");
        }
    });
}
function GetDetail(productId) {
    $('#pCarrier').val(null).trigger('change');
    $('#pCategory').val(null).trigger('change');
    $.ajax({
        url: '/AdminManager/GetInfoProduct',
        data: { productId: productId },
        type: 'get',
        dataType: 'json',
        success: function (res) {
            var data = res.data;
            $('#pId').val(data.Id);
            $('#pNumber').val(data.Number);
            $('#pPrice').val(data.Price);
            $('#pDescription').val(data.Description);
            var newOptionCarrier = new Option(data.CarrierName, data.CarrierId, true, true);
            $('#pCarrier').append(newOptionCarrier).trigger('change');
            var newOptionCategory = new Option(data.CategoryName, data.CategoryId, true, true);
            $('#pCategory').append(newOptionCategory).trigger('change');
            $('#modal-info-product').modal('show');
        },
        error: function () {
            $('#errorLoadModal').modal('show');
        }
    });
}
function UpdateProduct() {
    var regexPrice = new RegExp('^[0-9]+$');
    var productPrice = $('#pPrice').val();
    if (!productPrice.match(regexPrice)) {
        $.notify("Giá bán không hợp lệ", "error");
        return;
    }
    if (productPrice <= 0) {
        $.notify("Giá bán phải lớn hơn 0", "error");
        return;
    }
    var productCarrierCheck = $('#pCarrier').val();
    if (productCarrierCheck.length === 0) {
        $.notify("Chọn một nhà mạng cho SIM này", "error");
        return;
    }
    var productCarrier = productCarrierCheck[0];
    var productCategoryCheck = $('#pCategory').val();
    var productCategory = '';
    if (productCategoryCheck.length === 1) {
        productCategory = productCategoryCheck[0];
    }
    var productDes = $('#pDescription').val();
    var productId = $('#pId').val();
    var productModel = {
        Id: productId,
        CarrierId: productCarrier,
        Price: productPrice,
        CategoryId: productCategory,
        Description: productDes
    };
    $.ajax({
        url: '/AdminManager/UpdateProduct',
        data: { jsonProduct: JSON.stringify(productModel) },
        type: 'post',
        dataType: 'json',
        success: function (res) {
            var data = res.data;
            if (data.Status) {
                $('#modal-info-product').modal('hide');
                GetAllProduct();
                $.notify(data.Message, "success");
            }
            else {
                $.notify(data.Message, "error");
            }
        },
        error: function () {
            $.notify("Lỗi không xác định", "error");
        }
    });
}
function LockProduct(productId) {
    $.confirm({
        title: "Thông báo",
        content: "Tắt hoạt động mặt hàng này?",
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                $.ajax({
                    url: '/AdminManager/IsActiveProduct',
                    data: { productId: productId, isActive: false },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            GetAllProduct();
                            $.notify(data.Message, "success");
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}
function UnLockProduct(productId) {
    $.confirm({
        title: "Thông báo",
        content: "Mở hoạt động mặt hàng này?",
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                $.ajax({
                    url: '/AdminManager/IsActiveProduct',
                    data: { productId: productId, isActive: true },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            GetAllProduct();
                            $.notify(data.Message, "success");
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}
function DeleteProduct(productId) {
    $.confirm({
        title: "Thông báo",
        content: "Bạn muốn xóa mặt hàng này?",
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                $.ajax({
                    url: '/AdminManager/DeleteProduct',
                    data: { productId: productId },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            GetAllProduct();
                            $.notify(data.Message, "success");
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}