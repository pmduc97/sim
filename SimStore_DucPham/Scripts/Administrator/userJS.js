﻿$(function () {
    GetAll();
});
function GetAll() {
    var table = $('#user-table').DataTable({
        destroy: true,
        "ajax": {
            "url": "/AdminManager/GetAllUser",
            "type": "get",
            "dataType": "json"
        },
        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Xem _MENU_ mục",
            "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
            "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Đầu",
                "sPrevious": "Trước",
                "sNext": "Tiếp",
                "sLast": "Cuối"
            }
        },
        "columns": [
            { "data": null, "width": "30px", "orderable": false, "className": "text-center" },
            { "data": "FullName", "width": "150px" },
            { "data": "Email", "width": "150px", "orderable": false },
            { "data": "CreateTime", "width": "100px", "className": "text-center" },
            {
                "data": "IsActive", "width": "100px", "orderable": false, "className": "text-center",
                "render": function (id) {
                    if (id === true) {
                        return "<span class='label label-success'>Hoạt động</span>";
                    }
                    else {
                        return "<span class='label label-danger'>Khóa</span>";
                    }
                }
            },
            {
                "data": "Id", "width": "200px", "render": function (id, type, row) {
                    if (row.FullName == 'DefaultCustomer') {
                        return "<span class='btn btn-default btn-xs disabled f-right'>Mặc định</span>";
                    }
                    var getdetail = "<a class='btn btn-default btn-xs' onclick=GetDetailUser('" + id + "') style='margin-right:3px;'><i class='fa fa-eye'></i> Chi tiết</a>";
                    var roleString = '';
                    var lockString = '';
                    if (row.IsActive) {
                        lockString = "<a class='btn btn-danger btn-xs' onclick=LockUser('" + id + "') style='margin-right:3px;'><i class='fa fa-lock'></i> Khóa</a>";
                    }
                    else {
                        lockString = "<a class='btn btn-warning btn-xs' onclick=UnLockUser('" + id + "') style='margin-right:3px;'><i class='fa fa-unlock'></i>  Mở&nbsp;&nbsp;</a>";
                    }
                    if (row.RoleId === 'admin') {
                        var upRole = "<a class='btn btn-primary btn-xs' onclick=ChangeUserToCustomer('" + id + "') style='margin-right:3px;'><i class='fa fa-angle-double-down'></i> &nbsp;&nbsp;Hạ&nbsp;&nbsp;</a>";
                        roleString = upRole + "<span class='btn btn-info btn-xs disabled f-right'>Quản trị</span>";
                    }
                    else {
                        var upRole = "<a class='btn btn-primary btn-xs' onclick=ChangeUserToAdmin('" + id + "') style='margin-right:3px;'><i class='fa fa-angle-double-up'></i> Nâng</a>";
                        roleString = upRole + "<span class='btn btn-default btn-xs disabled f-right'>Khách hàng</span>";
                    }
                    return getdetail + lockString + roleString;
                },
                "orderable": false
            }

        ],
        "order": [[3, 'asc']]
    });
    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}
function GetDetailUser(userId) {
    $.ajax({
        url: '/AdminManager/GetInfoUser',
        data: { userId: userId },
        type: 'get',
        dataType: 'json',
        success: function (res) {
            var data = res.data;
            $('#pIdUser_User').val(data.Id);
            $('#pAvatarUser').html(AddAvatar(data.Avatar));
            $('#pFullNameUser').val(data.FullName);
            $('#pCreateTimeUser').val(data.CreateTime);
            $('#pEmailUser').val(data.Email);
            $('#pPhoneNumberUser').val(data.PhoneNumber);
            $('#pAddressUser').val(data.Address);
            $('#pIsActiveUser').val(ConvertActive(data.IsActive));
            $('#pRoleIdUser').val(ConvertRole(data.RoleId));
            $('#profileModalUser').modal('show');
        },
        error: function () {
            $('#errorLoadModal').modal('show');
        }
    });
}
function LockUser(userId) {
    $.confirm({
        title: "Thông báo",
        content: ConfirmUser.Lock,
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                $.ajax({
                    url: '/AdminManager/LockUser',
                    data: { userId: userId },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            GetAll();
                            $.notify(data.Message, "success");
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}
function UnLockUser(userId) {
    $.confirm({
        title: "Thông báo",
        content: ConfirmUser.UnLock,
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                $.ajax({
                    url: '/AdminManager/UnLockUser',
                    data: { userId: userId },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            GetAll();
                            $.notify(data.Message, "success");
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}
function ChangeUserToAdmin(userId) {
    $.confirm({
        title: "Thông báo",
        content: ConfirmUser.UpRole,
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                $.ajax({
                    url: '/AdminManager/ChangeUserToAdmin',
                    data: { userId: userId },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            GetAll();
                            $.notify(data.Message, "success");
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}
function ChangeUserToCustomer(userId) {
    $.confirm({
        title: "Thông báo",
        content: ConfirmUser.DownRole,
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                $.ajax({
                    url: '/AdminManager/ChangeUserToCustomer',
                    data: { userId: userId },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            GetAll();
                            $.notify(data.Message, "success");
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}
function UpdateProfileUser() {
    var userId = $('#pIdUser_User').val();
    var fullName = $('#pFullNameUser').val();
    var email = $('#pEmailUser').val();
    var phoneNumber = $('#pPhoneNumberUser').val();
    var address = $('#pAddressUser').val();
    $.ajax({
        url: '/AdminManager/UpdateProfile',
        data: {
            userId: userId,
            fullName: fullName,
            email: email,
            phoneNumber: phoneNumber,
            address: address
        },
        type: 'post',
        dataType: 'json',
        success: function (res) {
            var data = res.data;
            if (data.Status) {
                GetAll();
                $('#profileModalUser').modal('hide');
                $.notify(data.Message, "success");
            }
            else {
                $.notify(data.Message, "error");
            }
        },
        error: function () {
            $.notify("Lỗi không xác định", "error");
        }
    });
}