﻿$(function () {
    GetAllRole();
})
function GetAllRole() {
    $.ajax({
        url: '/AdminManager/GetAllRole',
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            var data = res.data;
            var html = '';
            $.each(data, function (key, item) {
                html += "<tr><td class='text-center'>" + (key + 1) + "</td><td>" + item.Name + "</td></tr>";
            });
            $('#content-table').html(html);
        },
        error: function (e) {
            $('#content-table').html('');
            alert("Có lỗi: không thể tải được dữ liệu");

        }
    });
}