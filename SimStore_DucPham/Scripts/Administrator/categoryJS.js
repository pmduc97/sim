﻿$(function () {
    GetAll();
    $('#add-category').off('click').on('click', function () {
        CreateCategory();
    });
    $('#update-category').off('click').on('click', function () {
        UpdateCategory();
    });
});
function GetAll() {
    var table = $('#category-table').DataTable({
        destroy: true,
        "ajax": {
            "url": "/AdminManager/GetAllCategory",
            "type": "get",
            "dataType": "json"
        },
        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Xem _MENU_ mục",
            "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
            "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Đầu",
                "sPrevious": "Trước",
                "sNext": "Tiếp",
                "sLast": "Cuối"
            }
        },
        "columns": [
            { "data": null, "width": "30px", "orderable": false, "className": "text-center" },
            {
                "data": "Name", "width": "300px", "render": function (id, type, row) {
                    return id + "<span class='label label-info f-right'>" + row.CountProduct + " mặt hàng" + "</span>";
                }
            },
            { "data": "Description", "autoWidth": true, "orderable": false },
            {
                "data": "IsActive", "width": "100px", "className": "text-center",
                "render": function (id, type, row) {
                    if (row.Id === defaultCategory) {
                        return "<span class='label label-default'>Mặc định</span>";
                    }
                    if (id === true) {
                        return "<span class='label label-success'>Hoạt động</span>";
                    }
                    else {
                        return "<span class='label label-danger'>Tắt</span>";
                    }
                }
            },
            {
                "data": "Id", "width": "200px", "className": "text-center", "render": function (id, type, row) {
                    var getdetail = "<a class='btn btn-default btn-xs' onclick=GetDetail('" + id + "') style='margin-right:3px;'><i class='fa fa-eye'></i> Chi tiết</a>";
                    var lockString = '';
                    if (row.IsActive) {
                        lockString = "<a class='btn btn-danger btn-xs' onclick=LockCategory('" + id + "') style='margin-right:3px;'><i class='fa fa-lock'></i> Tắt&nbsp;</a>";
                    }
                    else {
                        lockString = "<a class='btn btn-success btn-xs' onclick=UnLockCategory('" + id + "') style='margin-right:3px;'><i class='fa fa-unlock'></i> Mở</a>";
                    }
                    
                    var deleteString = '';
                    if (row.CountProduct === 0) {
                        deleteString = "<a class='btn bg-purple btn-xs' onclick=DeleteCategory('" + id + "')  style='margin-right:3px;'><i class='fa fa-trash'></i> Xóa</a>";
                    }
                    else {
                        deleteString = "<a class='btn bg-purple btn-xs disabled'  style='margin-right:3px;'><i class='fa fa-trash'></i> Xóa</a>";
                    }
                    if (row.Id === defaultCategory) {
                        lockString = "<a class='btn btn-success btn-xs disabled' style='margin-right:3px;'><i class='fa fa-unlock'></i> Mở</a></a>";
                        deleteString = "<a class='btn bg-purple btn-xs disabled'  style='margin-right:3px;'><i class='fa fa-trash'></i> Xóa</a>";
                    }
                    return getdetail + lockString + deleteString;
                },
                "orderable": false
            }

        ],
        "order": [[1, 'asc']]
    });
    table.on('order.dt search.dt', function () {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}
function CreateCategory() {
    var categoryName = $('#iName').val();
    if (categoryName == null || categoryName == '') {
        $.notify("Tên danh mục không được rỗng", "error");
        return;
    }
    var categoryDes = $('#iDescription').val();
    var isActive = $('#iIsActive').is(":checked");
    var categoryModel = {
        Name: categoryName,
        Description: categoryDes,
        IsActive: isActive
    };
    $.ajax({
        url: '/AdminManager/CreateCategory',
        data: { categoryStr: JSON.stringify(categoryModel) },
        type: 'post',
        dataType: 'json',
        success: function (res) {
            var data = res.data;
            if (data.Status) {
                $('#modal-category').modal('hide');
                $('#iName').val('');
                $('#iDescription').val('');
                GetAll();
                $.notify(data.Message, "success");
            }
            else {
                $.notify(data.Message, "error");
            }
        },
        error: function () {
            $.notify("Lỗi không xác định", "error");
        }
    });
}
function GetDetail(categoryId) {
    $.ajax({
        url: '/AdminManager/GetInfoCategory',
        data: { categoryId: categoryId },
        type: 'get',
        dataType: 'json',
        success: function (res) {
            var data = res.data;
            $('#pId').val(data.Id);
            $('#pName').val(data.Name);
            $('#pDescription').val(data.Description);
            $('#category-info').modal('show');
        },
        error: function () {
            $('#errorLoadModal').modal('show');
        }
    });
}
function UpdateCategory() {
    var categoryName = $('#pName').val();
    if (categoryName == null || categoryName == '') {
        $.notify("Tên danh mục không được rỗng", "error");
        return;
    }
    var categoryDes = $('#pDescription').val();
    var categoryId = $('#pId').val();
    $.ajax({
        url: '/AdminManager/UpdateCategory',
        data: { categoryId: categoryId, categoryName: categoryName, categoryDescription: categoryDes },
        type: 'post',
        dataType: 'json',
        success: function (res) {
            var data = res.data;
            if (data.Status) {
                GetAll();
                $('#category-info').modal('hide');

                $.notify(data.Message, "success");
            }
            else {
                $.notify(data.Message, "error");
            }
        },
        error: function () {
            $('#errorLoadModal').modal('show');
        }
    });
}
function LockCategory(categoryId) {
    $.confirm({
        title: "Thông báo",
        content: "Tắt hoạt động danh mục này?",
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                $.ajax({
                    url: '/AdminManager/IsActiveCategory',
                    data: { categoryId: categoryId, isActive: false },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            GetAll();
                            $.notify(data.Message, "success");
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}
function UnLockCategory(categoryId) {
    $.confirm({
        title: "Thông báo",
        content: "Mở hoạt động danh mục này?",
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                $.ajax({
                    url: '/AdminManager/IsActiveCategory',
                    data: { categoryId: categoryId, isActive: true },
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            GetAll();
                            $.notify(data.Message, "success");
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}
function DeleteCategory(categoryId) {
    $.confirm({
        title: "Thông báo",
        content: "Bạn muốn xóa danh mục này?",
        buttons: {
            'Hủy bỏ': function () {
                return;
            },
            'Đồng ý': function () {
                $.ajax({
                    url: '/AdminManager/DeleteCategory',
                    data: { categoryId: categoryId},
                    type: 'post',
                    dataType: 'json',
                    success: function (res) {
                        var data = res.data;
                        if (data.Status) {
                            GetAll();
                            $.notify(data.Message, "success");
                        }
                        else {
                            $.notify(data.Message, "error");
                        }
                    },
                    error: function () {
                        $.notify("Lỗi không xác định", "error");
                    }
                });
            }
        }
    });
}