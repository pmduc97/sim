﻿using SimStore_DucPham.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SimStore_DucPham.Helper
{
    public class InfoHelper
    {
        public static string GetFullNameByUserId(string userId)
        {
            string fullName = "Không xác định";
            string sqlQuery = "SELECT FullName FROM AspNetUsers WHERE Id = @UserId";
            SqlConnectData sqlConnectData = new SqlConnectData();
            List<SqlParameter> paras = new List<SqlParameter>()
            {
                new SqlParameter("UserId",userId)
            };
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnectData.conn);
            ParameterHelper.AddParameter(sqlCommand, paras);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                fullName = sqlDataReader["FullName"].ToString();
            }
            sqlDataReader.Close();
            sqlConnectData.conn.Close();
            return fullName;
        }
    }
}