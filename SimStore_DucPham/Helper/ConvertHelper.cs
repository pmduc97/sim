﻿using SimStore_DucPham.Models;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace SimStore_DucPham.Helper
{
    public class ConvertHelper
    {
        /// <summary>
        /// Chuyển đổi string time 12h sang string time 24h
        /// </summary>
        /// <param name="dateString"></param>
        /// <returns></returns>
        public static string DateTimeTo24h(string dateString)
        {
            DateTime date = DateTime.Parse(dateString);
            return date.ToString("dd/MM/yyyy HH:mm:ss");
        }

        /// <summary>
        /// Chuyển list carrier sang list chuẩn Select2
        /// </summary>
        /// <param name="carriers"></param>
        /// <returns></returns>
        public static List<Select2Model> CarrierToSelect2(List<CarrierModel> carriers)
        {
            List<Select2Model> select2s = new List<Select2Model>();
            foreach (CarrierModel carrier in carriers)
            {
                Select2Model select2 = new Select2Model()
                {
                    id = carrier.Id,
                    text = carrier.Name,
                    IsActive = carrier.IsActive ? "Hoạt động" : "Khóa"
                };
                select2s.Add(select2);
            }
            return select2s;
        }

        /// <summary>
        /// Chuyển list category sang list chuẩn Select2
        /// </summary>
        /// <param name="categories"></param>
        /// <returns></returns>
        public static List<Select2Model> CategoryToSelect2(List<CategoryModel> categories)
        {
            List<Select2Model> select2s = new List<Select2Model>();
            foreach (CategoryModel carrier in categories)
            {
                Select2Model select2 = new Select2Model()
                {
                    id = carrier.Id,
                    text = carrier.Name,
                    IsActive = carrier.IsActive ? "Hoạt động" : "Khóa"
                };
                if (!select2.id.Equals("DefaultCategory"))
                {
                    select2s.Add(select2);
                }
            }
            return select2s;
        }

        /// <summary>
        /// Định dạng số điện thoại có dấu chấm
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public static string FormatPhoneNumber(string phoneNumber)
        {
            return Regex.Replace(phoneNumber, @"(\d{4})(\d{3})(\d{3})", "$1.$2.$3");
        }

        /// <summary>
        /// Định dang tiền tệ sang tiền việt nam
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        public static string FormatPrice(long price)
        {
            return string.Format("{0:0,0 ₫}", price).Replace(",", ".");
        }

        /// <summary>
        /// Chuyển đổi khoảng tiền sang chữ phù hợp
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string FormatNamePriceProduct(string id)
        {
            string result = string.Empty;
            switch (id)
            {
                case "0-500000":
                    result = "Sim dưới 500.000đ";
                    break;

                case "500000-1000000":
                    result = "Sim 500 đến 1 triệu";
                    break;

                case "1000000-3000000":
                    result = "Sim 1 đến 3 triệu";
                    break;

                case "3000000-5000000":
                    result = "Sim 3 đến 5 triệu";
                    break;

                case "5000000-10000000":
                    result = "Sim 5 đến 10 triệu";
                    break;

                default:
                    result = "Sim trên 10 triệu";
                    break;
            }
            return result;
        }

        /// <summary>
        /// Chuyển đổi định dạng tìm kiếm số sang chữ phù hợp
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static KeyModel FormatNameNumberFind(string number)
        {
            string star = "*";
            int lengthNumber = number.Length;
            if (number.IndexOf(star) == -1)
            {
                return new KeyModel()
                {
                    Id = 1,
                    Name = "Tìm Sim có chứa " + number
                };
            }
            if (number.IndexOf(star) == 0)
            {
                return new KeyModel()
                {
                    Id = 2,
                    Name = "Tìm Sim có đuôi là: " + number.Split('*')[1]
                };
            }
            if (number.IndexOf(star) == lengthNumber - 1)
            {
                return new KeyModel()
                {
                    Id = 3,
                    Name = "Tìm Sim có đầu là: " + number.Split('*')[0]
                };
            }
            if (number.IndexOf(star) > 0 && number.IndexOf(star) < lengthNumber - 1)
            {
                return new KeyModel()
                {
                    Id = 4,
                    Name = "Tìm Sim có đầu " + number.Split('*')[0] + " và đuôi " + number.Split('*')[1]
                };
            }
            return new KeyModel()

            {
                Id = -1,
                Name = string.Empty
            };
        }

        /// <summary>
        /// Chuyển đổi carrier sang Class ảnh của carrier đó
        /// </summary>
        /// <param name="carrierId"></param>
        /// <returns></returns>
        public static string ConvertCarrierClass(string carrierId)
        {
            string classResult = "default3";
            if (carrierId.Equals("viettel"))
            {
                classResult = "Viettel3";
            }
            if (carrierId.Equals("mobifone"))
            {
                classResult = "Mobifone3";
            }
            if (carrierId.Equals("vinaphone"))
            {
                classResult = "Vinaphone3";
            }
            if (carrierId.Equals("vietnammobile"))
            {
                classResult = "Vietnammobile3";
            }
            return classResult;
        }

        /// <summary>
        /// Chuyển đổi trạng thái không dấu sang có dấu
        /// </summary>
        /// <param name="oldStatus"></param>
        /// <returns></returns>
        public static string ConvertStatus(string oldStatus)
        {
            string newStatus = "Đợi xác nhận";
            switch (oldStatus)
            {
                case "DaXacNhan":
                    newStatus = "Đã xác nhận";
                    break;

                case "DangGiaoHang":
                    newStatus = "Đang giao hàng";
                    break;

                case "DaGiaoHang":
                    newStatus = "Đã giao hàng";
                    break;

                case "HuyBo":
                    newStatus = "Hủy bỏ";
                    break;

                case "DoiXacNhanMail":
                    newStatus = "Đợi xác nhận mail";
                    break;
            }
            return newStatus;
        }

        public static string GetClassByStatus(string status)
        {
            if (status.Equals("Đợi xác nhận") || status.Equals("Đợi xác nhận mail"))
            {
                return "warning";
            }
            else if (status.Equals("Đã xác nhận"))
            {
                return "primary";
            }
            else if (status.Equals("Đang giao hàng"))
            {
                return "info";
            }
            else if (status.Equals("Đã giao hàng"))
            {
                return "success";
            }
            else
            {
                return "danger";
            }
        }

        #region Chuyển đổi tiền sang chữ

        public static string PriceToString(long gNum)
        {
            if (gNum == 0)
                return "Không đồng";

            string lso_chu = "";
            string tach_mod = "";
            string tach_conlai = "";
            double Num = Math.Round(double.Parse(gNum.ToString()), 0);
            string gN = Convert.ToString(Num);
            int m = Convert.ToInt32(gN.Length / 3);
            int mod = gN.Length - m * 3;
            string dau = "[+]";

            // Dau [+ , - ]
            if (gNum < 0)
                dau = "[-]";
            dau = "";

            // Tach hang lon nhat
            if (mod.Equals(1))
                tach_mod = "00" + Convert.ToString(Num.ToString().Trim().Substring(0, 1)).Trim();
            if (mod.Equals(2))
                tach_mod = "0" + Convert.ToString(Num.ToString().Trim().Substring(0, 2)).Trim();
            if (mod.Equals(0))
                tach_mod = "000";
            // Tach hang con lai sau mod :
            if (Num.ToString().Length > 2)
                tach_conlai = Convert.ToString(Num.ToString().Trim().Substring(mod, Num.ToString().Length - mod)).Trim();

            ///don vi hang mod
            int im = m + 1;
            if (mod > 0)
                lso_chu = Tach(tach_mod).ToString().Trim() + " " + Donvi(im.ToString().Trim());
            /// Tach 3 trong tach_conlai

            int i = m;
            int _m = m;
            int j = 1;
            string tach3 = "";
            string tach3_ = "";

            while (i > 0)
            {
                tach3 = tach_conlai.Trim().Substring(0, 3).Trim();
                tach3_ = tach3;
                lso_chu = lso_chu.Trim() + " " + Tach(tach3.Trim()).Trim();
                m = _m + 1 - j;
                if (!tach3_.Equals("000"))
                    lso_chu = lso_chu.Trim() + " " + Donvi(m.ToString().Trim()).Trim();
                tach_conlai = tach_conlai.Trim().Substring(3, tach_conlai.Trim().Length - 3);

                i = i - 1;
                j = j + 1;
            }
            if (lso_chu.Trim().Substring(0, 1).Equals("k"))
                lso_chu = lso_chu.Trim().Substring(10, lso_chu.Trim().Length - 10).Trim();
            if (lso_chu.Trim().Substring(0, 1).Equals("l"))
                lso_chu = lso_chu.Trim().Substring(2, lso_chu.Trim().Length - 2).Trim();
            if (lso_chu.Trim().Length > 0)
                lso_chu = dau.Trim() + " " + lso_chu.Trim().Substring(0, 1).Trim().ToUpper() + lso_chu.Trim().Substring(1, lso_chu.Trim().Length - 1).Trim() + " đồng chẵn.";

            return lso_chu.ToString().Trim();
        }

        private static string Chu(string gNumber)
        {
            string result = "";
            switch (gNumber)
            {
                case "0":
                    result = "không";
                    break;

                case "1":
                    result = "một";
                    break;

                case "2":
                    result = "hai";
                    break;

                case "3":
                    result = "ba";
                    break;

                case "4":
                    result = "bốn";
                    break;

                case "5":
                    result = "năm";
                    break;

                case "6":
                    result = "sáu";
                    break;

                case "7":
                    result = "bảy";
                    break;

                case "8":
                    result = "tám";
                    break;

                case "9":
                    result = "chín";
                    break;
            }
            return result;
        }

        private static string Donvi(string so)
        {
            string Kdonvi = "";

            if (so.Equals("1"))
                Kdonvi = "";
            if (so.Equals("2"))
                Kdonvi = "nghìn";
            if (so.Equals("3"))
                Kdonvi = "triệu";
            if (so.Equals("4"))
                Kdonvi = "tỷ";
            if (so.Equals("5"))
                Kdonvi = "nghìn tỷ";
            if (so.Equals("6"))
                Kdonvi = "triệu tỷ";
            if (so.Equals("7"))
                Kdonvi = "tỷ tỷ";

            return Kdonvi;
        }

        private static string Tach(string tach3)
        {
            string Ktach = "";
            if (tach3.Equals("000"))
                return "";
            if (tach3.Length == 3)
            {
                string tr = tach3.Trim().Substring(0, 1).ToString().Trim();
                string ch = tach3.Trim().Substring(1, 1).ToString().Trim();
                string dv = tach3.Trim().Substring(2, 1).ToString().Trim();
                if (tr.Equals("0") && ch.Equals("0"))
                    Ktach = " không trăm lẻ " + Chu(dv.ToString().Trim()) + " ";
                if (!tr.Equals("0") && ch.Equals("0") && dv.Equals("0"))
                    Ktach = Chu(tr.ToString().Trim()).Trim() + " trăm ";
                if (!tr.Equals("0") && ch.Equals("0") && !dv.Equals("0"))
                    Ktach = Chu(tr.ToString().Trim()).Trim() + " trăm lẻ " + Chu(dv.Trim()).Trim() + " ";
                if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = " không trăm " + Chu(ch.Trim()).Trim() + " mươi " + Chu(dv.Trim()).Trim() + " ";
                if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && dv.Equals("0"))
                    Ktach = " không trăm " + Chu(ch.Trim()).Trim() + " mươi ";
                if (tr.Equals("0") && Convert.ToInt32(ch) > 1 && dv.Equals("5"))
                    Ktach = " không trăm " + Chu(ch.Trim()).Trim() + " mươi lăm ";
                if (tr.Equals("0") && ch.Equals("1") && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = " không trăm mười " + Chu(dv.Trim()).Trim() + " ";
                if (tr.Equals("0") && ch.Equals("1") && dv.Equals("0"))
                    Ktach = " không trăm mười ";
                if (tr.Equals("0") && ch.Equals("1") && dv.Equals("5"))
                    Ktach = " không trăm mười lăm ";
                if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm " + Chu(ch.Trim()).Trim() + " mươi " + Chu(dv.Trim()).Trim() + " ";
                if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && dv.Equals("0"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm " + Chu(ch.Trim()).Trim() + " mươi ";
                if (Convert.ToInt32(tr) > 0 && Convert.ToInt32(ch) > 1 && dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm " + Chu(ch.Trim()).Trim() + " mươi lăm ";
                if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && Convert.ToInt32(dv) > 0 && !dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm mười " + Chu(dv.Trim()).Trim() + " ";

                if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && dv.Equals("0"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm mười ";
                if (Convert.ToInt32(tr) > 0 && ch.Equals("1") && dv.Equals("5"))
                    Ktach = Chu(tr.Trim()).Trim() + " trăm mười lăm ";
            }

            return Ktach;
        }

        #endregion Chuyển đổi tiền sang chữ
    }
}