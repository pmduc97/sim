﻿using Quartz;
using Quartz.Impl;
using SimStore_DucPham.Data;

namespace SimStore_DucPham.Helper
{
    public class TaskJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            BillData billData = new BillData();
            //Task
            billData.HuyDonHangKhongXacNhanMail();
        }
    }

    public class TaskTrigger
    {
        /// <summary>
        /// Bắt đầu sau secon giây
        /// </summary>
        /// <param name="secon"></param>
        public static void Start(int secon)
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<TaskJob>().Build();

            ITrigger trigger = TriggerBuilder.Create()
            .WithIdentity("trigger1", "group1")
            .StartNow()
            .WithSimpleSchedule(x => x
            .WithIntervalInSeconds(secon)
            .RepeatForever())
            .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }
}