﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace SimStore_DucPham.Helper
{
    public class ParameterHelper
    {
        /// <summary>
        /// Thêm parameter vaof sqlCommand
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <param name="paras"></param>
        public static void AddParameter(SqlCommand sqlCommand, List<SqlParameter> paras)
        {
            foreach (SqlParameter para in paras)
            {
                sqlCommand.Parameters.Add(para);
            }
        }
    }
}