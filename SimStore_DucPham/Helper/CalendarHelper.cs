﻿using SimStore_DucPham.Models;
using System;

namespace SimStore_DucPham.Helper
{
    public class CalendarHelper
    {
        /// <summary>
        /// Giá trị mặt định của PI
        /// </summary>
        private static readonly double PI = Math.PI;

        /// <summary>
        /// Get full thông tin ngày dương lịch nhập vào
        /// </summary>
        /// <param name="dateDuong"></param>
        /// <param name="timeZone"></param>
        /// <returns></returns>
        public static DateViewModel GetInfoDateByDuongLich(DateModel dateDuong, int timeZone)
        {
            DateViewModel date = new DateViewModel()
            {
                GioiTinh = dateDuong.GioiTinh.Equals("nam") ? "Nam" : "Nữ",
                DuongLich = new DuongLichModel()
                {
                    Day = dateDuong.Day,
                    Month = dateDuong.Month,
                    Year = dateDuong.Year,
                    NgayThu = FormatNgayThu(TinhNgayThu(dateDuong)),
                    CungHoangDao = FormatCungHoangDao(dateDuong)
                },
                AmLich = GetInfoAmLich(dateDuong, timeZone)
            };
            return date;
        }

        /// <summary>
        /// Đổi ngày dương ra ngày âm
        /// </summary>
        /// <param name="dateDuong"></param>
        /// <returns></returns>
        public static DateModel ChuyenDuongSangAm(DateModel dateDuong, int timeZone)
        {
            int k, dayNumber, monthStart, a11, b11, lunarDay, lunarMonth, lunarYear;
            bool lunarLeap = false;
            dayNumber = JdFromDate(dateDuong);
            k = Convert.ToInt32((dayNumber - 2415021.076998695) / 29.530588853);
            monthStart = GetNewMoonDay(k + 1, timeZone);
            if (monthStart > dayNumber)
            {
                monthStart = GetNewMoonDay(k, timeZone);
            }
            a11 = GetLunarMonth11(dateDuong.Year, timeZone);
            b11 = a11;
            if (a11 >= monthStart)
            {
                lunarYear = dateDuong.Year;
                a11 = GetLunarMonth11(dateDuong.Year - 1, timeZone);
            }
            else
            {
                lunarYear = dateDuong.Year + 1;
                b11 = GetLunarMonth11(dateDuong.Year + 1, timeZone);
            }
            lunarDay = dayNumber - monthStart + 1;
            int diff = Convert.ToInt32((monthStart - a11) / 29);
            lunarMonth = diff + 11;
            if (b11 - a11 > 365)
            {
                int leapMonthDiff = GetLeapMonthOffset(a11, timeZone);
                if (diff >= leapMonthDiff)
                {
                    lunarMonth = diff + 10;
                    if (diff == leapMonthDiff)
                    {
                        lunarLeap = true;
                    }
                }
            }
            if (lunarMonth > 12)
            {
                lunarMonth = lunarMonth - 12;
            }
            if (lunarMonth >= 11 && diff < 4)
            {
                lunarYear -= 1;
            }
            if (lunarYear > 1999)
            {
                lunarDay += 30;
                if (lunarDay > 30)
                {
                    lunarMonth++;
                    lunarDay -= 30;
                }
            }
            else
            {
                lunarDay += 29;
                if (lunarDay > 29)
                {
                    lunarMonth++;
                    lunarDay -= 29;
                }
            }
            if (lunarMonth > 12)
            {
                lunarYear++;
                lunarMonth -= 12;
            }

            return new DateModel()
            {
                Day = lunarDay,
                Month = lunarMonth,
                Year = lunarYear,
                LunarLeap = lunarLeap
            };
        }

        /// <summary>
        /// Đổi âm lịch ra dương lịch
        /// </summary>
        /// <param name="dateAm"></param>
        /// <param name="timeZone"></param>
        /// <returns></returns>
        public static DateModel ChuyenAmSangDuong(DateModel dateAm, int timeZone)
        {
            int k, a11, b11, off, leapOff, leapMonth, monthStart;
            if (dateAm.Month < 11)
            {
                a11 = GetLunarMonth11(dateAm.Year - 1, timeZone);
                b11 = GetLunarMonth11(dateAm.Year, timeZone);
            }
            else
            {
                a11 = GetLunarMonth11(dateAm.Year, timeZone);
                b11 = GetLunarMonth11(dateAm.Year + 1, timeZone);
            }
            off = dateAm.Month - 11;
            if (off < 0)
            {
                off += 12;
            }
            if (b11 - a11 > 365)
            {
                leapOff = GetLeapMonthOffset(a11, timeZone);
                leapMonth = leapOff - 2;
                if (leapMonth < 0)
                {
                    leapMonth += 12;
                }
                if (dateAm.LunarLeap && dateAm.Month != leapMonth)
                {
                    return new DateModel()
                    {
                        Day = 0,
                        Month = 0,
                        Year = 0
                    };
                }
                else if (dateAm.LunarLeap || off >= leapOff)
                {
                    off += 1;
                }
            }
            k = Convert.ToInt32(0.5 + (a11 - 2415021.076998695) / 29.530588853);
            monthStart = GetNewMoonDay(k + off, timeZone);
            return JdToDate(monthStart + dateAm.Day - 1);
        }

        /// <summary>
        /// Get thông tin và Can Chi của âm lịch
        /// </summary>
        /// <param name="dateDuong"></param>
        /// <param name="timeZone"></param>
        /// <returns></returns>
        private static AmLichModel GetInfoAmLich(DateModel dateDuong, int timeZone)
        {
            DateModel dateAm = ChuyenDuongSangAm(dateDuong, timeZone);
            AmLichModel amLichModel = new AmLichModel()
            {
                Day = dateAm.Day,
                Month = dateAm.Month,
                Year = dateAm.Year,
                LunarLeap = dateAm.LunarLeap,
                YearCanChi = FormatCanChiYear(dateAm),
                MonthCanChi = FormatCanChiMonth(dateAm),
                DayCanChi = FormatCanChiDay(dateDuong),
                MenhCungPhi = GetCungPhi(dateAm.Year, dateDuong.GioiTinh),
                MenhCungSinh = GetCungSinh(dateAm.Year),
                ConGiap = GetConGiap(dateAm.Year)
            };
            amLichModel.ImageConGiap = GetImageConGiapByYear(amLichModel.YearCanChi.Chi);

            return amLichModel;
        }

        /// <summary>
        /// Đổi số ngày Julius jd ra ngày dd/mm/yyyy dương lịch
        /// </summary>
        /// <param name="dateDuong"></param>
        /// <returns></returns>
        private static DateModel JdToDate(int jd)
        {
            int a, b, c;

            if (jd > 2299160)
            {
                // After 5/10/1582, Gregorian calendar
                a = jd + 32044;
                b = (4 * a + 3) / 146097;
                c = a - (b * 146097) / 4;
            }
            else
            {
                b = 0;
                c = jd + 32082;
            }

            int d = (4 * c + 3) / 1461;
            int e = c - (1461 * d) / 4;
            int m = (5 * e + 2) / 153;
            int dd = e - (153 * m + 2) / 5 + 1;
            int mm = m + 3 - 12 * (m / 10);
            int yy = b * 100 + d - 4800 + m / 10;
            return new DateModel()
            {
                Day = dd,
                Month = mm,
                Year = yy
            };
        }

        /// <summary>
        /// Đổi ngày dd/mm/yyyy dương lịch ra số ngày Julius jd
        /// </summary>
        /// <param name="dateDuong"></param>
        /// <returns></returns>
        private static int JdFromDate(DateModel dateDuong)
        {
            int a = (14 - dateDuong.Month) / 12;
            int y = dateDuong.Year + 4800 - a;
            int m = dateDuong.Month + 12 * a - 3;
            int jd = dateDuong.Day + (153 * m + 2) / 5 + 365 * y + y / 4 - y / 100 + y / 400 - 32045;
            if (jd < 2299161)
            {
                jd = dateDuong.Day + (153 * m + 2) / 5 + 365 * y + y / 4 - 32083;
            }
            return jd;
        }

        /// <summary>
        /// Thuật toán sau tính ngày Sóc thứ k kể từ điểm Sóc ngày 1/1/1900
        /// </summary>
        /// <param name="k"></param>
        /// <param name="timeZone"></param>
        /// <returns>Kết quả trả về là số ngày Julius của ngày Sóc cần tìm</returns>
        private static int GetNewMoonDay(int k, int timeZone)
        {
            double T, T2, T3, dr, Jd1, M, Mpr, F, C1, deltat, JdNew;
            T = k / 1236.85; // Time in Julian centuries from 1900 January 0.5
            T2 = T * T;
            T3 = T2 * T;
            dr = PI / 180;
            Jd1 = 2415020.75933 + 29.53058868 * k + 0.0001178 * T2 - 0.000000155 * T3;
            Jd1 = Jd1 + 0.00033 * Math.Sin((166.56 + 132.87 * T - 0.009173 * T2) * dr); // Mean new moon
            M = 359.2242 + 29.10535608 * k - 0.0000333 * T2 - 0.00000347 * T3; // Sun's mean anomaly
            Mpr = 306.0253 + 385.81691806 * k + 0.0107306 * T2 + 0.00001236 * T3; // Moon's mean anomaly
            F = 21.2964 + 390.67050646 * k - 0.0016528 * T2 - 0.00000239 * T3; // Moon's argument of latitude
            C1 = (0.1734 - 0.000393 * T) * Math.Sin(M * dr) + 0.0021 * Math.Sin(2 * dr * M);
            C1 = C1 - 0.4068 * Math.Sin(Mpr * dr) + 0.0161 * Math.Sin(dr * 2 * Mpr);
            C1 = C1 - 0.0004 * Math.Sin(dr * 3 * Mpr);
            C1 = C1 + 0.0104 * Math.Sin(dr * 2 * F) - 0.0051 * Math.Sin(dr * (M + Mpr));
            C1 = C1 - 0.0074 * Math.Sin(dr * (M - Mpr)) + 0.0004 * Math.Sin(dr * (2 * F + M));
            C1 = C1 - 0.0004 * Math.Sin(dr * (2 * F - M)) - 0.0006 * Math.Sin(dr * (2 * F + Mpr));
            C1 = C1 + 0.0010 * Math.Sin(dr * (2 * F - Mpr)) + 0.0005 * Math.Sin(dr * (2 * Mpr + M));
            if (T < -11)
            {
                deltat = 0.001 + 0.000839 * T + 0.0002261 * T2 - 0.00000845 * T3 - 0.000000081 * T * T3;
            }
            else
            {
                deltat = -0.000278 + 0.000265 * T + 0.000262 * T2;
            };
            JdNew = Jd1 + C1 - deltat;
            int result = Convert.ToInt32(JdNew + 0.5 + (timeZone / 24));
            return result;
        }

        /// <summary>
        /// Tính tọa độ mặt trời. Để biết Trung khí nào nằm trong tháng âm lịch nào, ta chỉ cần tính xem mặt trời nằm ở khoảng nào trên đường hoàng đạo vào thời điểm bắt đầu một tháng âm lịch. Ta chia đường hoàng đạo làm 12 phần và đánh số các cung này từ 0 đến 11: từ Xuân phân đến Cốc vũ là 0; từ Cốc vũ đến Tiểu mãn là 1; từ Tiểu mãn đến Hạ chí là 2; v.v.. Cho jdn là số ngày Julius của bất kỳ một ngày, phương pháp sau này sẽ trả lại số cung nói trên.
        /// </summary>
        /// <param name="jdn"></param>
        /// <param name="timeZone"></param>
        /// <returns></returns>
        private static int GetSunLongitude(int jdn, int timeZone)
        {
            double T, T2, dr, M, L0, DL, L;
            T = (jdn - 2451545.5 - timeZone / 24) / 36525; // Time in Julian centuries from 2000-01-01 12:00:00 GMT
            T2 = T * T;
            dr = PI / 180; // degree to radian
            M = 357.52910 + 35999.05030 * T - 0.0001559 * T2 - 0.00000048 * T * T2; // mean anomaly, degree
            L0 = 280.46645 + 36000.76983 * T + 0.0003032 * T2; // mean longitude, degree
            DL = (1.914600 - 0.004817 * T - 0.000014 * T2) * Math.Sin(dr * M);
            DL = DL + (0.019993 - 0.000101 * T) * Math.Sin(dr * 2 * M) + 0.000290 * Math.Sin(dr * 3 * M);
            L = L0 + DL; // true longitude, degree
            L = L * dr;
            L = L - PI * 2 * (Convert.ToInt32(L / (PI * 2))); // Normalize to (0, 2*PI)
            return Convert.ToInt32(L / PI * 6);
        }

        /// <summary>
        /// Tìm ngày bắt đầu tháng 11 âm lịch. Đông chí thường nằm vào khoảng 19/12-22/12, như vậy trước hết ta tìm ngày Sóc trước ngày 31/12. Nếu tháng bắt đầu vào ngày đó không chứa Đông chí thì ta phải lùi lại 1 tháng nữa.
        /// </summary>
        /// <param name="yy"></param>
        /// <param name="timeZone"></param>
        /// <returns></returns>
        private static int GetLunarMonth11(int yy, int timeZone)
        {
            int k, off, nm, sunLong;
            DateModel dateTime = new DateModel()
            {
                Day = 31,
                Month = 12,
                Year = yy
            };
            off = JdFromDate(dateTime) - 2415021;
            k = Convert.ToInt32(off / 29.530588853);
            nm = GetNewMoonDay(k, timeZone);
            sunLong = GetSunLongitude(nm, timeZone); // sun longitude at local midnight
            if (sunLong >= 9)
            {
                nm = GetNewMoonDay(k - 1, timeZone);
            }
            return nm;
        }

        /// <summary>
        /// Xác định tháng nhuận. Nếu giữa hai tháng 11 âm lịch (tức tháng có chứa Đông chí) có 13 tháng âm lịch thì năm âm lịch đó có tháng nhuận. Để xác định tháng nhuận, ta sử dụng hàm getSunLongitude như đã nói ở trên. Cho a11 là ngày bắt đầu tháng 11 âm lịch mà một trong 13 tháng sau đó là tháng nhuận. Hàm sau cho biết tháng nhuận nằm ở vị trí nào sau tháng 11 này.
        /// </summary>
        /// <returns></returns>
        private static int GetLeapMonthOffset(int a11, int timeZone)
        {
            int k, last, arc, i;
            k = Convert.ToInt32((a11 - 2415021.076998695) / 29.530588853 + 0.5);
            last = 0;
            i = 1; // We start with the month following lunar month 11
            arc = GetSunLongitude(GetNewMoonDay(k + i, timeZone), timeZone);
            do
            {
                last = arc;
                i++;
                arc = GetSunLongitude(GetNewMoonDay(k + i, timeZone), timeZone);
            } while (arc != last && i < 14);
            return i - 1;
        }

        /// <summary>
        /// Tính ngày thứ mấy
        /// </summary>
        /// <param name="dateAm"></param>
        /// <returns></returns>
        private static int TinhNgayThu(DateModel dateDuong)
        {
            int jd = JdFromDate(dateDuong);
            int NgayThu = (jd + 2) % 7;
            return NgayThu;
        }

        /// <summary>
        /// Định dạng Can Chi cho năm
        /// </summary>
        /// <param name="dateAm"></param>
        /// <returns></returns>
        private static CanChiModel FormatCanChiYear(DateModel dateAm)
        {
            int can = (dateAm.Year + 6) % 10;
            string _can = string.Empty;
            switch (can)
            {
                case 0:
                    _can = "Giáp";
                    break;

                case 1:
                    _can = "Ất";
                    break;

                case 2:
                    _can = "Bính";
                    break;

                case 3:
                    _can = "Đinh";
                    break;

                case 4:
                    _can = "Mậu";
                    break;

                case 5:
                    _can = "Kỷ";
                    break;

                case 6:
                    _can = "Canh";
                    break;

                case 7:
                    _can = "Tân";
                    break;

                case 8:
                    _can = "Nhâm";
                    break;

                case 9:
                    _can = "Quý";
                    break;
            }

            int chi = (dateAm.Year + 8) % 12;
            string _chi = string.Empty;
            switch (chi)
            {
                case 0:
                    _chi = "Tý";
                    break;

                case 1:
                    _chi = "Sửu";
                    break;

                case 2:
                    _chi = "Dần";
                    break;

                case 3:
                    _chi = "Mão";
                    break;

                case 4:
                    _chi = "Thìn";
                    break;

                case 5:
                    _chi = "Tỵ";
                    break;

                case 6:
                    _chi = "Ngọ";
                    break;

                case 7:
                    _chi = "Mùi";
                    break;

                case 8:
                    _chi = "Thân";
                    break;

                case 9:
                    _chi = "Dậu";
                    break;

                case 10:
                    _chi = "Tuất";
                    break;

                case 11:
                    _chi = "Hợi";
                    break;
            }

            return new CanChiModel()
            {
                Can = _can,
                Chi = _chi
            };
        }

        /// <summary>
        /// Định dạng Can Chi cho tháng
        /// </summary>
        /// <param name="dateAm"></param>
        /// <returns></returns>
        private static CanChiModel FormatCanChiMonth(DateModel dateAm)
        {
            string _can = string.Empty;
            string _chi = string.Empty;
            int can = (dateAm.Year * 12 + dateAm.Month + 3) % 10;
            int chi = (dateAm.Month + 1) % 12;
            switch (can)
            {
                case 0:
                    _can = "Giáp";
                    break;

                case 1:
                    _can = "Ất";
                    break;

                case 2:
                    _can = "Bính";
                    break;

                case 3:
                    _can = "Đinh";
                    break;

                case 4:
                    _can = "Mậu";
                    break;

                case 5:
                    _can = "Kỷ";
                    break;

                case 6:
                    _can = "Canh";
                    break;

                case 7:
                    _can = "Tân";
                    break;

                case 8:
                    _can = "Nhâm";
                    break;

                case 9:
                    _can = "Quý";
                    break;
            }
            switch (chi)
            {
                case 0:
                    _chi = "Tý";
                    break;

                case 1:
                    _chi = "Sửu";
                    break;

                case 2:
                    _chi = "Dần";
                    break;

                case 3:
                    _chi = "Mão";
                    break;

                case 4:
                    _chi = "Thìn";
                    break;

                case 5:
                    _chi = "Tỵ";
                    break;

                case 6:
                    _chi = "Ngọ";
                    break;

                case 7:
                    _chi = "Mùi";
                    break;

                case 8:
                    _chi = "Thân";
                    break;

                case 9:
                    _chi = "Dậu";
                    break;

                case 10:
                    _chi = "Tuất";
                    break;

                case 11:
                    _chi = "Hợi";
                    break;
            }
            return new CanChiModel()
            {
                Can = _can,
                Chi = _chi
            };
        }

        /// <summary>
        /// Định dạng Can Chi cho ngày
        /// </summary>
        /// <param name="dateAm"></param>
        /// <returns></returns>
        private static CanChiModel FormatCanChiDay(DateModel dateDuong)
        {
            int jd = JdFromDate(dateDuong);
            int can = (jd + 9) % 10;
            int chi = (jd + 1) % 12;
            string _can = string.Empty;
            string _chi = string.Empty;
            switch (can)
            {
                case 0:
                    _can = "Giáp";
                    break;

                case 1:
                    _can = "Ất";
                    break;

                case 2:
                    _can = "Bính";
                    break;

                case 3:
                    _can = "Đinh";
                    break;

                case 4:
                    _can = "Mậu";
                    break;

                case 5:
                    _can = "Kỷ";
                    break;

                case 6:
                    _can = "Canh";
                    break;

                case 7:
                    _can = "Tân";
                    break;

                case 8:
                    _can = "Nhâm";
                    break;

                case 9:
                    _can = "Quý";
                    break;
            }
            switch (chi)
            {
                case 0:
                    _chi = "Tý";
                    break;

                case 1:
                    _chi = "Sửu";
                    break;

                case 2:
                    _chi = "Dần";
                    break;

                case 3:
                    _chi = "Mão";
                    break;

                case 4:
                    _chi = "Thìn";
                    break;

                case 5:
                    _chi = "Tỵ";
                    break;

                case 6:
                    _chi = "Ngọ";
                    break;

                case 7:
                    _chi = "Mùi";
                    break;

                case 8:
                    _chi = "Thân";
                    break;

                case 9:
                    _chi = "Dậu";
                    break;

                case 10:
                    _chi = "Tuất";
                    break;

                case 11:
                    _chi = "Hợi";
                    break;
            }
            return new CanChiModel()
            {
                Can = _can,
                Chi = _chi
            };
        }

        /// <summary>
        /// Get ngày thứ mấy
        /// </summary>
        /// <param name="ngayThu"></param>
        /// <returns></returns>
        private static string FormatNgayThu(int ngayThu)
        {
            string _ngayThu = string.Empty;
            switch (ngayThu)
            {
                case 1:
                    _ngayThu = "Chủ nhật";
                    break;

                case 2:
                    _ngayThu = "Thứ hai";
                    break;

                case 3:
                    _ngayThu = "Thứ ba";
                    break;

                case 4:
                    _ngayThu = "Thứ tư";
                    break;

                case 5:
                    _ngayThu = "Thứ năm";
                    break;

                case 6:
                    _ngayThu = "Thứ sáu";
                    break;

                case 7:
                    _ngayThu = "Thứ bảy";
                    break;
            }
            return _ngayThu;
        }

        /// <summary>
        /// Get cung hoàng đạo
        /// </summary>
        /// <param name="dateDuong"></param>
        /// <returns></returns>
        private static string FormatCungHoangDao(DateModel dateDuong)
        {
            if (dateDuong.Month == 1 || dateDuong.Month == 2)
            {
                if ((dateDuong.Month == 1 && dateDuong.Day > 19) || (dateDuong.Month == 2 && dateDuong.Day < 19))
                {
                    return "Bảo Bình";
                }
            }
            if (dateDuong.Month == 2 || dateDuong.Month == 3)
            {
                if ((dateDuong.Month == 2 && dateDuong.Day > 18) || (dateDuong.Month == 3 && dateDuong.Day < 21))
                {
                    return "Song Ngư";
                }
            }
            if (dateDuong.Month == 3 || dateDuong.Month == 4)
            {
                if ((dateDuong.Month == 3 && dateDuong.Day > 20) || (dateDuong.Month == 4 && dateDuong.Day < 20))
                {
                    return "Bạch Dương";
                }
            }
            if (dateDuong.Month == 4 || dateDuong.Month == 5)
            {
                if ((dateDuong.Month == 4 && dateDuong.Day > 19) || (dateDuong.Month == 5 && dateDuong.Day < 21))
                {
                    return "Kim Ngưu";
                }
            }
            if (dateDuong.Month == 5 || dateDuong.Month == 6)
            {
                if ((dateDuong.Month == 5 && dateDuong.Day > 20) || (dateDuong.Month == 6 && dateDuong.Day < 21))
                {
                    return "Song Tử";
                }
            }
            if (dateDuong.Month == 6 || dateDuong.Month == 7)
            {
                if ((dateDuong.Month == 6 && dateDuong.Day > 20) || (dateDuong.Month == 7 && dateDuong.Day < 23))
                {
                    return "Cự Giải";
                }
            }
            if (dateDuong.Month == 7 || dateDuong.Month == 8)
            {
                if ((dateDuong.Month == 7 && dateDuong.Day > 22) || (dateDuong.Month == 8 && dateDuong.Day < 23))
                {
                    return "Sư Tử";
                }
            }
            if (dateDuong.Month == 8 || dateDuong.Month == 9)
            {
                if ((dateDuong.Month == 8 && dateDuong.Day > 22) || (dateDuong.Month == 9 && dateDuong.Day < 23))
                {
                    return "Xử Nữ";
                }
            }
            if (dateDuong.Month == 9 || dateDuong.Month == 10)
            {
                if ((dateDuong.Month == 9 && dateDuong.Day > 22) || (dateDuong.Month == 10 && dateDuong.Day < 23))
                {
                    return "Thiên Bình";
                }
            }
            if (dateDuong.Month == 10 || dateDuong.Month == 11)
            {
                if ((dateDuong.Month == 10 && dateDuong.Day > 23) || (dateDuong.Month == 11 && dateDuong.Day < 23))
                {
                    return "Bọ Cạp";
                }
            }
            if (dateDuong.Month == 11 || dateDuong.Month == 12)
            {
                if ((dateDuong.Month == 11 && dateDuong.Day > 22) || (dateDuong.Month == 12 && dateDuong.Day < 22))
                {
                    return "Nhân Mã";
                }
            }
            if (dateDuong.Month == 12 || dateDuong.Month == 1)
            {
                if ((dateDuong.Month == 12 && dateDuong.Day > 21) || (dateDuong.Month == 1 && dateDuong.Day < 20))
                {
                    return "Ma Kết";
                }
            }
            return "Không xác định";
        }

        /// <summary>
        /// Get hình ảnh con giáp
        /// </summary>
        /// <param name="chi"></param>
        /// <returns></returns>
        private static string GetImageConGiapByYear(string chi)
        {
            string _image = string.Empty;
            switch (chi)
            {
                case "Tý":
                    _image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ4AAAChCAYAAAA2qQYDAAATs0lEQVR4nO2dd7QURRbG3eiqu3/s7h+rng1n1z3unrNZEXQxgIFkQiWJIJKDoAgKijksQVHMiKBPMKGgoAR1RTCBT8lmxYQBQQURDIDhbv1qXr+dmfdmprunu6p7pr5zvoPC65nqqu/duvfWraqdRGQnR0fTtN4Ax+qk9QYkiD9U3FuxlWI/xTGK0xTnKdYqrlHcqLhJcYv8H1vq/m5j3c/U1j3Ds2MV+9d95t5132H7PRNB6w2wxJ0VmysOV5yquFxxm8SP7Yor6r7zTMWDFH8Sw/slntYbYIg/VjxUcZziYjEjMr+gLbSJttFG2mq7v5zwyuDPFXspzlbcKukBbaXNvRV/Ifb70QnPB3+qeLJkfKztDcc0ddihOF+xh2TezXb/OuHlsYniJMl1+isNWMKbFZuK/f6uauERIXZRXFZ8vCoSBCjdFH8k9sehaoS3m+JQxXd8DFCl413JROapm4atNyAAd5GM4Nb7HJRqwkeKwyTTR7bHqWKE933JRKfvBxiIasU6xUGSgkS19QaUYAvJ+DMOwUCf0Xe2xy91wttd8Z4QHe6QixmKe4j98Uy88L4nmWl1U7h+dmgEnyr2kUzf2h7fRArv14oLQnevQyksVPyt2B/nRAmvg2SqOxziBTNJJ7E/3taFR2XGLWV2pkNw1CjuKlUqvN+Li1htYpXiH6TKhNda8ZMoes+hLDD1tpMqER4Jzm+i6TeHCMBYDJYKFh4rEOMj6y6HqDFB8QdSYcKjqnZ6lL3kEAtIOBupgDYhOiLXudH2j0OMoPA09mKDuEVHyP5Y1D3jEDsWScylVnGKjt8aJ7r0gpWO2HJ9cYkOP2FOHL3hYBRMu7H4fHGIjsVoV1lSOZgpMUS7cQhvTFw94GAN10jChTcwvnd3sAzGNpHCayNuRaKSwdhGtrwWlehY8HdlTZUPikr3koQIj5DbVZlUD6hqKTvNEoXwXD1d9YHTrqwKr0P87+iQUHQWS8Jjj4Tz66oX1PL9TgwLjySx25jjwLJaqN1rYYXXy8x7OaQAfcWQ8Nhs7fa9OnggxbKnGBCeK+h0yAcFpLEK72Bz7+KQMrSUmITHnomVBl/EIV1YLQGqWIIIr4/Jt3BIJQZIxMKjmnid0VdwSCPQiK/9Gn6Fd5bZ9jukGFwcE4nwOHN4g+HGO6QXHItbcqOQH+GdYbrlDqlHSatXSnScpbvWeLMd0g40U3STUCnhdTbfZocKATcshRbecgsNdqgMUBwcSnhNbLTWoaLQTEIIb6KVpjpUEqZIQOERDlfyhXQOZsDFfz+TAMLrbqedDhUI7t31LTx3rJhDVODgJl/C42brSrhk2CEZYCP4r8SH8FxZu0PU6Cc+hDfLZIt2bN8uL61aKXdNmSQXDD1VerZvJ8ce2FSz46EHysiBfeT+O6fJ+g/c5Y0pBkfWFRUeNz4bi2a3ffWV3DF5onQ87EBp3eRvcuT+/5KjD9hH2h/UTE5qe5i0a/oPabXPX6SN+reBJ54gSx5fKN99+62p5jlEB6LbnaWI8FqaasnXX38t14+9TAvr+BYHyCVnDZXHH3lI3lrzuqx7/z3Z+PFH8sLK5XLx8NOkXbN/6p/DCs6/f4apJjpEiyOkiPDGmmrFf+fM1mLq2/FYWfbM4oI/h4W7746p0qFlc/3zJ7T4tyyvXWKqmQ7R4XIpIrynTbRg7VtvSq/jjpSubVrKmldf9vXMk48+oqdkxDfoxA7yxedbY26lQ8R4RgoIj2sBvjLRgolXjJHW+/5VZt19R6DnHrz37nq/j/92SBVI0aGxBsJrbqoFp57UUVuvoFbr6x075MJhQ7TwzuzbQ7Zv3xZTCx1iAttjGwhvmKlv73F0azn9lK6hniWyPfrf+0rnww+Wl59fFXHLHGJGfWVytvBqTH1796Na6fxcmNQIVo7UylH77yMPzbovsjbRli+/+ELeePUVeeSB+3Wap+b6q3V+ccHcB3W0/dWXX8p3330X2XdWIaZJI8IzVvRJJHtGz26hB/HyC87Reb/bJ91Qdlu2bdsmr774vNx247XSp8MxOpeI/8l07pH/R+gENfdOvVXWvvmGfOvyiWHAFJUjPPZWGHOYzh0yQCeKsS5hcM9tt2hBjL/ovLISyghuwiUX6LZkC60UOx1+kEy76Qadb0wyyB6QDfhs86e2m+Jhh2QWKeqF90eT375k0WM6LzdyQG89fQXFY/Pn1AUYp6jp8fPAzyPWeffdq1dHgggun4O7d5Znn3oi8PebAAn6Ef176bQVyfgE4U+SJbwjTH/7zNtv04N31SXny0frP9Ri+OSjDTJjWo088ejDRacy/t1LPvNMEDAgTKtt9/t7WaLzSEKbdyHiDoutn30mj855QPuRUeGOm2/UlnzhQ/Mi+8yI0FayhNfX9LcjgGtGX6wHD9/qrH49pc8JR2vfbfLV4+WbbwpfmUFky3MEKX4T0N533nLthAY+XLkktzjh0gtl08bgt91TJHHD5aO1Dzm4WyftP2aD6fz1l1+S1158Qd59+y3986Ww5pWX9PLixWeeXv93CVrj1he1eMIbbaMFFAnMnXGPnrJYxbhsxDA9DZfySZjeGHCKCbB+foE1LWTpohAjU1u+cEqBSLpfx/b1n3HaySfKIw/O0sl1hINVP7F1S00qd84Z1FduVEJ95olFBUWI/8n6NlE/fuzzy5cmKRofJ1nCq7HZks+3bpXNmzbpCNMPXlixrH6gsGB+gECPP2T/HKHg4zG4p/foqq0owQpWl+jVCyK8NWK/5PnVy54L9P6kbrKFj/XzCiMK8dgD95P+ndrLnZNv0mL3kulY9SHdu+jPo/BithIwQUaCoFMqnvBSdcXnhg/X6cHxHPyP168v+vOvvLBaT8vZ1o20CbnELZ9t1sIH5OteXr1Kap98XDofcYiujiFlo59RLoBf8XVp1UIWzJtT1F3Y/OkmWbxogfZv+X6sZaOWWH0vvxz4woWsMpZ/3HkjlT83V2697mqdYPcE/MD0OyPt+wjAFFUvvFrLjQkEImF+q72On3TV5QWde3wkxJk9UN2OPELmzrxHrr7sIlm65Gn5dONGbXHxOamauenKcTLl2qv080zPWMJx558daDrGsUe0jUXdWCdKwhDVgC7H61KvMaPOavRzWF5kSiVICDP90/aEgemgXnjRhVMGgKOMILzOxW9DLB+8m3vMy1uvv6ZTLvVBgJq+8KGIagG+EOkGApuzlfXzksd83t233qwd+qcXZm5VwF8KM/Bjzx3ZoHqa9Ab+mh8fc8o1V+pnaMuw3idraxrE+hK4JAzM+/XC+8ByYwKDwTzl2LY5nTyoa0eZOH6s3H/X7XrAeh9/VM6/H9O8iV7fZWqeXjNZO+6FBgxnnoCHJbMXV62QHse00VFn17aHBhbf0J4nab/UA6kikuCl/Dh47ehL6p/bumWLrk30m/DGR8XNSBgI/euFt9lyY0JhRe2SBuIrZj0QItMqVTHZU3Ux4uthZVi3xR+7ftx/Qlk+Ahn8rezokl+SUvlEiiHw3bDy5PvGX3iu3gpQ6vvw86Jcy44QaC3dwgPvvfO2Tj2cN2RgfaFoY+Tf+O1n4Fmqw8/zKxoGmsgQfxAhhhEeJN/Has38WTN1/pF1X6b3Us+RkyPyJor1M81iEalXTFAKJRssVdULL/XAiX9/7Ts6vUDZVWPTDtEw6QaKDIIIhsFGcHzuRcNO01NwUNEhBvw6LBFiY58Jgoo6mc1nz5kxPUkJ48ZQOcLzwG851oREa4541ABjFVlKYsDDDOqlI87QG5K6tTs8sHBx8pkqVzz7jA5wohRbtrhJyCfU0mUj/VNtIRA5jjq1X2SD6pVGERDwZ5DIkmfPP32QXvoC+XnFKEhCmRRRCpAz1Vac8AD+HysJUVoUVhnwzYKuaMDstVNE4ieq9cPjDm6W1ECiMeQEF6lLp/gF1cTeKkcUJF/I6gAWxu8zBCckpz98///5PJLIw3p1j0R0vGOKkJNOSVUCOQgIOkgOlzvATK0XnjFYxowaEerZmydcoYOb7PrDKy8+r6w2sVT28OxUiQ7kJJBTtWQWFBR9Bq0yzifT4nNPP6ktFRaGOrwgzxPNknNkzdUT3+hzzgzdHs6VIb+XQuQsmaWqSCAoyPizqlGu1aNk/5031ujEdZgVDFId2VEnwUYY8ZGDpJAhpcgpEqix25b4sXTxU6ECAo/k8digRC4OaxMm/8bUiPCyCxqwoCTA/QYa/Tsfp5fwUoycsqjErSTHAaLRsCXvw/v00P4iS2flWk4qrCkA9Qo5KZ+iwqbUc5RxUYWccuQUghovfbcB6t78rtF6pHiU6JPp7T9nD48kUNH1c+efrVMzXs3eyudqtbAaq8sjgubYD2oHKwA5pe/GN/vYwqSrrggkEsqm2EuBMKJMy0BSIV6Kxau+ZtNPztTaqb3eVecHVL2wakM1NaXx+IFsE+A0Lk7Y4k9WTvgT/9LSwUc5m32Mbm+0iUJTGlbF89uwcKQ6vKUxqkg4tTTqpS7SLEzhNTdco9tGVfKowf3r2tNUV8L4mVrXvfeuTkiToKZ8i18QonhIKRhWGyJ0omtIlQsV1hbwZ7G1odsWmKqojcsXALV3iIxqX+rdKH/C6WdgvI04lEaFXeMtRRLM/EKwrMb/k3ahLL7UlknK9HmO1RnfwYmyoDNvr9HRuYWN3g02dMNUh0p+wPSTX4aE1WF7JZaHKt988HdMt3EILqcdWVEyRarF8OZrr2qLlV8GhqXDcmPRKMGCBFPkHDlliz0lH2+wevVwgyMsqiKlsmrps436aQwWRaL4cfg9lE5lgy2C+dXMcfKykcO0QIh6qWih3Gv1sqU6EX7FBaN0gQEpHaw0pf1sdySZzIbwTZ98rKderDW+HHts2U+SkCPdGj20x9gxZbZABEmtGhtsGDxItEqtHjV2WBD8O1YXKNakyMATIVYmaERcDtmgRO0f5wBSB5hddYwFI/eHwFKGRo8pM3Ywo22w8+ypBf/VtXVYiqkTr9f1e/h62YOPRSHn5m3SZqCjSKeUQwIcLHNK0ejBjMaOok0isIY464gwv4KZSNErO2Lqu27Mpb5K1qMkESrbMQl8UoqCR9EaO3w76XjtpRf1umz2wOOks2oBWGsl3+a3rImIk7Viqltg0HVedrfxfSmoLC6GgodvG71uIOngFCr8q2wBEHliET1gfVh75S4OnP0cC6UiS/bBTr3xOn0eDNM01hKS5OV4iVKCw/9kdSPp5/D5RNHrBoxdsJIGkO0n8MgXVH45EtUvRJ7s2aVyhYiSxG+x06N4hkMh8SPJEep9vMoSkrrhai1OF0hh8FAMRS9YMXqlVNLB1EYBZ35hAVNfFFaIFAdLZm+/8bomUTSpjzCHVSYcJa+UgkYv0Us6EAfHZbCawOK+l+hlyk34FsIkoeQletBdG9oIKInCV2PvBCLkkB38NQdf8HVtqLsouQBYSWCKJbUCqalzKAnfFyXDVBbzOyQSvq+Gh90tNdKh8tBbAgjvp+KiW4fyQTT7MwkgPDjRSlMdKglTpIC+igmviZWmOlQSmkkI4UErtdEOFQEKiwtqq5TwOltosENl4GQpQ3jsxVjb8DMdHIqCReYfSxnCgxVfmewQOeorjQvRj/B2U0zU9X8OiQZaaTSFIgGFB0cYbrxDelHS2kkA4e2quM5s+x1SCOryd5EIhQf7GH0FhzRigPjUUxDh/UBxpcm3cEgVnpeMRiIXHjzE3Hs4pAxsm/CtpaDCg9PNvYtDSjBTAuoojPB2V9xk6o0cEg9O/tlTDAgPuvJ4Bw8c6hlYQ2GF9z3FBWbeyyHBWCgZLRgTHvy14kYTb+eQSOBu/U5C6qcc4cEO8b+fQ0JB5VJo7ZQrPHhL/O/okDBMlTJ1E4XwWE6r+NNEHerBqZ6MuXXhwd+L8/eqAaRO9pIINBOV8GAbyWzedahMMLbtJCK9RCk8ODDGF3ewC30xSlSMWnhwTHzv7mAJXMQRqU7iEB4JxVTcUe7gC6zD+q468cs4hAfZ6FHRV5FWCeZLiU07YRmX8CCVqI/F0RsORsByWNlpk0KMU3g71TXciS99WCSZ83Ni00bcwoMcMe+OPUsPmF597ZsohyaEB/ETXAFp8jFDYvLp8mlKePD7iuOj7CWHSDFBYoheC9Gk8DwOErfCkSQwFkPEsA5sCA+yvObWdu2DtdfIlsGC0JbwIIUFbrukPVBlEsmCfxjaFB4kenL1fOZxm8SYo/ND28LzSCWzm3rjB1NrF7E/3okRHvyNuA1EcYKViN+K/XFOnPAgBQZsnXT7dqMDVo5zb0LtBouL1htQgGwadxUu5YOE8B5ifzxTIzyPLcTt5wgD+oy+sz1+qRUeZMWD6feDgJ1fjeAMQxL0nF1te9xSLzyPpF6GKm4IMBDVAo5/HS4GFvejovUGhOBudZ1cUddXhwR9wNGvsZYwxUHrDSiD3CZOTmqZjwGqNODDdZNMH9geh6oTXja5/mqSZC5tq1TwbpMVm4r9/nbCyyNTTg/FeYo7CgxgmsA7PKTYU1I4nRaj9QbEyF9I5q7U2ZIuS0hbaTNJ31+K/X50wiuDVNUeqjhOcbHiNkkOaAttom200UgFsG1ab4Al7qzYXDLRMScf4axvl/jBd1AKNk0y0ehBktmTYrs/nPAskqTr3oqtFPspjpWMQPAXaxXXSKaChnXk7NvLt9T93ca6n6mte2Za3Wdw90Obus9ObRQaNa03wLE6ab0BjtVJ6w1wrE7+D1c93jznIh5BAAAAAElFTkSuQmCC";
                    break;

                case "Sửu":
                    _image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ4AAAChCAYAAAA2qQYDAAAbQklEQVR4nO2debQU1Z3HTTKJk5j8Mckfk+RkkpPJHGfOmZnMjCGiwQWMAqK4griAiLIrLrjgrrgCoogbgiCCuCC4oogbiAKiIov7rigiICCCIqjwm/u5/e6zul5Vd3V3Vd2q7vs953fgvddddavut377vbWDiOzgxEnaYn0AGZN/ULKzkvZK+iq5UslkJY8oWajkHSXrlKxXslG+x8am361r+szCpu/w3WFK+jUdc+emc9i+TutifQAWZUclbZScrmSSkpeUbJHksVXJ4qZznqFkTyX/mMD1ZVqsDyBF+YmSfZQMVzJf0iFZVDAWxsTYGCNjtX2/HPFqkH9ScrySB5RskvyAsTLmE5T8UuzfR0e8CPJzJcdKwcfa2nJOc4dvlMxU0lMK12b7/jri+aSVkrFS7PTXG9CE45TsKvbvd0MTjwjxSCWLSs9XXYIApbuSH4v9eWgY4u2k5FQlH0aYoHrHR1KIzHNlhq0PoEL5qRQItyripDQS1igZLIV7ZHue6oZ4P5RCdLqigoloVKxUMlAynqi2PoAI0lYK/oxDZeCece9sz1/uiPdrJVOruOEOxZim5Ddifz4zT7wfSMGsrq/uPjsE4HMlvaVwb23PbyaJ9zslT1Z9ex3KYbaS34v9ec4U8bpIobvDIVlgSY4QRzzdmTGhxpvpUDkmKvmZNCjx/iguYrWJpUr+VRqMeB2UrI3j7jnUBExvJ2kQ4pHg/C6e++YQA5iLk6SOiUcFYmRst8shboxS8iOpM+LRVXt3nHfJIRGQcE68Azot0hG5Phzv/XFIEDSeJtpskAbpCNmfivvOOCSOOZJgq1XSpOOpcaTLL6h0JJLrS5J0+AkzkrgbDqkCsxu7z5cU6ShGu86S+sF0iTnaTYp4VyZ1BxysYbRknHgDkrt2B8tgbjNJvI7iKhL1DOY2lvJanKSj4O/amuofNJX+STJCPEJu12XSOKCrpaY0S1zEc/10jQd2u7JKvC7JX6NDRtFNLBGPNRLOr2tc0Mv3B0mZeCSJ3cIcB8pqFa9eq4V4x6dzXQ45QB9JiXgstnbrXh0MSLH8VlIgnmvodPCDBtJEibdXetfikDO0k4SIx5qJJSleiEO+sEwidrFUSrzeaV6FQy7RX2ImHt3EK1O9BIc8Ao6UXa9RCfHOTHf8DjkGL46JhXjsObw65cE75Bdsi1tyoVBU4p2W9sgdco+SWi8K6dhLd3nqw3bIO+BM6CKhKMTrlv6YHeoEvGGpauK9ZGHADvUBmoOrIl4rG6N1EPls1Sq55dqR8vD0qfLtt9/aHk4taC1VEG+MlaE6yMz7p0vn3XeRA3b7P7lz/Fj5Zms87wPcvn27bNq4Ud5/5215ZfEi+fjDD+TLTYm+2HK8VEg8wuF6fiFd6ti+bZts2bJFNn/1pWzdukWTIPBz6veTbrpeOrT6b2m/y39qGTNymGz5+uuazv/pihUy/faJ0q/boZrQHJd/Bx7dVR6cemdSBOSgv5AKiNcjiVE0Ilau+FhmP/qI3Hr9tXLtZRfLsPOGyOjLh8ptN10nTz/2qNY6337zTfPnIeiIC89pJh3S4S//pTVfGFlLYf3az2TmfdOkT9eDm4934G67SM/OHeSQPVs3/+6aSy7Q2jAB8N7dyMRz24rViDWrPpUp427SGqXTrv9TRCQj/B5CQMRFz82XbYp0YMkLC6XHge2LPntk+7byzhuvhZ4PUprvb1i/XpYtekGemPGgnHtSvxbnZVwQftYD931PbqVhn33y8SRuBRs3RSIeb7auh5cMWwMkOvnYowLJFiaHt/2bjL3mKm2Gwe1jbywiLFrvwbvvKDoPmvLF+c/KPZNulYk3XCs3Xz1cJlw3Sq445wzp3eUgfcygc028cbQm3rTJE4t+P/vRRPQNC8H/WSIQz7W11wAi0SP23bMi0nnJNW7UVfo4ry9bKoftvVvR3++6dVzzeZa//5423cd2bq8JdvAeu7Y4Vth5MLV8j3/N707q0U1WLP8wqdvSVyIQ7/6kzl7PwDe7747JOhoNmmic+nNP7CsXnDJQTu/dU47Z/++B5Nj/r3/WPt740VcrMv21yBRCavDq0sX6OBD8qA7t5NC9WodqtyjSUR0bP7AaHzIi2LKuJPF447OLZivEd999pyNGQyQmEp/ssrMGy5xZM7VZ+3zdOvnqy03y9ebN2oknV/f6y0vl7om3aG3j1T7mGH6CnNaruzaj3fbdS/888Kgu2lyivSAyx+KYl599uj4/WhAilyId571h2GU1R81lQHS7o5QgXrskz16P2PD5eu1XMYlM9CnHHa1NIsFFVBAUPD7jARnU48jIWgpCLXh6tj5/z4M6ymFtd5e5T8zS5J5++23SdZ89tPZFK3YIIDG/v2zIYH2M7U1BScLYT0oQb1gaI6gHbPriCz1pQ/qfoCeS1ATOP3m6akHqY+RF5wUSxS9Eyxu/2CAvv/SiJhGBSO/DO8tFp52kI2U0HhrxoDatpEu7Ns3fg4yY8leWfF8NjStBXQYjpATx5qUxgihA9X/47jvyxivL5L233pT16+y/CIiI8503X9f+EBPcqfX/Nk8opu75eXNjOcfIi88PNLVeueTMU7WmwvQGfRYyzpv9pJza65hmXxHN+NhD37vwjHfUpRfJC/OeqXncEfCchBCP1wIkauijgNrkkw8/pJ1nUgLdO+0rvQ7ppB3y4ecP0b4Uea5PPlquzUpSwA/jHOTDcOpvHHGFHhOT1yIaVROPVjmjT0+dBvFqk2qAT3j8oQeUJB75OdIfXf++hww46nDt31161mna/2M8/J5KCTk7fsb/pP676pMVMu+pJ7T/SVDCsSjRpQDUKhxrQbw2aZy9FFav/ERPsDejHhSBYUYwJ9z8G4ZfLlMnjtfJ0BcXzNNJVm7uFxs+19pjm89/2dZUusJM4eCjTRc//5zMfXyWPHTPXTL55hvl6qHnaxMK8Yka/Y5/UBoEU4cZw7RBmqm3TSiqSFQCNBkatdQ5Ic3RHdvp83IuQABz5bln6b+f2beXTiRzjxiT1nidO2hz7E298CC9//ZbNc9dRLA8tgXxBqd19iC8/fprMmRA75L5p1Jk7Py3v+iUAiaPm3viMV21mRl8wrFaW57V73gt/EwAwN/7dj1EVwhwxCE7tctqzh8kaEAStfiC1YAcXblzoMkwo3zWpELQuJASX/GcgX10LjDsweH39981Ja3gAujOZD/xJqZ1dj9IMYy9ZkSzz0TExs0jUivn62RJIK0hPQ8BaY6LBg/SmqhSEKiEldq85+OBw7xCvvHXXaNTM90P2E//Ds1mtJ1fuMfUi0kFpYjJEkA8a02fTAxPJzcE7US3xNIXn9etOxTH8Vdsk6qcoC3Jh722dIn2QSFft/321sQwprASEExBonLnNdqe//PgmrwdGtxfzfASlrFa6PVjN9Ei4rG2YkvaozAg54UJ5OmEcH6Q4zI3N6vCRAL8SjQPGg8yorUJjvBfK8X8OU+WTQBXI5hhUxNOGTi9P/YS799sjMJg9acrtb8F+YJ8IoIBTJZtcoVJvyMO0VqbfBgENL8j1YG5hYC0Q5GnqwSkbsqZ20pl6BmnVO13xoR/9xJvP5sj+eiD97WJpW8tDJhfb94sS0JZDJAG4mdcgwVzntJERPtBPMzeeYP6y1MzZ0RuuuRzp/Q8OvS8pJqIuqOOk8oID7ll7O8lXh+bIyH9QVNkqQnB3yN9YJtkfsHHMpNpet9Mh8lnq1fr3BpuAg8NBCSCxp+dcssYncJ569VXWkSV5PFI9PJ78ogm3+YVCLds0Yvap+ShjTJWArgMYICXeFfYHAm5NX++zQ9adsirlbqxxx28v06ZpKkZcdRJ0j7/7FydQkEoYxGp4x7go5ErI8/o7SymskC+74w+x+mcoxd8Fn8XcoJ7p0xqcV7SKMZCYJJPOOzAsmOlpb7cfU4BwzORSokK/CPyb4GT35RygQBUMx65957mJPRJ3Y/QGsE46abKUC4pXIlAdFOWIi/IQ0KDpgmISI0w4YVWp12ba6doS8wvJpk8HNoLDUdUzGfw715auEDXfymRBZGPJlDuTTkfmPNC4AwQb7KXeJl/xSeVBhLMfm1ztvodrUFUMihZmTIaKQwmlvIQv8O88XmIh+ahgyMJzUjClg5kxkvARHTLQhvKb0w+US7np8jP50dfMVSPF7KiAcm/YbKNeaWWCigVhpGea+e4pbQyx35u7hxLs1eEWV7iLbQ8mLKg/Qez5L2h+Dn4SICokhqp8ZfQHEfvv48mARh1yYXNWg6C4k+y1iEJ88uDAKiH4pdCQnxY/gYBSK0QfEBESENpC3IGaXS9wkxF9ReeeqL+uf+Rh+m2+krSLHyWKg7lwQzgBS/x3rY8mLJgsiiGe28oE2AAkfB5WOCCc87/edLxtzAvJHTN94g6+QwTUUvnbphAIIh//qAB2vRCPOrI/I1IdPl77+qAyjQCUJclmfvmqy8XVRqoQFBHpYjPz9SCOc7aNav1Q9WsZZW2C6tQoPXRirgblfQIJoj3vMSrPLuZMpgUJsLcUDQJqQnAxBrHHbJdPPhkrf3QchAMP4i/mTouvhcdJ2hHo0niFlM5QENRhSB3RpEev820JlGVMZ8ffsHZuuHBaGV8PLQ1gYe5bhLRrLUAZjEReUKuJaybBeIRyFA3tpQ09mOtl3gbLA+mLGZMu7vZJ4NcdJDQ/YEJM6UliMWTz88mNbPwmaf13wgyaAsnH6hN2FWF9zmz3jUOokFmfDp/0AIBIdS7b77RrHUxe5CRoIJ0S9CxiGwB3TLm90NPP1n3KVJfJXrnd3TUYKo5t/843CfuBw+pyTVmABtyQzxSE9x078SgPSAYkR3/ZzKpFmCC0DRoQLQMSwZZZLPy44/0sQzxaB8CNHXGUR3gQcAdQLMEEQDN5A1mqGp88O7bzW3zXqLykAE0nllpxr/4heZ+4BuSXObho7PHrMMoIrAyzbgVtHhlIHFssNlLPOvg6Q/rX6NT1lurJRFr6qDkukjUkk6AQESD3HB69tBw+FJoBEA3M6bWSzzMddDKsCBhEin8B/0NooN1n63RvXCQHy3EOMKOx7G8vhl5SPKBpFaoWZtF3YyPlAx+IP4qLgKdJYsXLtD/p8HT387VsSlthA97xy03pzOJEWGdeNxgclSUmiACGsAPolU0ln/SmDC+B0iZcJM7NnXa8vfBx/fQ/hETRS6M7mVMsfk+0SIg5xclraKrDop4aFiOY9q1zPn0eB4p+JyYeaJwUiRoVFZ9UYdmlRi+GWYf3w+fDbJBUHJ8PCBEsPiA3rW5aG3uFSZ70pgbmu8NnyXNEtQ6BuF4MAnIePiyBGumFj+FHYu4aWaxDAGBv3i9renJDiICpgsTg4ZAqxBIsLiFSeSGQ1agTakiFibM5Mbwwx57sOBDsQI/jGx8D/+I45s8GSkdtBGJX34mcjX/J2JlnUjRtSpy+EtiaPfNX32l84u02KPpuVY0NCu/vIt9zHU888RjmvSG3OzJQuOBf8xcG8fA/fD6ihnCZivEo75IW443HUBJya/tmCzyYUFPM1qGSA9tgRlCkzBxkBcC0kaFdiJTzzoJJgwNYHw5NBYTh5NOt0YY8SA3SwZpp3/grin6Z6MpCWogCBoTTWXqyFQYIBsgAiWIYQE214Of6a0cQDqISsqHrhZvYIIGR8NBTlIo+HBoTVPlCApKEDbfQdOjGTNSm/VjQ6rpFJ5qVtp7l9oZEjGpRZ9VvgzlrzATyHfwocx2W6YozyRTz8XUQW7+BsEpzHu1CJoCkGopVeOECEwkJSzA2gySwqQl0NpEqcZ/wnxCcB4U2s8BROOaqVrgp/KgYHbxySAauTX8PG8y2JTRIBvgX5NrxEwTnaKlgxLIVHHwd0GU+rclrE01gYzfEhQ98uQSpXmBOYnS8o4pMakVtIZZEA0RmQCK4v7vQPy3XntVnwdtVaoCgJ+F1jHVDwjE5LMckFYuJhoTSJLXu2ofwpIG8Xb4mjW4VFvQXvh3+Hnk39DOBD2YSK7d5Nu4Zzw0fl8zaKx8l8VLOUBRAjnxkpl/zzeEJ54svB/UJ8uRziv4X5g6tAeChrhpxBVaG/k3vjFt6ESfpGDCjsnf2DgHkHMjSUsKBK2F+SZy5Zowq9dfean+DhoQs4+mhnzUh9F4pvsYv47/4+zzYDAG/k+Oj5KZIRy+HueK0nWNJteLipLZ2y4JFJXMEm8S8PtS+EJh0VZQJ0YUwd9jstA8TAg/ezUn5tesmsd/DFuxD3HQZvidmEi0EVErJEYLQnQ0FVoGv5LPUybjIaIQz8/mvPwfzXbnhHFaO4bl0/gu+Tu0ot8dCRO0HwnknO2TXNQkMDHps3mbFSkPhTV98vQTkVVDvFCtoMwxGsT0veGzlarRYjbRoPhsmDpIiDmEcCaXh+8I+RBSQZhhYLqQgzQTrgbnRVui1fFJCYZoWiDirmRFHQ8XJbccoqgtKvFGUJbfMaGYnzBfBLMVlCKoRdBUOPKYOUANF3NZjqgQhc/pjXFU1MqDg/nEx0ODoeEw26SFeIgwlfzsjdaTEkjKIvaMBg/lUNQImnjrO74NHSNhe6AweWiAKBvWRBX2pKNbxYBEs3+L1zDBRN535+36e+QXibLJKUJEyGaITPMCubyg9vQkhFwl15FjFLW+W13sAykxhXFODoEFzjsgrUEKI2ppDNHJWmU2TU4uDOTh8PXI8SW1+JyHgLo012TSJTlG0WIfa8sb6U0rtZKqEqFDmZyg6TujbEVLFESs5ngktmmbouRWardMnHuSzN62rbgE805ggimvE/yH9QXdRHgkXquZEDQMUSp+FZqJiogJHoiWSTFgauPYC4WI3ESOQRoQUrJbU9jK/VqECLeaxeAZRYsF3cjiNEdAzorotprJoGOXKNLf2EhSl8Rt3A4+0Sz+KQ497UjkCNHUAFJcdeG5sS+8NsJxcxq9BqHFFhaprzTDSS/nc+noMkBjEVVSuqJiwWIZNjJkgUzYtqtxCD5f80o1NSZSPmjCcvvYxSGYWxpWcxrFehG4aU9q25RRMKdNqRTh6DKhyM1K/ChrRutdCDDo5klwZ/Y0ELhNWWobM7IOFB8t6AaTdyOtYiJSgFnzLtZpVCH5zPayOUbgxoypbUVLBBhUOTAbWAeBLSyC2rv9QvG93I4DWRGiZtZOkIwutQuqV0hqk0fMIUK3okVS2XybkpW/eI//RGRYCqQVSvlw5NFoGyK3VsnW/ZUI4+ShwbejlEZVhMXhdJLwmgGIX+4YBAwEKDyAFPep09LQGrVGTfUn4dd9JoHQzbeRVF43wNoHv8ZjzUS55XfcbP+ibq9ASvZQBkS91FCD2p7oKIaYBCj4kRCeRUA0kNLgSfMnG+og/MzL5VitRm8c62UZP+Y/qArz6P33liUO5wzaiZOmT8YSZXsNevJyFmyUfN1AKi9YIfvuXf9A0Z0myyigRBW24MarTQBlLchCXx5FeV4yRw8eLfP4kBC52g2yw8D+J6XWcLB1Ram+OQIvtHapa0Towonj9QYpouQLVlJ5pRTJWO8mM3TlVrIXL71xpRLDaD7SLIZUaAa9ii2F9iHciFJjIy0SBTSbliMfJPbvNJVRlH2lFJLKS/Qo3usnd/fvF95EBURCe5WrStC2XsubdqoBZjpsPOQCze4HUcBWtH5fuOgBU9ePNs8Byr5ED0nltaE41dRoMbnVLDbm+7RalUsYk1xOk3zelw/7hfJXpTs2US0p9YBRpaFik3FEem1oai9Kpr5K10i1wHzSsVFup1CzMj8N0LIUNg6aSM0inqhgPQqry0ppPdaFpPiuikoR+UXJSGqvho9DG9HcybJDWofQoEywdy1suTRNnGCdRlizAK3q1TwEaElN3KZuaLbq8B6XBUQsdsooIr8aHulhaZA1gdYhIlYmn+iXLR7oXElz3xA6Y0q9n4K+w0ojaTpiICwEZGcFlmWyiIhFR5CO/KVZ6J1BnCAVEO/n4l6YXBVI4RClhxGPlWlxmUW2ZyMJjfnOaHRLNPsLqYB4yBgrQ60DPDxtql7mSGcyC3locqDlnhVpQfvD1DHGSwC3yhGvlZWh1gHQPphDSmFUOAg4MPn+fVUaAK2lCuIh1t5v5pB70FgcyKsoxOtmYcAO9YFjpQbisRZjefpjdsg52H71J1ID8RCrL1B2yCV0p3GYRCXeTkrW+I/s4BACuNIihSJVEA85K+XBO+QXJbWdVEi8nynJzLbhDpkFK+l/KjESD+md6iU45BH9JQKXKiXej5QsSfMqHHKFl6XAkdiJh+yd3nU45Awsm4jEo2qIh6TX3OaQF0yXCjhULfF+rWR9WlfkkHnQFvNbSYF4SCrt8Q65AJt6VsSfWoj3AyW53kvBIRbMlgIXUiMe8jsl69K4OodMAnfrD1IFd2olHtIl+etzyCjoXKqKN3EQD5mQ/DU6ZAyTpAbOxEU8ymmp7ibqYBXs6smcWyce8kdx/l4jgNTJn6RGvsRJPKSjFBbvOtQnmNtOEgNX4iYeMiDBC3ewC+Y2Fp4kQTzkyuSu3cESRkuMHEmKeCQUpyZ1BxxSB3XYSF0nUSUp4iEs9MjsngoOkTFTSizaqVaSJB5CJ+pTSdwNh1RAOaymtEmYJE28HZoG7siXP8yRwv45ifAiDeIhbDGf2rZnDjUD81p23UQtkhbxEPwE10CafUyTBHw6v6RJPOSHSqrf/tMhaYySmKPXMEmbeEYGiqtwZAnMxSBJkQO2iIdQXnO1Xfug9hpLGawSsUk8hMYCt1zSHugyqbngX43YJh5C9OT6+dLHbZJQji6K2CadV+hkdqY3eWBajxTL822bbH75F3ELiJIElYjfi/15zhzxEBoMWDrp1u3GB7Qc+95UvBosKbE+gBLConHX4VI7SAj/RuzPZ26IZ6StuPUc1YB7xr2zPX+5JR5CxQPz+0mFN78RwR6GJOjZu9r2vOWeeEZIvZyqZHUFE9EoYPvX0yXh4n5cYn0AVcpOTTf5o4iTUs/gHrD1a2ItTEmI9QHUKLxNnJzUoggTVG/Ah+suhXtgex4ajnhe4fVXY6Xw0rZ6BdfG+093Ffv32xHPJ5icnkoeUVLZuzmzCa7hUSW9JGfmtJRYH0DC8kspvCv1AcmXJmSsjJmk76/E/n10xKtB6KrdR8lwJfOVbJHsgLEwJsbGGBPvALYt1gdgUXZU0kYK0TE7H+Gsb5XkwTloBZsshWh0TymsSbF9PxzxLApJ152VtFfSV8kwKRAEf3GhEl42SwcNdWTv28s3Nv1uXdNnFjZ9Z3LTMXj3Q8emY+cyCo1brA/ASWOK9QE4aUyxPgAnjSnWB+CkMeX/ASKwEB78G86SAAAAAElFTkSuQmCC";
                    break;

                case "Dần":
                    _image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ4AAAChCAYAAAA2qQYDAAAclUlEQVR4nO2dadQVxZnHTTITJzH5MJkPk+RkkpPJHCfnzEwyY4xL3I0ioriCuICIAiruuO+7gKKIioqgCK4IKm64IbigoiDivuAGIq6gggu4PVO/um9d6/bb3bf73u6uuvft/znP0fdyl+qqfz97Va8hImuUUkrR4nwApXRNcT4Aj+QflKytpJuSwUqGKZmk5C4lc5QsVLJMyXIlK+R7rOh4bVnHe+Z0fIbPDleyf8d3rt3xG66v0wtxPgBHsqaSjZQcqWSikqeUrJL8sVrJ/I7fPErJJkr+KYfr816cD6Ag+bGSLZWMUPKoFEOypGAsjImxMUbG6nq+SuI1If+sZF8l05SslNYBY2XM+yn5hbifx5J4CeRnSvaWio+1uvOathy+UjJdSX+pXJvr+S2JF5B1lYyVWqe/3YAmvELJeuJ+vrs08YgQd1cyL3692hIEKH2V/KO4X4cuQ7y1lByu5K0EC9TuWCyVyLzlzLDzAaSQn0iFcO8lXJSuhA+UDJXKHLlep7Yh3g+lEp0uSbEQXRVLlQyRFkhUOx9AHdlcKv5MiXRgzpg71+vXcsT7pZLJDUx4iVpMUfIrcb+e3hPvB1Ixq8sbm+cSIfhYyUCpzK3r9fWSeL9RMqPh6S1RDzOV/Fbcr7NXxOslle6OEvkCS7KbuF9v58SjM+PKJiezRHpMUPJT6aLE+72UEatLLFDy79LFiLeNko+ymL0STQHT20O6CPFIcH6TzbyVyACsxcHSxsSjAjEys+kqkTVGKfmRtBnx6Kq9MctZKpELSDgX0gFdBOmIXO/Mdn5K5AgaT3NvNsibdITsD2Q9MyVyxyzJudUqT9Jx15Ska11Q6cgt15cX6fAT7shjNkoUCsxuLj5fHqSjGF12lrQPpkoO0W4exBuW1wyUcIbR4jnxDszv2ks4BmvrJfG6S1mRaGewtpmV17IiHQX/sq2p/UFT6R/EE+IRcpddJl0HdLU0nWbJgnhlP13XA6ddOSVer/yvsYSn6COOiMceidKv67qgl+93UjDxSBKXG3NKUFZraPdao8Tbt5jrKtECGCQFEY/N1uW+1xIGpFh+LQUQr2zoLBEEDaS5Em/T4q6lRIthC8mJeOyZeLrACynRWnhGUnSxpCHewCKvokRL4gDJmHh0Ey8t9BJKtCLgSKL9GkmJd3Sx428ffLJ8uaz89FP5+quv5KXnnpEP32v7A015cEwmxOPM4fcLHnxb4IvPP5PzTjlBrhh1ntw4YZwcuMeu8sy8J10PK29wLG7djUJJiHdE0SNvF8x9bLbstMn6st0G/yc7bryeHNx3N/ngvXcj349mbBPU1Xr1SMdZuosKH3ab4I4pN8q2f/2TdFvnv2T7DdaRU484WJZ/9GHNe1atWqW14PiLLpARJx8n1427XN5Y+KqjEWcGOBO7Sage8foUP+b2wUXnnKFJZ2S3rTaREw85QM495XhNsnOOP0rOOmaoHNZ/T/3vhqR9t9taLjjjZHnswZny1eqWfUART1hqmHhPORhwWwCt1X+H7ppIpx95qFx7xaWye7fNa4hoZOdN15feW24se/fspkmJSeb1XTbfUH/26SfnuL6cRkBzcEPEW9fFaNsBmM/RZ59eJdYt11+jAo3P5aRDDwwlXvd1/0eG7NFL+YF/lUtGnC2L33xDJl52ifRY/3/1v/faYiO57Lxh8v674Rmt7777Tr784gsdzPD/HmF9aYB4lzkZage++eYbWb16lTY1X3/9tcuhpMZrL7+kzaUh1q6b/02OGNBXev7tL9XXDth9F7nq4gulb4+tZPiJx8rCl1+Us487UpNt6L79NMkem/WA9P77xtXP7LHNFjLhkgvlqTmP6fcvfOkFeeKRhzRJD917D9l/t530Zzwi33hJSTzCYScPpPv222/lhQVPy5UXjZKRp50ko844RcZecJ7cfevN2nyhTXzH22+9Kfv32TlUuxk5fsggfVO9+dqr8snHlWYfbrRxF46UHTZaV04beqi++Qg8Du7Xp5OG7LP1ZqGme9SZp+rPeQIe/PdzSUG8fm7GKfL6Ky9XfaOg4JxfeNZp8sarr7gaXmLMefhBGbJn70jikWK5bORweeX552T5so+qZOHGOuHg/TW5iIoBaZZLhp+lTXEcmZFrxo5xedlh4Lm7iYnn7Fgx7vC9tv279mtIuGI+MEfbqIUwkztgpx4y99FHXA0xMahSkDweuGtPbXoZN0FEkCyYSaJYTC831n67bK9NLsHFteMuk6VL3tYVkNOPOkx6rPfnmrmwpeeG62hT6xkYUCLi8WRrZzE8TvgTsx+SBXOfkCWL3tJmC63wwPQ7dB7MTDJkbAXNB956baG88sLzWpuTVMZ9II2Cb3fsAftpgtkEIq2CRjR/D+69kxw1aB85aK/e0merTbVgjoPEO2yfPX1MQqPK/1USEM/btvZPP/lY+34m33XmMa1ZVMGcfv7ZSh2JoskIRm6+dqK+mbiubf7y35GmlEAFIkLCXTbboPpePgupPcVgSUC8W50NLwFwyEnCMtlEfItef831kDIDwROaLc6Hg2i4IiSdcUNMumW+inSpihCcjB99vjz+0CyftB9H1sUSjyc+e/949Yfvv1f7OpShZtx5u+vhpAaBRCXv9rl8p6J4G7gWmMx65COiJQgj4MJ3JPg4pN/u+t8QTDWRM+6KByC6XVNiiLeFu7ElB5qBCA/y3XrDta6HkwpoodtuvE4nk48a1F93rWBubZCng1Bx5CMniHmedc90bXqj3kdw4gm2lhjiDXc4sMR48dkFWttxV98z7RbXw0kFIl1uFtqlyPWhnQgwqFYYvPfOkthUjJHzTz9JZj9wv05O8/c+O26ry2121Hvpuec4vNoanCsxxPPWO7VBpp4Fw7kme99K+HjZsmplgcZQNBLJ4KH77a2vhQCKCL7f9t00cU4deohuKIg1vR1Eg2SrvvxSVz7Mv911802Or7iKxyWCeDwW4EuHA0sEggmca50+UA62R1n6unhu/jw5fMBectyBA2Xe449WX6cJYGCvHWTfnbfTJMNXw40geicRjZYkmq2nAcn/Tb76yqq25LdITnsCUnRwrBPxNnI5qiSgtERW30z09Funuh5SYnCD2I0DJJLpwfvog0pzN76aXRLjv5Dv1Rdf0Fpsz2231K8TwZp0UpyccNDgGvPtCdge24l4Q50OqQ6YfLu/DcecXFirAOLRYRKMTqnOXD9+rNxy3SRNqhryqJuMa4R8kI36LWmTuGACGX3O6T5pOhvVzmSbeBNcjigOFM8vP39E1ZdBW7Ti3oVnn5obWjJDkxEo2UEB5Fr69mLtD9IswWsXDztTVnz6SdWHwzw/dN89upRmEslE+x6XEydJCPG8bPqkxwyn2SwI5of+No9af1KBUiBRKFF5nNYi4p09c4b22UxfniGeiWLpYDbfybzwGgSGjJ6C00RriMfeCu/6jch5UTS3TROF9FbrzwsCt+H2m27QieKgeQ0TSHWsCkhIBs9/4vGq1qRKwVxQRrTfz43pKb6SSpGiSrz/cDueziCtMOyEY2omlDu+FfrxkgLtRdSK7xfXv0eVYuWKFfLh++9rn9C8TgsU/qHRiEYmjBndqSLiEf5TLOJt7XgwNfhs5UptRuzJpMERs9uuIKdnd6QEqxQki0mzaN9u157a5zV7M4Iy9oJzfbYK24pFvEGOB1MF0Z/t0yEjTz1Rk7GdwUkDaL5TDj9Izjj6cB1AhPXdYXIXvfG6bn232+JtoW7r8e40/aAWQzxv6ipTr7m6Jk+F5multEmjIFiiaQCTatql6Nsz0SrBCD4dLgi44aorqvm+oOAXe+ySjBCLeBPcjqWC5xfMlz27b1GdwKMHD+i0Abrd8c7iRXpDD40QRKem3Z2Agn0nEBOzjLmN8gnJd5KC8hQ6pWKI5/wRn2g16pJm8mhspEEyC5g2JAMcb7SLb34QwYPxbdljSwnNaDU0n9nkUy8SHqNcFY9NLbmeKvGc7ximMG5MLOmDmXc3v+2D9AO9ezQVDDvhaN3JQr/b5AnjtR9FagJwngnvfX/pO3pbIVr224KjQjRcvT68esLGcP7LPg+Pa9hk/qvEc3pYB4HDSYd8v9kZzdesqYDIwd1qENo0GBApvvz8s9pJp40cDUNKg43VFNfvnFrcI3cpiUVFqEmEa5l46cXaRFfTKf4m2F8Xi3jvuBwJpSROUzJONBn7tGCiaQHCD6KZgKJ61EKhWTlSAtAXF/YeKgYATcim6dsmX6+T1+zxRRtDFrMftlEwZr6LPrpGSYf5RaMD04Qw9ZoJTY0rZ1BErhLvE5cjoQvXTCQ77DF5aXHv7bfqO59cF7u64vagouEA7wvu8EJumniVLPvwA000tKDdkkSujR1e/XtuI8fsv6+ONKffMkWnONIALU93irnhjB9H0BC1rzhMOAYNFwKNh0YnmTzz7rtSz1+BgGt+EI+tfmYiKYinTQXQz2ZaxcmFUWqL61/D3GKKg90iCD1+nGQQ3HTD4Tn0011/5RWdPoOWpnETdwGNSErozimTdYs7mpOjx9CwVBqenP2wHvO9t91SzdOZVi+OqKDuih9KW1NUQjnoPnDt5rsIPLAgNvD3ivZZY0CUVyWeU9htPixYGrBX1U4tYGqZZPvsklC/aMN1OpWaWEAWjdKc/TrmmDQGkTdaLur7bDLw3aY9H9OOSeR1AhtMrL1HmLIXzaG8Bz8N/5YqDf4n+TqOsIjK2enfU7/FzjLOXgnuvKMnj5uGm/vRWTN8ye/5QTyjnbhr8aeSgvSD6dQwQimJJCtph+ACmd1XUYtIsZ1FZwHNa4N676j9PFrWIUfwM+TXMM1EzfZ+WAhAIwBuhCElNwPfhVazXQEqDWhto+EIbtDImGPSIiSTIZY+RSCw5xYNx74L0kP0KOKqmOZS0kV2kwWfp/RIjdgx/DC1Zld8GuKxGDRGkutiss3k8l0jTjq2U9sRC0ZCmiAB88ci2f/OotPjR47PBBxoKt4PqCIESceeD/av0ppu9kiY78KUAn3cWAdhqLcCfDv7e/AX8SnZf2GXyTD3aECITakMkgV9VzIAHHGBZuRvGivofgGQPCzI4oZwGPXWmFqnxLP9MUxRPaAJ0DC8ny1+mEdIEGdaWVBOHqAagEZAk0y6fEz1c/zXtIpDEF4jsCDHx0KaAr0tVAhA8ORPfD38Kg7dMVoMc0hOkbGzqyz4XQQy4OpLL6q5FvsGIngKugcER/ZWSBONA9qjwuYCq+Awz1cTXDhNp9itPmgwzhmJAi3d9q4rkqWAAw3jiGcLO/BZYHxBiEhkyIJCYDSBIRJ+EYCQ5PmCfhVpH1IqdjqE19F2JKI546RKGmVuKQmi2cgVBseEpsZkArQoXcVRTQBRAslNKoqbxbYERoiiHZ8wWpNOcZpADpoxiMEiBCMxNjoHnXtMJtoLx7leV29Q6HoBmDj+xsnHL8IP4m+OhQUsVLBlHd8Pc//IjPtqAgtSIZCRFIftj+FX8juYZbv3Tm/TVFoLs65Nu4qEDTisiJOk6l0Hv4n/iBCQgPvumBZ6BgvVDXKHDvdk1CSQnd4CkCy4cwpzg4+DP4R2Yud92O56fB62CkLKoP9josu4ReN7+TxtSBCCVIwxUScfNkSPj4WyT/NEaF8CJGvtBSYAARw7FvwtXAoqFBDV9NjhVxKZk5AmVYP25OgJfhPNV++AR8psWIgjB/av3oRE4GGugRF8RW7uNIFchqgpmTltEmCi8L/CJgnfLC6VgHDcl+7E7dh3YJsVmiLRSjjf5OjCtAApGDqB0QYkg9EGEITP0wnCzn72rNqfMSdVGac+SEizQSdMGCe+qZ3aMEV9cnh8N8TE/MadHMXNioYjiOC9XKsZU9QZerbY/mCBqGkSmOBiBDaoIiQxK2ECQcL8IRYNDcIiQyYWGv8raDY5+Ic0CqaKKBCtxzZCFg8iYfLvv+O2moQuxCSFQZXAJjw+Kq+TKLZNsC1E19xs+JOYRLQTPiTkJmjCF2SnGAnleteN1kLb4c9xfSS54z7HnKDt+F1Hp0nVtEV50Qj67pIlOnCAGPW0XFqBVNNuuFYngcmj2USl7EWUZwr1BCpgTEcnNBoR2FoMEj547926czh4w+Aj8jpVjDCNxfeAKZMmdNLSCMEWSWC+P2w7ZJBI/Bff15AwjqhoRcd5vJpGUK9a32lXIv+FyUE7YCKJBOulTJIIGgznn537RiMRRBAomEOuiXIpmxGBsneVBUOzocns8h4VAcb73NNPVQ9VNEJ6hNQJpbIgefDtMJHBz9TcKOrfuBEpv8WZ26RCg60nNdya1nevNvsYYOIwHzRx0iNH5BqWA0srppsDTcffmDdIZg46RPgdtBYmE98PkhKpQjSz+40o2iSYiXyDKRdIA/gOO5GLaaU+W49QJMJJICc5siJOGBda3hPUbPbxbntjFCBgsEwWFBaKo8C4w8P8RhYeQDZKa0Sg1Ebxz+zvMCctmV5BImT69EhAm4NxiFTN++jvsztLcPhn3FWJ20ge22eihDn/vJ+qiTnGzO4+bpR0+HvPzJsbPplu8EfxfUN3FHjGV5TjjrBgdITgz1FjpWXIPqyaiJGyEcTErBJ8kNKwE74IJKJ+axMS8lF/pcJiXiONw844TCtn99mar3LcxlytveNSHLgTpnMFULojGm+mV4+0CSkZj9BpQzcy3+mQUoAFNg+eixIIQtcL5GIR7aMgDFnwGTGBbLChfJYkBYFA1rCDc8ipkXohKrVrt0SrEJvDdMK+D62M9qXmSjMnPiABDSkWUiWNuhfT/DsttdMRFl6kVNIgrJcuioBUA6hIhLU0UR6jmyVOG6URghc0lU08Q66w38eMohG5mexTQDHHBFekeSBl0H8kmU4XTVTfIQT2cKdZ6KE9Xh9TFgRn4yUlA34c9Vbd/WH5S0SOaBS6l5t14BsVxkBdlVRIWGqFyg2+o01ktC0VD6odYTvO0LyePoI+9Jgy7w9mtGG3yycRKhL4dcY3hICYM4IKkq9oRlqPKKGlLc43I/iPtPqfdez3aRpuAspmpJLwV+32fMw2x5cRcdvNpEbQgJ4FEzZCD2b07ihaMvvc2TjyBAskXPHVSILGbeYJE+qvpDdI/EIy09ViOlFoh0eDkHYw2gUfEI0Uti8jKyHQIR9o/iYI4joN+H97Xwbml7OS7dNFjUBSevc8ReRRtN4dvk3zQFgncT2BXCymbYao55oN4/hHJIjZA2EvOAV5Ils0DgtL2YpaLU/M4YF3cQnfLIS8IDcYKSNyhqRpSMfYUTU5xaiSGEGJvXHdM0Qevu3V4wbwe+rl66IERxwfh+4S8xpazkS1LDCOfJhfhwlmwTFlNoiM8bUgZL2Ol0aFMZEwJ7HdvaM5Am2X5Pcw1c1ut8wZsY8b8OYBK0RySR6TGSbsWaC8RWt82s/SihUHIkUK8WjQLEp4tvDIARCM2PE5qfvio1JlQfNiohFuMqobNB14jtgHrHjzSCnMTaPE43gKTA7FeIIJeu2CyeEoocET0tcDBORsFyJlyl+4BGgmtCj5QNPORaSK6accR4WE9q2o55WRH+Sxn3TqmF1yRLCYe24kynVoYoIR3AAiYeq5Hh9XYVD3kVKIFw/RY2FtpztMICYLSjCAVsD/wVczDxhmFxqPFkUbsJMrKXHJ6dEGxX4LFrweeA99dDQ30G5EcyhBEHVcWtEx0WbnF6DFPurgHbpLiMAhHqQl6UzNlhQJndovPrOgkxvQAqj7ED3Em8eGQpq4Hj20DHc+5SgWniCAzt0wX4fvsvd2JBHKXSRpIRBNAPiNWSw6/mvwxNMwMXtz7df4m0ic7pYWImCix4Y6fVByECw2h9BE1SxJg5BMTrKHAM1D7qteg2WYYC7ROmz2pjKBNmwGjCXY1ZxGOBeZsdDyj2b3GIkflIw4ezR8FLjD7RMygwIpMHFmT2kU8P1IsJLvYuEw5/Th1Xtaoi2YcwIY8otJ/MEosJmHYKFes2ec4F5gnmmEwKx7WLFI/Gh4pJ+jQcYCv4+WIZzwqIVIezI8lQuK83SV0FBAAJDkuWFGICDaq1Fwo+C3ka9jsw/5QiJmIlw0c5pSHvNCFzX9gjQIvPma082DBvtJCuL9TDyJbsNAT5ydzbcFjYgP1OhpnxABTWSeAp5E7GMjmgXjJlghqsdvxayTOww2HSTxDwlQ6ISuZwVyBNHszyUF8RBvnrAbBMncqBYjJIvSEX5TmjwggU1eoD2KJgfKfFE3XBwBHe0mAxwLEcqvOOKt62SoCcFmGBzsqAlnj0HciQRJgBYjlWE3kYYJqRGzkTpPULtO8hC9MP/XEdaXBoiHePl8M0DSNC4lgcmlK7jZw2kwfbSt812kVqj30sGCKea/JKlJ+hZ5/hwmOGlzKOkXs9+2YNBYHMmtesTr42DAifHQ/ffETjpF/ywfV4CJJ8DBZ8IPc+g76U7neuQjWicH6ahxgPpfw8RjL8ai4secDCSK7a7doJBgJlptVxCN24eWG+HYM/oVm0n1NInFSn4sTRAP8bozmVTEgJ16RPpeT8x2cj5IYSBxzn4R6tGcnUIUS1+hY1Q7jaMkCfHWUuLs1kkCHHu7Baqq8bbaVBOz3YELQCCEhvPgqY1wJTSFIimJhxxT8OBTg62MnItin4fMrv8kRf4SmaKutpMUxPupkqXFjr8xoOFIIBNtkgguUSgoYP9EMiQeMrDQSyjRijhAEvIpDfF+pOTpIq+iREuBgjUcyZx4yGbFXUeJFgPbJhJzKS3xkBuLu5YSLYKpkpJHjRDvl0q83s5UolDwyPBfSwHEQ7xpjy/hHBzqmZpDjRLvB0rSP9uzRLthplS4UBjxkN8oWVbE1ZXwErhbv5MG+dMM8ZBe+V9fCU9B51LD3GmWeIiz9tYSzjBRmuRNFsSjnNYyp4mWaBp0XbDmzomH/F5Kf68rgNTJHyQDzmRFPKS7VDbvlmhPsLY9JCO+ZEk85MAcL7yEW+gHo2QlWRMPGZbftZdwhNGSMU/yIB4Jxcl5zUCJwkEdNnHXSVLJg3gIGz2cPoq0RCaYLnU27TQqeREPoRP1gTxmo0QhoBzWdNokSvIk3hodAy/J13qYJZXzc3LjRt7EQzhi3rtjz0pEAvOaaN9EM1IE8RD8hLKB1H9MkZx8uqAURTzkh0pGZjlLJTLFKMkheo2SIolnZIiUFQ6fwFocIgXzwAXxEMprZW3XPai9ZlYGSyOuiIfQWFBul3QHukwyKfg3Ii6JhxA9lf18xeNqyTFHl0RcE88Incyl6c0fmNbdxf16e0M85N+k3ECUJ6hE/Fbcr7N3xENoMGDrZLlvNzug5Tj3pqHdYHmJ8wFECJvGyw6X5kFC+Ffifj1bhnhGNpdyP0cjYM6YO9fr17LEQ6h4YH7fSTn5XRGcYUiCnrOrXa9byxPPCKmXw5V4/cQ4R+D41yOlgOJ+VuJ8AA3IWh2TvDjhorQzmAOOfs21hSkPcT6AJoSniZOTmpdggdoN+HB9pTIHrtehyxHPFh5/NVYqD21rV3Bt45SsJ+7nuyReQDA5/ZXcpaRlHl8dA67hbiUDpAXNaZw4H0CO8gupPCt1mrSWJmSsjJmk77+I+3ksideE0FW7pZIRSh5VkvxJyvmDsTAmxsYYC+kAdi3OB+BI1lSykVSiY04+wllfLfmD36AVbJJUotFNpLInxfV8lMRzKCRd11bSTclgJcOlQhD8xTlKFkqlg4Y6sv308hUdry3reM+cjs9M6vgOnv3QveO7WzYKzVqcD6CUrinOB1BK1xTnAyila8r/A9W5M9PV9KFuAAAAAElFTkSuQmCC";
                    break;

                case "Mão":
                    _image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ4AAAChCAYAAAA2qQYDAAAXWUlEQVR4nO2debQU1Z3HTTITk5j8Mckfk+RkkpPJHGfOmSwTQ1zihkbZFFEEWQSRXRAUAUERF1DZRBAUEQSf4AaCCoIogiCKiiKLCoqiKAgIyI4iiPib+6l+1VZ3V3dXVVfVraqu7znf4/HxXr9bdb/v3vtb7u93jIgckzJl2NQ+gJTVSe0DiBD/RfF4xXqKXRSHKk5VfEZxmeJ6xV2KuxX3y3fYX/u1XbXfs6z2Z/jZYYpdaz/z+Nrfofs5I0HtA9DEYxVPVeyjOEVxheIhCR6HFVfW/s6+iqcr/iiA54s8tQ8gJP5Q8WzF4YqvSDgicwrGwpgYG2NkrLrfVyq8Cvhvih0UZykekPiAsTLmjoo/F/3vMRWeA/5U8TLJnLEOF85p7PC14jzFdpJ5Nt3vNxVeHusoTpDcQ3/SwEo4UfFE0f++q1p4WIgtFd8sPV+JBAZKG8V/Ff3zUDXCO06xl+InDiYo6dgkGcs8dtuw9gG44I8lI7htDielmrBDsbdk3pHueUqM8L4vGet0s4uJqFZsVewuMXBUax9AGdaVzHkmhTvwznh3uucvdsL7peJ0Dy88RS5mKP5K9M9n5IX3Pclsq7u9vecUNtij2Eky71b3/EZSeL9RXOj59aYoh0WKvxX98xwp4TWTTHZHimDBTnKJ6J9v7cIjM2NyhS8zhXvUKP5EqlR4v5fUYtWJ1Yr/KVUmvPqKO/14eykqAltvI6kS4eHg/Maf95bCBzAXPSTBwiMCMdK315XCb4xW/IEkTHhk1U7z8y2lCAQ4nEPJgA5DdFiuc/19PykCBImngScbBC06TPYX/H4zKQLHYgk41SpI0fFXk4ouviDSEZivLyjRcU6YE8TbSBEq2HYDOfMFITqC0WlmSXIwUwKwdoMQ3tCg3kAKbRgjERdet+CePYVmMLeRFF4DSSMSSQZz61t4zS/REfBP05qSD5JK/yARER4md5plUj0gq6ViN4sfwkvz6aoPVLvSKrxmwT9jilI4cuSIrFz2qry0YL4c/PLLMH91C9EkPO5IpOc6zVj83Dzp3LyJXNrwn/L8nFlh/mpy+X4nIQsPJ3F6MUczFj37jFx4+klS74T/NXjlpc1l08cbQh2CeLy95lV4HcJ5rvjj8OFDcmDfPjny9de+fu47K9+UtufXy4oONj3zZFny/HO+/h4H6CwhCY/L1um9VwfYvfNzuXfEELm+e2e5787h8sG7a3353PfXvCPdWzXLER08/+QT5NmnnvDld7gALpZfSwjCSxM6HWLG1JocYbRueLY89sBE2bvb+9/tu2+vlo5Nzy8QHWx04l9kzgwt00MCaaDCOyO8Z4k/Jo0dZSuQ/ld0VOezubJj22eGVWrFt99+K4e++kr27d1j/PvmjZ/Ip598LFs3fyqvvrhIOl3c2PYzYYM6f5JHJ03Q9LRylgQkPO5MrArxQWKPpYsWykVnnGQrErbFbq0ultv69ZbxI4fJ5LGjje141OAb5aZeV8rVl7c2tlMs1k7NLpCul1wozc46tajoTPIZmvCWuMhicSO8TmE+RRKAYdGva4eyYvGTI2++QecjXyE+C49s4q2hPkJCgJ+tyWknhia8m6/pofNx0Yij+xpOhXdtuONPDlj1BvbsFprwene8TPcj0zjGF+FRc3h7yINPFF5ZvFAa/+NvoQive+vmuh+XsrhlLwo5Ed41YY88afj68GHp27ldKMLrcNF5uh8XlF31yomOWrobQx92ArHwmTm2QmnXuL5cXPcfvgmvVf2zdD8qQDMlLwmVE16L8MecTOzft9dwIFtFQkTj/bVrZOZDD0r9v/3RF+HhcokIOGx6Ft4KDQNOJI4ePWr46KwimT3tEePf1q5eZfx/r/aXGilON17d3bPwSBqICEgO9iS8OjpGm2TMf/qpHJFMHH2HfPPNN7Jn1y4ZectAeevN5cb3zXyoxrPwcExHCPwVuBbeeC1DTTBWqNWsfp0/5WyLr7+8JPvvZLEQ8mpx7pmehcfnRwiTxKXwMIeT3JBOC9atebsg7IUxQGB/+atL5fbr+vhyzosQaPz3M3EhvLZ6xplsYEhccs7pvlmwMRAeoO+uY+GlZcUCwDurVkjjU06oNuFRuMmR8OhsnYQmw5HD0hcWBC66CAqPi+D/Lg6El6a1BwDcKRNG3VGNwgNdxIHwntI2vASDrOM2jc6pVuFRsq6k8Oj4nFqzAWD6g5NDEV1EhYd1e6yUEF4kAn1Jw8aPPiwIl1WZ8MC5UkJ4wzQOLJH44sABGdCja44wSApo+Pc/V5vwRkgJ4S3VOLBEgVQorjMOH9g/Kwicx/cMv12WvfSikSkchOhINogoXpMiwqMtwFcaBxYrcOvr7RXL5fWlS2TJgudkwZzZMnfmdOMsN27EEENY7S5okBUEF3ZWvv5a9uc//vADGTqgn1zWuJ6vwjvv5L9qfCslgYsOjRUILzL5NFEFVw/JJr576K1yRcumRriLVYyMkAtOrWME6K2xWCtJCMjHVwcPGgJkFeR7uJHG9UVoLU3hhmQ6Rxhcjy0QXm+tQ4owKD9BnRKyiItdVyxHVrbhN14nj095QA4dOpTz+fz/mtUr5cN17xkr6dZPN8mGD943RD6oz1WuzoOML8LIZiZbhVejc0RRBClLbI/XdmlvbGF+bIWkpnNB2yk++XB92e2Yc52ZSNr87NOCeyGVY6rYCC9N+rTg823bjMvRTU77+3fb2Ckn5Py/F7ZucJbMeuxhIzdvw/oPHI2jVPWArPBqt3ic1BEG1URzhMfdikMlf6SKwLZ3Tfs2OZPLqjfvyRlG6Qm2TK9nMEMotSJBUM888bgsnPu0scVy5qPQz5ZNG40yFiZG3HS9rdiKragRBiWzCFJkhfdfescTHXApJz8R89Z+18iB/bkBnXlPzTTKgvmx/UKMlWE39DcEjgWMuLmLwco4dsjgAqOF/7cTHzXyIo7/FovwztU8mEiA6k75Wyki3LZls/HvnM049JMpDB68d2xRK9YvUoiH1dXp74nAhe5yaCgW4XXWPBjtIOW80Un/VzCRTeueYjiBKa5D4Zz2FzYyLM3Pt283tkX+P0jhueUNPa/Q/SrLwWjUYgpviObBaAVbmp3oipHvNStvPv34Y6FVCXDCQX2v1vw2y4JyVsdUvSvluVlPesoK5k7szh3bDZcLdfCCjL26IWfDiMNwqZjCq8oWn6veWFbRjS6zFh31iNmSdYsOEq6LONgqssJbpnkwoWP7Z1ulZ9uWtpOHtdijzSWGsHBP2H0PITLis+DhifdGYsXDeayh+LZbvCEW4ZX3ZCYIhMBG33pz0QnEknzykanG99r50KBpPRLuokKTbtHBqy5rlbXAI4yPxCK8LZoHEyqIu1Ko2pwwjAVWC6tfjNXwnmG3GYkA1sk1VzYamsydMd34HjeGSZAccGUXOfjlF7pfbznsFIvw9moeTGjY9fmOnBAUq9XyV142XCTlCueQwEkuHf67Yt/DZyDi5v88LXTh4ej+9uhR3a+4HNBa9QmPvhPmRBElIBYKu7a4qOzEkvbEFUUml0m2+x5isR+9v07mz37St8QCpyTyEQMcFIvwqgIkbrIaMUmEu0g7ArOnP+q4TBhB+PXr3jWyRi5v0rDg3+8cNND4TNKf/Co95pR33DRA5+t1g+oRHv42a7o5bhTy34Db+64YJuDh+8fnhLIIbSFKgvzFrOEgOXrwTRrfsCtUz1aL68Mah8UdwnYIWJ3yJxHjw66vBGc3s0vi6uWv53wPqeyASEjYooPj7xiq7f26QM5Wm2jh7d2z22hYkj9Rt/S+yriUQ8YvRRHNr2O5PnL/fcZWbG1Uh2/P3J4BVq1pHROzZaXDP6hjtWPlpa5eDJBjXCTancKENLDJ7iBNHIsWsO2Sg8cKVjNuTPZn2b74XhIECJEBWgjQvwJRsmUTHzW37TFDBmlZ7Ti7Un8vBshxpyTWgbxxw0fS5rxzi07Y4Gt7ZTtb43ylopMVbKc4kemYSDoU+XpYtGbTFDrpvPvWaiNdiq0u6DSpYuzRtkVBzmBEkeNATmTIjKiCeYOrGHF5mFEKE+vfW5tdwQCGCdnB1CbmbGj+LKsoDmb8grhRdAgOkuTAth8T5ITMEpkkgD/NSRIAYuLK4hMPTzEa2GFAUECRVeSBu+8yjJD8agBRYt/Ol8chYmEiJ0mgRu9Y/AdO3uk1k1xNoN05EF9cFBIASq12hABjhJy0qMjn0rgFKwBhMN3CCJqsyhwFYoScRNDEpb7TYNjv8hBRZExcKFbkpL4n7rIPPSPsts4kkdgxVnvMkHPZJ3HXGzEIdAsjaJJ/9+UXB3S/arf4H0nqhW6K64RVb1gnSXOP2fmu4EI3XKl1SD7iyJEjoTYn9kpCa5xDvWaxkFUTMxSUsEiUSwVnb9QNi6vbtTb8jFwS9xLbRawvL3xe96t2C9uiPYkpU0boyxphcELOS6Q7WYspBkkiKiZu69/b9c/j5KbGS8xgW6YsMYUZia+WugfBpBHqssZVzRgtoTJu4xN096OBMX62++8aadxY69y8SWa1Ur+XmigmvAiPbBg35c4iAtvCjIkpRUu+XLFgPRdiaFhHNvK0mvuzvSeom8LtM0CwnRKzry1ZbCQI4LbwIjoSCD7b/N2tr00fbzCyhDF8zN8Fhg641vVnU5yH+yMxQtFStLEsvk3GCClKry5+wUhXAo9Onlh0wqhTbMVLC+YbK2CLc84wmqDkA0e0l/oo/bp2yGaLbN+6xehJCwjlkQNoxUMTxrn+fOKz+/buCeKVBoWixbdj126ArYbgPec5bvLTdnP3rp0yacydJVcKhPreO29la9CZHbG5yENSKCI2gSO6Zb26rkRBnh+fBR57YKJxkYgzJN0b7UBOoFvh3dy7p9HKIEYo2W4gNg1WaKHerdXFBRMyeexoI7O41KQhVM5w/bt1kinj78le2uGuLOcwxGlme5DG7tbd0b1VM0MUZCtbb5pRdcoOrKpur0PS0RvrPUYo2WAl8i2laEZHY2Hztlg++XqpAtmkqmM8cKbj8o/dWRDDhLYB1EShYKL5dRJKnRgcbLNsqQiP7BFWYzJcil0/ZIu3pt47IQYL/sqYoGxLKRjZJnqck7i5XypNCUOgmEXL6jN72iNGZAOwxZVazaz/xurKNsyloXKiIDHUtDhJnUesrKZs8XbgnEpFeTfC4w8nBpe3TZRtogcj2TaU8xErSbkJoWeY3e0wSElXc5XAwuzTqfxksyKSXmUKiX4V5X4GgZtZIxRwxFnMFr5j22e2z8b3cKfDjfBiloPnqG1o5Bolvzj/WceRCDKHi221lCUDpLZTugJRUU/O7qwIWamm3jcuay1zrnTqWsFNw+qIawerGNdNMezft9fRH4FJVvT8uyERhuNGyTASreE5z5GO7iYKwaQU24op84+1abpHMBwAae8Fn6POgpSnNYFYKYrtZlVCuJw5SacvZYGyCuPfc/q5WNl0BIoJHLeGh201DTIHL8yb49l5C/NLxLIamrfDmGiEzT1Ys24KvjGzwCK5fITQiIeSQu8lnspnEJXgbFgKnDnd+PIwRKj3EhN0FBfC+6lotm45U5W6lliOXZpfaGxHowbfWPBv3Agzm5vgZ8OIwKVCUiXbummcsHJSlsJrQinnOnrVOgF/ZE7dNkOu7xuXyz0s8z8TF8KD47UMtRa4MyopesO5DXDn1VzlTGJ1svXBgVd1y25f5ipCWM3r77X+DsJyTsG9XKeOauK+pmUecUySIvoqJbw6WoZaC+KclQiP1QoLFMNgnKU0GcS3hjsCy9N6fqQKAOUuuKNaiejYltetedvV82JgkFDg5PMprxET0NHPtfCgNtNpyr13V7zq4NagDRShsfxVrNi1RSbfzCLxSpJQzXAcyQCImZ4YxG6LrVT8gdgdC+zIH00MQM5WUW2VE14LDQM2QHpSfhlYL8SvRzEdnM9uLmVjkVIbxcudWlZRjBeqUOHw5vdSM5kaK7QmIDvmqI3zlz5pTj7fam1HGBSJ9iw87mJsDH/MGUuP5nJuEzrtSD1jrFdim/jlOP8VExRuFIRCShSt3YnnVvr780lRSKzY/L61FHt00rZgwqgRUT/jYcb/UCoQHtSamUz5/I5Nz694sikxhi8OkCOHmwTnLuKGJGaSMkWalDXdyFrM0U/ivDbbFZjA2HHy+/ALciaMMLKZxsXoRHjHKWrNOCR7g4LXld6jwI3C+chJVSXSpii0GGS7KD4/P8OE8ZX7OfyRWMERBVqxdaGIS+HBfiEPvgBc48O3R7SBmK21XYAb8nP4+GruucuIYjCBbMOIm5R5s3BPsXivnyR9K/+PYO3qVY7OtoTgIpokUHa1ExfC+4ni1nDHbw/ONkwW7goa2JGaThCe1ZD4KK4M4rB49xFYfv+K7FanvkZUhFWQEBqGBGcvr85iL66fMbcPKjjn8Wymb7EUeU7OhBEDWRA/Fh+FBzuF+ggugBixEiGrgPXgzWrGYdwPI8WOuGywgFkhMQzsBMgqi8jNlC3IH4k109kKpxXjyUKO2FmPnqWO9ORGeD9QXBXmU/gJ/IJBlBtDbO0a1zfy7TCCWK3w41HWlnR3/kuu3XXKOsZaxnjAoiXNq9hWiSvJWnu5FMmuwU8YAeAxRyO+Cw+eGd5z+AtWviDuzLIyGXc+1DZNwxYSAkjs5G4vsV+2Q1oQQM6o1FEu5wohW4XIjdMx0FvDdFhrBNcmHGvJrfDgtPCexV9wIA+q1RNbLv5Cv4L3rIhOx4rljVNaI2aKSx15Ed4vFSOxtrsFqwJGCf46Au1EEnAmW/tfVEI+x88ETdKxnBTzRvQYWpqA0/PXEoLwYCTT492AGCoOW7ZgskhIhyL4zhVJLvh4ESPZJWa6lR/g/m254uEQw8ZNJozPoKinaw15Fd73FL/rNJIQcNg3BclZjBYDCJIULVZI4q/4+GgLz3kOfyLGAvlxFOkuZTB4Bec9fn9+yQ3zfIk7iGwaTT69RZLRQmjCg79R3BXG0+kEhgDOa1YffG5EGiC9MSDbN1+3lqTwG/x+nNt33XaL0aHI9FVyT/eNpS/ZJhyEAI5bvxOP+qlEeLBZ8M+XIqIgc8mzdioVHpwc/DOmiBimSIW68UN4hNNiV6gthWdQ1ZM51y48+HupgvNeCsN18gfxQTN+CQ82kMzl3RTJBHPbSHzSi5/Cg90CfPAUemE0RvGLfgsPxqJNdApXoIGvrzoJQng4FGPTwzJFWRCHdZx14pRBCA9y0SORrUirDNRVK3lpxyuDEh4kE9U+0zFFHEA4rGK3STEGKbxjageeii9+WCyZ+jmBaSNo4UFKzEei7FkKR2B7dXRvohKGITzIOSG2CaRVhBkS0Jkun2EJD35fcaSfbymFrxgtAVivxRim8Ex2lzTCESUwFz0lZB3oEB4kvJbGdvWD2KtvYTA31CU8SGJBbK9LJgBkmfgS8PdCncKDWE9pPl/4eFAC9NE5oW7hmSSTOd16gwdba0vRP9+RER78D0ngBaIIgUjEb0X/PEdOeJAEA65OxvLebkTBKkfdG0+3wYKi9gEUIZfG0wyXyoFD+Feifz5jIzyTdSW9z+EFvDPene75i63wIBEPtt8tLl9+NYIahjjoqV2te95iLzyTuF56KW53MRHVAsq/9pEQgvt+UfsAPPC42pdcukFYdYB3QOnXQFOYgqD2AVRAuonjk3rTwQQlDZzh2kjmHeieh6oTnpW0v6LrSPHenPEHz0bT2xNF//tOhZdHtpx2irSvDq6KTnjgGZ5VbC8x3E5LUfsAAuTPJdMrdZbEayVkrIwZp+8vRP97TIVXAcmqPVtxuOIrirk1/vWCsTAmxsYYQ8kA1k3tA9DEYxVPlYx1TOUjDuuHJXjwO0gFmyoZa/R0ydxJ0f0+UuFpJE7X4xXrKXZRHCYZgXBeXKZINxMyaIgjW9vx7K/92q7a71lW+zNTaz+D3g8Naj87tlao39Q+gJTVSe0DSFmd1D6AlNXJ/wfwGsx/EpNEPgAAAABJRU5ErkJggg==";
                    break;

                case "Thìn":
                    _image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABSCAYAAADD2VOmAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QTYyMUJDODczMjUyMTFFODgyQThBRUQxRDkwNTc1NDAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QTYyMUJDODgzMjUyMTFFODgyQThBRUQxRDkwNTc1NDAiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBNjIxQkM4NTMyNTIxMUU4ODJBOEFFRDFEOTA1NzU0MCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBNjIxQkM4NjMyNTIxMUU4ODJBOEFFRDFEOTA1NzU0MCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pv5NeyQAABQQSURBVHja5F0HWFRXEz1UARUBQQ0qNhA7IrEX7DVi9xd7x4a9YoFYYkNFYtdYsMQSexcEO/aCBVFRBGyAIiC97D9zeWuUKCrswqqT73yI7t63O2/uzDlz731Rk8lkUCErTChEMCDkI2gSNAjJhERCFCGS8JwQpwofWDMXr21KMCfYECoTShBMCIaEvAQ9yXmMFEISIZYQTXhNCCPcJ1wj3CEEEhJy+kuo5XAEWhJaSahCKKKgcdnBdwmXCPsJvlKk/hAO5IhqS+hJqEfQzYHv9YhwkLBVitDv0oGcywYS+klTNTeMI/MYYbn087twYH7CUMIIQnEVKlDswHmE06rswC4EZ0JFqKZxNd9AmEl4pkoONJXubi98H/aU4ETYpgoObEn4MxfzXHZsDWE8ISa3HDiBMIuQB9+vMfXpT7iXkw7UIiyVisWPYKESzTqdEw7MKyXiLvixjGViX8I+ZTown5R42+LHNNbXvQm7leFAbcKWHzDyMto7QlfC0a95sfo3DOz6EzhPPss8CNaKjMAxhMX4uSyAYEt4ld0IbEyYm2PiNSUF13zP41lwcG470FLiiZm3/DgCM4Ex4aEsB+3Y3t0yu7rVZdNGDpOFPA2SqYBNyMxHX4rABTmtMJ6FBMOifAW0sGuPfds2q8JUdiFUy0oEts6N250QHy+LfP1a9jo8TOY0wkEW+y5GFaLwFEH7U3763PzWlZoDOWppaWkICXpCHywNd25cRwFDQ2hrq4RKtJWUyvqM//A5Bw5G+jpFjpq6ujrexUTj+P49NDWAFu07QoUWvWZIKuXNl6ZwQUJITs2NG5cvypbO+V12+sRRWVJS4r9zhn7v3KiO7O/1a2QqZJO+poj0IRRTxi1MTU2B22wXTB46AK/DwhAV+QYLZzhh7zYPzJ40Fn9MmYDk5KR0ORAdjQf+9yBLU6llV+6yF8yMB+aTpq9S7OLpU7h9/QoCA+7j5tXL0M2bF0mJidDU0oaamjpOHNiLP+fOxnkfL1w6ewZN29jh8rkz8Lt6RVUcyIH1v8xyYAuJQCpWoce+w6aVy3DG8xgqWllTlCWjlLk5rl44j7z58yM+LpYQB8OCxiICqQpj0OhxKFK0GM6d9MSS2c6Y5bYcxUqWUgUn9pcIdsqnHNhXGVfcsfEvUI5D1Nu3OO11AkbkKP0Chlg6ZyaKmBYVzot6G4lxLnPQumPnj97bqGVr7Nm6CdcvX1QVB7JGrkM4k9GBJQkNlHHFuo2aokvvfgh+/BipRFXOe3sKhzwK8MebiHCYFP4FM91WoFCRXxD+6iX9nr7e/pZypOfBfQh/+RJlLC1VZRpz2rOXO/DDHNicoK/oq4W9fIGnjwMRGRGBClZVUdm6GoaMm0T50Aevnj+DTa06WLpxK0VkAWxa4S6ozJUL53Dp3GmayhHEC4MwZvpMmvrVVKmYNEH68u1HNGa/Muq+/20/WdOq5WVj+veSUWWVXb/kK1v0+3RZo0oWssUzp8vu07+7ukyVDejQRvb4QYDsRWiIbGCntrLAAH+ZClsqoc6HSqRgpnovi0YyTJDilJRk3L15HSP72OPJowfImy8/Rk11Fjdu8cwZqFzNBu6bd0CPqvKNy5eITMfA79pVKiomhIKq2OrimduIcEHuQAtC0eyMyLxu85oV9PMVtPOQ/FJTA0UUAu7cFnRFNNju3kb5KlZwXuQO78MHEUyyzWWxO/LT9L105hSKligppnnNBrbYSYWnyq/VVdWBbPU+bKjy6tqK7IzGkfWX+xKEPHmMJw8fIDEpESVKlUH9ps0pqi6Kv2NHuVG+C6aceMbzOGa4LsU5by+scp2Hxw8DYF2zFhav80AeHV2K3ncUqflUueH6mGAjLyJVsjuaaTEz2DZvJXidGhWCZkSC1+zaj659BwiHMGE2tyyP0haWOHFwP+KIuqx1WwTnMSMQ+OA+ChqbYKDjWIpeHTGeijtPTqpN5VM4yz0/juATB/bhn80bUbGqNXR19WBgZIQm5MD9O7YRtorpzfkt9GkQVi9ZgIf37iIiPEyQ5Dw03Tv17CNeU5g4oRpN/Y8imyIX9FelzMsq5Fu/pMofeN8fdRs3ze5QvMhmxhFoRDDO6iisKo7s2UnR14KiqyxiSXWUr2yF5QvmiKmpThLNrHRpbq4g8s1rbFu7WnA9pi0OYybAsmJl/NalG0qUMcfriLCPxj555BDmTZtEmvktgh49xKpF87HQ2QkraVwuUN+uxVOxZslCpNBPBVkpjkBmrYZZvg3a2pjlvpIcYoALPicxfYGbiJjpjkPwRk8PU+YuhO8pb9LA10SB+PuvNeg/YjQSExPQuPVvNJVjRc+PmwdqVNxYkdynwqNH7/3DaTzKV6pMkXoC+7ZvRemylug71BF6NL3jYuOEgsmnrw8dna/bs3lkzz8iwm2btVDYNGYHyjd0Z9nYeWx1GjXBpbOncYCmbuVqv1JRqI0qNr9SDtQRU+aCjxeGT3JCL4fhOLZvj2ig6usXQP78+sKB7AxuKLCzuVOTGB9Pjk6mqI5Fpx69heMunz8rKBGTbLPS5rAoVx49abz8+v/VAMwCjAsVhr6BAe5cv4rdWzZi6lxXReZBY02JUespYjRvmnILZkzBsIlOeEIfvlHLNhg/sC8sKYrykZMMjAqix6D07TQJ5JzkpCTE00+OQg0tTTGtmc7cvHIZZStWgsPYSVRUtMm5MbDr2k10cMyosrPk8zp8QMi7+k2af9J5co7JWrtrn/5wGT8K7bp2h0UFhW5dNNSUkqFWdkfiD3yYcmGDZi2FPEtIiKdqrAar6jXEtNXQ0MDafw68f62+QQGKrnjo6OkiKPARGpOzDQyN8OJZKEVyU3TrNwBb1qzC2ZPHRXe6eKlSOLZ3N26RE12WLMPcFWuwzm0xpgwfjA49eqE3RaGm1r9fgxQPFas7uHXlkngPR2plm+pITEgQM0JBlkfDxcWFb0k3iMyVdePcUtnaBo1b/QYjY2NoamjCsXc3lClbDgVNTEQVZkrDr2MU/sVUaGFNTU0c3rUDtW0bI4am8dPHj9C5V18smeWMFu06iPTgf8dP3BRNyresUsxKlcbqxQvQqmMXhAY9pil9Q7yvWs060CInctNi7pTxdJMMRWOCi1efoSMFV+VixUxBUVxQU+prpUIBZ0ZMi5v9W0GPHkKztu3E9OQp2YAINTcK5Mb57FnwU9Fo4C+5efUKkasGjx4v2l6Pib74HDuCHgOHUBR6irzKDube4Vo3V8qRkURxAtC2qz2NqyGmejKRd10qPs9Dg4X6YTIuGraaWjhEN2ni7HkwNFKosknmb8Q99GRFjhpNX+42JW3LCpXQuWdfVKhSVeTAD42pzImD+/AmIgL1mjaD72lv4pFVsdNjg4iimW7LcYeKBdOWYiVKUCqoKYoMm5FJIcxduQ52/7OnNJBIjj4sIp8jjq1Wg4Zw99iOvOTEulTYlm3ZAad5rkTWjaFgS+SoY0LF27qydX4j4O4dohaxsK5RU5DkmKgobFu3GhuWL0U0/bnqrzUyvP42wkk3D580DWe9jqMqvc+2WSuqlB5II57WqkMn9BkyAts3rMUfy1ajpLmFkIlOIxwQTRHKTYqdG9ZR/nwoctoT4omlzC3ejx9DdEiHorE7RbAS9XQkOzBScmK2rrJq0Tw8Ioa/9ehJQUk4oTtOmS7+be+2zUjDx4tDTHOGUJW1pGq7YZkbYim3XfU9ByeiGXMmjsHxA3uIAlXHOOdZwnls3OLnqc+Vmx3HC04cmSVKlxH6+kMHBhILSEpITG9sKM8ieAq/5OZvdkbhtVz7/oMFoWX5ppc/H8JevMCy+XNwhqKL81lqysdZogBNN9vmLUWeeh0RDn+/W5g/bTJuXbuMIRMmiyKTEBePGnUbCPWwZ6uHWC/hPFfbthEmz1mAfsNHYfjEqVShS4tonztlgmjgsnHl5TYaV38lWoi6FIERWR0hNSUFLmMdcc7bEyMp4l6EBNP0eSsirB6R50f+96ChqSHUSEJc+gFLJtC+p3ykNtgrUY3lLS/32b/jAr3WhLgek/Adm/4S0u3Qru1UcYOIHiUIvuk0bDDmTB6HqxfOihyYmpaKe3433xcqlm08tTU0lXqeMohpDP+hNqF6lvgfOYOr6X0/P4QGB4lODFc9jkquvDUpoWtr5cFxKhjJKUlER+qK953xOoaiZiXgsXKZoCFa2tqC3vBPTgWsUIwLF8YWqs4ly1iI8VjlFKLIHDPNBWZER0yLF0cjKh73bt3AL0WLC1nJSwBXSK2wBufobm7X/j8NCkUVEMICeT9wCGFlVkdi7foiNIToyiXR50ujaGCxzxqXcxZHWQHDglRkYmBTuy5V1VK4fe0KkpKT8UBquMojhz9NEpHwQaMnoEmbtkLasYLZvn6tWIDi6P2FciFTpsFjJ4guTQhxQcde9uI63LioUc8WXocOoHrd+pjh6qbMfmA1eXzfkD77N98qvss8lVjPFiR6wWvALNOYDNuRdHr14pmIiKLFS2DV4vk4d9JLVFmeXhxtrTt1IbpyA2E0jXm6cUpgXtik9W9ChvENPrDjb4x0mkHKZKUg1Lwwz4vvnega7MDiJUuTemmMA9u3YsDIsYLSPLp/TyghJRrvYI2SO/Ah0s9KfPPhQJ4efYc5om7DJoLbseTaSXnLjyLs1fPn3NkXXRTPw/vFrisdXV3ie9XISYWFeuDdBw95C4e0iYgbCM3tOoiKy2Ps2rSBCsZ8VLCyFtSIqy9vEalR3xbWVIHlZkZONKNqXI+0cVJikqRYdJTpwHMfrgvzjqNrWXEgS7QmrdNPPXA1HDxmgqAxwcTZuJUf/uqF6EizQphClXP3Ng/RSeFuCyuJI3v/QZ2GjUW/j1tc3NHpPtBBjPc8JASDxownpzRD6NMnolD4nvYhHhiJviNGfVQgOPLfhIfTOA9gUb6iaEwoUPNmtDSCDzLIt8OE9ooYvU2nrqJb0qVPf+wgssu/Mx9jca9BsovzYzjRjQKGRqhoVRXnvU+KxqxNrdqYvtBNrNoJaWhmJtKCWAaj9/HUtx8wWGjZl89C/3UepRFefGdH7yHO2W/YKDHltZXnQD7QfefDRSU2fmbBLaZoiujMTBoyQHRiGjZvjV0e63Ht4nmxQsdObdGuI6Y6DkFKUpJYA4mJjhJ8zbhQIZru5ZBMU7C8lRW6D3AQjuc0wXl1dJ/uCKOI5irPmy95zYUj+Y8p44lMmwsH8nLBbZKAcXSTZri6f7LVpQDjBbjhGSOQj4DyWTG77I7OX9jI2AQ+xNdad+yCwqampEvzC8XA6ydcsZNFnoqGusY70bnh90S8eiU6KTyVOXeqa6i/pyCcOytUtca9zTdFlW3cqs37FT11cv5Y59mUPxPEroYH/ndRnCq9tpaWsqbv3++/a4YdoB0IexRxFc5H8fFxguvJu8NMQ5iycL5jJ/IiExeetFTikiFPxa4sdnI7+x5o3aHLf1bmWG9PGTYItWwbimYsE2ru9vQmzczrMceJ8ly/6Iu8pIRGTXVB87btleFA3mtXB5/ZnXUC6Y8SKZfdqxiZmHz0u++ZU0LftrTriI49eouIYuOKySpm99ZN6NSjD1p17Cw6zvJUwO0oeTFg3dzTYSjc58wk2aiDBs1awLJSFRzft0es3nHR4XWSNkSNbJu1VFb+Wy93Hptcibzvb0ndaYVfnSOrbLkKpCZOYe3SRQgm8stcbcfGdULide7ZRygKTyLA1WrWEtU9hQrLtJFDhablow9svOLHzn9OEdzPcbRom/FSZXXSzIEB/iJlDJvg9P4GKdj49M9IQrz8Lz4lFPlwBh/tMlPklXmrBmvVMpblBDg/RpByqGhtI3qF3kcOCw1cpFgx0aVmichOqlGvgWhQMFEuYGAkxurWf5B477gBvcVruf/HDdWZS1fAolwFZXK/ZciwyfxzZ+UcWdcr8srnfU4iKSlRJHcmzuvcF6ElVWPuqMjoPw0qJFxE8ujqiE4NSzZeB375PBQhT56IjZY9Bw0VBUM0IcLDMNS+k1hXnrV0BXLAgpC+ASvyaxzISeciwUqRn4Bb9Ew5OJcxnflSnuL21b1bN3Hq+FHRzuLuDKsYnp7cwDjjeYJU0Ajihg454UB+/s3G/zCOTM5h8OqzQh9Ww5V3/vTJYimSt3N8yXjK8y5+pjLc2YlPiMe1C+dFI4GXTDk31qhbXxByJZu3VBeSP0l6M8E6Re9MTElJkcVER3/Va4nuyCY69JeRfHv/d9FRb2WuzlNlI3vbiyNhOWDvCFaf89GXzgvzreUHeZWFChlH3o1LF0VFzqccpfGhjUMmZ6W/5sB1Q6Qff9fBz2d7kX78P+VzL/iaA9enCBN/Quf5Exwyc97XOpCNn0zk9hM5j/fZ8VGG8C/q/m84DckKZZM08I9svMTbWZK1X7RveWoHl/CBUl74UY0fMdrra533rQ4UvUukHzze/gM6j9fGeZPV/m95k3oWLhQn3aU/fyDncS+U1yUOfesb1bN4wRSpKzEWufDkXAXbBUllnMvKm9WzefElSO9gB3ynzlspSdb7WR1AXQEfwlMi2xu/I8fxojgfnB6G9GdlITcdyPZS6lZ0JPipsON4Owb3vnh/yU5FDKiMp/jmlRg89xRLqpDzuEDMz2quy0kHyo23gw5A+in4crnkNN59ewTpz5H2UsYFcuJJ5rzO3EaiPvWlCFW2cVHjIwH8JPNbyrxQTj9L30KqekwbqiKbR2wzRNptpHfRD0o/o3LiC6nl4pOBeO2SDzlWl5zJ+ZL3cfBWY97bkXFvrkyqmFGSyOfCxU/evSIVrkDJkTlqajLV+v+J8HQ3kKY59x951ZC3JqRKWjxWcuKb3HDW9+DA787+L8AAe7nOm+TfjW0AAAAASUVORK5CYII=";
                    break;

                case "Tỵ":
                    _image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ4AAAChCAYAAAA2qQYDAAAZB0lEQVR4nO2dabRU1ZXHTdIdOzH50OkPnWSlk5VOL7vX6nTSrSgaHIAok+IIgsogyKwoIoIDDjgwiSIOIJMIKoKgoIwqgiggCiI4K06gIqgggiKgsvv8Tr1T3rqvql5Vvbr3nKp7/mvthb5Xw7n3/N+e97kHichBXrzELdYX4CWZYn0BXpIp1hfgkPyDkkOVNFPSQ8kwJdOULFCyWslGJduV7FCyS37Arpqfba95zeqa9/De4Up61nzmoTXfYfs6nRDrC7AkBytppORSJVOVvKhkr0SPfUrW1XznACXHKvmnCK7PebG+gJjkp0qaKhmhZKXEQ7JCwVpYE2tjjazV9v3yxKuH/LOSrkrmKtktlQPWyprPV/IrsX8fPfEKkF8o6SQpH2tf7T2tOOxXslBJZ0ldm+3764kXkgZKxkum019tQBNOUHKk2L/fiSYeEWJ7JWvz71dVggClg5J/FPv7kBjiHaKkn5IPCtigasdmSUXmFWeGrS+gCPmZpAi3tcBNSRI+VdJfUvfI9j5VDfF+LKno9KMiNiKp2KKkj1RAotr6AuqQxpLyZzyKA/eMe2d7/yqOeL9WMrOEG+6RiVlKfiP299N54v1IUmZ1R2n32SMLvlDSTVL31vb+Okm83ylZUvLt9agLS5X8Xuzvs1PEayOp7g6PaIElOUvs77d14tGZMbmeN9OjeExR8nNJKPH+KD5itYn1Sv5dEka85ko+L8fd86gXML2tJCHEI8H5XXnum0cZwF5cKFVMPCoQo8p2uzzKjdFKfiJVRjy6ameU8y55RAISzrF0QMdBOiLX+eW9Px4RgsbTyJsNoiYdIftT5b4zHpFjmUTcahUl6fir8aSrXFDpiCzXFxXp8BPmRXE3PGIFZjcSny8K0lGM9p0l1YPZEkG0GwXxhkV1BzysYYw4Trze0V27h2Wwt04Sr4X4ikQ1g70tW3mtXKSj4O/bmqofNJX+SRwhHiG37zJJDuhqqXeapRzE8/10yQOnXVklXpvor9HDUbQTS8RjRsL7dckFvXx/kJiJR5LYD+Z4UFYraXqtVOJ1jee6PCoA3SUm4jFs7edePQxIsfxWYiCeb+j0CIMG0kiJd1x81+JRYWgiERGPmYmXYrwQj8rCBimii6UY4nWL8yo8KhK9pMzEo5t4S6yX4FGJgCMFzWsUSrzL4l2/RwWDB8eUhXicObwt5sV7FIg9X38tn279RA58/73tpRhwLG6dg0KFEO+SuFfuUTg2vvGajLt5mOz8wqnUap1ary7ScZbuptiX7VEw3nrtVbn+sn6ybcvHtpcSBJzJOyRUF/Haxb9mj2Lw8eZNMm7UcPnkI+fOJucJSyUT70ULC/YoAt9++61s+XCz9vUcA83BJRGvgY3VelQVGkoJxBtnZake1YRJUiTxCIer+YF0VYdN774jb776su1lhMGD/34pRRCvo511egTx7f79Ok1y4MCBWr/7/NNtsvvLL/V/f7Nnjwzo3lkGX9RbvvvOuQlTnrtbMPH8sWIWQTIY7UV+blDvbrJkQeYxNF/u/EKGXHqR3Hr91bJh7Qsy8bZR0uyw/5YHJt7tUiLZgIObCiIeT7auhocMVxy+V6R56YXVctuN10nbpsdoMiEXdjgr/RpIN+Lqy+W0YxtK678drn/f6si/yn3j75L9+5zcNlTwv0oBxPNt7TEDs7niqSdl+FWD5KwTjk0TDoGAS+Y/pl+HHzf0igHp353R+Gj9njUrn7V8BXWihxRAvDnWlpdQrHv+OTmnRZMMwiF9zmkr61av0q9Z+9xK6XxKi5SGa/i/Muraq+SFFc/I3m++sbz6goCvkJd4PPHZR7MxA/K8vmG9XNajS5p0vdqfIR9+8L7+/fzZM6VT62Zy8lGHyejrr5FX16+rFMIZEN0eLHmI18Te2pILAgJKX5DNEG/erBmaeDcO7K99uO5tT5VVTy91MWotFCdKHuINt7iwxOL5Fcs1sYImFq2GBsSsjrlpiGz56EPby6wvRkoe4q2wuLDEYtWyp+TUY47UpOvZ7nT54J2NsnvXLrllyOB0YFEFeE5yEI/HAlSU41AtwMx2bt1ca7qNb76e8btdX+7UUW8VgFwPHKtFvEY2V5VU4LONvuFaaX74n2X6pPEZv9vx+Wdy8XnnyLWXXJi1elGBYDy2FvH6W11SwvDuW2/Ks0uekCl3jZFzW/5dxt96szaxQdw79nZtfrueflK1EC/dmRwk3hSbK0oa7p8wVlcfOpx0ojy1cJ7s27c34/cb1q6Rtn9PVS8Wz33E0irLjmmShXi+6TNGTLnzNk2qRx6Ylv4ZZpeyFxFttzan6N9f0++CWqSsYHCaaAbxmK2omqurBEwac4vWeNs+SY0rv/7yet0UMGLwIGl3wnGadJDv/XfetrzSsmK/pIoUaeL9h62V0LrNXzR/6d+711kRGSbfPlpOOur/dLpEl8xaNk2Xw/i399lnyisvVaUR+k8JEO/EuL/9rVdf0dl5nGqGVfiXqI6c1taPnRtcKTtM4HD6cQ3TvhzdJgO6nyd33zJCp1iqFC0lQLzucX97j7anyanHHKFvfLAwjhbo27G9bH7/vbiXFCumjr0j47qpw6IFq6BCURf0g1oM8YbG/e3TJ0+QpYvmy/o1z+u/cLL01/W/SNqdeLzeCDaBxOln27ZVpQk2Gs8If2x79ybCzR4hAeJNifvbg2TCz+Omf7V7tzzz5OPSr8u50u3M1jp/RQ3zueXL4l5e5Jgw+uY06Zo3+B+5a2Tsf/u2oFMqhnjOPOKTRCldGRTGzcZMuv3WSL8T80byds/XX0X6PUHg06aJd/ifk0S8xRIg3mrLi6kFtN/lvbvpjbmqb6+y9Z+RvsC8k6Yw8wk3Duqvtc4F57aV5U8ujsW0B4mHoOUrrMeuVLwgAeI5lyxC88158H69Kac0aiALH5lVr89jIuvRmdM1udAwlKn4fHD/xHH6Z8bJX7poQTkuIS/wa4PEozslPNRTpXhXAsRz6sQXA0yuyW/RGv7kvEdL0kbvbXxbT2WFW8sRgpzbh14vLZTGMz+jDy7qasE9d9yWJruR/l07yo7tn0f6vQ6AC0wTb6flxWQFJaTxt45Mb0zrow+TUdcNlpdfXFMQAc0JSkxlBTcYs0p1wJA6LBd2bBc5AYjoWx7xFy0EUB1anaCHdwb1Ol//jn68KgVcc5t44J0335D2zRpnEIPcH0dzMQCTDZychEbpc3YbPa8QHJQxBJ45ZZJseu9defCeCekxQSPMskY9Kkjf3cCeXfX6yFlSwTAjjeQyr+jTXR5/bI58/dXuSNdhAXskQDxnQQDAoHLQF2KQ+ezmTfRGjRk6JEM70UYO4YwZoxIAiQgoeI/ReJhTfkb1JJjExsdb/sRi/VloHvzCUvNr2z/7VF5cvUq7CHSY4DvSAED0jMYONnguW7xQ+7LBPwBmLS7t1lmemDdXnypQRXCfeABioR3YDOZOOXoV/4/KBz+7tn9f+WL7dt04GZxdMGbzs61b9ecQOWY69EdkbDaahnYlyM7RXwxSQ1bmWQsF0TgEu/KCHlpTQ2o0KlqW78KcMtTz/LPL9dFiXAc9ecEBbtOVQuTLe2idIvKGgETl5D0rHO6bWgO6N0gosylXXthTVzYMadBSzJmygWccf1QtcjGZj+nF3KJFzmz8N01IKiU3XX6pnH/GyakhG6UpOYcEDccYIYRhrJDp/rqAP7pi6RK5uPM5GYFKLqFCwx8C1xQOMkzlBi1H9AtpaR7gcyHzyGuukKcfX6Tb4isQGaa2Iq4AX82QLyg46GgriERqJFfQgHnFp+P4VjSkGRWkKQGfqk2TRlo70sDABhtCPDrjgbwdwOTfpo67s5avGBRaoCA+cxVBk88fQvi1aDhIDPhu8/qwXNKlg/YNKywQyQgunEynZMPbr7+mE8qQLbwRs++bol8TroOi0dB4+QrwdPxiBomi0ShoUUOKO4bdkDPYQDvSR5eNGLy/51mn6c9EQ5njYmmDykYmulRwFdB2BlwLpDV/BJAVLR68fgItDuGuEGSkU5xLIOcDf+HzZ83UR3OhpbSWaHWCnmNAk1Hn5WdsOhsXnmXIBrQWGoSNhQBBTcTG5iqnkQdMR8tK45l+OgS/lBMCsuHqi/vUIt6QARdrfxa/D21M0htNrAkXMMcEW2i6oD+LS0AwVAHISCA7VzIrBAQTnC3y2EMPyivr1qbrvPhFEKLY1io0F34Umi9ICDRgtsiWiNX4lPidTPrzB0FVhJ9BDE4CGHblQP3ZVEpYE2skcAkTj/etXPbD86e5vv7nd8p4Da4G0TLgusPmnCjccWSUzKqmVlOfiA/SoLGCB+jg2FO/Dft4RK9EnryGbmHTwg5Icufy9fA/TZCT7fdocHNQD9E12nrYlZelSUc6hp+TjoHQ4fcTcJH/cxgZTQJT7K7FDZCsZnODpODQw2znlaCZICWvwZwHETS/Rjqe3EynRyjPhY8iCwtzF0HTTtqFAxgZhzSNDWhPAqps7ydqd7htPqMtKjE9OflAfhBthF+14OGHtN/EUWAchojmMZuOtoFAZqMfmnqP1oj8HsHPpK8QPw7Tjd+IFuI1vBcT2TyQciGQIeImxUPQgL8aPlEgCFJLvMa8H/+POi/EhuD8jFSNOarWMWQ0gsbe+u4iyNdh6nDcIQkT/Jy0Sa2XPKGpHjAPQeBiNp6JMX5HFQQ/zwBNxUkAvIY0jgGNreasFIQUC/4c5puqDD/LZy4hbthUk3YBRM/GR+UPwkFktL7HPuzjIkzhnkAArUXNlsoD5veGgZek++Uwe8GNJ91CcAOZMM0GaD9KdkHfi+CCIyvCeTtzFh6fg5955/Abcx5JxoHcJujAbOM3GmLzBzOopo+R8UgI7Rgyhn2sjTe6BOqpbBhkowxn5j/QgmymeXoOJbEgcahW0OfHfxPF4itCINNZw4E8mD40EYlpfDNMLeQhJ4mppcYMiJTxHUkV5atMrH7maR3FQmYCDtOJQ/rGVGL4DpoQHMN/iR/ozgTEa56l3AVhSH9gCgFmLfj74HvQVl1Oa5XuqCHNQtKb/NyaVSt025MJNt57+y0dREBqfDxMKI8MgIhoWQadcgFtyjqCXcuPTL8vowsH4fscQq2BbmSd1SU5AM4wCddZm9fk9ag0GGfddEbXJZAnnEuEgEyUQRDTnYKPaMwxWlbP2jY9puD5YnMod7ZqDmYYM+7IYwhqHWHhUyoKdIxka02ikkGi2HS50CUS3uBsmtK01odBaQ+CQQiaEjC3lMFoTsUsY0IhXiFPZMTsG9OaTbgeInRHItysh/Yk/pgy/CPTnkSKAl+MjhVSJ5jG19a/pF9Hs4IpYfH6h++fqklEtGseGzB25FCdhskG3o8moh0fE06pjc/Dt8NEQ0oCjro0HiYcktaleZnYc+RkgqzHlCX+YEbIQ5TIZlJ2w/SaXjzSKSbFwYQaphdtQr4OkE4xPiAgX5erscBoViokQeJAPMw6ZKbcRl4xHyiPFWLy8RtpgnAAWQ9mTPxRtKQpSBoHc24GEHFszewr0S0FffJvRquZ5k8TXc5VZpaUSLbmgml336VNK6QlhQPRMJcL58yWwX17a7LgB9Y192G6cHg/mtlUUriG4LgAETJVD8vIeRRt4g/fRkPR+RHUVGgnfDKCAchgcmtoG5x5yIWfxsayySSdSWEYk01L+4HQqQlozxlTJqZ/Ru4PvxHQT2g+J6hBs4EDjoiIIT0d06wRLfrGKxt0uoXSmSGe0cwWkfPw7cQ/boDka7gLxWggk1YxJovHOJnWe3wo3kfuL2zmIJAZleTzcfQpb3206YP0d6ABGTzi92YiDjIW0vBgNCrfzx8DJT79XYrsJnVD8FJIF3XEyPu4gcQ/YIUNRBuZ54Mx6xAkEq3ygA2nTy/twA8dkj75wAiPDkDzmIfjkVAmGU2kDLEIRMjFMYNBYwDa1vQX1tVhApmZQMs1BEQUy4lcfBat/0GiW0LeB6wk/pFS+FX4SKRQcMrDo5UkiPHFAO3u4dSL+W966yitAT26qD6PyBWBXKROMIVEzuT7iGx117P6fDRrLg1FwpjRzIs6na2rJfyLBqV1CiIbIppkOEEM/225Pb7OR0ohTjdzRQ3MW3CIPCj4dBCKagS+HRWKXJEk2sakQ/AT8flMyYyarskXkrCGmDPvnZxucaeUli2wwBRzyhQEzfadBBeYV7Sv8THJC9IraPnU+Dofoock/rGhaJtwrxt5t1nTpui5DSLRfOkLCIT/xwHbRMAINVzThwdhMbX4j2g95kEIYAyRiZZpfaLAT70WEuonddc8QJngBW1rgod8wrmDDqCgx4Ym/kHJRJNB/42qhXHaAYPg2SoVJgAh/0c0C5HwERmVpNeOWRDeRys9wPfjs+h8IS943qkt9b+0XPH9CL2BmGK0oOlgAZTbgj15udZikt4WUfCDkpGKmBqJEowXmrwYxDApFrTO7PvuTZfJTKnLaDMIZqJY0hpmJgMfDlJhrulQDgKtZor7d464SVcZCF4w0aa9HpNMvx9g/BJzz8/5TAiYbZaXoMKBGm3Bj4ZHOlpapDPAiSfSZANpzjRk4ueUxMzmYiZpgaLzhKiWDpMgSEaboMMQGeICcoIEIGhEY9qDz70AtNib8hwmGDJBchLOfDbpHRpLCYiCpIPIjpTJaI8pmHi/kIRHt4DOErQJ6Q5TZMdJ19P9SoOh7fDdTL6N5lGiSiJIksqAwR1eB3n4l3Z4iv9oUD4nHDVjntGUaCoSxOFjNxC6moNml9Iatd1gkGEGwi2DaPaXUgTxkHFWluoYyPgTGZLYpX0J/4yIFeIROZImocvYpFggJie68xoce0hrWquYCtu5Y4d+HZqTVqZsvhmlOSoZaDWi3/AJBRAyCLRk0NSSbnEELCQrv/IRr4GVpToGiIT2IILEZGISSYfQM4f/ZoIMIlnMIpqK84zDZKKUZU46MCDaJVEcPHKDz18052GtNYls6UDhc4NzuFRDiKwJHOiMSR+JobQq9VuHDvVpKCUQD3F2Ri5uEKmaIRqSvNkO2sFskibhDJdsOTYiXHr6MMfG6ceEBx8JT3ASnNEFwRmLYCKbRLRZB/+Pvxn1uX5FgMbinNyqi3jtLCzYWRAIMF+RK5USlGzExBxSsCdyJbJlCozz78I+XvAcFDQuR6fl+y5MPubWsUeLdpJ6EI9ZDCdCI1eAj8ajr9BMpkkgn6DpMNPh49PyCSkUghJOtdInlh6dvVKBEHyYurJD2Kzkp1IP4iGJ70zOBkwmSWIiUxoHaE/CHDK0Q5cKI4xoKk4DJRjQDQEzp+t0SHAmN5dA6uBRGmEheEFrmnNUHEO60ziXFEK8Q5Q4eXWuAJ8NTUg1gTKXGYPMBdI0DF5TU6WBE42YzTSHhfwdBJ9bc/iPo4ArWVMoUiTxkIExLz4RIBCgY5lKBI0CnFpA8wEpG7pNSGDT68dJp2hOjrUgV+g46tR2UgTxfq4kM9Ty8KgNhkR+JmUkHtIt1kvwqET0kgL5VAzxfqLEequDh7N4WVIcKTvxkOPjuw6PCgNjEwVzqVjiITPiuxaPCgGF6qJ4VArxfq1kR1xX5OE8GCz+rcRAPCTx7fEeaXCoZ9EcKpV4P1LiRMOXh1Vw/ClciI14yO+UOJ/N9IgMuFt/kBL5Ux/iIW2ivz4PR0HnUsncqS/xkMnRX6OHY5gq9eRNOYhHOS3xp4kmCJzqyZ5bJx7yR/H+XhJA6uRPUgbOlIt4SAtJDe96VCfY21ZSJr6Uk3hI7wgv3MMu9INRyiXlJh4yLLpr97CEMVJmnkRBPBKKzj3Vw6NkUIctuOukUImCeAiDHlXzKNIEY6HUMbRTqkRFPIRO1MyRd49KAuWweqdNckmUxDuoZuGefJWHZZI6PycybkRNPIQj5hN/7FkFAfNa0NxEfSQO4iH4Cb6B1H3Mkoh8urDERTzkx0p+eJirh2sYLRFEr7kkTuIZ6SO+wuES2Iu+EjMPbBAPobzma7v2Qe21bGWwYsQW8RAaC/y4pD3QZVKWgn8pYpN4CNGT7+eLH/dKhDm6QsQ28YzQyexNb/TAtLYX+/vtDPGQfxM/QBQlqET8Xuzvs3PEQ2gwYHTSz+2WD2g5zr0paRosKrG+gBzC0LjvcKk/SAj/RuzvZ8UQz0hj8fMcpYB7xr2zvX8VSzyEigfm9+Mib34SwRmGJOg5u9r2vlU88YyQeumnZFsRG5EUcPwrT3COvLhfLrG+gBLkkJqbvLnATalmcA84+jXSFqYoxPoC6iE8TZyc1NoCNqjagA/XQVL3wPY+JI54QeHxV+Ml9dC2agXXNlHJkWL/fnvihQST01nJAiX7c2xgJYFrWKSki1SgOc0n1hcQofxKUs9KnSuVpQlZK2sm6fsvYv8+euLVQ+iqbapkhJKVSvaKO2AtrIm1scZYOoBti/UFWJKDlTSSVHTMyUc463E89pDvoBWMx3ATjR4rqZkU2/fDE8+ikHQ9VEkzJT2UDJcUQfAXVyvZKKkOGurIwaeX76r52faa16yuec+0ms/g2Q8taj67YqPQcov1BXhJplhfgJdkyv8DfJz3EJqw2EwAAAAASUVORK5CYII=";
                    break;

                case "Ngọ":
                    _image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ4AAAChCAYAAAA2qQYDAAAdBUlEQVR4nO2dd7RV1Z3HTTKJk5j8MZk/JsnKJCuTWc6sNZnEsaGxgVFA7ApiAbEACnZUsGAvgKKIDUEUxYogWLEhWFCxIfaCXURRQRQL1t/sz75vP/c77/R7ztnnvHe+a/2W8t679+x7zvf++v7tNURkjVpqKVqcL6CWzinOF1BL5xTnCyiZ/JOStZV0VzJYySglU5XcrmSBksVKlitZoeRT+QGftvxsecvfLGh5Da8dreSAlvdcu+Uarj+nc3G+AIeyppJNlByp5EolTypZLfnjKyULW655lJLNlPxzDp+v1OJ8AQXKz5RsqWSMkoekGJLFBWthTayNNbJW1/erJl4T8i9K9lNyk5JVUh2wVta8v5Jfi/v7WBMvhvxSyd7S8LG+av9MK4evlcxWMkAan831/a2J55H1lUyUtk5/RwOacJKSDcX9/e7UxCNC3F3JE+HPq0OCAKWfkp+K++fQaYi3lpLDlbwZ4wF1dLwtjci8UmbY+QISys+lQbj3Yz6UzoQPlAyTxj1y/Zw6DPF+LI3odEmCB9FZsVTJUCl5otr5AmJIV2n4MzWSgXvGvXP9/CpHvN8omZbihtdoi+lKfivun2fpifcjaZjVFenucw0ffKxkoDTurevnW0ri/V7JnNS3t0YU5ir5g7h/zqUiXm9pdHfUyBdYkt2kJp7uzLisyZtZIzmmKPmFdFLi/UnqiNUlFin5D+lkxOuh5KMs7l6NpoDp7SWdhHgkOL/N5r7VyAA8i4OlAxOPCsTYzG5XjawxTslPpIMRj67a67O8SzVyAQnn3DugiyIdkett2d6fGjmCxtNcmw2KIB0h+71Z35kauWOe5NhqlTfp+NbUpKsuqHTkkuvLk3T4CbfmcTdqFArMbuY+X16koxhdd5Z0HMyQjKPdvIg3Kq87UMMZxkvJiTckv89ewzF4tqUkXk+pKxIdGTzbTMprWZKOgn/d1tTxQVPpn6UkxCPkrrtMOg/oamkqzZIV8ep+us4Hpl05JV7v/D9jjZKirzgiHnskar+u84Jevj9KwcQjSdwpNuY8/cTj8s3XX7teRllBWS3x7rVmiLdfMZ+rOHz77bfy9VdtJ5steetNOfPYo2T1l186WlUlMEgKIh6brTvcvtf3310it82YJqs+/WHS2dWXTpADdttJk7JGIEix/E4KIF4lGjq//OILmT93jqz8ON535I1XX5GThx0qi196ofVnZ594nBzcbzf5/vvvU6/jq69Wy3OLFsrSJe+kfo8KgAbSXIm3eXGfJR5Wrlghyz/8oB05Pnj/PTlw911k9PEj5LWXX5Lvvvsu9H34+5uvv0Y+XLZM//t79feXnjdWLjrrzETrWb16tSb8io8+1P9++43XZZ8dt5FTjjxUVn3yie9rWNvD982VV154PtG1SoZukhPx2DPxVIEfJBbuv/tOefTB+zVRbDzxyEOya9e/S/d1/0cGbN9Djh06SCaee5bMuOoKGTNyhBx30GAdOBhgTr/4/LPW9/nog2VyQN+d5f577ky0Hkz10D16y8hDh6j3+1zeW7JE9txmS+mx3l9k9qwZvq+54crLZafNusiVEy5s9zkqhKclZhdLUuINLPJThAGCPPvUk/q/99x6s36gEOXD99+Xx+Y/IKcPHyZ9/rGpftgQzwj/7rXh31r/vXv3rvLAPXf5mlJIefi+e8mbry6OtSYCENYDUXfevIt+/ykXnifLlr4rB/fvq//df7vucuv069UX4GxNstcXvyK333iD7LjphnLKUYfJsveWZn2risaBkjHx6CYuzV3hge27Uy+5Y9aNcv2US7VGwRfbb+dtZduN/q8N2aLEaJpXX3pRXn/lZVn46CNy03VXa9Lx/nE00DtvviFnHHOkHNJ/d+m79Rat773dRuvK0YP31cTyu3bP9f9X/W4DufaySdoXBN98840m8WerVmn5/LNV+t8V0YRwJHK/RhLiHV3s+sPx4rNPS58tN5XdttpMa7YkRAsSCIMGhAj8u1eXdeSl556JXAvm9KQjDk51TTTw3tt31/7f1ZMulrtuninXXT5J+5XjzzhF+6ekc8afeYr+slUEHByTCfGYObys4MWH4r677siEbGEy9qTjdaAQBYIbNG2aa6Dx+OKgsc0XCf/wsvPHyS03XKc14diTR8qJhx8kzy8qnXsdBMbihm4Uiku8I4peeRhI8qIR8iTdyEOG6LxeHGAaZ1w1RZMordbDJ8T3xDSPOm54u89LhMx1KoRQrReHdMzSfavwZYcAB3yvbf6RC+HQXNOuuExrsURrUgFEv15bNX39bTb4q/TdanOZc3vl90nBmcBNQnGI17f4NYeDKDYrouHPjTruaB193jv7Vln6ztup1jTntltk+43XzWxdBE5p11IicMJSauI96WDBgSC6S+PIY856KFOoxUqx4Lx/+snKpkpii198vk0k23rNlKbXyGnDj9DanWi2mcqJQ9AcnIp467tYbRjIqe3Ro1usB4e/hOk8/8xTdVrkkfvnyYNz7tb+GMlkHHZSKM3ARLSQbPu/r9dKcCJSktoXjDqtKU146N57yMvPPZvR3XOCLpKCeBOcLDUEECiOn3TYgD11Utku+NvIKic285qp+prDD9hP5t5xm/bz9t9lO1mx/IfRf+eeekJq4vGlqTgmS0LiEQ6X6kA6zM3FEdHsnj276RQESdfY76tISL4Ok5sENBOQAuG6I4YMlIfn3as17FGDBuiaLO9LjfgY9TvvOkmbDNihZ6R7QIT77tuliu2SggfxK0lAvP5u1hkMzBomMuhB8SCffuKxxO9LqY0HnDRBi39oX9+kU0iNkPDlS4L281vrtCmTdV7OLt+ZoOLKiy9oQ8ohe+yqv0ysr6K+HufuxiZe6caK0TUycNftA80rkWUa8EDxpV54elHs10Aa0h5pzCclPSovdKTs0nXjNr8btl9/Heg8NG9Ou9IfnTKmrFYxMLgpFvE42bp0hwzTz0ZJy+9hUiMN8uds0FF83uknaw1Cvx74ePlyXaWIG2jQgjXiwP1T+22YZcz6W6+/pr8wXlKa+jClMvNzzDftVRWp13pByuDfJAbxStnWjq9jfCqvnD5iWOS+CCoAE84epf8eE3ft5In6NZgvouU4xKV6cOGYM9p1vcQVfLtFjz+q34tSmN/fUELDf+SLsMsWG+mf0d5FIwPdLxXFYIlBvFnOlhcCSlhB1QFKXFF1VUg2efw5bV6Hn/bJyo9jXR+C0vvn9cviCCmW3t020ZEvWguN69V2tqCBIfmkcWe38SGPO/gAeXLBw1XUfJRiQonHic+ZR7O61SdGwT0MtAgN239v3wcFITFFUSAhO+60k2Rwn53koL366Hxb3JoskTKt8RAmTl0WrYj2OmrQPrp1i/Vh1gksvP4bOUCb0GhGfE/7y8Z7kROk0hJHO5cMRLdrSgjxuuVxVTotiPToKknrIKNx6NoIetC0E8UFebakD499E2z8oUpBfx1k8VsHnSbkES+/4DxtVvHn6Ne75tJLtC/q/XtygNRmaQS1f25yeFdNvEj/G42Jf/j4w/Orut1yawkh3ug8rnjEvv1aTQ4baGi4TAPyYqadvd0D79ktVrcwZgry4y/F1cIEImedeKzWdBBvh03W1/4X16Q93mg40jKmM9qAa9mkoqKCFjxp2CEy89qrdHRLJMtr7c9D1Ewgo/OFLT2HvA6tWVGcJSHEm5/HFenOtTUE+a15d85O9V63TZ8W2GUMwd967dV2r+EBUjKjGeDUow/X+UA0Dfsi+CJQgSCdErQhiJ1qpDq4Bo0FXL/ftltrn+/l55/TZTyIYu9QM0BTE6lCOO2PqmuaZDUBD633JJm9viNfUvZi4KZAUkNu3A2i8wriEQkgHscC5LJzmX0F+FX2jSXRiglOCrQDTnpQTo/r4FNBNIgx5aLx2sRhqoLMNA+dViui48cferDdNTFt5NHs1+BrAbQhWhBiejeEG5AKIhHM62j0ZN8Fa4REaM+gdUFIgGa0mw5ItVSQfNwcONaOeJvkdUWc5AljR7fr2MCEsMchDdiOiKPul8jFJGIK8cWSpj5I6lJf5f291zvhsKGtf8fmb4A2RIvSrhUEumrGnHDMD0Tvsk6sdRFYkEZ65snHW10MPhMBzvGHHOir3UsOtse2I96wPK/Ihmkcb+/N5YGkDjiUacTU4cwnIVccYU+uybkZ0CDK3ls0uKkHY0rxFaP27WLmeV9I45eS4WfUeu0vEn97500ztaswsPcOrX/HHg20Je5C3Ki8JNCdyV7iTcn7qnZeygibnSFlMyD1kGTfA2mJOPk4NPLsmdMzGWFhInK0KTVlEsKYdny+c04ZqTUmOcWbp12rNaJZA9EtwQokM77fswuf0IQjoY7prhD5pooP8XJv+sRcmW+u/U1PU9z3gvRFFJHY10rlgfQFftORAwdEEpDf08MXpdHCgKklwOL9MMs0j3oBufkCkVu0k8ts2zTbJ83PaCJA0+PHsj6Cj6ApBSUDBfE2xGNvRSEVaJoxTYRnBGe7WRBIRHX9Es2iLUy/HGbyrltm6XRH0N5X45PNuu7q1GtjNIXdpYwfyReAKgQtWZhTqigkjr3XJmLWeT5lXs3PqFnzRQNUQfjcBFwVAAnIn9rE+8+irszDnnrJRW1uLubC68wnBd0pUcRDkxDdEmFi5kjNoGnQZqQ1wsw1xOTvkwJthzkl/ULVxCY4BCQXGKpxFenpljZRMRqO7mbaubiXpFuY1ZI2N+oA/2UTb+sir0ww4e3Mpd5K8tVbh4QURI6YGySoyXPy+ecmDiCILEnKkpgGRNiY4zDyJdF89BBSseG1BED4cGg3M+IirpA3NFYCInIfKoxtbOINKvrqlKzGnXpimxtMRIdPg1lhswslIioDBCCkFkxrORrTrg6QZ7ObRNFsDOqxo2i0YVDimZyg8btI0YSlOghMmH0StQWS9yP9Yt6La5ueOrQTppKUCL2A+L2Y4jgbhEgVYaIrjCE28ZLN4soIJF8Z3ZBUA3DzSU+Yjlz8JOMfoRlIGuPHUZjn/03VBAJDWrStl1wEGiSAeQ2dwKGaUl2fKglpFRpUeR3kh1Q4/RDMb1MS66YdirWhDRFSMWhCtNiFo0+PDHZIOPtVSCqEMYWmUsLAZCbGhpEesFMJYYI/RjMloAJiR4I0auIHmaGMc++4Xf8ezQJBnnpsQWuvm9EyEJEePeBtaw8z1fhuh+2zpx7OQ+7P21XsJ2hi/FrMNjk6/DXSNoP67Bj5Wq7FayqMqTbxSrF1na181E3ZFohGCdOEEIlN2ICRFrYGM21LQ/fso8eGQUKmQkFW0g7z771HEw7tgXNv/CcceEw475um784rvC/XDfsbKhJRGp+ACG1NObDi2g7caRNvgePFtAPaikiToCPw268cdkZOmE01EI7UCCaQ9AJEg5CGXAQHRJZk/dF+JG0xi0b74RdShmKYYtQusDhC4MIEqKjINUpwDei8IdFe0Q0/Nh6ziVfaGVgEIfhzUc2X/J7WdhMVo9kwZyaFghk3vh5kNo2jFOuNdoNsPGBSICZ9gRB8eF0ATCvBga1p8ROvuPj81pwdjQSka8K+PFHC5zJ14ThgOGWzlaCc8ZpNvHcdLyYUkIkgIewBQQxMJF3GTGs3FRKIAfn0bn8VkeKD3X3rTfoBMZrW7g7BnOE34vyb17PRBp/Qq7XoRvZOraLqQDXBdA1TbSA/GdQ5HUcwxX4dM14QpODr4l4QKZd4nt5HNvGS7WZ2ALQQidSgB2RMKlqLnjm0FKUknH8vQWmBQkjf4OgbbQrxqHvyoDHLPHQ6Q+ic9kvF8N6sicAIP43r6hG4Le9HAwTvZ0bRphHchajkMM0M7Mcw4zLQ4CWeOLWyUsQDLzyzKHCnmS3kB/ER0QJoIb+/oaeNB8puLjQEP6O1HW1n6r5syqYyQN6ulbjqofJ3tLKTeCafx34Qkt/4dPY16FpmhJlfy3tcwaT7HZmAr4dJpZHV+KhG8GdpNigpvrCJVwmQCiE4CHtQkEIfP6DMMw2YVCNGtIyRsIdvEzVjavHTjDZDc+EbMrybf5v9Dab7mPfBBzQTnCAbkSYkBPijtj/HHl5Mf1ri9WjxW70g+GHHXFDqpuTEk8oRD/Cww1Id+FeYGRLFaMeFCx7WGomELuUwHohXMxnhIZPUNtUS9jcwE4XfEfGiYSASppe+PEiuya7+HpPKTDuqCryWn9PSxN+zoy0N8dBkTLEHJKd5L5osyBeGvY4vEusrKypnagF5tjBfT5tDq8P3xquv1K+jHYlB15AJ02XMq60lICk5P15LJE2dGG2FD0hymUSvX9MpvhU+JeQkGY4G5DWYaAhpCJpUIBCaDTJT1SDSjjPVnuidfR4lxReVJB6gGyPusQLk9fD16L/j3xCHjUaYWPM3BAZoQkiC6cTEmcNXKJ+RjqGkBQHCrkUwglZsBCcb6K2JRLVxKhJRJjfod0TfHI1gp5vQlETXJcXKyqRTvMAc2o2RYYImI8DwdsMYjUjUiDkFJohAe+FHofEgIimXR+ffr82d6TbxNdVjG7tD2eNLCodThPA3o0xjWsHnJOGNCSbIMASlbk0gVlJ8VIkEchCIKI0vFddseX+G+TUtRuT2WhPMhw7RhXsiWpLQ/Ixr0fNHYZ8OFr/dYRAXc200HHlFiGdMbdqZK35Cvs4+mA8NZ3r9SCmVuB2+TQK5dCWzOEALpZ22TlRL/guNQXRrDlZBMMP4gTbxEIiJs8/WQr9uYSoWpisFkmGueX9DeghIYwCvJ80CWYICnTDBtHoTxJT/zBeHbZFmGlYJ0aZkVtpsYxSYVUeuK47Ph5Yyf4cp5EF52/CN+WVbIWkTb2BwyTlj9B4RNuigMUkSE4gQhNjvhZ+FVmbQYuvPum6szSNlNUhpzrBIMvaMspxXm5HaoTxn/ubihCdOFow2TQJT3K6lOWAWaStCe1CRoCOElIsZsEM1AR+I0peZRRIlZg8DD9r7O6Y5meNFgT1pwAi+IBrVm9w10jjqYLiOsvWmnRjtYJTv/EwoJ1WaMReQv8RVC9CmLarUX5G44JtPrZU8G+1V06dO0d0rZton9Vn8s1g+lDKxpE/YHEQaxvt7zDE+HElmtiraPh8+Hslk8nhxyESUHZVy4UvlRzpIj8Y3f0eussT+HWjTCFp463uRICAg0iQPluToJ7QWYJcYVY04renUbynF0f0SNMHUK0TO5AD9fofmpvTmtxmKa9jTDRBKgSVHm9b3Qjf7FAn8sbSjY3u0JI4BiWsCg7BaMWdeEExw2naapDFfCvacsHkbX5AUECbfb6oV1Rj2pXjfI+1IkALRZrNPYdsbiwI1VsxqXK0TJAQjlNIgFE2jQXPxEJx6yBk0Si1MCGjojMZMkoIh6AgaAgSoGXu/BETqFRjQ/d9ONnQXAWqm7FRL075uOpXbaL71/qJbqMLGxyIEDFF/E2RO6VROAg5ztvOC/H/SY+wdoN2GbmSh0yVlBExQ3KqGLUTC+IH4cxwHQELZW4rKSyjJmRJdFPDriIa9B0QT7FRgjEW7ERaVT6kY4ITbSd/YWqfLOrogb5/ww+Zxtl+mPdMiiZDbizOfxdScbaFJYcED9+V5W7OC79CeXMeUFQX2OJjukDQEIL1B17ENtBEaJYudZ0ECeUj9hI3HJV3kdzYaaZtmh5sXBN8xZbkNZiwalIso0EMWtB+5LfsUbPs4dj8S8HvGwNrjMohq6WDxtlNlLbQ/BU0pYA3es9FIxdg125LDdzBjbqNoXQECUvpi0gB1XSZK0VJFBYNSG6kHsvzMXfHrEqalyjvQm04QIleStn7lNq/Ebd+yhSYFv/koJLRtN4IvE5+nIggcRYvkMny7CiCNQc6Mrg+bBOzZhbBekO7AJJOyGTNyhM4VUjbDVNOoSmoFUtMxzfsS8NiNCFFCsttr8mksMHuIIR0jypqZ21cwAodvI7kcN1AlEJywr8NOVfCQaaWnW8UL/C4qI5SudHezMpNEl94cHGabszLo2Quq33qFurN9OCDEIzHNgCJKdUmORy0BQo8byOWAlaoB0tBbZ5fIyM/hf7HNshlAXlIijKaFWFFlOGrApHkMkclRVvBkHxB6wEouR0pVERDM7DazhbkuzZLPAJ+NCByzGnWEPFs27ZO/K4bII6WQUh6i5wIEJn4zVOLm3OLCtHXFOfeigkcMgMhD9JBSHhvqCkSwXgIQJLAbLWsQhROQkAYKqgmzuSfOSIuSIdaxoaU8KNkVCBxGeHJnCHtl8ypRQUAmZWHq/YaC03bPnJSKHB8a+6BkpBIjxIsCOTW/PF8W0+rDQN2Z7mLa7L0+IDlESnxhHSwlQeyj4ZH+jhZZWhBNssfCfvjs0S2iDYlghk5omlHt65uRvCXP5e0vCYj3S6mj23bABKLlzGR4fD0IURTQgKR0bPJRBjTTEkoIotlfSQLiIROcLLUCwPRSmKeVippukfBL82B2k/bzFYTJ4sOtKOKt72SpFQJ7WWlTKtrJp37sLb/R02cmIpQIXSQF8ZDczzerOmhHyuKQvaTX9B4lj7C5nbJcScBCfHkVh3h9HSy4RgQIJuyN4t6mhpKMod1bmiAeezHeKn7NNaJAHi+owkHPIFG4Q3Dxn0kTxEM6RGdyRwPbNsParOhgoRbsCLrTOEjiEm8tJZU+SqYjgtSKXxODLWzudrABCK60S6FICuIhwwtefI0Y4KA+xuCGkY/ZgFl108REqLaThMT7hZKlRa6+RjwwgZSulbAWe/r5CjoRiDkbP5cMiYcMLGLlNZKDxlDyid62fbuZtKDRFgdKDC4lJd5PlDxVxOprpAOt8ewB8ZvbwrQp+4zfHPCMNDiSOfGQLfJceY1sQGs9o9PsU43YQ2Jm/uUEtk3E4lEa4iH59gLVyAyMS6OpwGzD5IitnDBDEnAoLfF+oyT8XPQapYE9qZ7W+hzwsZLfSQHEQ+r2+AqBsbmQjzFoOYChnon40wzxfqSkMlvYa+SGudLgQmHEQ36vZHkRn65GKYG79UdJwZ1miYf0zv/z1Sgp6FxKxZssiIdclv9nrFEy0GufmjNZEY9yWmm6D2vkDqZ68sydEw/5k9T+XmcAqZM/S5N8yZJ4SE9pbN6t0THBs+0lGXAla+IhQ3L84DXcgmebCU/yIB4yKr/PXsMRxkuGHMmLeCQUp+V1B2oUDuqwsbpO4kpexEPY6FHqIwRrxMJsCdm0k1byJB5CJ2rpdhjXiA3KYU2lTYIkb+Kt0bLwmnzVwzxpzM/JhRdFEA9hxHw99qw6wLxG7ptoRooiHoKfUDeQlh/TJQefzitFEg/5sZKxWd6lGplinGQcvQZJ0cQzMlTqCkeZwLOgJ74wDrgiHkJ5ra7tuge110zKYEnEJfEQGgvq7ZLuQJdJ0wX/NOKaeAjRU93PVzyukJxydHHENelsoZO5Nr35A9O6uzh+3q7J5pV/l3oDUZ6gEvEHcf+cS0c8hAYDtk7W+3azA1qOuTeJd4PlJc4XECJsGq87XJoHCeHfivvnWRniGekq9X6ONOCece9cP7/KEg+h4oH5fTfhze+MYIYhCXpmV7t+bpUnnhFSL4crcTbYt8Rg/OuRknNxPytxvoCUslbLTXY61rwk4B4w+jW3FqY8xPkCmhROEycnVdxhYuUBPlw/adwD18+h0xHPFo6/miiNQ9s6KvhslyrZUNzf75p4HsHkDFByu5KvAx5glcBnuEPJvlIxcxomzheQs/xaGmel3iTV0oSslTWT9P1XcX8fa+I1IXTVbqlkjJKHlOR/unF8sBbWxNpYY+4dwK7F+QIcyppKNpFGdMzkI5z1Is5Y5xq0gk2VRjS6mTT2pLi+HzXxHApJ17WVdFcyWMloaRAEf3GBksXS6KChjmyfXv5py8+Wt/zNgpbXTG15D85+6Nny3pWMQrMW5wuopXOK8wXU0jnl/wGyTWjZIqyfwgAAAABJRU5ErkJggg==";
                    break;

                case "Mùi":
                    _image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAACiCAYAAADGB8YmAAAbZElEQVR4nO2debQV1ZXGTdIdOzH5o5M/OslKJyudXnav1d1Jt0EwcTYKiOIIgsggs+KEiDjPyqAo4gDi9ARxQBBQRpVBZBAnxFnBERFFBRUcQNTd9Tv3nud59arurbq3qk5VvfrW2uvxHu/ed6rqu+ecvfe399lJRHYqrDBbZn0AhbVssz6Awlq2WR9AyuwfHNvVsbaODXBshGOTHJvj2ErH1jq2ybHNjm2R77Gl/LNN5d9ZWX4Nrx3p2MDye+5a/hu2rzM1Zn0AFm1nx/Z07AzHJjr2jGPbJH5sd2xV+W8OdWxvx/4phuvLhFkfQIL2Y8cOcGyUY8slGbIFBWNhTIyNMTJW2/erIGAE9s+O9XFspmNbJTtgrIy5r2O/EPv3sSBgCPuZYz2ltAfb7n6yGcTXjs11rJeUrs32/S0I6GOtHJsgTZ2DvIGZ8WbHWov9+10QUEoeZVfHnq701HIKHJnujv2j2H8OLY6Auzg22LG3qz6m/GOdlDz5TC7P1gcQ0n4iJeJ9EOjRtCx86NgQKd0j288pdwT8oZS82fWBH0fLxQbHBklGAt7WBxDA9pPSfqdAOHDPuHe2n19mCfgrx6aEvu0F3Jjq2K/F/vPMDAF/IKXldnMtd7uAJz5xrJ+U7q3t55tqAv7WsQU13uQC1bHIsd+J/eecSgJ2kpKapEC8YGU5Ruw/79QQECXIbXXd0gK1oMGxn0oLJ+AfpPBwbWK1Y/8mLZSA7Rz7uP57WKBOsCR3kBZGQAKl30Rx9wpEAp7FydICCEhGY3REN61A9Bjj2I8kpwRE5XtvdPeqQEwgcJ2YIjsp8uHpzo7yLhWIFQhgExE1JEE+XP2F0d6fAglgsSQg8YqbfHyKCvJlF2ROYo0Vxkk+9hGzor8nBRIGy3Fse8K4yEfSu1Cy5AfTJCbvOC4CjojnPhSwiLGSEQKeGNcdKGAdPNtUE7C9FBmOPINnG2naLkryISwo5FT5B+LWP0rKCIirXqhaWg5Q0UQSnomKgIWer+WB7l6pIGCnuK+0QGrRRSwTkBqOYt9nYPv2bfLUimXy5prXZMeOHZG838cfbpR33nhdnn1ypSxd8LAseWS+LF+8QJ57+il56/U1snHDe/LZp5/Ijq+/juAKQgEt4e/FEgEJNhcFRC68v369DOxypPQ6rL1MvmW8Q5In5YvPw3eG4zWrVq6QieNvkKH9j5cuB+4jh+/VWg7bs5Wyw/faXY7Yu410PmAv9fcuG3a6TBx3vax87FH5aOPGGK7MF6Traq62q4eAfZK4uqxh4/sbpH/nw6Xtbv+l7JA9/k9GXXC2vPri8/LNN9UjVFs++1RmT5siZ/TrJe3+8t+N7xPGTjz2aJlxz2T1XgmhvyRMQIrGi7pdH9x63TVNCAGRmBFnT50i3377re/rnn/mKTnrhL6+xGrX6n+ka9v9pF+nw+SUHl3l9N7d5bRe3eSErkdJ90MOUmQ3f//ckwbIKy88l8QlE5r5jSRIwEJYWgEfffCBnNz9mCYEhBzdDj5AkcwLDz0wXY45cG9P4vU+ooNceeE5alZb/dQTas/H3hBA6E83b5bXXnxB5k6fKtdefrH0PerQxtced/DfZcGcWfLdd9/FfdkIWRMh4D5xX0ke8ObaNWoZ1UTo+NfdpOPf/iKjLzqvmXPy4H33qP93E4/XQ7rXX30l9N+edmeDDOrWufG92EvizMSM/SVmAlLT8WzcV5EXvPv2W2o/pmdBnIeBxxyhPGSNR2Y9oJwLk3gsr/NnTq+bMBveXaeW/TMH9FZ/f9CxnWTRvFiF6az3oVQzYQnYL87R5xHMYOZSjNd6/YjL1LK5bOEjctR+f22yXN438XbZ/PFHzd6HpXbbtm3y5RdfyLavvpLvKuwl3fj0k81yx7jr5ODd/yQTrrkqysvzwgkSEwFRN2+Ie/R5w4LZDzaZ3SBBnyMPkQtOG6T2hPwMr/nehluUB42nzB7v6ceXq33h3bfdLOOvGiHDzxkqFw4+STkWvHbEuWfK+NEj5f7JE1Xccetnn1V0cFj22X/yN2IGfyBwPUkYAp4Z98jzgq+3b5dPNm2SNS+/pJY/996OWU87HOedcoIiHODruCuHK4+W/WLQsAsxQTzjG0ZersI9zJSWwQE8kRKQnsyJRjezCLIRKx5dpIgAIZjtPMMpzlJ85D5t5NA9dpMpDbeqeB0z2bHt9vcNv/BefK1GRt5zzGUXKfIn4Pn6gXbBgQqaghLw9GTHny2wHyM1dv6pJ4YKGENCltNzTx7Y+DOIS2zvnEH9nWV2mNx09Si1DE+/+0655/abVZjl4iGnykWnn6zigCzn7vgfRrwQcr/x2qs2UnQg0CwYhHz0Gn4n4cFnBsx6E665MtSSac6E5vc3jLpC3lv3jlq+P9+61ZM4zGpfffmlMn4Pgj304AwZe8UlMrj3cYrU5ntC6Cl33KZ+P2HAmarFTEEI2CXpkWcFG9a/22zW63LQvo3ORVgjd1wPUT784H15YdXTaubr2bGtmhkhOfvNxfPnRnjlgcGJVXUT8JnEh50BrHvrTRnSt2cjeXjYN4+5Sv3cjP/x88vPGiKTbx7XJDDsZUftu4f0O7qj2kOyl2QWrBWrnnhcLe98IBhD9w4HyuNLFlf0lGMAIuW6CNgqydFmBe+8+YYKFpvkOevEfvLlF583/g7L6fc//0L9DCkVJAsyG6J0YS+IwgVC14JNH32oAs9D+/dSWwRSerdcOzrpPWEbqYOA45McaRYAGU7u0aUZYRAE6BAIMTdmPJY/Ar/kbfF0mdnaV/BkO/99L+VU4Mma+0QyJbxfreEVctM4L7wfRCS+mCBulRoJiBud54P/QgMHwczvuq1b+/2VaODWsVerpZSfQajLhw1pXJL9jBmVgPLmTR+rNJzXPhKVDVmQWoBGkNmY90EogQOTENhH/FxqIGCPpEaYBbz9+loV9qjFuQhis6Y2FRixByR37P69epyJD95bL2eXSYjnniA49zg0AYt2amW8tPpZlciPi3wYM6d7doNszFosyR1a/1mFWBbNm1PXteDYsC247boxdb1PSNCgKhQBOWk8D4c91w1k8YQ04iQfRjoNoruBaOGJpUuUIe/XDk29qHUprxFIwf9FQhCwkNtLSUVCvUXc5NPGsmgpa5EEBkgIAs6wNMhUgeqz0uzUWhUG4WTEQTydEUG2T1FTTkGrvkAE5ATuwvt1QH73tOO7qVgaVWro+KIiXaf991QSK3dIh4KknAJveGcJQMD9bY0wbUBkQK4X4KV6Jf3DGqoWxAeoVfCszQo6DMl+kOq5jOIgCUDAkdaGl2LMnTFNxfRQqRDnO7XnsYEJRy4WI1iN4hmQt/WKKVIVV08KLuUg9lOVgMusDS/FILtBFgSxKbj6kvM9CUemYeR5Z6n0F+RDpUIlG10MdIE6GROUK16v79WxXZ73gY9LFQJynEKi/nkWQfcBs5bDtFHnn6VCJWtffVkt2Q03XKteA4EhIbMnwgC/GbNDm/+Vta+8ZPkKYwOfXjjmS8A97Y0tG2DvhsbOb7lFcQLIOuBolDzoXsqZIdcbZNkmC5JjUNbrS8AhFgeWCSD+dEvt+Z694RPLljQu0QvnzqpZF/jAvXdZvspY0UQp7SZgg71xZQMQjFnuxiuHyxVnn6E0gHy/dUvTyBXFRcjiayEgy3SOMUkqEDAz4lOUwyxz7JdIlzH7oAYmtIEOLorWaNXgFy6hIHzYwD5qP+cmF3KsasVFoy8+P/axWwTdVT0JSO2H9Xq+asBDRM921YXneuZoeegEd0lrMTMlKDtqBJJ4isw12fja49C2qkvWXbfcpLR9ENSPgJcMPS3xMScIco0kO5oR8N9tjqoaKLwmGBw0/oahIqGQm0yGqVaOEyzR1Ikcf/jBqkkQChbyyU8ue6zZ9aBK8QpuM+ac4z/Eg4AHWR1SBVD5RRliLfspTNVlDBuiQiNx4+5bJ8jR+/1NBa0x4nrUZiAIZTZm9qaLFaEaMi2EaZBameNFs5dzHCweBOxvdUg+eGr5Ut+wR1hjVqJZTxwF2xAKx0R3uWIrQNiFet4VixeqWl40hbrA/Lrhl6rXsV91z+otYAZsPPDGJOBwmyNC+kTtgnYe+Erbslo9yUrLMrMp2Ymo8NpLL8r5pzQvSmefx1LLvpV2G5qU/B8td0m58f9aKq+NKrqcY5R4ELDBxkhYFulvzH4JozaCGYDAbbVi7/bllhWVCn38jC5VLMvUYbA/DNNtCs0eHawee+Qhlfnwk2nRio0PFe9NlwPGCQEZ8zWXXqDeC8UNYlTzdcykOUdjKMYkYOJHq9LVk31SUNK0Kz9AkvhUmD0w5W55eNZM9ZWHhmaP7EMYIvKeEIXYG/o/Qjl0pGfWgjyEevj+xdWr1FI67c475NIzBzfrZkpqDq/cDFJDON6XJR/xAWOmWRHFRTgr1BB7FSvhRecc88WDgCuTHAElhrQYC0MU6iaq9TwmJsis5J5VwizROA6Q8qTjOquvEKva+zFzL3l4vlK8mD8nFTdvxv3NxsmHz6/IKeepOPCkeBBwTZIjoLg7zOzHzEIajFoGgs0k9gk+E+vjq+p9V25zBgi9EHurZ78YxtgDsjR7faiobkOG9cKzz6gPCMXhdCzweh8Iy/WZ4MNqI54ZI94QDwK+V/ElEQNpk1uMiREyYaZDLeze2zEzsT+EuHQY0BkFvrIEEoCmaaN+gMyWyNyDEGhA5yPUzOnXIo1xEc/zU8FQ+A3oZsX3pOkIt/C9nj3ZHphF517GNsLUA3INLPk4NA03jlV1wzkAF9GMgIkdKgEgoDu8gmfIno79EXuuoG0s3AZBWQ4BlWTVVCjsyxgPIRHaYbj/H4KT82Vf6CUi5f+nTmpQf49upvzMjDnS+y/omR93Trix8XXsR/nQmf/Ph0R3yM8w4JpdAiLKNAnGQ8SZ0KDLu1+DRwxSEfTFKXA/JIz3XraodJATXaf8Z5xeanmjaY8ZxzON+B25Z1Qq7vZnGK/hKARAipCfIbvnGgG5Yd0poZIRwEbuBbzIh/H39e9kGLQAa0bARDH9rklNbiyhGN37hE84y3Clh8WeSodOeMAQkYfO3koTGzkUMxuxNq/9JqEeOtHzQAkGszy6l0i2ATSG5G8RP+RD4SYhy7Put2KOm3wwtb7M6GYTSj9jeSe0g/FaP5Iyq+cAdgloOgjs3cwOUBBRzyRNlmiHXIgM2G9BOjfIRnB4H7MHDb15DbE+Zi9I5J5JWObp8UccjyWSdmsQCRm9VrLwwPFYAedvvPzcavW6scMvabJHZa8HmG11fJIZTIdygmZzqsU1yeaYzlaGYXcJ1ssuChH3DWU5xJlw33w29kCn0iDc+nfe9izmZrnUSzgzBjE3c3bDW4XoEEkv2ZNuKu2/qN3QPVQIlZhaP4LWqnvpjh0qvabfD4LRvAjCoXbRXi+zOUckBCFfEMMZyQE8l+BECcjswAP0azXBftDUzbEP08U6zGgq3ueQlxQXDoImCacKcSoQMTxFcmdmYy8IafCg9fvpZkBnG2kwlmT9N6jPRSTAsgvQ/j360Dw1syr189IlSotokhrRgfuMD0IuUZEPI4idA3g6IYnO6+RPmb38QIrKTMVRCKTBubnufZh+MAR9zZ/zHnQLZcYyC8tn3jNZ/T7esul4aO+Z2KJu6kjog1mX9Bm/Q6yOE46Yud0drFjG2cPxAcGp8RKf6uWZJpSVyMa+lVZq5nuQOswBPMMwiQaiqwGCmhtxXU8LIGeJCLs3OhzsjfBmcTjwoDVB2cO9/Pxqtaya/ZyH9OnReJwphKETKcFr/TOWYx48MyihD16PYAJ1Du+HU8LveBEH4lYK/bCXhUiQnRnc63fIqLDnZBnX3jDXkpOAtGcgOtFUXDXgOequnhizBSoRlkgegl4625dJwkykwy4AsSf/TwkkeVicDXO24jUIBLw29Cyt7gA2yzNE1T2WIWGtRUeMA0cJcASD18ynU4540ZrMbCFyEAMEnqm4xMUI1QARTNk9xeDa4SB8wjlsZEyYzcgy0AQcB4BgNmTlNRCV5ZeZy4sMxOcIFOM9s9QiOND7Ry/ikDGBCPXKxNi/AjPgzld6S+uIAMu+/iBheNhhVDsphqcYocHmiPxgLnNkLMhWuIEDwoMklsfMpr1fHASIx6zF6UF+ZGAWJRQDubwCzXEY+06uDeUL37PXZN9pdrHnVE0zXGXO8BmHpxwrlSI0CGcqTAjF6BYXgHggSyxeKrOFTnnhwbK8MYsQHglaFG7DIBnxRRNcD/It/TvkqClNyAk8BamplOQDQjJm3QTZBrIXpL8QCOApIn1n6dRhEZZe3QWUdrS2SeZnZFG8WrKhTTSjACz7ulNXDuApyU9tURJOB7o888EhXNCk5EGxpOGo6CwCqhKEpMi0/BQuaTDED+7YIUuvu38M8cQctW3zLEpKdVmmVpl4mSnLMn/O7OgX5kiL6eIkDYLsZsBcz5I5E6n+p2StMB3Ptpbaj7Qb8UcTZFvc18nsTxgpJ/AtTMdWWRxYReABerW6yLq5+8B45cBZfhM+4y1O+LbmSG0oBpDdqPQgmTUQFjCDUF0X5OHrTEccxNKnVOLhugvPTUP1Y9Ypk1I0x0R8M8oS0hSgYnOiVBakEkiupKfjgRGk1lkCMhyVyMGSxt4L0QIyKgQGxAG9xKhhjRgkpCMPTDoRB4pZzk9cS+zSzMYgcjBjkaQBc3Z0Q8X2bKlsUEm4pVJVGlkPHRskXVapNJMQjltLiHeJhJ6uCRcNOUUFpMm7hiEeaTmECOgOyaqYIHzi14wIb5e0nvm7WjuIQDVnsx+o2KAydS16UcxACL8HT4hFb9DJr1bKz6IBDNKkCE0f6hsk/4hTkWSRCyYgzuzJV75H3ECGhe0BmZhKpw/R5NyrERFCA2ZKDQQIOmfNrJ6jvR+o2qIXS43ehxSbu22F23QXAWZAd/jCNFTXXmm8IIBYkJIcLeWkfOX7MKebM7N51QEjPIB0Ghvf36CC6GgLyeDkDFWblGOpOaYBaRRCgUpHnWqZFqTwm/1YytAQ2gYZHXfNCcstpDbBByUn5ZduBDqmITUH1eAdYswKeIdeG/nbr79WKUTQASJW8PM00wBmTPdJ64qAb7xue2hJIdBBNak8qgtHgUJv1MjmA2TZ1Q4IUiovlbFXawxboOjJnAXRHeb4WAYTgY/qwlJ5WCGzIaIDcyYkOG3WybLZd3fVIjaYFrCfRL9oes9anJpzBD6sEEtt6RWzHTXEJsHMbgKAeg9zltHVbmkBoSIdnCbmp88WyTlCHdea6gOrn1m5oslSTJhGF7UD9oSIF3RGAU8TrzUtIBRERwYduMbRyjlCH1iNzbYy1ADgAZo9o5ntKN4xwUnjZutb4nlpwuL5cxs/IDhJOZJaeWGh+PCsEgF7WBlqQNCKw+0Nu4FToveDpNn4Pi3Aa9ceMYHtHKldvNBXaiDgzySF3rAG3Q7MLqXkit0502/L/Vz07/DA09TWAiW0XoYXzH7Q9nDiAt7vz6UGAmLjLQw4EIgNUgWnyUUNsTsHC6gh1jIuljy3/s4myElTjMTYEETomuScgX7DvhyrRsBWFgYcCChkzDQdSy3epRsUk5tllrpjVRrADG2WXXLAYQ7RRuogIJba8+OokNMPDz2gX7hFdy3VhnomLRInhLZapECNcqV2JRkEAueK/ApCwC6JDzsgECKYxCKB71W4TYWZGbzm34vmzbEw4uagdRtCCTNtmCMFTE+JgIDUiqRSloHnaxIQJbRX6SLtPKiiM3+X/SPdFWyDmXjMpRd+n9lp/WdVTJ8DILr8sURAQCyVSmnacJiVcLTLMMWdGqS/qB1254jR+8VxbFdY0CrOLEQa3Pu4PPSAaaJ89rOgBNzFsdrEdDGCWKBZqESsb8799zX7PYK841zLNUYYJ4kDDKuBlhxN+mU73nrGT02HK76hF6mBgNiwRC8hAOjR5+7P53fMFQl/r+Ig8sq2sxCIKdwNyXFIdLPMDCLQ7CchCfhTxzYkeRXV4A5G63ia17IKyVDSUDDEUs2ekGWPWhN3Z4KkAdHcHbmYBTnKNYN437GfSAwExFJ1kC0xPmoz3M4FZ/P6gf+j9x7GHpD+zfQitAkq52gl4p6dKVjK4JEMJ0gIToUl4I8cS0cUtwzCKeYyTHmlW5iQdqh6Fp+yU7MnYgbAwShwJDYCYvsmdTVBQa2FrgfBKZk/c7rtIYUC24NLhp7mSUCckyeXPWZ7iEFBOUcoPtVCQOzepK4oKOivp2tvzX7SWQFHfXkREOMgxAzkiadJDVyqlYC/cmxzMtcVHGzmKS43D73JCpBnIUylkz+nSFHHoh0TnKWlCx62PcRKIPr/G0mQgFhqZftZBd67GYDmg8SZJAgtUPWkGDQ3rYlH9RDwB46l+q7kARx4SI1LiuuEaVwIFxInIPZbx3JxcEWBmsA27PdSB4fqJSDWKe6rLJBaoJSqiz9REBC7Le4rLZA6UMpXN3eiIiBputR2Vy0QOZAc8cxTQ0DsD1LsB1sCCLn8USLiTZQExNpLqQi5QD7Bs+0gEXImagJiJ8Z2+QVso/GAmagsDgJiI+K6AwWsYazEwJW4CEhgsvn5UwWyCvK8oVQuQS0uAmIUpOSy0LWFYa4EKC6q1eIkIIYyNj0NWQqEBWm2SMItfhY3AXcqX0BBwuxhsZT6A8XKjyQIiNGaP7Xt3go0A8tu4LqOeiwpAmLsI1InZC3QDFMlxj2f25IkIPZDx0ZHd68KRIwxEpO362dJE1DbICkyJmkCz+IUscAFWwTESNsVuWP7ILcbaXotjNkkIIaAIVVlni0MqFoiExbUYrYJiOFtFXrC5HGHxBzjC2K2yWcayupiSY4fLLldxf7zTh0BsX+VotApTpDZ+J3Yf86pJSCGkIGSz9TVHWcYzHr09am5ei0usz6ACkbxe6GoqR8Eln8t9p9n5giobT8p6k1qAfeMe2f7+WWegBgZFJbl9Jwyk17Qw5FAP729bT+33BBQGyGbwY7Z7y6ePtAW9wxJSEQQlVkfQI22S/lmrwv0aPIN7gEtcWOXTsVh1gdQp3G6OzGtFnHiswvs8bpL6R7Yfg4tloCmcazYBCkdjpdXcG23ONZa7N/vgoA+xlLUyzGOQspMb9sK4BrmOdZbMrrMVjLrA4jZfiGls2pnSrZmRsbKmAke/1Ls38eCgBEYKt8DHBvl2HLH0nQUEWNhTIyNMSamSLZt1gdg0XZ2bE8pedN0emJTn8R5DfwNJGiTpOS97i2lmhnb96MgYAqM4O2ujrV1bIBjI6VEFPaTKx1bKyXFDnlq8zT5LeWfbSr/zsryayaV34OzM9qX3zvTXmvUZn0AhbVssz6Awlq2WR9AYS3b/h/c6csZ5up+CwAAAABJRU5ErkJggg==";
                    break;

                case "Thân":
                    _image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ4AAAChCAYAAAA2qQYDAAAaL0lEQVR4nO2dd7QVVZbG7e6Zdrrt/mN6/pjuXj3dq6dnOb3WZBtDNwYwIKAYQQwEUUDBAJLMIiaCCmJCgiKYQDCSDAiiqKigmAUURREFBRHMaU/9zn3nWbdupXtfVZ1K31p7LcK999Wt+t45O3x7nx1EZIfSSkvajF9AacU04xdQWjHN+AWkyP7Osp0ta2dZX8tGWjbdsnmWLbNsjWWbLdti2Tb5Adua/m1z02uWNb2H946y7KSmz9y56WeY/p6pMOMXYMh2tKy1ZYMtm2bZCsu+lPjxlWXPNf3MIZbtZdk/xPD9Um/GLyAh+6ll+1o22rInJBmShQXXwjVxbVwj12r6fpXEa4H9o2UnWHavZdslO+BaueYTLfuVmL+PJfFC2C8s6yEVH+ur2meaOXxt2XzLekrlu5m+vyXxHNbKsolS7fTnDayEkyzbTczf70ITjwjxaMuW+z+vXIIApZtlfy/mn0NhiLeTZQMtezvEA8o73pFKZJ65bdj4BdRhP5MK4T4I+VCKhE2WDZLKPTL9nHJDvB9LJTpdX8eDKCo2WNZfMpCoNn4BAdZGKv5MifrAPePemX5+mSPery2b2cANL1GNWZb9Rsw/z9QT70dS2Va3NHafS7jgY8t6S+Xemn6+qSTe7yxb2PDtLRGERZb9Xsw/51QRr7NU1B0l4gU7yVFi/nkbJx7KjBtbeDNL1I+plv1cCkq8P0oZsZrESsv+VQpGvAMt+yiKu1eiRWDr7SgFIR4Jzm+juW8lIgDP4lTJMfGoQFwR2e0qETXGWfYTyRnxUNXOiPIulYgFJJwTUUAnQToi17nR3p8SMQLhaexig7hJR8j+SNR3pkTsWCwxS63iJB2/NSXpsgsqHbHl+uIiHX7CnDjuRolEwbYbi88XB+koRpfKkvxgtsQQ7cZBvJFx3YESxjBeUk68fvF99xKGwbNNJfHaS1mRyDN4tpGV16IiHQX/UtaUfyAq/ZOkhHiE3KXKpDhA1dLiNEsUxCu8nm77tm2yds1qWfLQA3LXrdNkxtTJcvdt02XpooWybu2b6v9zBqZdGSVe5/i/Y7qxft3bimjnndZPTup6uHQ/uJ0c12E/6dGpnfQ/prOcP6C/3DntJvW6nKGrGCIePRKF9Ou+++47eWXl8zLmgrPliH32kA67/re02+U/PK3jbv8jh++9u4wYMkBeem65fP/996a/QhRAy/cHSZh4JIkL2ZizccN7Mmnc5dLpb3/xJZuXXTDwFNn+ySemv0ZUoKzWUPdao8Q7IZnvlS68sPxZOb3HMQ0RThvbbs7QRxIiHs3Whet7ffTBBXLMgW1riNRl3z2lZ6cD5ZDWrUIR7+brr5atW3J1+0ix/FYSIF7hBJ33zbhN+WhOEp3dv488t+xJWfXKy7Lk4Qfk2lGXyMBexymfzot4bNFD+/aSpY88bPprRQkEpLESb+/kvot5fPvttzL9huuk0193cfXVPty4seb1mz54X5Y99qhcN+Yy6bLfnp4EJCi5/cZJKlDJCdpKTMSjZ+L5BL+IUXz5xRdyw5WjXVcvUidO0jnxzddfy9rVq+SKC8/zjHr57KnXXiXf54N8L0gdKpZ6iNc7yW9hEkSdV444Tw5s9V81ZBnS53j54L3wE9O++upLmTtrphy2V2WrPqLNX6siYsg3+5ap8X2ZZHGyREw81MQbEv0KhrDlow/lkjMHua5QfbocKm+8/lpDnztz6hTp3fkQeWbpYyqXh3+oiU1wgp+YA8CRUP0aYYk3NNnrN4OPNm2UCwed7kq6I9v8TZ5+fEnV67ds/kgWzr1f+XRBqyCEfuettc3J488/+1SuHX1p8zaML5mTxDIHx0RCPGYO+zs0OQA+23mn93Ml3YF/+U9VFnNi8lVXNL/m1O5dZc6sGWprDQsCi1snT1DBy4Djj5VPtn4c5VcyBcbiBjYKhSHeGUlfedKAdOeedrJnBHrJsEEqWKh6zwcfyPGHdqh6HavXg/fdXffP5z2sflRFcoLAVS+IdMzSXZf4ZScItkA/0uGXuW2jK5Y96ZrbO+eUvvLZp/UfJPT5Z5/J11/l4UwYBTjj2yQURLyuyV9zcvh482YZMdjdp8OoRjy9dInre7d+vEWR0i1B/OSjixL+JqkEJyw1TLwVBi44EWz7ZKvaQv3KW8rh98mxedVtkUTdePU45fPdPmWi2kbJ510z8mK546ZJ8uyTS9UKl3MgDm6IeK1MXG0SIDk87uLhvqQj0GBV88Kn27er9IrfZ5AuITBxBioH77GLSqFMu/4aefftt5L74sljd2mAeBOMXGrMIEiYcvVYX8KccPhBSlHsh2efeFylWIJEAUF2bPu2cv+ddzTkF2YAU6RO4hEO506vDUjkulUk7DVUZ77ODWyfztWsJTbynGGqzpsz8Nv0S6mDeN3NXGe8eGjOvb7yJQIDeiaC8N4769Sq6Eek9j7k9rIhfXrKhvXvJnAnEgXn7oYmXu7GiuHQB22N+HVhEsCsmm6F/0EndFcrIUEFjT/UaFnJdJ02jJ158olV+TxSLOQMqZJkFAxuCkU8TrbOTUIJrHr5JdWEE/TQUaMEgaBi2EknVL2PfB4ko+TmzMXhuz2+8CHly4Ul3xm9usnsW26WWyddL8MHnSanHNdFBvQ8VkXFrLYZA43g/ywhiJc7Wbtfgthu1GmDQI7u0D13q3ofaZkgLFowV6mVw5IP1Yrbdj3ynKEqKs8Y+koI4t1j7PJiwtTrxruKOZ2GtB3liBfI6V11yYW1W+yJPVQU/PLK5+ThOffJvLvuVJ+DMNSO1156QSZcPlKGn3GqkldR3+3WcX+VXglLSNIwGWyVZGSdL/E48Tl30ew333wjj8yfIz0PaR/4YCGpl0qEnFvvIzvVvIcoma300D13bf43ouN777jV87NIIOO3oVhZ/MB85duFCUggbAYbxIludxQf4rU1d23xg3xZ0IPFn/JKazy5+BHPVIxbagXpe1ifDJnU+EtH+PboEnU/9vCDUd6SJHGA+BBvlMELix1vvbHatw9CP1w0dk6wbSJTd76+6/57y8SxY2TBPXfJfTNvV70Ydj+tHqJQxkMe5XZdROR8foYxRnyIt9TghcUOtjcvoaczWPji88+r3oug4Mx+vWteO/r8s6peR5lt/GUjmv//+WeWuV4LKZO5s2cqEakdt02+oWrVg3DI8J2vyyCeEg/icSxA5sKlesFqFkQ8fDUnYd5fv15OPOLgmteefPQRNdsp2yY5vbMson75ZW1eEPk86REIBrEgqp4uQL5Ry626HrCPUsc4tYAZBXkmOFZDvNYmryopECC4EchpzEWxK0gIUIhYO7dtXfNaolTIZgdBA+YEEanbdqrTJE8tWdwcpJC3yxloj60hXnAyKgcgyqS6EEQ8Sms0azvfS6rEmf4gsED+FAbz756lOs0gl3P+Cv5h/2O7qM/DP8y4T+eGZmWynXhTTV5Rkli0YJ5vt782/EGnr0cu7/ILzqmJYllFw0icqHxQj2Xrfv3lF1XjN7k8588mtXK99X85w3RxIV5uRZ9OEN0GFfkxfLAH7q3toXhxxbMqmnUShbxdI2B4Y79jjnT1H3M0aQAwTbSKePRWhG+Pyjjw3aiBBhEPo8brXMnwxdzKcKiM7YEAdVoqGmEK/BcNHVj1Wad1P1qJDXIyZUCDm0ORopl4/2b2epIHlQyixjClNKJTcmx2oB52JpORNdE8pMHwRob4DO7dU0myCFDsIEVDFEubpF05wy9FDrV5Gv8uNuIdYPhiEge+25rXXlH1VWROOPx+5Bt30QVV/h5pDspi9tcwenbDu+80v4bPp/6LqECVz5pWL7Z6lDCndjuqJqEN6ZBB5RgdxEa8PoYvxiiIVlGPHLTH/3kSj9WNkpaWqEPCvl0Oq3oNM5Dt2zLaPuqwpEjI52HMSXGTaBFFTxl/pW+fR06gDmrRxMtd+FQvcOIZMeu36kE+5qpQdaCS4FytqPN6KYg3vr9BLh52Rk003XH3/5VzTj1JTRt1qllyCkSPOxQuleIHCAV5gnw+VkY3JQnKEbuPp8FAHvvnKjVLh31V0nj5U0/kZWZKWKiUiiZeecRnEyipsQqFiXjd8n7OAALhKBo6/RqCCLrcVr/6iqFvaBwPiI147pXsAoLSF1tiI8SjkfvB++9RETBpFSoP9qCFYKSR2So5wzNiI55/E2nB8OoLK11rsmEMH475KZeeNbjGn7ti+Ll5y8s1gjfFRrzcjCmKCgzSDkM0v0jYaeT+SgjZ9GbibQ14ceFAGStMZxgdYWpAt8uBK6RXkLTrv5NIdgs+Cga4ll7i4Scxwp+2QWcWn1zawnlz5Lmnn4o1BUFjTtC0AEpdXAPCg5OOOkxp6Ugao7GjMoH2Tr+WSJiWxZzVX+sFWfhm4qUOkIrCOX4Sw3HIm0E4BJNIkEhJUBEYfd6Z6rVOFUkUoCvMKQZwGjVbDZK/CAjsSWTOyHBuzcjkGwVRM7VmfjEzPG8lvcTDCSf/pVUbrCRsWyh3nasQnfqsPNQ9o+7A8pufh7EdU3ZzAiEBqRm36QVcL0N/6gEyKiLmiWMvV2kbZFg0lqPvy+DZaOndagG9qW7HOHkZhCQVQjcYurcoQHHfb8gPhqKYshgSeFY7/kyfhF/HGKt40FkZGpTd7LlA9HsIHPTfqSNn6Jiqqq02lcQj+coqd3S7NnWnNFgVqL+2dDXATwv786lG1HOtzOjzq1pQ69XHWbFKIkDlFwHXAuULJT6tsFnhUEunGFXBRWrTKRCHOiZOOVFhPQTkgVAHJWnbqA8IMfy0e+T7GPaDnJ5tEEM5zNbf7aADfK8PMnl1oZGA5rNYNXEvVj77tGvjEHNZCILWvflGQ9/PAKrSKZlIIG/+cJN6UPS3MjaCFYYHT0WAh+jViU+qg+2QfolGfEC3RnB8LAbrvLnqdXVMgH3lYqXSK9JR++/lSz5cA7cuMvy59k0TCl59caXv9fFLlSGBQVUCOVMlM24yUR26NbRtRJ/4gxztedM1VynJuNtDpkmH3BpnxtZzLBSria5kUMdldXNq5vDXli5aqEZgIAjVvxRBvR0Q07nqEa3SHgnx7r79lkjuWYpQVTLLjUiAaBhScnCJ13QmggUcdKoTjDAL2oYZPcacO96Hno7tHyN1wgmMtDf2OqyjImUjU0KRzNtXTFogWaUZ6uO2vWYcVSKBqWavJR7gFyE994tKWQXPH9BfrZY8cLezJoiQeQ1OPjVYqhUU/8N0qoUxIla2bA3GXuCf+h2uRy6PVZZAg9Xe2debYlTJonIrBCXNgE8YpvxF5xlN1FRMaMZGn8dWjvPulxqJwmbefGPzNXMNpJFof3QDfioBj+6UYwqW12tTiCohaO6l72yLI4YMCLUVQjKCB86xCDPaLArDp2OiKCAixhf1yvPRtcZrqJqgiKZxKaq8ZQKokr4XotkHX45okYeaBJnqMSocRMKAQUBE4V4RuPYHKZ9lUL1c1exTqPZGOsGIPhvV3MVhNPvoJDDTqvBNMziAMQz+LEVt6AakZEjDqGg1wvMqGjWuYdqEa9W1EchwTpreenOEmoZurLbSXQCQ7CX7jxghqCYbtzF/D1+NpDJRsz3SzQlqRljkNqUSFlQf2H5J/JoiHnXetatXqQQ1eTyi65zBdWhPIcaUBYH0CdsvhyCTgqH5mmRz3OkUbegOOV2I1RcRQc7gOqasEIMZnUDKRKsheTB0dVrESWM2f0cTyP9BBrrIICFDG1EYO0/obrGfZ5GNvgxkXfydPF0GtXZ+cB3MWIhRtBqkIu6+bboahEiuDiUJSVseNk4+nf9OUF6zF+whZJRbM4pqxJ18LtdCVYX0T07gOYo298O37aAc5de4TVRJxQIVsd+hxgz8iYp4EI3tnVKfHuCN5Con9VrP4du5P25Ag1KY2yBENyO/hgTLOZZWI2w3WlgjkcwkKrZ5fjYja3Mw8R34HjeQ6wNWNGjYrneLhBCIUZ0NNogKxl50fqR+HvIq0jz4kvwbCeUMiQC84HvASi6PlHKC/BgypkaIQXL37TfWVH0epS77cVItNT3SltQKhKczjVxjhhF4pBSWG2/WC0SKzqM/6zECEPv2hw/mdrheo8aEUA1UzlQ1kP1neHZe4CF6WO6ODXXDLROva65UqPH+dU6IIgJFyawdf1a9oKmiYY2uMd30TWJ7aN9e6t+JwjOKUMeG5u6gZDesef3V5mYciMf8OhTJfkfHu5Lk4uFKpoRKZNS5Z0ZCPBLY9mkDBBt6QkGYIw1ShtAHJWO5OxreCR4sva/6YXMcKM1E+FJBDTpOowWTRDSJX7cZKi0lHjlHfa1swxmTQoU+Gh7rbugiEwVVCS2NUlWDJnUIc1DcJgD4GccDsH27VTPYxnX/B6ssJ/j4SbJUv61jvgrXStqG63IGNynHiVIH8X4hBYhuASfr6AdOkR6lMrjx6nF1r1SsdvaxZXSJQTgOaWECPJ9JPpDUCGNrveRYlM2cqxp/pymI/yetkxEQzf5S6iAeNsHIpSYM2hTth+qRLMZnI2igkbrR7ZIRFUwyoMzmBP29BCL4bW7nbPD/bmBeMv3DRNV+1ZQUYYp48MuPeK2MXKoBQBB7x9hlZw9RMnlKV/VuudqQNulVC0En3WAQGiLqHCJbMNOo7KskZbMnFi90vU60eviiKGWYrpAB7C4NEA8rxPlmqJHtWy5GpxcOPlMEGgkY7BMCmOiET0dzDisq26+eegDxWMWUn2ltvfiAJI69QJVEq1hSDoTFntwKIl5XAxdsBDSBn+U4gZumcDD/ntnN5Ahrat6xteJBaiaG6lWNCgcpG0jGFo/eT+cQWe3YRt2UMRpMFuC1VFBSjh7SAuLRi7Gu9jPzCRq66d7X5IEgettjSlPYbZdZLuT08MMgHiRkqhNk0wMliVAp3U0cO6bqfczjc56bZgfN3jrv6NZ8nhJwrtZPpQXEwwqlTIYMnEmmyUDSVrcdMm0gTI6P6BjRKLIqVk1SLKyYlQOSd1NiUgr/gG1VfyZEZyyZ32R4omJ8PFbGFM9TblYae1kY4u1k2aakr9wkkL/bj2/nIetOfYYuBg2L1GkU/De2T7ZXyl6zpk9VCWD+TISKD8lYXQIbNY7M+plBOToG/PBayFzP4KEEAVdcUyhSJ/GwYQlfvHGg2aNbX/daoFKm7xW/jYcfNPuO7ZBJBBCMqoZexSCbPv0HMusTIEmtzJ09M3AwN8SH2PiHYc7BNYDA1U7qIN7PLfP2eHMKcnlMg9IrHCRh1cIHQwJP07Xz6FC7sYWydUKWl55fofw9iEu9lW2btI0iaVPVhLnJQYCcvIdKSQqnxzOe/2cSIfGw3ol+hQTBCoQ4lNUJ8QAzS+yOOzk4O6Hof+UYAVYxfC4mz7NieRGQVZN0Cu2TEIzVs70KMnZVPh8DHMNOLNU1W8bSphCMwA/Fp3qI9xPLnk/yWyQFZOY6XUK0Sa4NMkEI8niQxUkmggcEm9RQcfJJNpP7YwsMc+q3Xuk4MDksqLLoIdwplMPjBMORyImH7ZPc90gO+GyNVCf0dsoK+MLyZ9Rn4RsyNJy6LKoVVjSv95K/I7kcFohCeR9BCNL4lIG2idBcqpd42IzkvksywLfCd4MoGJ1dDO2mkM9pPcd12C9wCCOpElTNuvufbRiZFds3tdd7rNXxjpsmqRWOz9Xvw1djFl8Q1q5ZrWrHrMj1npGRAGZLnTxqhHi/tiyzGmwvfNc0whZjShPyeNS/kIdVjG4yBJlMYudIeK85K5DDOR/ZDspofC6+oZ5ij6CAVdINHHfASkdzEj+T7T1leryPLfutJEA8rBDyeDdognIsFL4geTo78UihOM9e8wK+IYlknfujajFn1gy1alIpmTL+ymblDH7jjKmTXSfEGwZDPevmUKPE+5Fl7hKKAgECIhrFxyNZjJiAIn49rYhEswQvXr25+IhMC+W4rBSCZRouJEY87HeWbU7i26UdrIKU1VitwvhrbmAr56gEDlmmXgzZqPdC7BQGEgB36w/SIH9aQjysc/zfLxsgOYxertGkLn4b/iSpHbbapx9fokicYsEnyqWGudNS4mE/jCsvURRMkxbyJgriUU4r5DTRgoJxWTxz48TD/iilv1cEkDr5k0TAmaiIh7WXSvNuiXyCZ9tRIuJLlMTD+sX4xUuYhToYJSqLmnjYyPi+ewlDGC8R8yQO4pFQnBnXHSiROKjDhladhLU4iIfR6JGbo0gLjPkS0LTTqMVFPAwl6iNx3I0SiYByWIvTJl4WJ/F2aLrwknzZw2KpzM+JjRtxEw9jxHzux57lCGyvofomWmJJEA/DT8idgDSHQA4di0/ntKSIh/3Ysh+G+5ZIG8ZJDNGrlyVJPG0M/SgrHOkBz+I0SZgHJoiHUV4ra7vmQe01sjJYPWaKeBjCgly2S2YEqEwiKfg3YiaJhxE9lXq+5HGzxJijC2OmiacNJXO59cYPttajxfzzTg3xsH+RsoEoTlCJ+L2Yf86pIx6GwIDWydz17RoEqxxzbxrqBovLjF+Ah9E0XipcWg4Swr8R888zM8TT1kbKfo5GwD3j3pl+fpklHkbFg+33vTpvfhHBDEMS9MyuNv3cMk88baReBlq2sY4HURQw/nWwJFDcj8qMX0ADtlPTTX4n5EPJM7gHjH6NVcIUhxm/gBYYp4mTk1oe4gHlDfhw3aRyD0w/h8IRz24cfzVRKoe25RV8t8mW7Sbm73dJPIex5fS0bJ5lqZvn1QD4Dgss6yUZ3E79zPgFxGi/kspZqfdKtlZCrpVrJun7T2L+PpbEa4Ghqt3XMiZdP2FZmuZ+cS1cE9fGNSaiADZtxi/AkO1oWWupRMdMPsJZT+JgMH4GUrDpUolG95JKT4rp+1ESz6CRdN3ZsnaW9bVslFQIgr+4zDLOekJBQx3Zfnr5tqZ/29z0mmVN75ne9Bmc/dC+6bMzG4VGbcYvoLRimvELKK2Y9v83dajmP1cDjAAAAABJRU5ErkJggg==";
                    break;

                case "Dậu":
                    _image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAWbUlEQVR4nO1dCViN2Ru/dWtkFzOWMDSEYeyNFBEha9YpQrKHkN2gUYRkb+xLQvYsqexGCyIlEWWrkCRLStrr93/P+e6NtOjWvZln/t7neZ+bbznL73vP+/7ec873EQEQfSMtQ1qL1FSivqRRRVTfz+6rJSnrm/SjtCusTmpOugnyl02SsquXZp9KoxJV0r6k/vLD6qviL6lTVY79KHUAq5KuIk2VMziySKqkDVWhoH4qCjh7RaBRQmFtkjuQ8ixMTDpNYd2Xn7A2srb+qwA0IH2kyF7LWVhbWZu/OYDsSY5XcGcVKaztJbLGkoBXkdRT8X1UuLA+sL6UKoBNSqVrpSusT6UC4H8RPKnIDOJ38PKKTCDKAt6vpdmLbyysr3IF8P/B8r6UIlliUcArX9ot/xcJ63uJAGQc6VTx6k5HRloC/aaVpAPfWljfC+WJXwNwkiy1ZaQl4cqpzdhtNxk2JnqYZtgEU7s0wtKRPeGybC5ueR9CZvo7eXWutIRhUCwA9WWp5cWTEGxfNAKuDhMRHngJzx/exrPwAFw+6gSHcYMxqL462lF1Q5to4ODa+UiKfyq/LipeGBYyAahMGlv08rPx5O5FRD8OKvCKpPfROLBqDrpXVsOvVK1F2wYI9nYrXndyJIU0uYRlFEkYFgyTIgNoraiWRNz1g0VrLTSjqjuXFcFj59JilRP3/DZm9dIhN9EKe+wnwmXpWKQkKdQ9MEyKBGAVRbaCyduY+xjXrglaUPV6KiK4b7OTuYwrpzaiDd2vLVFWVrDPaQW0NpcwbL4K4GpFt4JJZKgvBtarzkHoqCrC9TO7Zbo/9JobuqiJ0EFJGe1FKujzk4jKPIZHIZexY9EQnNw6F97H1uHS4XXkOqYiKSFaHs1m2BQKoDqYQyuGpKfEItT/NPzcD5B1HOQa7OOO+LiwAu+5cXYPOpVV4SCy4PImpuBr80h2Kqx7tOH3GqmLscZSGdbdq6FzGTF+p2M6pOzXgEB2XlwF72IDEeJ3Eikf3xanezm1QsCoQADXylpiWvIbir4T8YfWT+ggaXQLibJODPmlKuxG9EHkPZ9827N2yjAOQkvSnX/lZU3pqQmIibyHiFA/hN/0QPSjYLyMDEXiu+fYvnAkv7dvDTGm6It5lNcTKaGjWIWrnpIKulcUY7yOGJM718Fpl+W8zJSkN3j1LFBaA7Kz0mXpMsMoXwBVZCmFSVz0fYzXa4JRLX8mECfD/7QrDUVX7F0+i0DrRpbxA1pJwDSqWg4nNi+muzJylfE6OpQAqMD9WP/aValjIbnOv3/9hMqbAlOtShwsGxN9BJx3o7q24s8BTaGnTIApq6ADqT6B1kmVgBOxf4uhr6LCh/egeiIa8o7IyohD0D/H4DRjMD2UEA6e30lHJH94Rc8yDdnZmUXtukp+APaRBbwP71/CxrQrjm9ZRgQ6/2ERE3ELW+ZbwKCcmAPZnnTdVFNq6Mdc1znbTuLgtCV1th2Xb1lxz+/BeelUjGvfmPzbeqSlvMHpPesJKPKhBJauSEwWpwzz5soY306ZjwZmkR2UxBhYVwyrzuoY1rgKv37rfANqWyB22IxFWMA5ZGWmIOq+JzLSP+Zbdz7CsMoD4K2i3s07bWeF/Y5zi3Rt6DVPjGyhidZUHQPy8Lr5uc4/Dfcha1Xl5yfpN0dh6d/lo9tgP8oI9294wKy5JgGizMHrr8EsWBkjmonw98yGxDEPYYJuC/5g2ovY8FbiLqUjXd+rmphTKOtu9XDj/BGssuyFB0FnZek+wyoXgFULbfUXkvAmEuun90Rq8vsi1xgbFYQx2locpK7lVfA45OJnZ7OxcLABB7cnARkZeqHQsu5cOYpVk8zQtawqB4Wp0wxjhN08gkOrBxMgfbBpzmQkvg2D85LZZJmCdTNL7SgmS2XWKlLmx3WVGI2SeRWWYcWXSKUAWshy941zB8gS1staKQ0TP/TTqMp94l+mhuS8P2URJ7Ys4H6QWcwBx5n53J1N17PJiSQi42cIDAEU49qVKeKvw4f4SPrdh4Orp+Px7cP8QTmMH0rXJ8Lfazdm9m5NoAnDWqrjdBuRL90rcz8kwjDLAdBZljv9TzsRZTlWrFrdty3lHdclAG5dPppzPCL0MrqW+wHN6dzG2RNy3cMe2F9mXWDZQRNWBg0xuH41ulYVemIR/mhYnXzwazx7cBhH1psR34vEqR1zuVUyEBcP7cbLyM5OxL3rXrjmuY+olgvVfZyicUyx+iARhhkHkO0fSZHlzqCLVuTUr+Q6xhrz4sl1im7XEfs0gI7k75DTkuMwurUmT+XWTTWBlHbGx0WQb6rIrdNhfH98oqOZGN2mIb++nSTrYFTp0uENuOblDIMKYlh2bIarp5zw/OEVeDkvpoivwgMK84/sepclVrJ0r6jCMFNlAGrIemdmRjz1T+BOT2lY7rSbSs67BlmFCN0qMj8mwpy+LXFo7UIC+k6e+712O3Dr6F1NDW9jH/JjHylTMNWqzI9P1KtFkVHIa8MDz6BPDXXyVUqcljDrtTJoAekzZxMUDFxdUn2xEOl1yb+xa5nqUvDorKZM5SgkzdNgAJoX9+5rXntgWEWVW82wJuoYq10HY9rWxoA6ZfEbHWvEiHTDmjjvyrjnpxiVkvQSI5rW5ZYU4isM49SPMcQnq/BAMvb3GvSAPpBF36EUrTwf1toSmtNeKXfaF/c8mGhKJQJOiXM/FiSk4EmV3Te3nw5dnVXcrhYk5gxAk+LcydKiLmWVYWveH4EX9/BUjtghdTyRyPEtGmKbsHhYd97x5nwYTcHnWeLmuRbccnYsGsP/nUIAmjcXAJzQviYv6/3rZ3AYZwRbMx0sGdEeo7V/wbGNNmDD+pNkYLphEyGnVhZI9JcAsuHMptEiQvPLhkokJgxAX1nvinseSpahRiSZ7YwojL1nktN2Rj/KMBgwZ/aszDlz77oHT9+WjhrE/52WHMst8DfuAwcgP2vJykzItw7r7k1hXEuEHpVF5EdFAnDiz0Akq2QAH1m/SNaufk18GYAyhyLWwWmG5IeyixZ7ntzxxuBGtdCJMpLnj27wYx/ePYWxRiUKAE3BrOhjwgsM0azMrXLLfMsityXyfgDG/a6MfctEmN//Jyy3qCrJSgSuJwWTjQTbYR0pGst1jSaGARglyx2PQ07CqIoI9/y9ZKopMtQbXSuXg+3wXmA5KJOl5t1h0qACWMR+//opelSpyP3i+f0rCy3rc1k3dRjl2FY4uMqI8toN2L/Sgk9t9f5RTOVJfKJYhQcXU63y9OCeyNTur0iUzADudxhAfKweReJEmWu7dGgjJf8izseYHNlgj77VVfExMQrvXkWgi5oKhhDHS3ofWaTyLh/dgA3W/ZH8IRrLLHTwJuYe7M3bwHYoo0jd4blrDgeOZSDsd0j9CpRFyXUXnuwAuq4YQER3ZL7nsrMS8eZFKIJ9XHlgeUnpW3bWh1zXzDPuiL+GduV/XzzohJ5VRWR94RSUDvFoObNn+1zXx8c9Jt81nwJJX0rXhhOBPyEcf3UPgRe2gvnA/SstKbjMJR4YCJs/RNi5UETgroX3sc28TB1JFDdpUBEJbx/L0t2viewAshz2qNOCL46m48KBNeSLGhOJLZszmdmjShmiNQ3hc3wLpLzt8R1f9K2hgrcvH+HJ3fMY3rQC0ZpX8NixmNOhs3sdJWVm0rHlGNq4BvG++hwIxhE7lhFRnjucZx9MHgR5YP00A7Cgs3ZKLwRdcsLmOYYE5nW4LLWGaSN6KEZsyq0WBtQuSxb4jQHMzEiltOkimZsQfbMpkGydP5ZHT9bJgXV/Ik74C2l99PqxAj/OIqCtWQ8+CcFkxVhjuK6cy61zahctymOfUeCwQieiRWzaismp7Y78vhlGvyIpPpg6r8YBbC0p79w+YQIgPGAPBaAY3L16jMrVw+sXIdi73IQT8VWWLbF6gghX3Ddgy7yRMCKC/82HMJPsrCRqoJCq+RzfxkEara1FQ2Yj+bMXBHICMtLjERcdgn0O1jx3ZaR6qqE2nY/BbR8vLBnZjz+MBYPZkE3BvP5dsHz0QF5mapIfWZ0GGotYZBW25ZzYshAjmtckbqmPhYM1MaWzJtIl85DJH15j5XhtvHp2m4byZLJCV8REBGH7grbw3KFLxy/CbngfivJiuvaZfKATpHgASiUrM5l8mi6mGelRw14WeN3b2DCsmjyEE+o5xp3If/lh0R8N8ODmFCLI9eHjNonSvx/guXMxzrluguuyGri43wT9NNTIKsUUbGbQQ3tFwIaRtYXCxqQtT90i713i5V/1sMfda2548TgAW/804se8nOdR+uZDvtWNOOZDmLeoSwS9PD20uOJ2Nz8pGYDPHvjRkFUmf3OzSNefc13PJwT+tlbCjgVKcLEjqzQQCDBL/AfXVyPawSZG1QjkQLg5jRXSN9IRzWphUsdmMGlYjftXduzCAWEYM6vKzkrFbjtjsrwQiuLPsG+FBaIfXUDAubV4eMsT+pT296ulRgFL/j6w2HM6j4LPwX5UaxS0kPchPoJ80zbigGwbssD9fI7vpqGkhtm9lCi9EqNrOZbwSxN/ZYmKEBbgAVcHwbcyv9dJla2+VUa/muoUaSvwQGVn1imnrtveTgi95sL/3rt8FLmJw/B2W4HYp1dxcPUsHoWnG/6Id7F5JzdKIDHFSuWkEnbTA1vmdsxz/LbPUfJt3TDw50p8bcKAIueotg2wbYElDzpeznZ8Jli6+MPWLRjZZeC1FynzdQs29KIfXcf8wV2x1mooWfklCjC3EPPEn6KtLowoZRvdqir5VMGnJb0P578PgjwpvzYm4G7iyLqBFIDuk9Wqc/dx88IuJCWEc9cjJ/Et9mQCk5gnAcTB2CtpUgvM5sm+dOZksr4WUQkryoE3wtluCsx+/REWrTTJ4TcmnyfmALJZYsPyAtkd1kgJFi1/4OsXo1rXpQ6z2eLXNDxj4em8FMN/qw3jOlXxl2kneB+djq6VRDi+6dO6TFryW5zYNIqvL4de20lUKRiH1thQflwOdiMN4LFzhaSZRV59+5qYlGg6i4m/1zqeRTAJ9ffkvmlGLx1y4OfyrL6lp76lIbwTQxpocF/IhqF191a4cmop+tVgU1u7CVwjyUKQoOYtquOPBuVztm+YNa3BI7n3sR3cqgbUqYTwm+7SGqgOYU+U1872BJ45upYvgzN714A93CMbLAnchyXp7pdiXqwJ1c/lXex96pCwbWJyp9+IjnSiIVJ4mhcXHUbW1IDP0Jzcak2UJ46ieVMix2+wfEx3bJrdhzIUZQxrLAz/GUb6sO7REb1rlKEg4c8zHvftK/nEA/OPRuoqFEDG0IPYgutnnOG+bQEm6lZGUzY1pssmK1Ik9d5DxF2WRhZr80V+olGsKf385GVkECz16vHF9s/ldfRdGr7TMadfJywyMSJeOIfIbjjRjcG882dc2BD8QEN9IIHjh+MbhyLq3hmKuHXw5M4u+q1Ovu8uRdVp8HPfxK1vw/QBuH/DCRP1hChu1lgAUrpY1EaSCRlWKoegf47kak9GGnu4cgEwZ0pf5kWl/OTWP7uwzqpLrmPex7bCsKKY05SJunW4WrSqhUH1qqFbRTXeyf2ObCE9C3f81uO272bKbx3he3IbHZ+I2Ch/bo0fE6Lge1yYy3P7+y8OluP4cvB3r4TMVEcss6jLo7WQPipRfSr8730rJvN7noZfwT3/M0R3ZNjy+HXJWVSSeVkzP3kdHQzPXZ9W08JunkOXsiJsXzSVLI5tGmJPPZMs4BXOu85E5x+EQDOuXT2kJj/n9wRe2EDc0odPvF7zdKUo7E0U6CyiwgIpACwgWvQU5w9sQjsV6TqvCO5bZ+Laqd6wMTUgujIEntsZ1VHmi+nzjLWw27Yd7Iax4MRmun/CjbOHStpVqeRa1pRpYT0/ycpMI/+znpi+EDjWWg2Cw7gBOefZsHWYMJC42C9YNqoZpxaMvrBAMn9Aa/JN5ynTYCQ3gy8LfHpPOwGnXdbBsIIKphg0o2zjGDy2NsLZvTMx6OeK3I86TmiBYO+tCLzohFk91fn6yOjWYuyzF8HZRoSVY4UZmnN7zXBwzVykfiyxJeZZWJd5a0e+paa8IguL5zu2ZhrV5/ummbAJgnHtGmLx8J64H3CZJ/q3fV04fWGzxyzCsgnQZRY9yYpm0NBfhX+OOGCXrSVm9WpJ4Anb1NgwZdF6xejaYH7Ti6jNqDYNKB3U5pO8QmQXVu/m9hVjyxwBvNWWmuQvW1DmsgFsJGRmfCi0H0WQPFs7ZN5cVJiw6fm1U7SQnCgkOUvN++LvWVIvkYKdi6dTVNUinygMQwYisxqp89eTLFOyYfobnyNsSwHGB64rl2BK5zacvrBAkvjuDj68C4LfyZUEshIvo51kaE/qQJnKMGUsGV4GBxyNkZ7ylgj+AT6/KAfJd3ORzNvbCpIsykvPu45BwtsoPskwq1dzangU3wVla9aN7zx4cucw5hsr8ZmVOX2ENV3D8srC3j62PKksZCdsRa1vjUoUVP5E4KVDZG1GHGQWSNjU/UTdWhivo0l8UZ1b3uD6NTG8WV3KeiZQVuJLZDqcuKorRW+5TiLku72tWBssCxKWhr2MCiBiG09D04i4WwouHXYia9Lm549tXEyWOINPGlh3q8WtxGZIOZ7K5VpRUxHWM9gKXivJLwN7vE5jrBjbHw9veROPjMXsvm3Jr9Yiq4wkN6LQzeYFbrAs0RbfvJKVM/d2avtiJLyJwPn99vS3kHqtnjSQz0QfXD0PPiecCcj76KdRTthhIAGOLQgx/8j2wQz6uRpZchMi3na4e+00gfZGUk8K7Ib3Qp/a6kSkd8mn6QXLV7f4KmSTeUpSLGUsoXyj96PbHmSVyXzqPisrkwcKFnF3U64sWJcAHpveYpY3q5c2wgK86F5GdfLut2ETtqZa1fmGyVKQr24yL5XXHLKzMsgnPecAsl2sTC67beKvPLTjIAr7nddOZV92KmzXaBbfSrLDZqiimyyVIr3mwFRhL9oULFkIOLcfJlo1eTQe1VKThmluX8bSsOjHwRR5P03LH9nwJwUMM8gxvy1IivyiDVMZX/WSn0TcvcwXlxgflErgRTfYjegIG5PmGNakIka30cTxzbY5i1QCr1MogDK/6sVUppcNiyvPH17F3atHiIQL+17Y0DaqokrpnDB3F+rvAavOFXD97A4i5CFIfPuE6IkP7C16UwajR/40qTSaKfPLhlKV6XVXWSQ9NZECiBV6VFXlxHdoIw3ihDpETxrxYGKqVYOojh1f0WOk2GFsO6yf1hvebtvI2AS/6DihM5Hj3YpqolSK/bor0xK8cF24hBPJZcCddlmN8MDLcN+2BDOMdDC1izacbUcQmF3IEpV5rtyUbzgaiKfhN3Bo7Wy6riknyXHP/REZWtI3PguVEr9wzVQhr/wz0OxH6eRz5pMvY5O1V9zXwMV+JhFwHfJ7wircqe0r6W/JC4rZCv04XIlf+Zeq3D86kZmRjJeRF4v8mpXviW2YrN+QZzRpyfGU6fjJu0lfitw+OiFVBX32pGjRMybiKh7fPq6YJuQVuX/2RGGW+C8UhX145/8BRIV/+um/DGKpfXxMqt8/f1dCAJl+/wBjSW7+TA3w/ROgJdbvH6GVk37/DLKc9PuHuOWk3z8FL0f9/p8RyFH/E/8dxv8Ar8D7j44BgtkAAAAASUVORK5CYII=";
                    break;

                case "Tuất":
                    _image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ4AAAChCAYAAAA2qQYDAAAaYElEQVR4nO2dabQWxZnHTTKJk5h8mMyHSXIyyclkjpNzZiaZMbgkuIDjgrgrCCqIrAKCIu6KC26A4oIboiCCK+KCgrghuIsb7ituoCIoIKIguD1Tv3pvXfv27fV9u7u6763/Oc9R7n37vdXV/37qWas2EZFNnDgpWqwPwEn7FOsDKJn8g5LNleyiZJCSMUqmK7lLyUIli5WsUrJayVr5Hmubfraq6TMLm67h2rFKDmv6zs2b/obt+7Qu1gdgUTZV0lHJ0UqmKXlWyQbJHxuVLGr6m8co2U7JP+Zwf6UW6wMoUH6iZEcl45Q8JsWQLCkYC2NibIyRsdqeL0e8BuSflPRTMkvJ51IdMFbG3F/JL8X+PDriJZCfKzlEajbWxtbPtHL4SslcJX2kdm+259cRzycdlEySlkZ/WwOa8EolW4n9+W7XxMND7Knkmejn1SaBg9JLyY/F/nNoN8TbTMkIJe8leEBtHUul5plXahm2PoCU8lOpEW55wofSnvCxkpFSmyPbz6nNEO+HUvNOP0jxINorlikZKiUPVFsfQALpJDV7xiEdmDPmzvbzqxzxfqVkRh0T7tASM5X8Wuw/z9IT7wdSW1ZX1zfPDgH4VMkAqc2t7edbSuL9Vsm8uqfXIQ7zlfxO7D/nUhGvm9SqOxzyBSvJAeKIpyszpjQ4mQ7pMVXJz6SdEu8P4jxWm3heyb9JOyPerkpWZjF7Dg2BpbertBPiEeD8Jpt5c8gAPIth0oaJRwZifGbT5ZA1LlTyI2ljxKOq9qYsZ8khFxBwzr0CuijS4bnOyXZ+HHIEhae5FhsUQTpc9geynhmH3LFAciy1ypt0vDWOdNUFmY5cYn15kg47YXYes+FQKFh2M7f58iIdyWhXWdJ2cItk7O3mRbwxec2AgzVMkJITb0h+9+5gGTzbUhKvi7iMRFsGzzaT9FqWpCPh78qa2j4oKv2jlIR4uNyuyqT9gKqWhsIsWRHP1dO1P7DblVXidcv/Hh1Kih5iiXj0SDi7rv2CWr7fS8HEI0jsGnMcSKul7l5rhHj9irkvhwpgoBREPJqtXd+rgwEhlt9IAcRzBZ0OflBAmivxti/uXhwqhs6SE/HomXiuwBtxqBZekIRVLGmJN6DIu3CoJAZLxsSjmnhZobfgUEXAkdh+jTTEO7bY8TtUGBwckwnx2HN4RcGDd6gu2BY3slEoKfGOKnrkDpVHpNZLQjr20l1S+LDrxIYvv5R333pT7p99h1x01uly8vDBMqLvwTK8d085fsgAOffUE2XOzBny3luLbQ+1rQPOhDYJJSFej+LHnA7fffedLHnnbZl312w579ST5ODd/k922eI/I6XPXl1k6mUTZPmHbj/vHMEJS3UT71kLA47Fmk9Xy6svPi+3XT9dzjnxGBnYfe9YsgXJkX0OkuefftL27bRVUBxcF/E62BitF19t3CifrFghb776ijz6wP2aaBefc4ZePnt13Um6dPjv1GTrsdP2sve2WzX/+6DddpTHF7i+85ywtdRBvIlWhqrw0QcfaBsNjTS4537Sa/edZb8dtpGuW/9PXZoNwd577qmFsuyD9+WCM06RXf/6X82/Y3le9OQTtm63LWOypCQe7rC1A+meevRh2We7rVMRC+23xzZbyK4BWvDwg7vLimUf6u9etPBxrS39n+m7T1d54Zmnbd1yXfju22/l/ffe1ebC4tdflfXrvrA9JD84+O8XkoJ4ve2MswaW12G9Dkit1UYdMURunna1HNSlc/PPdtvyz3LdlZfr73395Rel9x67NP9uz7//tQXB+d2j8+fJ119/bfP2EwE7d+qlF0nPXTrpse/f6e8yavgQueOm62XZ+0u1mVIScO5uYuJZ31Zs4vixgdorSo4ZeKjWAjdMntS8lO75ty3k7ttv1d85/+67pOtWf9E/Z+m+947b5IG5s1vYfHtvu6VMvvgC+XDpEu0xlxFrP1sjZx03MnQeeIEuP/cceem5Z2XdF9bPiMaATkQ8Tra2/rqw3O7X6W+pHQe02qpPPpZD996teQkmdAJYim6YcqWcdPggTTiAdiO+5/+ufvvuLlddNF6RdY5egl95/jl58pGHNImffuwRTXAb+Oabb+QyRaok84G2P/uEo2177jSC/4skIF4pytpXfrxChh7YLRXxdt/mf+WReffp6y8887Rmrdd/vz308mNAoNmLC884NfQ70boQmuV73+1ryzL/7/2+IjH39luatXaaF3LG1Mk2TYhBkoB4t9sanRcsc1GECCSJItq9d9aGf8u11+g33vzuqL69Wr35LEM3Xn2lto/S/B2WZoz6ovHCM09J9x23TW37Gu13xfnjZOPGDYWPW2pb1kUSjxOfS3O8+u03XpcqhALx7ps9S18745oprTQDbz6ps0vGnCnjRh0vh/XYt64QzdCDuuslr0isXvlJXQ6XX3tfO+myQsfdBAzNTSWCeJ1tjCoMLz77tHTr3DHxxEIivFIAsRp5SFHCC1E0MD2OGdin4bHv1bGDPHjv3YWPX2FniSDeWBsjCsNnaz7V9lnSScUZIZVGkDgoVpeFoDEZlw0sffcdGdR9n8BxERYi0E4mJs4GJDC/4qPC63rPlQjiPVr0aOJw9IBkbznLCDYMwHOtJ50WJTgWpx01TIdZbIHlffKE81uMC9vtpGGHyZOPPiTfKk8bu5VYpnGEwuTW66YV7ZmTGgokHscCfBlxoRUkdTBIiREuWb9unXYk6iEXIZRxp5ygY4jkhEmtnT96lFxz+cXaW/76q69sT4e88fJLcsie3wfBjzusn3y+trVZfseMG3QMM+xeDztgH718FwhCdHCsFfE6FjmKpMBJ8OZVgwSn4Z0339CfpxaPjEQ9xDvzuKOKfhh1gbiiycBcf9UVgZ/Be+V+ou73sQWF70JCe2wr4o0sehRJQAA3btkkdALw/Eb26133ckrWgqUdLYfWm3TBedoDvu6qifLJ8uWWZ6IlZs+8SWs0sjFhoKLHm5Xxy4SzRxc4Yg1dmewn3tSiR5EExN6ijOUxJx2n85LE/bDt0qbZkgieIB52mUAckQoeimDDQGEE5kPYfQ3rXXid73QJIF4piz6XvP1WYKyN5XfsycfLp6tqO6U988RjqVJsXO8X/89Zws86fqS27zZsyCbwikH/+WefyWsvvaDDGiT1b5p6ldaqOAVzbpkhjz84X3uw5oUKAtXTLz+/KDIg/PHyjyKjApSDFQx2E21BPHorrIS048DkUe5kJgvbZvzpo/TDMRUYVLMQIgibYDw/shNDDtxfkxWP7qH77tHLFEs5eVtSUXfderO2ERc+/KC2GakAabTKg+vRPOSe8UgpYjXFqLxQmBFoaU129V9T3oVXSok+Gp2yfjSbl2R4sHFeKfcR9TIyhoKBd/ZjL/H+vegRJAUJf29Ki9InrxfHgyUv651QHhxLzPGD+8v4007WGoV0U1FeKVoKm/Dh++/V3rHXC61Xeuy8g/aycQiSxBGZF9oC4r7XAv7DS7ydbYwgCVavWqmDot7Jmnb5Jc2/Z6nyT+aRhx6kbaAv168vfLxvv/G6tjVJb+Vhb6IRiduhBcMKFajgnnTBuS1y1SWx8cBuXuINtDGCJFizerVecrwTxjJEtcWdN98YmDRnOaaEyWsf8f8sq5Aij0rdxa+9ogPYZA+yJluYYDpcOvYs5dVP1bYhsbspF1+YKKcLKRmzBQzxEu8cGyNIAooeCXamfShoPUgL0AwEoo2tGBb7qgf06GK7mfq/RiUuZpmVQDxLnvq40odSACkgDPI0k0qDEM6HseneeOXlFpqR8AgeZCP4YMl7ulI5TS45SFg68ZzpoKOyJspJylooCbOA6V7ilfaIT8IY9BIknUyKAyj59oIcJ94x3rD5HEWjLFMEnZOCdBwhDAKv3t4NvwzotpduKoqrkOmz5666tdLYoiyZUdU4ZGTwRBl7vZoRovNiYqpYKBQA93iJt9DGCJKAsEFQaXqYkG0ALNFMLm81pVIk9zG4vZ/l4dEMTpkTSybEImSBR8h/+TdhDMgxbeKl2hhPko7Do0ZTs8TT4Rb0GUIqlNADPkcPRVCgHKLw4pGpgMz0GFPsWo8tyUuJnQvRLfaTPOUl3pu2RhEHJujqSy5KNLF4kXh7gDidCTybYHBU+ohurWMH9dX5TWJno485UqfP0Gx+75DwDs00YbYnBDJN4tiTrcapxsMyDej1RUMakpH099qLvHSGJJSuE1u8Z9ZtrapPKItCg4ZpTF6YkjSuv+0l3oeWBxMJ3vYkxIM8pjd2FtXLKXsTksrM6VP136BqpRXpFNkhEpkJQJGD/zOEhwhQU2niDRVBJhrLWXLNz2hUooGJOcA0wN4NWmanX3GZzohAZF5UqnV4MchOoB0nnDPaWoOSDyu9xFtjeTCRIGiaxKahlN1sxIM9l9QOQhPSGonnS4EANlDYZyEW9iIg4+ElPaEMtJEJU5CxCAptsHRCEn9FMQ4QqTK0mgkh4QjFLe/cJ2T1g3gehbEQN6uUXwZYUxni8SBZKuMIRNIcR4KsQZpsAaRBoxiNQPD5jGNHBH6WZRdNBTDOWZqpSiZu6AUODlmWIJJge5KDhlT+70YzQRReBP+1LMUj+x+iTIFjW6XCIHFZ+4B9WO8lXqmBo0BcLu6tJ4AK0EhoP/PzOOLxwFmOIJDpR+A7wio76MtlTADNZQoVMNrJ/+LN+rMtRvg7pLyeVY4C3qnRal5yoe38pKxp5i21o4Tzw/VeG/PAXTtXZtu1yhCPifaGQsKEWjxSVoBlDi1Ecp5lKInGNA/QFIOSZw373ClHDm0R+ae4ALJHOTAIeWWAd8lLwZ4taEcIj5aLqj3EeTpBvRwmVcZOCJCR37EzAvZdFVCZpRZQOZKEOHh2eJxUrBigCYwGTHI9sT2Wrbg4HEu0XmoH9w/UUEHCMgvmzblT/5vmcJZWwkbYYdQf4lBE9UxoZ+Hs0frvmnwwhKcapQJYXyniEWdL03hNKIQlEAJRoez/PWEHNKS/L4F4HSRgKY0KEtcrV15YizNSuWLCPTgVEOmlRc9o+5KsSlCCHzJSGR1kdmDzVUTjralMOAVQCkWXV5qHXCsIWBdYDo8G5HdoUv6fOBgGuilxZ6/kuO8/YKftdOA3jAxBgnMAIIlZJoMEMvoLYNGwBLYZoz+GiE1pKRORFi3CKaUNIHtBqCIN8dBYlFWxnRek8nq65FjNbgD04RJwNmESshxRJeMI8TTsR4MvPv9cx9m8NhrkwWHxLvPYYhCETcKxJ5uJo5ZcAth0uZGZIJ5nWjvZQwZtR5HDQ/ffo/8epVH+8VQELQLIldDREGTA/numIt+spq5/PE6CtjxYNA1aKij8wFKH7RS3jHu1C6ER/s0STTgEp4EWS1P9QTyNXZu4liWU+B9L+RGHHNj8nTgN3nItHAjulZfnncVvNqf7iAteOu7sVuEUil0rghYps9IWCXhBnI1cbBriUbnL1mI4G4Q+WF7Jd5oqZsIf2F30PPBA40qw0ExoK0AXF8s/2hOtxN9AsEXxVieeN0bbclTJkLuFXHwH24wBnAiv/UYAm7wx26MZjUv6DnAPYWPiWpuN5inRokhgqt2xJAfxK2yrNORjiSL/CUHwQlkiIQjhCJPbTLrzgKnlowHIr3VOH3mE1rDeXUZNVoRiAzQ2mpDr6PFA/OEXr1NBUh8vF9JG7ZtC4Lwk6bAkaFEWVdpCUD+IscXV57HU8QDDgscstRAxbeM3oRYTQwuL8YWVLJHFYLnnegjFy0Ae1dvI5BfSdyzf2I5h9wKJ/WVgJUeLQtDSlr77wbLFQSphDwtSsecxdpR3L+QshFyscUjwSqkkSXotyyEFqYClNCnRsRmjNDxzUaI8bBK0KH0vbbOPHzx4HIQocgAM/iSn/CQVjHxj25lxYL8lvR4Ni5MBsCmzGBMam+65iqFFs09p2xv9IJAcFuogXGI66/0tj40KZUfgrddf06VOLLX+LcMIRlNY6rf9WFrNFrmk8tBkWYyJGGIJjxiIw58q0dDtBc07URvRmL1A+Fxc2IWAK54onia2FET1xtW8QuyN+jpigqZoM0jol0Cr+UmPfUf4hFQc/RVB1+IJp92dFOekYmjV0I0ssjqkGLB0kpgPewgY6WYbWrRLWO4ULUUrICEVDH1ISmUvXiEpqyDCoqHQtJAnrJRdk1nZlLfdcG0rDxT77sShAyOzG/yNuJ4Lr+A8eQPYFUGrLSxKHVKBSFGaBiEcQqcWIPYV9nCDWvq+9YQiqGjxJ+jRRCT1AeXjUfvOxTVRhwneMBqVKpWoVJoRigNKUsqeBoGb9pRumzKyAQR1kzwIBHKyZUXYzuimz4FAMkFczrFlI22WXZZAKn/JZpw64vBATYlXimbkeyCff8OfRoVlmr9PViXJ5ym9r0jxp0HgNmWl2ZgRW4gkfVy+NK2Y9BmBY3/8DAfA1NdhKwZdj31J6IKH/cRDC/T2GWQ8vB1ffC9ZizSbhiN44GZPFOJySa7BU7Zx7EEDCNyY0epWtBx6QpM05EALZUk4I+wUBWkIg/iXSxL5aFjsvbAtIHA+/Aft8eBpe2Q5hnBUu1A9gv2VpqzKlGMBeneT7rtidrmvAEK3okUK3XybnaAw6OfeNlN7luRVGyFW3JKMw0EynYS734NE41AVwoZAUd4lyzl7thDTI9/LC0O/qzm8zouoCmavkEbz7jgKCZNcRxddRWrwQOjm20ghxw1QEUx2gTKgpCXpYYIniRdJ6RPdaI0cQhKVvvIL2i3qvAi6xZJU0uDp4rEbYEcmmRNIR2zRfzxWiRF53EAhB6ycNnJ4Q2QzgiPB4XZmGwqWUX+dWpaC08LLQtUJjUVh9hWVz0m23aCTzVteRWiHQtG461jaF9wztxLHm3oQecBKIUdKpa0k9gpZgdFHH6G1m/dtp9qEQG1UqMMvVJGQaYCs7BxAJoDWQTY0JAeKR41WIVDLVrc0EmEeRHmSkIEXIurv4gXjVeNdG5CCi8pB6+uU3cd15tDnCiH2SCkk90P0sGewf0x7X5zwoHjLOYeLk6j9Z7BSOWxq3ZIIJeTsZYf3SOwMW43vNOdkIJCa7rZvU5YbYZ9FOQbEGwmD+PcujruO7Al2ZIkOQE6D2EP0kEKODeUNJySBYR1UngTZcBbQRthSuvMrgARoILZrSEo6ljKW57SESgIyIlEaF9IRfvH/7VqbYnhbJPND2X/FYnZeJDo2tNCDkomLcRySadejrJz0EkFUwgpR+xaTwqL4Minp6H/Ia5ki4xEXu2Pp9pOOlyCqioZMCF1nFSr09CPxQcmI9aPh40CwNe7kGq9QBZxXpz1537hgN5632XHAgGU+7vgrensrVm/nR+Kj4ZHelgaZCBjlSXY0N8LDzasnAfuS1FvU3yf36z+kGSfEfyCeX9CENPpUHP0lBfF+LiU6MNkLnIA4r9Er7PyUV1qJFwAPO24MlEn5Qx84NnEHwuBRV9iuA3iBv5AUxEMmWhlqBOhZSFPgSWEo20PkAYjk7RILEzxo7Dj/fcTtBUO6zdKu7FmC/TpacSuOeB2sDDUEPCzd9JIwh0lHGem4vACh4zQWqTdKtfwOBSEhCBl1LZ1jeXjeBWNrqYN4SCnal4iv4REmIRypJEIw3jRUHsDjDmvC4eVAYxECCSIPGjBqkx/ywWbXqwqDwuJAXiUhnpWjX7zAo/Nvmu0XAtGUjuNEUE5FADhvQCjd0D1yuPZoyRnzX0hPMJhcbRiwU2ne9gbQIStBYqqs2cWzDYBNYuomHr0YVlvUySqwewCbIeI9soRS/0Y1MekuvYWssvvY0ZNGH9N+WASIrRHYpjoFEtJsjvecJLvAWBk390MxAfFIDverWH1dGCjT+Yk0QDykdJXJDqWHrjQOk6TE20zJx0WP3KGygCutQihSB/GQ4woevEN1EantJCXxfqakErv+OVgFGwz+VDIkHjKg0FtwqCIGSwIupSXej5S0DME7OHwPUkRwJHPiITsUdx8OFQNtE4l4VA/xkJuKuxeHioBNXBJzqF7i/UrJ6qLuyKH0oAv9N1IA8ZBCyuMdKgE29UzFn0aI9wMllWlhd8gN86XGhcKIh/xWSctabof2BMyt30sd3GmUeEi3/O/PoaSgcqku3mRBPGRK/vfoUDJMkwY4kxXxSKeVejdRh0xBsSDP3DrxkD+Is/faAwid/FEa5EuWxEO6SK1516FtgmfbVTLgStbEQ4bkeOMOdsGzzYQneRAPGZPfvTtYwgTJkCN5EY+A4oy8ZsChcJCHTVR1klTyIh5Co0cljiJ1iMRciWjaqVfyJB5CJWrlDmJwaAbpsIbCJmGSN/E2aRq4I1/1sEBq++fkwosiiIewxXzptz1zaAbLa2zfRCNSFPEQ7ARXQFp+zJQcbDq/FEk85IdKxmc5Sw6ZgoN+M/Vew6Ro4hkZKi7DUSbwLIZLgRywRTyE9JrL7doHuddM0mBpxCbxEAoLXLukPVBl0nDCvx6xTTwE78nV8xWPaySnGF0SsU06r1DJ7Jbe/MHS2lMsP2/bZPPLv4prIMoTZCJ+J/afc+mIh1BgQOuk69vNDmg59r1J3Q2Wl1gfQITQNO4qXBoHAeFfi/3nWRniGekkrp+jHjBnzJ3t51dZ4iFkPFh+K3dWpgWwhyEBevautv3cKk88I4ReRihZkeJBtBew/evRknNyPyuxPoA6ZbOmSV6a8KG0ZTAHbP2aWwlTHmJ9AA0Kp4kTk8rv+J7yAhuul9TmwPZzaHfE8wrHX02S2qFtbRXc21VKthL78+2I5xOWnD5K7lISfsJydcA93K2kr1RsOY0S6wPIWX4ptbNSZ0m1NCFjZcwEff9Z7M+jI14DQlXtjkrGKXlMSZmOvGYsjImxMcbcK4Bti/UBWJRNlXSUmnfMzkcY6/EHkDUO/galYNOl5o1uJ7WeFNvz4YhnUQi6bq5kFyWDlIyVGkGwFxcqWSy1ChryyN7Ty9c2/WxV02cWNl0zvek7OPuhS9N3V9ILzVqsD8BJ+xTrA3DSPsX6AJy0T/l/jFSivcoWK9QAAAAASUVORK5CYII=";
                    break;

                case "Hợi":
                    _image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ4AAAChCAYAAAA2qQYDAAAbtUlEQVR4nO2debQVxZ3HTTITJzH5YzJ/TJKTSU4mc5ycM7shYOJuFBB3BUEFUVkU3HHBBfcFEFxwQxBFcANRUFHcWEQRURRx3xcUUVRccAMXflOfuq+ufft19+2+t7ur+936nvM78O7r17e669u/vao3EpGNnDjJW6wPwElrivUBOGlNsT6AAsnfKdlUSVclg5WMVDJVyV1Klih5RckaJR8pWSvfY23bZ2vajlnS9jf87Sglh7Sdc9O277B9nYUQ6wOwJBsr2ULJsUqmKHlCyTrJHuuVLGv7zuOUbKXkHzK4vsKL9QHkJD9Wsr2S0UoelnxIFheMhTExNsbIWG3fL0e8JuQflRys5DYln0l5wFgZ8wAlvxD799ERL4b8TMkBUvGx1ref09LhayVzlPSXyrXZvr+OeD7ppGSC1Dr9HQ1owolKOov9+93SxCNC7KPk8ej56pAgQOmr5O/F/jy0DPE2UXK0kjdjTFBHx1tSicxLZ4atDyCB/EQqhHsv5qS0Et5XMkwq98j2PHUY4v1QKtHpygQT0apYpWSolCBRbX0AdWRbqfgzDsnAPePe2Z6/0hHvl0qmN3DDHWoxQ8mvxP58Fp54P5CKWf2osfvsEICPlQyUyr21Pb+FJN5vlMxt+PY61MN8Jb8V+/NcKOL1lEp3h0O2wJLsI/bn2zrx6My4usmb6ZAck5X8VFqUeL8XF7HaxHIl/yotRrxuSj5M4+45NAVMbw9pEeKR4Pw2nfvmkAKYi8OlAxOPCsTY1G6XQ9q4SMmPpIMRj67aaWneJYdMQMI5lw7oPEhH5HpnuvfHIUPQeJp5s0HWpCNkn5f2nXHIHAsk41arLEnHU+NIV15Q6cgs15cV6fATZmdxNxxyBWY3E58vC9JRjHadJR0Ht0gG0W4WxBuZ1R1wsIZxUnDiDcnu2h0sg7ktJPG6i6tIdGQwt6mV19IiHQV/19bU8UFT6R+kIMQj5HZdJq0DulqaTrOkQTzXT9d6YLcrq8Trmf01OhQUvcUS8Vgj4fy61gW9fL+TnIlHktgtzHGgrNbQ6rVGiXdwPtflUAIMkpyIx2Jrt+7VwYAUy68lB+K5hk4HP2ggzZR4W+d3LQ4lw3aSEfFYM/FkjhfiUC48JQm6WJIQb2CeV+FQShwqKROPbuJVuV6CQxkBR2Kt14hLvOPzHb9DicGLY1IhHnsOr8558A7lBdvi1l0oFId4x+Q9cofSo67Wq0c69tJdkfuwHcoOOBO5SKge8XrnP2aHDgLesNQw8Z6wMGCHjgGagxsiXicbo3XoUOgiDRBvvJWhZohvv/1Wnnp8qdx7xyz5/LMyvdCxtJgkCYlHOJzrC+k2bNggK15/TT5bm/7Xfr1+vaxe9Y7Mv/tOObzvPjKo1+7y+CMPp/49Du3A0/1zSUC8fnmObsN338nVl1wke27dRS4869R4f6OI+uUXn8vTTyyV26fdIJMvHyeXn3+eXKFk4kVj5JpLL5bJl12sfz592BHSf7fussvmm0nXzf5Dy+5b/lnm3nlHxlfmIJX37sYmXq7bit0+/cYqKXrvuI289cbrkcd/sHq13HLdZDm8X29FoM5VMiUV/v6ZJ138lDHYuCkW8XizdW4vGf7w/dVy2P69aggx8uQT5Ksvv2x37DfffCMP3n+vHLLPHg2TzS+9tt/Smd1swULwf5YYxMu1rX3BPXO02YMEfXvsIEcftL90+9N/ahP5yUffNzp/+cUX2nT26PK/qZHOyMF77iwrV7yZ52W3GgZLDOLNynNE1024XBMNApx74rEy66brq4Q476TjZNXKt+Wbr7+WCReeH0ia/Xf6m5xzwjA56sD9pFun/2qYfPw9vuJdt94sr7z4fJ63oBXAlnWRxOONz7lGswQVZvJ777C11np7bfsXGbpvT/0Z/t8H770n/Xbp2o4s++20vTyxZLFOk2Cyl6n/Q+SD9ujRlAYkEHng3rvzvA2ZgcDtk4+tL5Ehut1YIoi3Xd4jevXFF9r5bDOmTpbXXnpRev1tS7npmony8Zo1ckjvPdsR5NSjhsrLzz8nzz+1XBPP4J23VsgFZ46oakD+PXHIQK3Vkvh+zz+9PJd7QLBEdE66Z+aN18mU8ZcpDT9Gxo8Zqf+deuXl+gF8aO59WhuvW7cu1nnffvMNGXvGCBmw1y5y/cQr9ANqETtKBPFG2RjR66+8rPNrRoutevstWTR/ruz61z/pYGL9+nU6AvUSY4+tusiBu+8kB+zaVfbaZnPtpxGUPLu8so0Lubvhimwce8xBfXXqZd6c2Yk038C9d9WBB1ojLXAurgdSQ6jTjj5MP3h7b/tXfb34sN3Vg4L7YYSfd978/2S3LTrp+3PswP4yadwFmliklYIAyS4576yqG9Oj8//I3LusbtJ6vkQQb1GeI1m8YJ7cecv0aoKXG8qNwtRhWk8/5nD56MMPtJ/nNbVD9t1bpk2+ShPphWeekuMGHVj9HROElmRCqFIQMeM3Uqk4sY2ISYTJHnfemfLis0/r5PZ3MUjImJnkJx9bIi8996wWxkLe8KKzT9cPCSRrNijqud0WOl/J9wUBa3Cbuvb5d9+lSciDuOaD99Oexrh4REKIx2sBvspzJKNPPbFqWtEE3BTMpzGPTB4TfdXFY/VTz+c7/fm/5c4Z3+90u/bTT3TimN8THe+zw1aavJwTEBljmsgNdm8i+EBjjDhiiDZZPDBoG0wk3482Jerm/++9s1IT1fsgZBGJe+Wo/vtpkkcBPxnXZfrkSfLF51bKhaTo4Fg74m2R90iWPvyQTgBDCCoP3BA0FU/nrn/ZTEYcOURXIUy6xQhmBpDrO/uEY7TZxdzOvGGq0iyPycmHH6KP8xKUchzaK42Jhkho4CP69dGmkuh79Ijh+v8De+6WKcnCBFPNw2bynzwULz37TPX6efggXt+dd9TJd0tgeWw74g3LexT4IePO/V47QDS0HIQhwvXeWDQgfh5mCi1C9IqmM7+HbKRdAP7Tvt2204TFzAECDm58nEkkqqZ8Z4NAzQolR4KtkScfr33HhffdIw8vmKsfDn6PiUdrW0K1M9lLvMk2RoLJ8qc/0GCYVO9naMXZM6Zpv6X/rt3aPe2cxwu0Jr+7e9at+mdMII55nMkbdcpwuXHShHaaNpY2VCbZOPRpCg8b94Xz1zsWrcYY0PBoYP6WzzHJ982+TbsEljBVAohnrWh5y3XXVm9OlKDV0JKkHrzBBhOy5MEHqufDzBw3qL/+nWkEeP+9d3UUHGeSMe847EG5wzDBNcDZp+YMYeMQpJ706bqt1uo8PI8telAHKETZBFXXXnGJjuij/p6UEIEaARb3gftiGeSnaojH2op4yaEMQFTL01hvIniKyc8BIl2CE6MZmWzSJlQ/aHsyOTxyY4CbzudxJpzJBof22St4QpXJhhRGs0E6PsNFQCPj6/k1dhLB1EOs1e+u0vVpQPD12aefas2+7NFHtEty5rFHRgZM3BPuq3E3CgB8IYoUVeL9m93xiA7542g9CDXpkgt1Lgyg6ZiAMJ/s0pFn6+OIbqkD1zs/E2m0pKme+IUUEBr01uun6CiawAYSkuQFXr81qZAqWr70UX0efFbSRTdcdaWcfNhgrcHQqAQ3SD1ycyz35qM1hXon9b+Lh3g7Wh6MfPrJxzK4V7yuE8hHjx0ThOajwH/9VeM1CfzHorXI4WGiiYbrnZvgZcVrr2oNE2aaDcFI9RDZms8x0YDcGX5nUtING3CATp4D8oZob9yIRkkMMXE5yJEWCDuJh3iDLA9GA+2U5MaiZTBxRLCQLsivYuKMBsFHqte/d9LQQTqt88oLz4WS5/zTTtJEfuPVl2s0LRqViBINgwYfc9rJeuI5pl4OkcrNuytX6nFistFuYcdyTXGJzXGPLFxgcVbbQb+oxRDvPMuD0aA7pNGnO0qMz4YvednocyOPvWzUOfpY8op+snRrS1KT6qGeSvrH/yDwEGDeqC+DRxct1FF4VHqGIAGiA2q13u9Fa3m7bqg+UE5EqMHGuf6LzzlDX3tBMFo8xJtsdywVELHhqKdNPIhCAhmgVcKCBgQzSSVi6H692v2Osfnzi34hX7Zo3v26vEbjAglufNew64LIJgDi+r2mlSrO/bNvrzZIHHnAvtq3BEsXL9JpkzjXjzYtQERroFMqhniFeMUn7Ttx/bykAgEMHn1oYaApI5ok24+W8q7PMIImuvKC0TpKhCxoaH9ukH4+QK6Mbph6JhZzTORKyW3Ywf2+H4sKWGj5Amcdf7T+bM6sW/TPVHyMP8uDgJa+4+abNOmDAg7ITAqqILhHPMSLLvTlBBz6sIbPRqVSw+2sNQ5E0d+zYYNMueLSdqSgoZSSk5noIEHrUKs19U4qBZhXaqBoKMa/7quvtJ8X5achkIcObEB6xFvTJf9GtQXw0PAZhAO0S/EzDQekV0zFhrGT8wtqiEVzFgSPiYd4L1seTBX05/G0p0E6fC5KREwiyV0IQg4MQBxSFOZYOleIkElm1+tkhiBMOlEtfYML779Hm2e0HCbWtCpB7kgT2K+39u043jRMGEGTcm5A+5Qm3uJK89Dxgw/SaRe+H7KxThifFL+TUlmQ1rt5yjWhLVQ5A5+nSrzCxNuYHZKnaRAPgmAy0ULk+/iMCTP+HiQ/5YhDdfCBqaOXD4LGOTfRqknuoomIaCEyCW7yfG+++oou8UU9FCcccrDWjpTzGJf395CHerROHCsyo1khGCae6J2AhcjZaGfduxfxwJBKSrOvsAmQVKwSz1rxLgg4wlQhmjGvTAz+D479vbfP1LksY1qZcH9fGprjjGFHxv4OlmEy8fhhQcEKUex+3bcL/Xv8LlOFoaXJ28Dg9S+pGeP7YlL95DTXSm0ZjUbXyfixowKDDhZPxeklzAFwrZjEAzj59WqRYUL6gEng7xG6VSCz14ci5eFdxUbuLGm/Hr5j3I4Xv+D/QRJAS7uJetGkBBEEMfiTPDj8y8MSpNE4j78Xj4fBTz7WthRE49G3VSVeIUHCF/8syYTS/k6lAg1AqgLzhxmlyO4/luoHpCQnFqWdshBIS5AAKNGZ5DflNgMaFXARGGfUuXi4WPppfEKAD+o9hvJegVBs4gHSANzYStdH59BuXnwiJpJCOk6+MdWYKnwktEbQ37GqrV70mYb4tRU/Qw6gI9o24lFpwDckKjamkd+HVVzw7Yxp5loos1Hb9S+gIgAqEIprar2glZzUA5OiW4485OP/lLloeDRphenXXq2JSE6QhGvSRT6NCgSgmuHN7TE2fEzTpuUVSAJo1PSuwcDsMvbhhw7QD0zYgwF58d3IK5IuMTsy+KNa2rtMQFUA1JjaQhOP5kUmlYQsNxlHG7+GTD/mxSz3w8SSUuBYfCPMKyBy9U+aXsnVxBqMIMEkmtVsNGF6l0iadIm//MWYOYY2Kz+paJAl5ULkit+GJvQntkn/GNBYEOQynDN8mHY3CoKa4KIw6ZQgYDrRGGg7FlpjTk1OihuKOUbL6dVbSlvQzoR5onWKIMOrASAkRMTnIapMo2HTaBmjdamLUh258eqJumkA8F2G8IZYjJe2JzpzvGt+u7WtQfEDzcn1oQlNvo77YhZsB60/5lwFW5xek04pTAI5DGg6UyaizET+CzCxJH8pmFM54CYz+SSITfu7EVIYLHUEEJf/B5XG4giEJdJEG5FOoUcQnwztjI8FCFqIJgGLgbx/r+u3yrzOmTlD//4Kz/oRzg3xcBMgJf9CZAIttCipIa6RyBwfj7XHgDwkZT/v92DqC7CTgBc1CeRClMzq4Z7bZrYlhMfUfI55w2SxhA9tg+kyyyS9GsmUzEjYknJpZtUZVRCSxCSRIYdJVZAi0Skc9UAQUZMaoSLCZ2g7cnFErpCG86C5INFzy5+s6WAxrgXdzHQRmwcEbW1gEtT4drQ+cS7vGKkAmUpNgVBTMitEk0A9MEG0NbGAm2oDRMRUUYGgVorPhPYLyq2dedxR+hw0jXqT02gIEsfG5KJRKFFpMxyxHhazH7TFBSYfkphmBwr3aB3+T+u9+RuIhgbm+6j9AlOTjRJvvfmis06r0aA1JlZpQppjC1Im86KmSWCy3bHEB4llc5P5lxRCvUU5aBJqomhD42tVHXzlL+EbsWicBdtm0RALwOs1W7LTgbfPjWqI//xewf8ye7zge5pIlgeBMbCVBxWRqO/kAaJ0xoMXtQoO7YclKCBq2qIK0QgaB2g9SJLELJLiAGgbf2oC/wxAIIIWA7bBiLO2Ft8OkK4geY0fFrYWAtMJ8YjKvQuP0E40FADWvEZ9n1m2GHUMGhbzX1DUNIIWovU9Lih1RbUu+YV9VgD+ln8SzeJwCv34bGxNi/+FuYxKuWCa8ddMsAJxiWIxnyR7IRIkwhf0khTTRxOB/7xEq/hj+Iz+zuYkgok3LfQFRU3ru/XFPklBpIaTXm9lGuQyHb74X3yGX0cUCVHMdhfd2vr2+BfNhM9I/RbxVwEgHS3qRNb4l6RmMJ3mXEw+wQ6mDnPs1USYY9JDkNDvQ2KK8UGJYoO2ZasnaPNlbc2jBUbNYh/ryxsbAZEkzrZXq/gFs2cWuxji4SdhGpGxp5+iP8NfMjk2CElTAZqVVV8kYGu0k/LNnln2uD4nFQMK8qRz8AkhLt9J5E391PQW8oDQgu7dWBxN7DebRK08VBwXdwG6Eb6vBPijFGVBd7NgFVlUy7zReCxL1BpLaRoqBZg3yEJQwZa2mG9ICUlI7vI5ay+CtCpt6mg7qiakS/DbOH/YmgzO699OjHqs2RfQK3wvxOO64i5CJ8gquIkF7RZ0I8usDqlJMElhe86ZJC4lNi+JMJmkZ5hk+vdM4pddlyBlvTYpNBLa0LQloX1JcqNFDaGMH4k/aDb4Jo/IsRA9rK2Kc+N74hLEWQBFw4FpTC0w2m1hUaqUShBYZBM2QUwia2BN927VJ1KTjq9FVYToFq3JMZjPJPummN0KWO/AohuaSjkPhMP/o7IAGYmqCS7w++LWidGgcfbX827JVmAEbtqT+zZlzQDHnhX/lJyIAuut04BIfl8QDYmJwqyatiNMLsFE3B07CVRYlQbQOKaUB9HNTlWAcpppczJ+ZRLx+qBBAuFLgMBtynLfmDEpyOGRcmBNBsVxzFpUrRXthvMeNWGYQr9GSbLNGASlroo2I9LFJ6R72GyQaHYIpcCvFwIp8vH7JKTDTNNpw4o0HpSgY+h+MfvJFBiBGzPmvhVtXDBZEI6Ij6RuPWKg/aiZ0sFMAIDpjNKIze5nB8HRSCZxjIk1/hy7R0F8hJorOcIkq+i4XtPeBWiDCjoOk+ztQC4gQreizX3z7bjAZMXZSQrBbBrTB/CpSBw32oXSqFDSgnRow0bPAVmpt3qBLxt2PIt+CrRVhR+hm28jVl43UA/cUPyzeiUsNArNkAZ0q5DbirPjkln4jc+GRkpjc0X/3/MdXAeBDDt11js/JtprPsnveXenCroGWv0LisjXDeT+gpV6QGPRf0Ypiz1JuLH4MxT3TSRKXxwE8+axMLNs+1XPjFFtICql9EUvH50i9NERlRIpkuQl8oUE9AGy2wB/Q2Effw4C0fKE70VgQn6Qn0koMzY6afgbtpngO0j74PeRwonKPbIjlbd2TELbtFJFCZE9m5AXEJEvWMn9lVKNgqoCSVsm0OzMZIATHrVqDHKwwBkSJH0xM5ErhIAIVDWIrOl8ISWDtqUPj5+JavHzGFtQfo3fQ9Sg8dHu5CUdDZ68AyToWPxTf9s8bgV14gKZ3bqvlEJyfYle2iB69HfheoX+ONtbs/LAhO1Oimb1tjOxEChs03DzigVIbHZ195pdEueWXyNlUPclekiurw1NEyyMDmsZQgtgpm0veqG8FkY6NB3NBQa8syNKc7M1h+l8Zr9ks24D0qFNeeuPyStaRqzXhub6ouS0wISGvdwE0tEmbnslPbXZ4SGvtSIJ7t2rGFcCPzaMdKYa4z8/q9GIpunHw9QWoAM59ouSkVxfDd8svovY3ozotAhdG0SnYd3JNI9635ELWcg9hpEOLYhLURLEfjU80s/SIBsCLePe6gNRJZEukSQTGPSa+bxBg4K/VR1NjJb2r/JnhZz39QtUaCjjEWDQfZ3X60xTwgBJQLyfSUmiW/D6yy/pdAb5OlItRJUF2RmpCva2o2OFZlAeCKJOlkKaXd69IF9HIpzF3DSkQlp8uIIECkmAw/pzSUA8ZHzgqQoITNPiB+Zr81ME7RYEggYeiDivc6ImDdloxS85JkkIv6KI18nKUB06ErpIA8RDrL3fzKH0oLE4lFv1iNfbwoAdOgYOkCaIx1qMFfmP2aHkIGL6sTRBPKRUnckOhUC10zhM4hBvEyXv+8/s4BACuBKYQpGExENOyHnwDuVFXW0nCYj3UyWr8h2/QwnBi9Z+IikSDxmY6yU4lBFs3heLT0mI9yMlT+Z5FQ6lAmsO4EjqxEO2ye86HEoGlk3E5lJS4iHT8rsWh5KA95km4lEjxPulkkLt5uxgFewA+WvJgXhIadvjHVIHm3om5lCjxPuBkrn5XJdDgTFfKlzIjXjIb5TUrit0aCXgbv1OGuRPM8RDemZ/fQ4FBZ1LDXOnWeIhV2d/jQ4FA+8gbYo3aRCPclqpdxN1SARWGjHn1omH/F6cv9cKIHXyB0mBM2kRD+kulcW7Dh0TzG0PSYkvaRIPGZLhhTvYhX4xSlqSNvGQkdldu4Ml8PLcVHmSBfFIKJZiC3KHWKAOG7vrJK5kQTyEhR6leBWpQyTmSJ1FO41KVsRD6ESdl8XdcMgFlMOaTpuESZbE26ht4I585cMCqeyfkxk3siYewhbzpdr2rMWBeY21bqIZyYN4CH6CayAtPmZIRj6dX/IiHvJDJWPTvEsOqYI3DaYevYZJnsQzMlRchaNIYC6OkJx5YIN4COU1V9u1D2qvqZXBkogt4iE0FrjlkvZAl0kqBf9GxCbxEKIn18+XP66VDHN0ccQ28YzQyexMb/bAtPYR+/NdGOIh/yJuAVGWoBLxW7E/z4UjHkKDAUsn3brd9ICWY9+bhlaDZSXWBxAiLBp3HS7Ng4Twr8T+fJaGeEa2FbeeoxFwz7h3tuevtMRDqHhgft9JePNbEexhSIKevattz1vpiWeE1MvRSlYnmIhWAdu/His5FPfTEusDaEA2abvJ7d/F1HrgHrD1a6YtTFmI9QE0IbxNnJzU4zEmqKMBH66vVO6B7XloOeJ5hddfTZDKS9s6Kri2q5R0Fvv32xHPJ5ic/kruUvJ1yASWCVzD3UoOkhKa0yixPoAM5RdSeVfqbVIuTchYGTNJ338S+/fREa8Joat2eyWjlTysZJ0UB4yFMTE2xphLB7BtsT4AS7Kxki2kEh2z8xHO+nrJHnwHrWBTpRKNbiWVNSm274cjnkUh6bqpkq5KBisZJRWC4C8uUcJbi+mgoY7sfXv52rbP1rQds6Ttb6a2nYN3P3RvO3dpo9C0xfoAnLSmWB+Ak9aU/wfdbe2+pqwkcQAAAABJRU5ErkJggg==";
                    break;
            }
            return _image;
        }

        /// <summary>
        /// Get thông tin mệnh cung phi
        /// </summary>
        /// <param name="yearAm"></param>
        /// <param name="gioiTinh"></param>
        /// <returns></returns>
        private static CungPhiModel GetCungPhi(int yearAm, string gioiTinh)
        {
            int n;
            CungPhiModel cungPhi = new CungPhiModel();
            if (gioiTinh.Equals("nam"))
            {
                if (yearAm < 2000)
                {
                    n = (100 - (yearAm - 1900)) % 9;
                }
                else
                {
                    n = (99 - (yearAm - 2000)) % 9;
                }
                switch (n)
                {
                    case 0:
                        cungPhi.Phi = "Ly";
                        cungPhi.Hanh = "Hỏa";
                        cungPhi.Huong = "Nam";
                        cungPhi.Mau = "Hồng (Hồng nhạt)";
                        cungPhi.CungPhi = "Ly Hỏa";
                        break;

                    case 1:
                        cungPhi.Phi = "Khảm";
                        cungPhi.Hanh = "Thủy";
                        cungPhi.Huong = "Bắc";
                        cungPhi.Mau = "Đen (Xanh lam nhạt)";
                        cungPhi.CungPhi = "Khảm Thủy";
                        break;

                    case 2:
                        cungPhi.Phi = "Khôn";
                        cungPhi.Hanh = "Thổ";
                        cungPhi.Huong = "Tây Nam";
                        cungPhi.Mau = "Vàng (Vàng marông)";
                        cungPhi.CungPhi = "Khôn Thổ";
                        break;

                    case 3:
                        cungPhi.Phi = "Chấn";
                        cungPhi.Hanh = "Mộc";
                        cungPhi.Huong = "Đông";
                        cungPhi.Mau = "Xanh (Xanh lục nhạt)";
                        cungPhi.CungPhi = "Chấn Mộc";
                        break;

                    case 4:
                        cungPhi.Phi = "Tốn";
                        cungPhi.Hanh = "Mộc";
                        cungPhi.Huong = "Đông Nam";
                        cungPhi.Mau = "Xanh (Xanh lục nhạt)";
                        cungPhi.CungPhi = "Tốn Mộc";
                        break;

                    case 5:
                        cungPhi.Phi = "Khôn";
                        cungPhi.Hanh = "Thổ";
                        cungPhi.Huong = "Tây Nam";
                        cungPhi.Mau = "Màu Vàng (Vàng marông)";
                        cungPhi.CungPhi = "Khôn Thổ";
                        break;

                    case 6:
                        cungPhi.Phi = "Càn";
                        cungPhi.Hanh = "Kim";
                        cungPhi.Huong = "Tây Bắc";
                        cungPhi.Mau = "Trắng (Trắng sữa)";
                        cungPhi.CungPhi = "Càn Kim";
                        break;

                    case 7:
                        cungPhi.Phi = "Đoài";
                        cungPhi.Hanh = "Kim";
                        cungPhi.Huong = "Tây";
                        cungPhi.Mau = "Trắng (Trắng sữa)";
                        cungPhi.CungPhi = "Đoài Kim";
                        break;

                    case 8:
                        cungPhi.Phi = "Cấn";
                        cungPhi.Hanh = "Thổ";
                        cungPhi.Huong = "Đông Bắc";
                        cungPhi.Mau = "Màu Vàng (Vàng marông)";
                        cungPhi.CungPhi = "Cấn Thổ";
                        break;
                }
            }
            else
            {
                if (yearAm < 2000)
                {
                    n = (yearAm - 1904) % 9;
                }
                else
                {
                    n = (yearAm - 1903) % 9;
                }
                switch (n)
                {
                    case 0:
                        cungPhi.Phi = "Ly";
                        cungPhi.Hanh = "Hỏa";
                        cungPhi.Huong = "Nam";
                        cungPhi.Mau = "Hồng (Hồng nhạt)";
                        cungPhi.CungPhi = "Ly Hỏa";
                        break;

                    case 1:
                        cungPhi.Phi = "Khảm";
                        cungPhi.Hanh = "Thủy";
                        cungPhi.Huong = "Bắc";
                        cungPhi.Mau = "Đen (Xanh lam nhạt)";
                        cungPhi.CungPhi = "Khảm Thủy";
                        break;

                    case 2:
                        cungPhi.Phi = "Khôn";
                        cungPhi.Hanh = "Thổ";
                        cungPhi.Huong = "Tây Nam";
                        cungPhi.Mau = "Vàng (Vàng marông)";
                        cungPhi.CungPhi = "Khôn Thổ";
                        break;

                    case 3:
                        cungPhi.Phi = "Chấn";
                        cungPhi.Hanh = "Mộc";
                        cungPhi.Huong = "Đông";
                        cungPhi.Mau = "Xanh (Xanh lục nhạt)";
                        cungPhi.CungPhi = "Chấn Mộc";
                        break;

                    case 4:
                        cungPhi.Phi = "Tốn";
                        cungPhi.Hanh = "Mộc";
                        cungPhi.Huong = "Đông Nam";
                        cungPhi.Mau = "Xanh (Xanh lục nhạt)";
                        cungPhi.CungPhi = "Tốn Mộc";
                        break;

                    case 5:
                        cungPhi.Phi = "Cấn";
                        cungPhi.Hanh = "Thổ";
                        cungPhi.Huong = "Tây Nam";
                        cungPhi.Mau = "Màu Vàng (Vàng marông)";
                        cungPhi.CungPhi = "Cấn Thổ";
                        break;

                    case 6:
                        cungPhi.Phi = "Càn";
                        cungPhi.Hanh = "Kim";
                        cungPhi.Huong = "Tây Bắc";
                        cungPhi.Mau = "Trắng (Trắng sữa)";
                        cungPhi.CungPhi = "Càn Kim";
                        break;

                    case 7:
                        cungPhi.Phi = "Đoài";
                        cungPhi.Hanh = "Kim";
                        cungPhi.Huong = "Tây";
                        cungPhi.Mau = "Trắng (Trắng sữa)";
                        cungPhi.CungPhi = "Đoài Kim";
                        break;

                    case 8:
                        cungPhi.Phi = "Cấn";
                        cungPhi.Hanh = "Thổ";
                        cungPhi.Huong = "Đông Bắc";
                        cungPhi.Mau = "Màu Vàng (Vàng marông)";
                        cungPhi.CungPhi = "Cấn Thổ";
                        break;
                }
            }
            return cungPhi;
        }

        /// <summary>
        /// Get thông tin mệnh cung sinh
        /// </summary>
        /// <param name="yearAm"></param>
        /// <returns></returns>
        private static CungSinhModel GetCungSinh(int yearAm)
        {
            if (CheckYear(yearAm, "1930,1931,1990,1991"))
            {
                return new CungSinhModel()
                {
                    Menh = "Thổ",
                    TrungHoa = "Lộ Bàng Thổ",
                    VietNam = "Đất bên đường"
                };
            }
            if (CheckYear(yearAm, "1932,1933,1992,1993"))
            {
                return new CungSinhModel()
                {
                    Menh = "Kim",
                    TrungHoa = "Kiếm Phong Kim",
                    VietNam = "Vàng chuôi kiếm"
                };
            }
            if (CheckYear(yearAm, "1934,1935,1994,1995"))
            {
                return new CungSinhModel()
                {
                    Menh = "Hỏa",
                    TrungHoa = "Sơn Đầu Hỏa",
                    VietNam = "Lửa trên núi"
                };
            }
            if (CheckYear(yearAm, "1936,1937,1996,1997"))
            {
                return new CungSinhModel()
                {
                    Menh = "Thủy",
                    TrungHoa = "Giản Hạ Thủy",
                    VietNam = "Nước khe suối"
                };
            }
            if (CheckYear(yearAm, "1938,1939,1998,1999"))
            {
                return new CungSinhModel()
                {
                    Menh = "Thổ",
                    TrungHoa = "Thành Đầu Thổ",
                    VietNam = "Đất đắp thành"
                };
            }
            if (CheckYear(yearAm, "1940,1941,2000,2001"))
            {
                return new CungSinhModel()
                {
                    Menh = "Kim",
                    TrungHoa = "Bạch Lạp Kim",
                    VietNam = "Vàng sáp ong"
                };
            }
            if (CheckYear(yearAm, "1942,1943,2002,2003"))
            {
                return new CungSinhModel()
                {
                    Menh = "Mộc",
                    TrungHoa = "Dương Liễu Mộc",
                    VietNam = "Gỗ cây dương"
                };
            }
            if (CheckYear(yearAm, "1944,1945,2004,2005"))
            {
                return new CungSinhModel()
                {
                    Menh = "Thủy",
                    TrungHoa = "Tuyền Trung Thủy",
                    VietNam = "Nước trong suối"
                };
            }
            if (CheckYear(yearAm, "1946,1947,2006,2007"))
            {
                return new CungSinhModel()
                {
                    Menh = "Thổ",
                    TrungHoa = "Ốc Thượng Thổ",
                    VietNam = "Đất nóc nhà"
                };
            }
            if (CheckYear(yearAm, "1948,1949,2008,2009"))
            {
                return new CungSinhModel()
                {
                    Menh = "Hỏa",
                    TrungHoa = "Thích Lịch Hỏa",
                    VietNam = "Lửa sấm sét"
                };
            }
            if (CheckYear(yearAm, "1950,1951,2010,2011"))
            {
                return new CungSinhModel()
                {
                    Menh = "Mộc",
                    TrungHoa = "Tùng Bách Mộc",
                    VietNam = "Gỗ tùng bách"
                };
            }
            if (CheckYear(yearAm, "1952,1953,2012,2013"))
            {
                return new CungSinhModel()
                {
                    Menh = "Thủy",
                    TrungHoa = "Trường Lưu Thủy",
                    VietNam = "Nước chảy mạnh"
                };
            }
            if (CheckYear(yearAm, "1954,1955,2014,2015"))
            {
                return new CungSinhModel()
                {
                    Menh = "Kim",
                    TrungHoa = "Sa Trung Kim",
                    VietNam = "Vàng trong cát"
                };
            }
            if (CheckYear(yearAm, "1956,1957,2016,2017"))
            {
                return new CungSinhModel()
                {
                    Menh = "Hỏa",
                    TrungHoa = "Sơn Hạ Hỏa",
                    VietNam = "Lửa trên núi"
                };
            }
            if (CheckYear(yearAm, "1958,1959,2018,2019"))
            {
                return new CungSinhModel()
                {
                    Menh = "Mộc",
                    TrungHoa = "Bình Địa Mộc",
                    VietNam = "Gỗ đồng bằng"
                };
            }
            if (CheckYear(yearAm, "1960,1961,2020,2021"))
            {
                return new CungSinhModel()
                {
                    Menh = "Thổ",
                    TrungHoa = "Bích Thượng Thổ",
                    VietNam = "Đất tò vò"
                };
            }
            if (CheckYear(yearAm, "1962,1963,2022,2023"))
            {
                return new CungSinhModel()
                {
                    Menh = "Kim",
                    TrungHoa = "Kim Bạch Kim",
                    VietNam = "Vàng pha bạc"
                };
            }
            if (CheckYear(yearAm, "1964,1965,2024,2025"))
            {
                return new CungSinhModel()
                {
                    Menh = "Hỏa",
                    TrungHoa = "Phú Đăng Hỏa",
                    VietNam = "Lửa đèn to"
                };
            }
            if (CheckYear(yearAm, "1966,1967,2026,2027"))
            {
                return new CungSinhModel()
                {
                    Menh = "Thủy",
                    TrungHoa = "Thiên Hà Thủy",
                    VietNam = "Nước trên trời"
                };
            }
            if (CheckYear(yearAm, "1968,1969,2028,2029"))
            {
                return new CungSinhModel()
                {
                    Menh = "Thổ",
                    TrungHoa = "Đại Trạch Thổ",
                    VietNam = "Đất nền nhà"
                };
            }
            if (CheckYear(yearAm, "1970,1971,2030,2031"))
            {
                return new CungSinhModel()
                {
                    Menh = "Kim",
                    TrungHoa = "Thoa Xuyến Kim",
                    VietNam = "Vàng trang sức"
                };
            }
            if (CheckYear(yearAm, "1972,1973,2032,2033"))
            {
                return new CungSinhModel()
                {
                    Menh = "Mộc",
                    TrungHoa = "Tang Đố Mộc",
                    VietNam = "Gỗ cây dâu"
                };
            }
            if (CheckYear(yearAm, "1974,1975,2034,2035"))
            {
                return new CungSinhModel()
                {
                    Menh = "Thủy",
                    TrungHoa = "Đại Khe Thủy",
                    VietNam = "Nước khe lớn"
                };
            }
            if (CheckYear(yearAm, "1976,1977,2036,2037"))
            {
                return new CungSinhModel()
                {
                    Menh = "Thổ",
                    TrungHoa = "Sa Trung Thổ",
                    VietNam = "Đất pha cát"
                };
            }
            if (CheckYear(yearAm, "1978,1979,2038,2039"))
            {
                return new CungSinhModel()
                {
                    Menh = "Hỏa",
                    TrungHoa = "Thiên Thượng Hỏa",
                    VietNam = "Lửa trên trời"
                };
            }
            if (CheckYear(yearAm, "1980,1981,2040,2041"))
            {
                return new CungSinhModel()
                {
                    Menh = "Mộc",
                    TrungHoa = "Thạch Lựu Mộc",
                    VietNam = "Gỗ cây lựu đá"
                };
            }
            if (CheckYear(yearAm, "1982,1983,2042,2043"))
            {
                return new CungSinhModel()
                {
                    Menh = "Thủy",
                    TrungHoa = "Đại Hải Thủy",
                    VietNam = "Nước biển lớn"
                };
            }
            if (CheckYear(yearAm, "1984,1985,2044,2045"))
            {
                return new CungSinhModel()
                {
                    Menh = "Kim",
                    TrungHoa = "Hải Trung Kim",
                    VietNam = "Vàng trong biển"
                };
            }
            if (CheckYear(yearAm, "1986,1987,2046,2047"))
            {
                return new CungSinhModel()
                {
                    Menh = "Hỏa",
                    TrungHoa = "Lư Trung Hỏa",
                    VietNam = "Lửa trong lò"
                };
            }
            if (CheckYear(yearAm, "1988,1989,2048,2049"))
            {
                return new CungSinhModel()
                {
                    Menh = "Mộc",
                    TrungHoa = "Đại Lâm Mộc",
                    VietNam = "Gỗ rừng già"
                };
            }
            return new CungSinhModel()
            {
                Menh = "Không xác định",
                TrungHoa = "Không xác định",
                VietNam = "Không xác định"
            };
        }

        /// <summary>
        /// Get thông tin con giáp theo năm sinh âm
        /// </summary>
        /// <param name="yearAm"></param>
        /// <returns></returns>
        private static ConGiapModel GetConGiap(int yearAm)
        {
            if (CheckYear(yearAm, "1930,1990"))
            {
                return new ConGiapModel()
                {
                    Ten = "Ngựa",
                    TrungHoa = "Thất Lý Chi Mã",
                    VietNam = "Ngựa trong nhà"
                };
            }
            if (CheckYear(yearAm, "1931,1991"))
            {
                return new ConGiapModel()
                {
                    Ten = "Dê",
                    TrungHoa = "Đắc Lộc Chi Dương",
                    VietNam = "Dê có lộc"
                };
            }
            if (CheckYear(yearAm, "1932,1992"))
            {
                return new ConGiapModel()
                {
                    Ten = "Khỉ",
                    TrungHoa = "Thanh Tú Chi Hầu",
                    VietNam = "Khỉ thanh tú"
                };
            }
            if (CheckYear(yearAm, "1933,1993"))
            {
                return new ConGiapModel()
                {
                    Ten = "Gà",
                    TrungHoa = "Lâu Túc Kê",
                    VietNam = "Gà nhà gác"
                };
            }
            if (CheckYear(yearAm, "1934,1994"))
            {
                return new ConGiapModel()
                {
                    Ten = "Chó",
                    TrungHoa = "Thủ Thân Chi Cẩu",
                    VietNam = "Chó giữ mình"
                };
            }
            if (CheckYear(yearAm, "1935,1995"))
            {
                return new ConGiapModel()
                {
                    Ten = "Lợn",
                    TrungHoa = "Quá Vãng Chi Trư",
                    VietNam = "Lợn hay đi"
                };
            }
            if (CheckYear(yearAm, "1936,1996"))
            {
                return new ConGiapModel()
                {
                    Ten = "Chuột",
                    TrungHoa = "Điền Nội Chi Thử",
                    VietNam = "Chuột trong ruộng"
                };
            }
            if (CheckYear(yearAm, "1937,1997"))
            {
                return new ConGiapModel()
                {
                    Ten = "Trâu",
                    TrungHoa = "Hồ Nội Chi Ngưu",
                    VietNam = "Trâu trong hồ nước"
                };
            }
            if (CheckYear(yearAm, "1938,1998"))
            {
                return new ConGiapModel()
                {
                    Ten = "Hổ",
                    TrungHoa = "Quá Sơn Chi Hổ",
                    VietNam = "Hổ qua rừng"
                };
            }
            if (CheckYear(yearAm, "1939,1999"))
            {
                return new ConGiapModel()
                {
                    Ten = "Mèo",
                    TrungHoa = "Sơn Lâm Chi Thố",
                    VietNam = "Thỏ ở rừng"
                };
            }
            if (CheckYear(yearAm, "1940,2000"))
            {
                return new ConGiapModel()
                {
                    Ten = "Rồng",
                    TrungHoa = "Thứ Tính Chi Long",
                    VietNam = "Rồng khoan dung"
                };
            }
            if (CheckYear(yearAm, "1941,2001"))
            {
                return new ConGiapModel()
                {
                    Ten = "Rắn",
                    TrungHoa = "Đông Tàng Chi Xà",
                    VietNam = "Rắn ngủ đông"
                };
            }
            if (CheckYear(yearAm, "1942,2002"))
            {
                return new ConGiapModel()
                {
                    Ten = "Ngựa",
                    TrungHoa = "Quân Trung Chi Mã",
                    VietNam = "Ngựa chiến"
                };
            }
            if (CheckYear(yearAm, "1943,2003"))
            {
                return new ConGiapModel()
                {
                    Ten = "Dê",
                    TrungHoa = "Quần Nội Chi Dương",
                    VietNam = "Dê trong đàn"
                };
            }
            if (CheckYear(yearAm, "1944,2004"))
            {
                return new ConGiapModel()
                {
                    Ten = "Khỉ",
                    TrungHoa = "Quá Thụ Chi Hầu",
                    VietNam = "Khỉ leo cây"
                };
            }
            if (CheckYear(yearAm, "1945,2005"))
            {
                return new ConGiapModel()
                {
                    Ten = "Gà",
                    TrungHoa = "Xướng Ngọ Chi Kê",
                    VietNam = "Gà gáy trưa"
                };
            }
            if (CheckYear(yearAm, "1946,2006"))
            {
                return new ConGiapModel()
                {
                    Ten = "Chó",
                    TrungHoa = "Tự Miên Chi Cẩu",
                    VietNam = "Chó đang ngủ"
                };
            }
            if (CheckYear(yearAm, "1947,2007"))
            {
                return new ConGiapModel()
                {
                    Ten = "Lợn",
                    TrungHoa = "Quá Sơn Chi Trư",
                    VietNam = "Lợn qua núi"
                };
            }
            if (CheckYear(yearAm, "1948,2008"))
            {
                return new ConGiapModel()
                {
                    Ten = "Chuột",
                    TrungHoa = "Thương Nội Chi Trư",
                    VietNam = "Chuột trong kho"
                };
            }
            if (CheckYear(yearAm, "1949,2009"))
            {
                return new ConGiapModel()
                {
                    Ten = "Trâu",
                    TrungHoa = "Lâm Nội Chi Ngưu",
                    VietNam = "Trâu trong chuồng"
                };
            }
            if (CheckYear(yearAm, "1950,2010"))
            {
                return new ConGiapModel()
                {
                    Ten = "Hổ",
                    TrungHoa = "Xuất Sơn Chi Hổ",
                    VietNam = "Hổ xuống núi"
                };
            }
            if (CheckYear(yearAm, "1951,2011"))
            {
                return new ConGiapModel()
                {
                    Ten = "Mèo",
                    TrungHoa = "Ẩn Huyệt Chi Thố",
                    VietNam = "Thỏ trong hang"
                };
            }
            if (CheckYear(yearAm, "1952,2012"))
            {
                return new ConGiapModel()
                {
                    Ten = "Rồng",
                    TrungHoa = "Hành Vũ Chi Long",
                    VietNam = "Rồng phun mưa"
                };
            }
            if (CheckYear(yearAm, "1953,2013"))
            {
                return new ConGiapModel()
                {
                    Ten = "Rắn",
                    TrungHoa = "Thảo Trung Chi Xà",
                    VietNam = "Rắn trong cỏ"
                };
            }
            if (CheckYear(yearAm, "1954,2014"))
            {
                return new ConGiapModel()
                {
                    Ten = "Ngựa",
                    TrungHoa = "Vân Trung Chi Mã",
                    VietNam = "Ngựa trong mây"
                };
            }
            if (CheckYear(yearAm, "1955,2015"))
            {
                return new ConGiapModel()
                {
                    Ten = "Dê",
                    TrungHoa = "Kính Trọng Chi Dương",
                    VietNam = "Dê được quý mến"
                };
            }
            if (CheckYear(yearAm, "1956,2016"))
            {
                return new ConGiapModel()
                {
                    Ten = "Khỉ",
                    TrungHoa = "Sơn Thượng Chi Hầu",
                    VietNam = "Khỉ trên núi"
                };
            }
            if (CheckYear(yearAm, "1957,2017"))
            {
                return new ConGiapModel()
                {
                    Ten = "Gà",
                    TrungHoa = "Độc Lập Chi Kê",
                    VietNam = "Gà độc thân"
                };
            }
            if (CheckYear(yearAm, "1958,2018"))
            {
                return new ConGiapModel()
                {
                    Ten = "Chó",
                    TrungHoa = "Tiến Sơn Chi Cẩu",
                    VietNam = "Chó vào núi"
                };
            }
            if (CheckYear(yearAm, "1959,2019"))
            {
                return new ConGiapModel()
                {
                    Ten = "Lợn",
                    TrungHoa = "Đạo Viện Chi Trư",
                    VietNam = "Lợn trong tu viện"
                };
            }
            if (CheckYear(yearAm, "1960,2020"))
            {
                return new ConGiapModel()
                {
                    Ten = "Chuột",
                    TrungHoa = "Lương Thượng Chi Thử",
                    VietNam = "Chuột trên xà"
                };
            }
            if (CheckYear(yearAm, "1961,2021"))
            {
                return new ConGiapModel()
                {
                    Ten = "Trâu",
                    TrungHoa = "Lộ Đồ Chi Ngưu",
                    VietNam = "Trâu trên đường"
                };
            }
            if (CheckYear(yearAm, "1962,2022"))
            {
                return new ConGiapModel()
                {
                    Ten = "Hổ",
                    TrungHoa = "Quá Lâm Chi Hổ",
                    VietNam = "Hổ qua rừng"
                };
            }
            if (CheckYear(yearAm, "1963,2023"))
            {
                return new ConGiapModel()
                {
                    Ten = "Mèo",
                    TrungHoa = "Quá Lâm Chi Thố",
                    VietNam = "Thỏ qua rừng"
                };
            }
            if (CheckYear(yearAm, "1964,2024"))
            {
                return new ConGiapModel()
                {
                    Ten = "Rồng",
                    TrungHoa = "Phục Đầm Chi Lâm",
                    VietNam = "Rồng ẩn ở đầm"
                };
            }
            if (CheckYear(yearAm, "1965,2025"))
            {
                return new ConGiapModel()
                {
                    Ten = "Rắn",
                    TrungHoa = "Xuất Huyệt Chi Xà",
                    VietNam = "Rắn rời hang"
                };
            }
            if (CheckYear(yearAm, "1966,2026"))
            {
                return new ConGiapModel()
                {
                    Ten = "Ngựa",
                    TrungHoa = "Hành Lộ Chi Mã",
                    VietNam = "Ngựa chạy trên đường"
                };
            }
            if (CheckYear(yearAm, "1967,2027"))
            {
                return new ConGiapModel()
                {
                    Ten = "Dê",
                    TrungHoa = "Thất Quần Chi Dương",
                    VietNam = "Dê lạc đàn"
                };
            }
            if (CheckYear(yearAm, "1968,2028"))
            {
                return new ConGiapModel()
                {
                    Ten = "Khỉ",
                    TrungHoa = "Độc Lập Chi Hầu",
                    VietNam = "Khỉ độc thân"
                };
            }
            if (CheckYear(yearAm, "1969,2029"))
            {
                return new ConGiapModel()
                {
                    Ten = "Gà",
                    TrungHoa = "Báo Hiệu Chi Kê",
                    VietNam = "Gà gáy"
                };
            }
            if (CheckYear(yearAm, "1970,2030"))
            {
                return new ConGiapModel()
                {
                    Ten = "Chó",
                    TrungHoa = "Tự Quan Chi Cẩu",
                    VietNam = "Chó nhà chùa"
                };
            }
            if (CheckYear(yearAm, "1971,2031"))
            {
                return new ConGiapModel()
                {
                    Ten = "Lợn",
                    TrungHoa = "Khuyên Dưỡng Chi Trư",
                    VietNam = "Lợn nuôi nhốt"
                };
            }
            if (CheckYear(yearAm, "1972,2032"))
            {
                return new ConGiapModel()
                {
                    Ten = "Chuột",
                    TrungHoa = "Sơn Thượng Chi Thử",
                    VietNam = "Chuột trên núi"
                };
            }
            if (CheckYear(yearAm, "1973,2033"))
            {
                return new ConGiapModel()
                {
                    Ten = "Trâu",
                    TrungHoa = "Lan Ngoại Chi Ngưu",
                    VietNam = "Trâu ngoài chuồng"
                };
            }
            if (CheckYear(yearAm, "1974,2034"))
            {
                return new ConGiapModel()
                {
                    Ten = "Hổ",
                    TrungHoa = "Lập Định Chi Hổ",
                    VietNam = "Hổ tự lập"
                };
            }
            if (CheckYear(yearAm, "1975,2035"))
            {
                return new ConGiapModel()
                {
                    Ten = "Mèo",
                    TrungHoa = "Đắc Đạo Chi Thố",
                    VietNam = "Thỏ đắc đạo"
                };
            }
            if (CheckYear(yearAm, "1976,2036"))
            {
                return new ConGiapModel()
                {
                    Ten = "Rồng",
                    TrungHoa = "Thiên Thượng Chi Long",
                    VietNam = "Rồng trên trời"
                };
            }
            if (CheckYear(yearAm, "1977,2037"))
            {
                return new ConGiapModel()
                {
                    Ten = "Rắn",
                    TrungHoa = "Đầm Nội Chi Xà",
                    VietNam = "Rắn trong đầm"
                };
            }
            if (CheckYear(yearAm, "1978,2038"))
            {
                return new ConGiapModel()
                {
                    Ten = "Ngựa",
                    TrungHoa = "Cứu Nội Chi Mã",
                    VietNam = "Ngựa trong chuồng"
                };
            }
            if (CheckYear(yearAm, "1979,2039"))
            {
                return new ConGiapModel()
                {
                    Ten = "Dê",
                    TrungHoa = "Thảo Dã Chi Dương",
                    VietNam = "Dê đồng cỏ"
                };
            }
            if (CheckYear(yearAm, "1980,2040"))
            {
                return new ConGiapModel()
                {
                    Ten = "Khỉ",
                    TrungHoa = "Thực Quả Chi Hầu",
                    VietNam = "Khỉ ăn hoa quả"
                };
            }
            if (CheckYear(yearAm, "1981,2041"))
            {
                return new ConGiapModel()
                {
                    Ten = "Gà",
                    TrungHoa = "Long Tàng Chi Kê",
                    VietNam = "Gà trong lồng"
                };
            }
            if (CheckYear(yearAm, "1982,2042"))
            {
                return new ConGiapModel()
                {
                    Ten = "Chó",
                    TrungHoa = "Cố Gia Chi Khuyển",
                    VietNam = "Chó về nhà"
                };
            }
            if (CheckYear(yearAm, "1983,2043"))
            {
                return new ConGiapModel()
                {
                    Ten = "Lợn",
                    TrungHoa = "Lâm Hạ Chi Trư",
                    VietNam = "Lợn trong rừng"
                };
            }
            if (CheckYear(yearAm, "1984,2044"))
            {
                return new ConGiapModel()
                {
                    Ten = "Chuột",
                    TrungHoa = "Ốc Thượng Chi Thử",
                    VietNam = "Chuột ở nóc nhà"
                };
            }
            if (CheckYear(yearAm, "1985,2045"))
            {
                return new ConGiapModel()
                {
                    Ten = "Trâu",
                    TrungHoa = "Hải Nội Chi Ngưu",
                    VietNam = "Trâu trong biển"
                };
            }
            if (CheckYear(yearAm, "1986,2046"))
            {
                return new ConGiapModel()
                {
                    Ten = "Hổ",
                    TrungHoa = "Sơn Lâm Chi Hổ",
                    VietNam = "Hổ trong rừng"
                };
            }
            if (CheckYear(yearAm, "1987,2047"))
            {
                return new ConGiapModel()
                {
                    Ten = "Mèo",
                    TrungHoa = "Vọng Nguyệt Chi Thố",
                    VietNam = "Thỏ ngắm trăng"
                };
            }
            if (CheckYear(yearAm, "1988,2048"))
            {
                return new ConGiapModel()
                {
                    Ten = "Rồng",
                    TrungHoa = "Thanh Ôn Chi Long",
                    VietNam = "Rồng trong sạch, ôn hoà"
                };
            }
            if (CheckYear(yearAm, "1989,2049"))
            {
                return new ConGiapModel()
                {
                    Ten = "Rắn",
                    TrungHoa = "Phúc Khí Chi Xà",
                    VietNam = "Rắn có phúc"
                };
            }
            return new ConGiapModel()
            {
                Ten = "Không xác định",
                TrungHoa = "Không xác định",
                VietNam = "Không xác định"
            };
        }

        /// <summary>
        /// Check năm truyền vào có nằm trong list không
        /// </summary>
        /// <param name="year"></param>
        /// <param name="listCheck"></param>
        /// <returns></returns>
        private static bool CheckYear(int year, string listCheck)
        {
            string[] list = listCheck.Split(',');
            foreach (string item in list)
            {
                if (year.ToString().Equals(item))
                {
                    return true;
                }
            }
            return false;
        }
    }
}