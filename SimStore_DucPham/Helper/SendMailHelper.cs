﻿using SimStore_DucPham.Models;
using SimStore_DucPham.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Mail;
using System.Text;

namespace SimStore_DucPham.Helper
{
    public class SendMailHelper
    {
        /// <summary>
        /// Gửi mail cho khách hàng
        /// </summary>
        /// <returns></returns>
        public static string SendMailActiveBill(SendMailModel sendMail)
        {
            string code = sendMail.Bill.Code;
            MailMessage mail = CreateMail(sendMail.CustomerMail, sendMail.MailSubject, EditBodyMail(sendMail.User, sendMail.AddressOrder, sendMail.Bill, sendMail.Products, code));
            SmtpClient smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                UseDefaultCredentials = true,
                Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MailSendId"], ConfigurationManager.AppSettings["MailSendPassword"]),// tài khoản Gmail của bạn
                EnableSsl = true
            };
            smtp.Send(mail);

            return code;
        }

        /// <summary>
        /// Gửi mail kích hoạt tài khoản
        /// </summary>
        /// <param name="sendMail"></param>
        /// <returns></returns>
        public static ResultModel SendMailActiveAccount(SendMailActiveAccountModel sendMail)
        {
            try
            {
                MailMessage mail = CreateMail(sendMail.Email, ConfigurationManager.AppSettings["SiteName"] + " | Email bắt buộc để xác nhận đăng ký thành viên : " + sendMail.Email + " - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), EditBodyMailActiveAccount(sendMail.UserId, sendMail.Email));
                SmtpClient smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    UseDefaultCredentials = true,
                    Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MailSendId"], ConfigurationManager.AppSettings["MailSendPassword"]),// tài khoản Gmail của bạn
                    EnableSsl = true
                };
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                return new ResultModel()
                {
                    Status = false,
                    Message = "Không gửi được mail kích hoạt: " + ex.Message
                };
            }
            return new ResultModel()
            {
                Status = true,
                Message = "Gửi mail kích hoạt thành công"
            };
        }

        /// <summary>
        /// Tạo mail để gửi
        /// </summary>
        /// <param name="mailCus">Mail của khách hàng cần gửi đến</param>
        /// <param name="mailYour">Mail của bạn</param>
        /// <param name="mailSubject">Tiêu đề mail</param>
        /// <param name="mailBody">Nội dung mail</param>
        /// <returns></returns>
        private static MailMessage CreateMail(string mailCus, string mailSubject, string mailBody)
        {
            MailMessage mail = new MailMessage();
            mail.To.Add(mailCus);
            mail.From = new MailAddress(ConfigurationManager.AppSettings["MailSendId"], ConfigurationManager.AppSettings["SiteName"]);
            mail.Subject = mailSubject;
            mail.Body = mailBody.ToString();// phần thân của mail ở trên
            mail.IsBodyHtml = true;

            return mail;
        }

        /// <summary>
        /// Chỉnh sửa phần thân email phù hợp kích hoạt tài khoản
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        private static string EditBodyMailActiveAccount(string userId, string email)
        {
            StringBuilder Body = new StringBuilder();
            Body.Append("<p>Chào " + email + ",</p>");
            Body.Append("<br>");
            Body.Append("<p>Cảm ơn bạn đối với việc đăng ký tài khoản tại " + ConfigurationManager.AppSettings["SiteName"] + ". Trước khi chúng tôi có thể kích hoạt tài khoản của bạn một bước cuối cùng phải được thực hiện để hoàn thành quá trình đăng ký của bạn.</p>");
            Body.Append("<br>");
            Body.Append("<p>Vui lòng lưu ý - bạn phải hoàn thành bước cuối cùng này để trở thành một thành viên đã được đăng ký chính thức. Bạn chỉ cần ghé thăm URL này một lần duy nhất để kích hoạt tài khoản của bạn.</p>");
            Body.Append("<br>");
            Body.Append("<p>Để hoàn thành quá trình đăng ký của bạn, vui lòng ghé thăm URL này:</p>");
            Body.Append("<a href='" + ConfigurationManager.AppSettings["Url"] + "/kich-hoat-tai-khoan?UserId=" + userId + "&Email=" + email + "' >" + ConfigurationManager.AppSettings["Url"] + "/kich-hoat-tai-khoan?UserId=" + userId + "&Email=" + email + "</a>");
            Body.Append("<br>");
            Body.Append("<p>-----------------------------</p>");
            Body.Append("<br>");
            Body.Append("<p>Nếu bạn không đăng ký tài khoản bên chúng tôi. Hãy ấn vào đường link bên dưới. Xin lỗi vì sự phiền toái này đến bạn</p>");
            Body.Append("<a href='" + ConfigurationManager.AppSettings["Url"] + "/huy-kich-hoat-tai-khoan?UserId=" + userId + "&Email=" + email + "' >" + ConfigurationManager.AppSettings["Url"] + "/huy-kich-hoat-tai-khoan?UserId=" + userId + "&Email=" + email + "</a>");
            return Body.ToString();
        }

        /// <summary>
        /// Chỉnh sửa phần thân email phù hợp xác nhận đơn hàng
        /// </summary>
        /// <param name="user"></param>
        /// <param name="addressOrder"></param>
        /// <param name="bill"></param>
        /// <param name="products"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private static string EditBodyMail(ApplicationUser user, AddressOrderViewModel addressOrder, BillModel bill, List<ProductViewModel> products, string code)
        {
            StringBuilder Body = new StringBuilder();
            Body.Append("<p>Cảm ơn quý khách đã đặt hàng của " + ConfigurationManager.AppSettings["SiteName"] + ", đây là đường dẫn xác nhận đơn hàng của bạn. Vui lòng xác nhận trong vòng " + ConfigurationManager.AppSettings["TimeBillLimit"] + " phút</p>");

            Body.Append("<a href='" + ConfigurationManager.AppSettings["Url"] + "/kich-hoat-don-hang?code=" + code + "&billId=" + bill.Id + "' >Kích hoạt đơn hàng</a>");
            Body.Append("<br>");

            Body.Append("<table border='1'>");
            Body.Append("<tr><td colspan='2'><b>Thông tin người đặt hàng</b></td></tr>");
            Body.Append("<tr><td>Họ và tên:</td><td>" + user.FullName + "</td></tr>");
            Body.Append("<tr><td>Điện thoại:</td><td>" + user.PhoneNumber + "</td></tr>");
            Body.Append("<tr><td>Địa chỉ:</td><td>" + user.Address + "</td></tr>");
            Body.Append("<tr><td>Email:</td><td>" + user.Email + "</td></tr>");
            Body.Append("<tr><td>Thời gian đặt:</td><td>" + bill.CreateTime.ToString("HH:mm:ss dd/MM/yyyy") + "</td></tr>");
            Body.Append("</table>");
            Body.Append("<br>");
            Body.Append("<table border='1'>");
            Body.Append("<tr><td colspan='2'><b>Thông tin giao hàng</b></td></tr>");
            Body.Append("<tr><td>Họ và tên:</td><td>" + addressOrder.FullName + "</td></tr>");
            Body.Append("<tr><td>Điện thoại:</td><td>" + addressOrder.PhoneNumber + "</td></tr>");
            Body.Append("<tr><td>Địa chỉ:</td><td>" + addressOrder.Address + "</td></tr>");
            Body.Append("<tr><td>Ghi chú:</td><td>" + bill.Note + "</td></tr>");
            Body.Append("</table>");
            Body.Append("<br>");
            Body.Append("<table border='1'>");
            Body.Append("<tr><td colspan='4'><b>Thông tin đơn hàng</b></td></tr>");
            Body.Append("<tr><th>STT</th><th>Số SIM</th><th>Nhà mạng</th><th>Giá bán</th></tr>");
            int stt = 1;
            foreach (ProductViewModel product in products)
            {
                Body.Append("<tr>");
                Body.Append("<td>" + stt + "</td>");
                Body.Append("<td>" + ConvertHelper.FormatPhoneNumber(product.Number) + "</td>");
                Body.Append("<td>" + product.CarrierName + "</td>");
                Body.Append("<td>" + ConvertHelper.FormatPrice(product.Price) + "</td>");
                Body.Append("</tr>");
                stt++;
            }

            Body.Append("</table>");
            Body.Append("<br>");

            return Body.ToString();
        }

        /// <summary>
        /// Tạo ra một chuỗi ngẫu nhiên với độ dài cho trước
        /// </summary>
        /// <param name="size">Kích thước của chuỗi </param>
        /// <param name="lowerCase">Nếu đúng, tạo ra chuỗi chữ thường</param>
        /// <returns>Random string</returns>
        public static string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
    }
}